<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model 
{
    
    public function __construct()
    {
        parent::__construct();
    }

    
    public function check_login($username,$password)
    {
        $sql = "SELECT id,type,name,email,contact1,contact2,address 
        FROM users 
        WHERE is_deleted = 0 AND username = '$username' AND password = '$password' AND type >= 0 AND type <= 100
        LIMIT 1";

        $query = $this->db->query($sql);

        return $query->row();

    }


    public function get_sub_units($unit_id)
    {        
        $sql = "SELECT *
        FROM subunits
        WHERE is_deleted = 0 AND unit_id = $unit_id
        ORDER BY subunit_name";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function reset_aop($state_id)
    {
        $sql = "UPDATE states SET is_state_active = 0 WHERE is_deleted = 0 AND state_id = $state_id";
        $this->db->query($sql);

        $sql = "UPDATE districts SET is_district_active = 0 WHERE is_deleted = 0 AND state_id = $state_id";
        $this->db->query($sql);
    }


    public function reset_aop_of_user($user_id)
    {
        $sql = "DELETE FROM users_area WHERE user_id = $user_id";
        
        $this->db->query($sql);
    }



    public function last_upload_date($typ)
    {
        if($typ == "upload_sales")
        {
            $sql = "SELECT upload_from_date, upload_to_date, file_id 
            FROM upload_files 
            WHERE ftype = '$typ'
            ORDER BY file_id DESC, upload_to_date DESC 
            LIMIT 1";
        }
        elseif($typ == "upload_dispatch")
        {
            $sql = "SELECT upload_from_date, upload_to_date, file_id 
            FROM upload_files 
            WHERE ftype = '$typ'
            ORDER BY file_id DESC, upload_to_date DESC
            LIMIT 1";
        }
        else
        {
            $sql = "SELECT upload_date, file_id 
            FROM upload_files 
            WHERE ftype = '$typ'
            ORDER BY file_id DESC, upload_to_date DESC
            LIMIT 1";
        }    

        $query = $this->db->query($sql);

        return $query->row();
    }

    public function get_master_id($table='',$field='', $where='', $value='')
    {
        $sql = "SELECT $field 
        FROM $table 
        WHERE $where = '$value'
        ORDER BY $field ASC        
        LIMIT 1";

        $query = $this->db->query($sql);

        $result = $query->row();

        if(isset($result) && !empty($result))
        {
            return $result->$field; die;
        }
        else
        {
            return 0; die;
        }
    }




    public function get_product_id($value='')
    {        
        $sql = "SELECT product_id 
        FROM products
        WHERE (product_name = '$value' OR product_code = '$value' ) AND product_category_id = 1
        LIMIT 1";        
            
        $query = $this->db->query($sql);

        $result = $query->row();

        if(isset($result) && !empty($result))
        {
            return $result-> product_id; die;
        }
        else
        {
            return 0; die;
        }
    }



    public function get_target($arr = array())
    {
        $target_id = $arr['target_id'];

        $sql = "SELECT *
        FROM target        
        WHERE target_id = '$target_id'
        LIMIT 1
        ";

        $query = $this->db->query($sql);

        return $query->row();
    }


    public function get_old_target($arr = array())
    {   
        $year = $arr['year'];
        $month = $arr['month'];
        $state = $arr['state'];

        $file_id_ack1 = $arr['file_id_ack1'];
        $file_id_ack2 = $arr['file_id_ack2'];
        $file_id_wstk = $arr['file_id_wstk'];
        $file_id_rstk = $arr['file_id_rstk'];


        if($month == 01 || $month == 1) { $month = 12; $year = $year - 1; }
        else $month = $month - 1;

       
        $sql = "SELECT SUM(s.quantity) as quantity, 'Acknowledgement 1' as type
            FROM upload_acknowledgement1 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1 AND ss.state_id = '$state'           
            WHERE s.file_id = '$file_id_ack1' AND month(s.invoice_date) = '$month' AND year(s.invoice_date) = '$year'


        UNION ALL


        SELECT SUM(s.quantity) as quantity, 'Acknowledgement 2' as type
        FROM upload_acknowledgement2 s
        INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2 AND ss.state_id = '$state'               
        WHERE s.file_id = '$file_id_ack2' AND month(s.invoice_date) = '$month' AND year(s.invoice_date) = '$year'


        UNION ALL 


        SELECT SUM(s.balance_with_ws) as quantity, 'Wholesaler Stock' as type
        FROM upload_wholesaler s
        INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1 AND ss.state_id = '$state'               
        WHERE s.file_id = '$file_id_wstk'


        UNION ALL

        SELECT SUM(s.availabilty) as quantity, 'Retailer Stock' as type
        FROM upload_retailer s 
        INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2 AND ss.state_id = '$state'        
        WHERE s.file_id = '$file_id_rstk'
        ";

        $query = $this->db->query($sql);

        return $query->result();
    }



    public function last_upload_date_all($typ)
    {
        if($typ == "upload_sales")
        {
            $sql = "SELECT upload_from_date, upload_to_date, file_id 
            FROM upload_files 
            WHERE ftype = '$typ'
            ORDER BY file_id ASC, upload_to_date ASC
            ";
        }
        elseif($typ == "upload_dispatch")
        {
            $sql = "SELECT upload_from_date, upload_to_date, file_id 
            FROM upload_files 
            WHERE ftype = '$typ'
            ORDER BY file_id ASC, upload_to_date ASC
            ";
        }
        else
        {
            $sql = "SELECT upload_date, file_id 
            FROM upload_files 
            WHERE ftype = '$typ'
            ORDER BY file_id ASC, upload_to_date ASC
            ";
        }    

        $query = $this->db->query($sql);

        $data = $query->result();
        
        $temp = array();
        if(isset($data) && !empty($data))
        {
            foreach($data as $obj)
            {
                $temp[$obj-> file_id] = $obj-> upload_date;
            }        
        }
        
        return $temp;
    }



    public function remove_backup($backup_id)
    {
        $sql = "SELECT u.backup_location FROM db_backup u WHERE u.db_backup_id = '$backup_id' LIMIT 1";
        $query = $this->db->query($sql);
        $row = $query->row();
        $location = $row-> backup_location;
        unlink($location);

        $sql = "DELETE FROM db_backup WHERE db_backup_id = $backup_id LIMIT 1";        
        $this->db->query($sql);
    }

}
?>