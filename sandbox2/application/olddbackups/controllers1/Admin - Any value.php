<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{
	public $user_first_name = "";
    public $user_last_name = "";
    public $user_fullname = "";
    public $user_email = "";
    public $user_mobile = "";
    public $user_role_id = "";
    public $user_role_name = "";
    public $user_id = "";
    public $all_modules = array();    
    public $user_state_aop = "0";
    public $user_district_aop = "0";  
    public $user_photo_url = "";
    
	public function __construct()
    {
        parent::__construct();
        
        $this->is_logged_in();
        
        $this->load->model("admin_model");
        $this->load->model("webservice_model");        
    }


    public function is_logged_in()
    {
        $data = $this->session->all_userdata();
        
        if(isset($data['user_id']) && !empty($data['user_id']))
        {
            $this->user_first_name = ucfirst($data['first_name']);

            $this->user_last_name = ucfirst($data['last_name']);

            $this->user_fullname = ucwords($data['first_name']." ".$data['last_name']);

            $this->user_email = $data['email'];

            $this->user_mobile = $data['mobile'];

            $this->user_role_id = $data['role_id'];

            $this->user_role_name = $data['role_name'];

            $this->user_id = $data['user_id'];            

            $this->user_photo_url = $data['user_photo_url']; 

            $this->user_created_date = date("M, Y", strtotime($data['created_date']));


            if($this->user_role_id == 2 || $this->user_role_id == 3)
            {
                $this->user_state_aop = $this->common_model->user_state_aop($this->user_id);

                $this->user_district_aop = $this->common_model->user_district_aop($this->user_id);
            }

            return true;
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."login/logout'; </script>"; die;
        }

    }


    public function validate_permission($module_id, $permission_id)
    {
        $chk_pm = $this->common_model->check_permission($this->user_role_id, $module_id, $permission_id);
        
        if(!isset($chk_pm) || empty($chk_pm))
        {            
            $this->load->view('noaccess');            
        }

        return true;
    }


    public function set_limit($type)
    {
        $append = "";
        
        if($this->user_role_id == 2)
        {
            if($type == "dispatch_report")
                $append = " AND ss.state_id IN (".$this->user_state_aop.") ";
            elseif($type == "farmers")
                $append = " AND s.state_id IN (".$this->user_state_aop.") ";
            else    
                $append = " AND ss.state_id IN (".$this->user_state_aop.") ";
        }
        elseif($this->user_role_id == 3)
        {
            if($type == "dispatch_report" || $type == "farmers")
                $append = " AND d.district_id IN (".$this->user_district_aop.") ";
            else
                $append = " AND ss.district_id IN (".$this->user_district_aop.") ";
        }

        return $append;
    }
    


	public function index()
	{
		$sfile_id = $file_id_ack1 = $file_id_ack2 = $file_id_wstk = $file_id_rstk = "";
        $details = $this->admin_model->last_upload_date('upload_sales');        
        if(isset($details) && !empty($details))
        {
            $sfile_id = $details-> file_id;
        }


        $details = $this->admin_model->last_upload_date('upload_acknowledgement1');        
        if(isset($details) && !empty($details))
        {
            $file_id_ack1 = $details-> file_id;
        }

        $details = $this->admin_model->last_upload_date('upload_acknowledgement2');        
        if(isset($details) && !empty($details))
        {
            $file_id_ack2 = $details-> file_id;
        }

        $details = $this->admin_model->last_upload_date('upload_wholesaler');        
        if(isset($details) && !empty($details))
        {
            $file_id_wstk = $details-> file_id;
        }

        $details = $this->admin_model->last_upload_date('upload_retailer');        
        if(isset($details) && !empty($details))
        {
            $file_id_rstk = $details-> file_id;
        }

        $data['counts'] = $this->common_model->get_counts($file_id_ack1, $file_id_ack2, $file_id_wstk, $file_id_rstk);
        $data['sdaop'] = $this->common_model->get_dd_list(array("typ"=>"state_district_aop"));
               
        
        $data['sales_year'] = date("Y");
        $data['sales_month'] = date("m");

        $this->load->view('includes/main_header');
		$this->load->view('dashboard', $data);
		$this->load->view('includes/main_footer');
	}


    function get_dd_list()
    {
        $typ = $this->input->post('typ');
        $id = $this->input->post('parent_id');
        $uid = $this->input->post('uid');

        $arr = array("typ"=>$typ, "id"=>$id, "uid"=>$uid);

        $data = $this->common_model->get_dd_list($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>1,"message"=>"","records"=>$data)); 
            die;
        }
        else
        {
            echo json_encode(array("status"=>0,"message"=>"")); 
            die;
        }    
    }


    /*----------------------------------------------------------
    Unit Management
    ----------------------------------------------------------*/
    public function units()
    {        
        $this->validate_permission(29, 4);

        $this->load->view('includes/main_header');
        $this->load->view('units');
        $this->load->view('includes/main_footer');
            
    }


    public function units_add()
    {
        $this->validate_permission(29, 2);

        $this->load->view('includes/main_header');
        $this->load->view('units_add');
        $this->load->view('includes/main_footer');
    }


    public function units_edit()
    {   
        $this->validate_permission(29, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"units", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);

        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/units'; </script>";
            die;
        }

        $arr = array("typ"=>"subunits", "id"=>$edit_id);        
        $data['sub_units_details'] = $this->admin_model->get_sub_units($edit_id);
        
        $this->load->view('includes/main_header');
        $this->load->view('units_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function units_save()
    {
        $unit_name = $this->input->post('unit_name');
        $sub_unit_name = $this->input->post('sub_unit_name');
        $hdn_id = $this->input->post('hdn_id');

        if($unit_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Unit Name."));
            
            die;    
        }
        elseif(count($sub_unit_name) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter atleast one Sub Unit Name."));
            
            die;    
        }

        $arr = array("typ"=>"units", "id"=>$hdn_id, "name"=>$unit_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Unit name already exist.")); 
            die;
        }
        else
        {            
            $data = array("unit_name" => $unit_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $unit_pid = $this->dbaccess_model->insert("units", $data);

                foreach($sub_unit_name as $snm)
                {
                    $data = array("unit_id" => $unit_pid, 
                    "subunit_name" => $snm,                    
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id,
                    "updated_date" => date(DATETIME_DB),                
                    "updated_by" => $this->user_id);

                    $this->dbaccess_model->insert("subunits", $data);
                }
            }
            else
            {   
                $this->dbaccess_model->update("units", $data, array("unit_id ="=>$hdn_id));

                $unit_pid = $hdn_id;

                $sql = "UPDATE subunits SET is_deleted = 1 WHERE unit_id = $hdn_id ";
                $this->dbaccess_model->executeOnlySql($sql);

                foreach($sub_unit_name as $snm)
                {
                    $data = array("unit_id" => $unit_pid, 
                    "subunit_name" => $snm,                    
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id,
                    "updated_date" => date(DATETIME_DB),                
                    "updated_by" => $this->user_id);

                    $this->dbaccess_model->insert("subunits", $data);
                }                
            }                

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function delete_record()
    {
        $user_id = $this->user_id;
        
        $typ = $this->input->post('typ');
        $rec_id = $this->input->post('rec_id');
        
        $arr = array("typ" => $typ, "id" => $rec_id);

        if($typ == "" || $rec_id <= 0)
        {
            echo json_encode(array("status"=>1,"message"=>"Illegal request made."));
            die;
        }
        elseif(count($this->common_model->is_exist($arr)) <= 0)
        {
            echo json_encode(array("status"=>1,"message"=>"Selected record not found in the system."));
            die;
        }
        /*elseif(count($this->common_model->is_owner($services_id, $user_id, 'services')) <= 0)
        {
            echo json_encode(array("status"=>"fail","message"=> OWNER_MSG));

            die;
        }*/
        
        
        $data = array("is_deleted" => 1, 
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id
        );
        
        if($typ == "units") 
        {
            $table = $typ;
            $field = "unit_id";
        } 
        elseif($typ == "soil_type") 
        {
            $table = $typ;
            $field = "soil_type_id";
        } 
        elseif($typ == "crops") 
        {
            $table = $typ;
            $field = "crop_id";
        } 
        elseif($typ == "crop_variety") 
        {
            $table = $typ;
            $field = "crop_variety_id";
        } 
        elseif($typ == "stock_verification_reason") 
        {
            $table = $typ;
            $field = "stock_verification_reason_id";
        }
        elseif($typ == "area_of_operation") 
        {
            $this->admin_model->reset_aop($rec_id);
            echo json_encode(array("status"=>1,"message"=>"Removed successfully."));        
            die;
        }
        elseif($typ == "product_category") 
        {
            $table = $typ;
            $field = "product_category_id";
        }
        elseif($typ == "staff") 
        {
            $table = "users";
            $field = "user_id";
        }
        elseif($typ == "roles") 
        {
            $table = $typ;
            $field = "role_id";
        }
        elseif($typ == "wholesalers" || $typ == "dealers") 
        {
            $table = "salers";
            $field = "saler_id";
        }
        elseif($typ == "templates") 
        {
            $table = $typ;
            $field = "template_id";
        }
        elseif($typ == "farmers") 
        {
            $table = $typ;
            $field = "farmer_id";
        }
        elseif($typ == "demos") 
        {
            $table = $typ;
            $field = "farmer_id";
        }
        elseif($typ == "farmers_temp") 
        {
            $table = $typ;
            $field = "farmer_id";
        }
        elseif($typ == "products") 
        {
            $table = $typ;
            $field = "product_id";
        }
        elseif($typ == "email_signature")
        {
            $table = $typ;
            $field = "email_signature_id";            
        }

        $this->dbaccess_model->update($table, $data, array($field." ="=>$rec_id));

        echo json_encode(array("status"=>1,"message"=>"Deleted successfully."));            
        
        die;
    }

    public function units_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
                
        //-----------------------------------------------------------------------------
        $aColumns = array('unit_name');
        $search_columns = array('unit_name');

        $sTable = "units";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*, GROUP_CONCAT(su.subunit_name) as uom
            FROM $sTable s            
            LEFT JOIN subunits su ON su.unit_id = s.unit_id AND su.is_deleted = 0
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            GROUP BY s.unit_id
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.unit_id) as count
            FROM $sTable s 
            LEFT JOIN subunits su ON su.unit_id = s.unit_id AND su.is_deleted = 0
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            GROUP BY s.unit_id
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> unit_name;

            $row[] = $aRow -> uom;            
            
            $row[] = '<a class="action_link" href="'.base_url().'admin/units_edit/'.$aRow -> unit_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("units",'.$aRow -> unit_id.');><i class="fa fa-trash"></i></a>';                       

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );
    
    
    }



    /*----------------------------------------------------------
    Soils Management
    ----------------------------------------------------------*/
    public function soil_type()
    {
        $this->validate_permission(24, 4);

        $this->load->view('includes/main_header');
        $this->load->view('soil_type');
        $this->load->view('includes/main_footer');
    }


    public function soil_type_add()
    {
        $this->validate_permission(24, 2);

        $this->load->view('includes/main_header');
        $this->load->view('soil_type_add');
        $this->load->view('includes/main_footer');
    }


    public function soil_type_edit()
    {   
        $this->validate_permission(24, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"soil_type", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/soil_type'; </script>";
            die;
        }

        $this->load->view('includes/main_header');
        $this->load->view('soil_type_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function soil_type_save()
    {
        $soil_name = $this->input->post('soil_type_name');
        $hdn_id = $this->input->post('hdn_id');

        if($soil_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Soil Type."));
            
            die;    
        }

        $arr = array("typ"=>"soil_type", "id"=>$hdn_id, "name"=>$soil_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Soil Type already exist.")); die;
        }
        else
        {            
            $data = array("soil_type_name" => $soil_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("soil_type", $data);
            }
            else
            {   
                $this->dbaccess_model->update("soil_type", $data, array("soil_type_id ="=>$hdn_id));
            }

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function soil_type_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        //$qs_name = $_POST['qs_name'];
        
        //if($qs_name != "") $quick_search .= " AND state_name LIKE '$qs_name%' ";
        
        
        //-----------------------------------------------------------------------------
        $aColumns = array('soil_type_name', 'created_date');
        $search_columns = array('soil_type_name', 'created_date');

        $sTable = "soil_type";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.soil_type_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            /*$row[] = '<div class="btn-group">
                        <button type="button" class="btn btn_action dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="'.base_url().'admin/soil_type_edit/'.$aRow -> soil_type_id.'"><i class="fa fa-pencil"></i> Edit</a></li>
                          <li><a href="javascript:void(0);" onclick=delete_record("soil_type",'.$aRow -> soil_type_id.');><i class="fa fa-trash"></i> Delete</a></li>
                        </ul>
                      </div>';*/

            $row[] = $aRow -> soil_type_name;                       

            $row[] = date(DATE, strtotime($aRow -> created_date));

            $row[] = '<a class="action_link" href="'.base_url().'admin/soil_type_edit/'.$aRow -> soil_type_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("soil_type",'.$aRow -> soil_type_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    /*----------------------------------------------------------
    State Management
    ----------------------------------------------------------*/
    public function states()
    {
        $this->load->view('includes/main_header');
        $this->load->view('states');
        $this->load->view('includes/main_footer');
    }


	public function states_list()
    {        
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        //$qs_name = $_POST['qs_name'];
        
        //if($qs_name != "") $quick_search .= " AND state_name LIKE '$qs_name%' ";
        
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('state_id','state_name');

        $sTable = "states";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.state_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
                        
            $row[] = $aRow -> state_id;

            $row[] = $aRow -> state_name;

            $row[] = "";
            
            //$row[] = $aRow -> status;
            /*if($aRow -> is_discharged == 1 && $aRow -> discharge_date!="")
            {
                $url1 = base_url()."opd/patient_details/".$aRow->patient_id."/".$aRow->id; 
                $url2 = base_url()."opd/opd_print_patient_details/".$aRow->patient_id."/".$aRow->id;                
                $row[] = '<a href="'.$url1.'"><button type="button" class="btn btn-'.BTN_VIEW.' actionbtn" title="View Details"><i class="fa fa-fw fa-eye"></i>View</button></a>&nbsp;<a href="'.$url2.'" target="_blank"><button type="button" class="btn btn-'.BTN_PRINT.' actionbtn" title="Print Report"><i class="fa fa-fw fa-print"></i>Print</button></a>';
            }
            else
            {
                $url1 = base_url()."opd/patient_details/".$aRow->patient_id."/".$aRow->id; 
                $url2 = base_url()."opd/opd_print_patient_details/".$aRow->patient_id."/".$aRow->id;
                $url3 = base_url()."opd/opd_edit/".$aRow->patient_id."/".$aRow->id."/02";                               
                $url5 = base_url()."opd/discharge/".$aRow->patient_id."/".$aRow->id;

                $row[] = '<a href="'.$url1.'"><button type="button" class="btn btn-'.BTN_VIEW.' actionbtn" title="View Details"><i class="fa fa-fw fa-eye"></i>View</button></a>&nbsp;<a href="'.$url3.'"><button type="button" class="btn btn-primary actionbtn" title="Edit Details"><i class="fa fa-fw fa-eye"></i>Edit</button></a>&nbsp;<a href="'.$url2.'" target="_blank"><button type="button" class="btn btn-'.BTN_PRINT.' actionbtn" title="Print Report"><i class="fa fa-fw fa-print"></i>Print</button></a>';
            }*/            

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );
    
    } 



    /*----------------------------------------------------------
    Roles and Permissions
    ----------------------------------------------------------*/
    public function roles_and_permissions()
    {
        $this->validate_permission(37, 4);

        $this->load->view('includes/main_header');
        $this->load->view('roles_and_permissions');
        $this->load->view('includes/main_footer');
    }

    public function roles_add()
    {        
        $this->validate_permission(37, 2);

        $this->load->view('includes/main_header');
        $this->load->view('roles_add');
        $this->load->view('includes/main_footer');
    }

    public function roles_edit()
    {   
        $this->validate_permission(37, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"roles", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);

        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/roles_and_permissions'; </script>";
            die;
        }
        
        $this->load->view('includes/main_header');
        $this->load->view('roles_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function roles_save()
    {
        $role_name = $this->input->post('role_name');
        $hdn_id = $this->input->post('hdn_id');

        if($role_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Role Name."));
            
            die;    
        }

        $arr = array("typ"=>"roles", "id"=>$hdn_id, "name"=>$role_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Role Name already exist.")); die;
        }
        else
        {            
            $data = array("role_name" => $role_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("roles", $data);
            }
            else
            {   
                $this->dbaccess_model->update("roles", $data, array("role_id ="=>$hdn_id));
            }

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function roles_and_permissions_set()
    {
        $this->validate_permission(37, 2);

        $sel_role_id = $this->uri->segment(3);

        if(!isset($sel_role_id) || $sel_role_id <= 0)
        {
            echo "<script> window.location.href = '".base_url()."roles_and_permissions'; </script>"; 
            die;
        }

        $data['sel_role_id'] = $sel_role_id;

        $data['roles'] = $this->common_model->get_roles();
        $data['modules'] = $this->common_model->get_modules();
        $data['permissions'] = $this->common_model->get_permissions();

        
        $arr = array("role_id"=>$sel_role_id);
        $data['permitted_modules'] = $this->common_model->get_permitted_modules($arr);
        
        
        $this->load->view('includes/main_header');
        $this->load->view('roles_and_permissions_set', $data);
        $this->load->view('includes/main_footer');
    }

    
    public function roles_and_permissions_save()
    {
        $role_name = $this->input->post('role_name');
        $chkbox = $this->input->post('chkbox');

        if($role_name == "" || $role_name <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Role Name."));
            
            die;    
        }
        elseif(count($chkbox) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please set atleast one permission to any module."));
            
            die;    
        }
        
        $this->common_model->clear_permisions(array("role_id" => $role_name));
                    
        foreach($chkbox as $module_id => $arr)
        {
            foreach($arr as $permission_id)
            {
                $data = array("role_id" => $role_name, 
                    "module_id" => $module_id, 
                    "permission_id" => $permission_id, 
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                );

                $this->dbaccess_model->insert("access_modules", $data);
            }
        }


        foreach($chkbox as $module_id => $arr)
        {
            $this->common_model->find_and_clear_permisions(array("role_id" => $role_name, "module_id" => $module_id));
        }
        
        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); 

        die;        
    }


    public function roles_and_permissions_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        //$qs_name = $_POST['qs_name'];
        
        //if($qs_name != "") $quick_search .= " AND state_name LIKE '$qs_name%' ";
        
        
        //-----------------------------------------------------------------------------
        $aColumns = array('role_name');
        $search_columns = array('role_name');

        $sTable = "roles";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.role_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> role_name; 

            $btn = "";
            if($aRow-> role_id >= 5)
            {
                $btn = '<a class="action_link" href="'.base_url().'admin/roles_edit/'.$aRow -> role_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("roles",'.$aRow -> role_id.');><i class="fa fa-trash"></i></a>&nbsp;';
            }

            $row[] = $btn.'<a class="action_link" href="'.base_url().'admin/roles_and_permissions_set/'.$aRow -> role_id.'"><i class="fa fa-cog"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Crops Management
    ----------------------------------------------------------*/
    public function crops()
    {
        $this->validate_permission(25, 4);

        $this->load->view('includes/main_header');
        $this->load->view('crops');
        $this->load->view('includes/main_footer');
    }


    public function crops_add()
    {
        $this->validate_permission(25, 2);

        $this->load->view('includes/main_header');
        $this->load->view('crops_add');
        $this->load->view('includes/main_footer');
    }


    public function crops_edit()
    {   
        $this->validate_permission(25, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"crops", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);

        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/crops'; </script>";
            die;
        }
        
        $this->load->view('includes/main_header');
        $this->load->view('crops_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function crops_save()
    {
        $crop_name = $this->input->post('crop_name');
        $hdn_id = $this->input->post('hdn_id');

        if($crop_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Crop Name."));
            
            die;    
        }

        $arr = array("typ"=>"crops", "id"=>$hdn_id, "name"=>$crop_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Crop Name already exist.")); die;
        }
        else
        {            
            $data = array("crop_name" => $crop_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("crops", $data);
            }
            else
            {   
                $this->dbaccess_model->update("crops", $data, array("crop_id ="=>$hdn_id));
            }
            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function crops_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        
        //-----------------------------------------------------------------------------
        $aColumns = array('crop_name', 'created_date');
        $search_columns = array('crop_name', 'created_date');

        $sTable = "crops";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.crop_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 

            $row[] = $aRow -> crop_name;                       

            $row[] = date(DATE, strtotime($aRow -> created_date));

            $row[] = '<a class="action_link" href="'.base_url().'admin/crops_edit/'.$aRow -> crop_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("crops",'.$aRow -> crop_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    /*----------------------------------------------------------
    Crops Variety Management
    ----------------------------------------------------------*/
    public function crops_variety()
    {
        $this->validate_permission(26, 4);

        $this->load->view('includes/main_header');
        $this->load->view('crops_variety');
        $this->load->view('includes/main_footer');
    }


    public function crops_variety_add()
    {
        $this->validate_permission(26, 2);

        $data['crops'] = $this->common_model->get_dd_list(array('typ' => 'crops'));

        $this->load->view('includes/main_header');
        $this->load->view('crops_variety_add', $data);
        $this->load->view('includes/main_footer');
    }


    public function crops_variety_edit()
    {   
        $this->validate_permission(26, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"crop_variety", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/crops_variety'; </script>";
            die;
        }

        $data['crops'] = $this->common_model->get_dd_list(array('typ' => 'crops'));
        $data['crop_variety_id'] = $edit_id;

        $this->load->view('includes/main_header');
        $this->load->view('crops_variety_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function crops_variety_save()
    {
        $crop_name = $this->input->post('crop_name');
        $sub_name = $this->input->post('sub_name');
        $hdn_id = $this->input->post('hdn_id');

        if($crop_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Crop Name."));
            
            die;    
        }
        elseif(count($sub_name) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter atleast one Crop Variety Name."));
            
            die;    
        }

        
        foreach($sub_name as $vnm)
        {    
            $arr = array("typ"=>"crop_variety_mas", "id"=>$crop_name, "name"=>$vnm);
            $data = $this->common_model->is_already_exist($arr);

            if(isset($data) && !empty($data))
            {
                echo json_encode(array("status"=>0,"message"=>"<b>".$vnm."</b> Variety Name already exist.")); 
                die;
            }
        }


        foreach($sub_name as $vnm)
        {
            $data = array("crop_id" => $crop_name, 
            "crop_variety_name" => $vnm,                    
            "created_date" => date(DATETIME_DB),                
            "created_by" => $this->user_id,
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id);

            $this->dbaccess_model->insert("crop_variety", $data);
        }

        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;     
    }


    public function crops_variety_edit_save()
    {
        $crop_name = $this->input->post('crop_name');
        $sub_name = $this->input->post('sub_name');
        $hdn_id = $this->input->post('hdn_id');

        if($crop_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Crop Name."));
            
            die;    
        }
        elseif($sub_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Crop Variety Name."));
            
            die;    
        }

        
        $arr = array("typ"=>"crop_variety", "id"=>$hdn_id, "name"=>$sub_name, "parent_id"=>$crop_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Crop Variety Name already exist.")); 
            die;
        }
        
        $data = array("crop_variety_name" => $sub_name,                    
        "updated_date" => date(DATETIME_DB),                
        "updated_by" => $this->user_id);
        
        $this->dbaccess_model->update("crop_variety", $data, array("crop_variety_id ="=>$hdn_id));

        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;     
    }


    public function crops_variety_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        
        //-----------------------------------------------------------------------------
        $aColumns = array('s.crop_variety_name', 'c.crop_name');
        $search_columns = array('s.crop_variety_name', 'c.crop_name');

        $sTable = "crop_variety";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*, c.crop_name
            FROM $sTable s
            INNER JOIN crops c ON c.crop_id = s.crop_id             
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.crop_variety_id) as count
            FROM $sTable s            
            INNER JOIN crops c ON c.crop_id = s.crop_id
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 

            $row[] = $aRow -> crop_variety_name;                       

            $row[] = $aRow -> crop_name;       

            $row[] = '<a class="action_link" href="'.base_url().'admin/crops_variety_edit/'.$aRow -> crop_variety_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("crop_variety",'.$aRow -> crop_variety_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Stock Verification Reason Management
    ----------------------------------------------------------*/
    public function stock_verification_reason()
    {
        $this->validate_permission(30, 4);

        $this->load->view('includes/main_header');
        $this->load->view('stock_verification_reason');
        $this->load->view('includes/main_footer');
    }


    public function stock_verification_reason_add()
    {
        $this->validate_permission(30, 2);

        $this->load->view('includes/main_header');
        $this->load->view('stock_verification_reason_add');
        $this->load->view('includes/main_footer');
    }


    public function stock_verification_reason_edit()
    {   
        $this->validate_permission(30, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"stock_verification_reason", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/stock_verification_reason'; </script>";
            die;
        }

        $this->load->view('includes/main_header');
        $this->load->view('stock_verification_reason_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function stock_verification_reason_save()
    {
        $stock_verification_reason_name = $this->input->post('stock_verification_reason_name');
        $hdn_id = $this->input->post('hdn_id');

        if($stock_verification_reason_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Reason Name."));
            
            die;    
        }

        $arr = array("typ"=>"stock_verification_reason", "id"=>$hdn_id, "name"=>$stock_verification_reason_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Stock Verification Reason already exist.")); die;
        }
        else
        {            
            $data = array("stock_verification_reason_name" => $stock_verification_reason_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("stock_verification_reason", $data);
            }
            else
            {   
                $this->dbaccess_model->update("stock_verification_reason", $data, array("stock_verification_reason_id ="=>$hdn_id));
            }

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function stock_verification_reason_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        //$qs_name = $_POST['qs_name'];
        
        //if($qs_name != "") $quick_search .= " AND state_name LIKE '$qs_name%' ";
        
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('stock_verification_reason_name');

        $sTable = "stock_verification_reason";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.stock_verification_reason_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> stock_verification_reason_name;                       

            $row[] = '<a class="action_link" href="'.base_url().'admin/stock_verification_reason_edit/'.$aRow -> stock_verification_reason_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("stock_verification_reason",'.$aRow -> stock_verification_reason_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    /*----------------------------------------------------------
    Product Category Management
    ----------------------------------------------------------*/
    public function product_category()
    {
        $this->validate_permission(27, 4);

        $this->load->view('includes/main_header');
        $this->load->view('product_category');
        $this->load->view('includes/main_footer');
    }


    public function product_category_add()
    {
        $this->validate_permission(27, 2);

        $this->load->view('includes/main_header');
        $this->load->view('product_category_add');
        $this->load->view('includes/main_footer');
    }


    public function product_category_edit()
    {   
        $this->validate_permission(27, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"product_category", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/product_category'; </script>";
            die;
        }

        $this->load->view('includes/main_header');
        $this->load->view('product_category_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function product_category_save()
    {
        $product_category_name = $this->input->post('product_category_name');
        $hdn_id = $this->input->post('hdn_id');

        if($product_category_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Product Category."));
            
            die;    
        }

        $arr = array("typ"=>"product_category", "id"=>$hdn_id, "name"=>$product_category_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Product Category already exist.")); die;
        }
        else
        {            
            $data = array("product_category_name" => $product_category_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("product_category", $data);
            }
            else
            {   
                $this->dbaccess_model->update("product_category", $data, array("product_category_id ="=>$hdn_id));
            }

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function product_category_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";         
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('product_category_name');

        $sTable = "product_category";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.product_category_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> product_category_name;                       

            $row[] = '<a class="action_link" href="'.base_url().'admin/product_category_edit/'.$aRow -> product_category_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("product_category",'.$aRow -> product_category_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Products Management
    ----------------------------------------------------------*/
    public function products()
    {
        $this->validate_permission(28, 4);

        $this->load->view('includes/main_header');
        $this->load->view('products');
        $this->load->view('includes/main_footer');
    }


    public function products_add()
    {
        $this->validate_permission(28, 2);

        $arr = array("typ"=>'product_category');        
        $data['product_category'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>'subunits');        
        $data['subunits'] = ddg($this->common_model->get_dd_list($arr));

        $this->load->view('includes/main_header');
        $this->load->view('products_add', $data);
        $this->load->view('includes/main_footer');
    }


    public function products_edit()
    {   
        $this->validate_permission(28, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"products", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/products'; </script>";
            die;
        }

        $arr = array("typ"=>'product_category');        
        $data['product_category'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>'subunits');        
        $data['subunits'] = ddg($this->common_model->get_dd_list($arr));
        
        $this->load->view('includes/main_header');
        $this->load->view('products_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function products_save()
    {
        $product_category_name = $this->input->post('product_category_name');
        $product_name = $this->input->post('product_name');
        $product_subunit = $this->input->post('product_subunit');
        $product_cost = $this->input->post('product_cost');
        $product_code = $this->input->post('product_code');

        $hdn_id = $this->input->post('hdn_id');

        if($product_category_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Product Category."));
            die;    
        }
        elseif($product_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Product Name."));
            die;    
        }
        elseif($product_code == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Product Code."));
            die;    
        }
        elseif($product_subunit == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select UoM."));
            die;    
        }
        elseif($product_cost == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Product Cost."));
            die;    
        }
        elseif($product_cost <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Product Cost greater than zero."));
            die;    
        }

        $arr = array("typ"=>"products", "id"=>$hdn_id, "name"=>$product_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Product Name already exist.")); die;
        }
        else
        {            
            $data = array("product_category_id" => $product_category_name, 
                "product_name" => $product_name, 
                "subunit_id" => $product_subunit, 
                "cost" => $product_cost, 
                "product_code" => $product_code, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("products", $data);
            }
            else
            {   
                $this->dbaccess_model->update("products", $data, array("product_id ="=>$hdn_id));
            }

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function products_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";         
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('p.product_name', 'pc.product_category_name', 's.subunit_name', 'p.cost', 'p.product_code');

        $sTable = "products";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS p.*, pc.product_category_name, s.subunit_name
            FROM $sTable p 
            INNER JOIN product_category pc ON p.product_category_id = pc.product_category_id
            INNER JOIN subunits s ON p.subunit_id = s.subunit_id
            WHERE p.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(p.product_id) as count
            FROM $sTable p 
            INNER JOIN product_category pc ON p.product_category_id = pc.product_category_id
            INNER JOIN subunits s ON p.subunit_id = s.subunit_id
            WHERE p.is_deleted = 0 $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> product_name;

            $row[] = $aRow -> product_code;

            $row[] = $aRow -> product_category_name;                       

            $row[] = $aRow -> subunit_name;

            $row[] = $aRow -> cost;

            $row[] = '<a class="action_link" href="'.base_url().'admin/products_edit/'.$aRow -> product_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("products",'.$aRow -> product_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Area Of Operation Management
    ----------------------------------------------------------*/
    public function area_of_operation()
    {                
        $this->validate_permission(23, 4);

        $this->load->view('includes/main_header');
        $this->load->view('area_of_operation');
        $this->load->view('includes/main_footer');
    }

    public function area_of_operation_set()
    {
        $this->validate_permission(23, 2);

        $arr = array("typ"=>'states_all');
        
        $data['states_all'] = $this->common_model->get_dd_list($arr);
                
        $this->load->view('includes/main_header');
        $this->load->view('area_of_operation_set', $data);
        $this->load->view('includes/main_footer');
    }


        
    public function area_of_operation_save()
    {
        $role_name = $this->input->post('dchk');
        $chkbox = $this->input->post('chkbox');

        if(count($chkbox) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please set atleast one District Name."));
            
            die;    
        }
        
                            
        foreach($chkbox as $pid => $arr)
        {
            $this->admin_model->reset_aop($pid);

            foreach($arr as $cid)
            {
                $data = array("is_district_active" => 1,                     
                    "updated_date" => date(DATETIME_DB),                
                    "updated_by" => $this->user_id
                );

                $this->dbaccess_model->update("districts", $data, array("district_id ="=>$cid, "state_id ="=>$pid));
            }

            $data = array("is_state_active" => 1,                     
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );
            $this->dbaccess_model->update("states", $data, array("state_id ="=>$pid));
        }

        $arr = array("typ"=>'states_aop');        
        $states = $this->common_model->get_dd_list($arr);
        if(isset($states) && !empty($states))
        {
            foreach($states as $obj)
            {
                $snm = strtolower(trim($obj-> state_name));

                $path = PATH_ALBUM.$snm;
                if(!file_exists($path))
                {
                    mkdir($path, 0777);
                }


                $arr = array("typ"=>'district_by_state_aop', "id" => $obj-> state_id);        
                $districts = $this->common_model->get_dd_list($arr);
                if(isset($districts) && !empty($districts))
                {
                    foreach($districts as $obj1)
                    {
                        $dnm = strtolower(trim($obj1-> district_name));

                        $dpath = PATH_ALBUM.$snm."/".$dnm;
                        if(!file_exists($dpath))
                        {
                            mkdir($dpath, 0777);

                            mkdir($dpath."/farmer", 0777);
                            mkdir($dpath."/demo", 0777);
                        }
                    }
                }
            }
        }        
        
        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); 

        die;        
    }


    public function area_of_operation_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = ""; 
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.state_name', 'd.district_name');

        $sTable = "states";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*, GROUP_CONCAT(d.district_name SEPARATOR ', ') as district_name
            FROM $sTable s 
            INNER JOIN districts d ON s.state_id = d.state_id AND d.is_deleted = 0 AND d.is_district_active = 1
            WHERE s.is_deleted = 0 AND s.is_state_active = 1 $quick_search $sWhere 
            GROUP BY s.state_id
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.state_id) as count
            FROM $sTable s            
            INNER JOIN districts d ON s.state_id = d.state_id AND d.is_deleted = 0 AND d.is_district_active = 1
            WHERE s.is_deleted = 0 AND s.is_state_active = 1 $quick_search $sWhere
            
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => count($rResult),
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> state_name;

            $row[] = $aRow -> district_name;

            $row[] = '<a class="action_link" href="javascript:void(0);" onclick=delete_record("area_of_operation",'.$aRow -> state_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Staff Management
    ----------------------------------------------------------*/
    public function staff()
    {
        $this->validate_permission(36, 4);

        $this->load->view('includes/main_header');
        $this->load->view('staff');
        $this->load->view('includes/main_footer');
    }


    public function staff_add()
    {
        $this->validate_permission(36, 2);

        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>'roles');        
        $data['roles'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>'reporting');        
        $data['reporting'] = $this->common_model->get_dd_list($arr);
        
        $data['keycode'] = $this->common_model->generate_key(5);

        $this->load->view('includes/main_header');
        $this->load->view('staff_add', $data);
        $this->load->view('includes/main_footer');
    }


    public function staff_edit()
    {   
        $this->validate_permission(36, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"staff", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/staff'; </script>";
            die;
        }

        $arr = array("typ"=>'states_aop_user', "uid"=>$edit_id);        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>'roles');        
        $data['roles'] = $this->common_model->get_dd_list($arr);
        
        $arr = array("typ"=>'reporting');        
        $data['reporting'] = $this->common_model->get_dd_list($arr);

        $this->load->view('includes/main_header');
        $this->load->view('staff_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function staff_save()
    {
        $role_id = $this->input->post('role_name');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $admin_access = $this->input->post('admin_access');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $cpassword = $this->input->post('cpassword');
        $app_access = $this->input->post('app_access');
        $keycode = $this->input->post('keycode');
        $chkbox = $this->input->post('chkbox');

        
        $gender = $this->input->post('gender');
        $dob = $this->input->post('dob');
        $qualification = $this->input->post('qualification');
        $doj = $this->input->post('doj');
        $experience = $this->input->post('experience');

        $cstates_name = $this->input->post('cstates_name');
        $ccity_name = $this->input->post('ccity_name');
        $caddress = $this->input->post('caddress');
        $caddress2 = $this->input->post('caddress2');

        $pstates_name = $this->input->post('pstates_name');
        $pcity_name = $this->input->post('pcity_name');
        $paddress = $this->input->post('paddress');
        $paddress2 = $this->input->post('paddress2');

        $reporting_to = "";//$this->input->post('reporting_to');

        $hdn_id = $this->input->post('hdn_id');

        if($role_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Role Name."));
            die;    
        }
        elseif($first_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter First Name."));
            die;    
        }
        elseif($last_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Last Name."));
            die;    
        }        
        elseif($email == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
            die;    
        }
        elseif($mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Mobile Number."));
            die;    
        }
        elseif(strlen($mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        /*elseif($username == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Username."));
            die;    
        }
        elseif($password == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Password."));
            die;    
        }
        elseif($cpassword == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Confirm Password."));
            die;    
        }
        elseif($password != $cpassword)
        {
            echo json_encode(array("status"=>0,"message"=>"Password and Confirm Password not matches."));
            die;    
        }
        elseif($keycode == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Key Code."));
            die;    
        }
        elseif($cstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address State Name."));
            die;
        }
        elseif($ccity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address City Name."));
            die;
        }
        elseif($caddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Current Address Details."));
            die;
        }
        elseif($pstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address State Name."));
            die;
        }
        elseif($pcity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address City Name."));
            die;
        }
        elseif($paddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Permanent Address Details."));
            die;
        }
        elseif($reporting_to == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Reporting Manager."));
            die;
        }*/
        elseif(count($chkbox) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Area Of Operation."));
            die;    
        }

        $arr = array("typ"=>"staff", "id"=>$hdn_id, "name"=>$username);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Username already exist. Please choose different Username.")); die;
        }
        else
        {            
            $data = array("role_id" => $role_id, 
                "reporting_to" => $reporting_to, 
                "username" => $username, 
                "keycode" => $keycode, 
                "first_name" => $first_name, 
                "last_name" => $last_name, 
                "email" => $email, 
                "mobile" => $mobile, 
                
                "cstate" => $cstates_name,
                "ccity" => $ccity_name,
                "caddress" => $caddress,
                "caddress2" => $caddress2,

                "pstate" => $pstates_name,
                "pcity" => $pcity_name,
                "paddress" => $paddress,
                "paddress2" => $paddress2,

                "gender" => $gender, 
                "dob" => $dob, 
                "qualification" => $qualification, 
                "doj" => $doj, 
                "experience" => $experience, 
                "admin_access" => $admin_access, 
                "app_access" => $app_access, 

                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id,
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

                            
            $userid = $this->dbaccess_model->insert("users", $data);
            
            if($userid)
            {
                //Set Password-----------------------------------------
                $data = array("user_id" => $userid,
                    "password" => md5($password),
                    "password_for" => 1,
                    "created_date" => date(DATETIME_DB),
                    "updated_date" => date(DATETIME_DB)
                );
                $this->dbaccess_model->insert("users_login", $data);

                $data = array("user_id" => $userid,
                    "password" => md5($keycode),
                    "password_for" => 2,
                    "created_date" => date(DATETIME_DB),
                    "updated_date" => date(DATETIME_DB)
                );
                $this->dbaccess_model->insert("users_login", $data);


                //Set Area Of Operation-----------------------------------------
                $this->admin_model->reset_aop_of_user($userid);

                foreach($chkbox as $pid => $arr)
                {
                    foreach($arr as $cid)
                    {   
                        $getarr = array("typ"=>"district", "id"=>$cid);        
                        $details = $this->common_model->get_detail($getarr);
                        $state_name = $district_name = "";
                        if(isset($details) && !empty($details))
                        {
                            $state_name = $details-> state_name;
                            $district_name = $details-> district_name;
                        }

                        $data = array("user_id" => $userid,
                            "state_id" => $pid,
                            "district_id" => $cid,
                            "state_name" => $state_name,
                            "district_name" => $district_name,                     
                            "created_date" => date(DATETIME_DB),                
                            "created_by" => $this->user_id
                        );

                        $this->dbaccess_model->insert("users_area", $data);
                    }
                }
            }
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }



    public function staff_save_edit()
    {        
        $role_id = $this->input->post('role_name');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $admin_access = $this->input->post('admin_access');
        $app_access = $this->input->post('app_access');        
        $chkbox = $this->input->post('chkbox');

        $address = $this->input->post('address');
        $gender = $this->input->post('gender');
        $dob = $this->input->post('dob');
        $qualification = $this->input->post('qualification');
        $doj = $this->input->post('doj');
        $experience = $this->input->post('experience');

        $cstates_name = $this->input->post('cstates_name');
        $ccity_name = $this->input->post('ccity_name');
        $caddress = $this->input->post('caddress');
        $caddress2 = $this->input->post('caddress2');

        $pstates_name = $this->input->post('pstates_name');
        $pcity_name = $this->input->post('pcity_name');
        $paddress = $this->input->post('paddress');
        $paddress2 = $this->input->post('paddress2');

        $reporting_to = "";//$this->input->post('reporting_to');

        $job_status = $this->input->post('job_status');
        if(!isset($job_status) || empty($job_status)) $job_status = "inactive";
        else $job_status = "active";


        $hdn_id = $this->input->post('hdn_id');

        if($role_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Role Name."));
            die;    
        }
        elseif($first_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter First Name."));
            die;    
        }
        elseif($last_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Last Name."));
            die;    
        }        
        elseif($email == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
            die;    
        }
        elseif($mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Mobile Number."));
            die;    
        }
        elseif(strlen($mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        /*elseif($cstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address State Name."));
            die;
        }
        elseif($ccity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address City Name."));
            die;
        }
        elseif($caddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Current Address Details."));
            die;
        }
        elseif($pstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address State Name."));
            die;
        }
        elseif($pcity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address City Name."));
            die;
        }
        elseif($paddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Permanent Address Details."));
            die;
        }
        elseif($reporting_to == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Reporting Manager."));
            die;
        }*/
        elseif(count($chkbox) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Area Of Operation."));
            die;    
        }

        if(true)
        {            
            $data = array("reporting_to" => $reporting_to,
                "first_name" => $first_name, 
                "last_name" => $last_name, 
                "email" => $email, 
                "mobile" => $mobile, 
                
                "cstate" => $cstates_name,
                "ccity" => $ccity_name,
                "caddress" => $caddress,
                "caddress2" => $caddress2,

                "pstate" => $pstates_name,
                "pcity" => $pcity_name,
                "paddress" => $paddress,
                "paddress2" => $paddress2,

                "gender" => $gender, 
                "dob" => date(DATE_DB, strtotime($dob)), 
                "qualification" => $qualification, 
                "doj" => date(DATE_DB, strtotime($doj)), 
                "experience" => $experience, 
                "admin_access" => $admin_access, 
                "app_access" => $app_access, 
                "job_status" => $job_status, 

                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );


            if($job_status == "inactive")
            {
                $data["admin_access"] = "no";
                $data["app_access"] = "no";
            }

                            
            $this->dbaccess_model->update("users", $data, array("user_id" => $hdn_id));
            $userid = $hdn_id;

            if($userid)
            {
                //Set Area Of Operation-----------------------------------------
                $this->admin_model->reset_aop_of_user($userid);

                foreach($chkbox as $pid => $arr)
                {
                    foreach($arr as $cid)
                    {   
                        $getarr = array("typ"=>"district", "id"=>$cid);        
                        $details = $this->common_model->get_detail($getarr);
                        $state_name = $district_name = "";
                        if(isset($details) && !empty($details))
                        {
                            $state_name = $details-> state_name;
                            $district_name = $details-> district_name;
                        }

                        $data = array("user_id" => $userid,
                            "state_id" => $pid,
                            "district_id" => $cid,
                            "state_name" => $state_name,
                            "district_name" => $district_name,                     
                            "created_date" => date(DATETIME_DB),                
                            "created_by" => $this->user_id
                        );

                        $this->dbaccess_model->insert("users_area", $data);
                    }
                }
            }
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function staff_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";         
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.first_name', 's.last_name', 's.email', 's.mobile', 'ua.state_name', 'ua.district_name', 'r.role_name', 's.keycode', 's.app_access', 's.username');

        $sTable = "users";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*, GROUP_CONCAT(CONCAT(ua.district_name) SEPARATOR ', ') as aop, GROUP_CONCAT(CONCAT(ua.state_name) SEPARATOR ',') as state_name, r.role_name, r.role_id
            FROM $sTable s             
            LEFT JOIN users_area ua ON ua.user_id = s.user_id
            LEFT JOIN roles r ON r.role_id = s.role_id
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            GROUP BY s.user_id
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.user_id) as count
            FROM $sTable s             
            LEFT JOIN users_area ua ON ua.user_id = s.user_id
            LEFT JOIN roles r ON r.role_id = s.role_id
            WHERE s.is_deleted = 0 $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = ucwords($aRow-> first_name." ".$aRow-> last_name)."&nbsp;<small>(".$aRow-> role_name.")</small>";

            $row[] = $aRow-> email;

            $row[] = $aRow-> mobile;


            if($aRow-> role_id == 1) $str = "All";
            else
            {
                $arr = explode(",", $aRow-> state_name);
                $arr = array_unique($arr);
                $str = implode(", ", $arr);
            }    
            
            $row[] = $str;     


            if($aRow-> role_id == 2) $txt = "All Districts"; 
            elseif($aRow-> role_id == 1) $txt = "All";
            elseif($aRow-> role_id != 3)  $txt = "HO";
            else $txt = $aRow-> aop;

            $row[] = $txt;

            if($aRow-> role_id != 3)
                $row[] = $aRow-> username."&nbsp;<small>(".ucfirst($aRow-> admin_access).")</small>";
            else
                $row[] = "-";


            if($aRow-> role_id == 3)
                $row[] = $aRow-> keycode."&nbsp;<small>(".ucfirst($aRow-> app_access).")</small>";
            else
                $row[] = "-";

            $row[] = '<a class="action_link" href="'.base_url().'admin/staff_edit/'.$aRow -> user_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("staff",'.$aRow -> user_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    /*----------------------------------------------------------
    Wholesaler Management
    ----------------------------------------------------------*/
    public function wholesalers()
    {
        $this->validate_permission(31, 4);

        $this->load->view('includes/main_header');
        $this->load->view('wholesalers');
        $this->load->view('includes/main_footer');
    }


    public function wholesalers_add()
    {
        $this->validate_permission(31, 2);

        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $this->load->view('includes/main_header');
        $this->load->view('wholesalers_add', $data);
        $this->load->view('includes/main_footer');
    }


    public function wholesalers_import()
    {
        $this->validate_permission(31, 2);

        $this->load->view('includes/main_header');
        $this->load->view('wholesalers_import');
        $this->load->view('includes/main_footer');
    }


    public function wholesalers_edit()
    {   
        $this->validate_permission(31, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"wholesalers", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/wholesalers'; </script>";
            die;
        }

        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);
        
        $this->load->view('includes/main_header');
        $this->load->view('wholesalers_edit', $data);
        $this->load->view('includes/main_footer');
    }


    /*----------------------------------------------------------
    Dealers Management
    ----------------------------------------------------------*/
    public function dealers()
    {
        $this->validate_permission(32, 4);

        $this->load->view('includes/main_header');
        $this->load->view('dealers');
        $this->load->view('includes/main_footer');
    }


    public function dealers_add()
    {
        $this->validate_permission(32, 2);

        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $this->load->view('includes/main_header');
        $this->load->view('dealers_add', $data);
        $this->load->view('includes/main_footer');
    }


    public function dealers_import()
    {
        $this->validate_permission(32, 2);

        $this->load->view('includes/main_header');
        $this->load->view('dealers_import');
        $this->load->view('includes/main_footer');
    }


    public function dealers_edit()
    {   
        $this->validate_permission(32, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"dealers", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/dealers'; </script>";
            die;
        }

        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);
        
        $this->load->view('includes/main_header');
        $this->load->view('dealers_edit', $data);
        $this->load->view('includes/main_footer');
    }

    public function salers_save()
    {
        $hdn_id = $this->input->post('hdn_id');
        $saler_sys_id = $this->input->post('saler_sys_id');
        $saler_type = $this->input->post('saler_type');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');

        $cstates_name = $this->input->post('cstates_name');
        $ccity_name = $this->input->post('ccity_name');
        $caddress = $this->input->post('caddress');

        $contact_person = $this->input->post('contact_person');
        $state_head = "0";//$this->input->post('state_head');
        $district_executive = "0";//$this->input->post('district_executive');

        $saler_type_txt = "wholesalers";
        if($saler_type == 2)
        {
            $saler_type_txt = "dealers";
        }

        if($saler_sys_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter ".ucfirst($saler_type_txt)." ID."));
            die;    
        }
        elseif($first_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter First Name."));
            die;    
        }
        elseif($last_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Last Name."));
            die;    
        }        
        elseif($email == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
            die;    
        }
        elseif($mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Mobile Number."));
            die;    
        }
        elseif(strlen($mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }        
        elseif($cstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select State Name."));
            die;
        }
        elseif($ccity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select District Name."));
            die;
        }
        elseif($caddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Address."));
            die;
        }        
        /*elseif($state_head == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select State Head."));
            die;
        }
        elseif($district_executive == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select District Executive."));
            die;
        }*/

        if($hdn_id <= 0)
        {
            $arr = array("typ"=>$saler_type_txt."id", "id"=>$hdn_id, "name"=>$saler_sys_id);
            $data = $this->common_model->is_already_exist($arr);    
            if(isset($data) && !empty($data))
            {
                echo json_encode(array("status"=>0,"message"=>ucfirst($saler_type_txt)." ID already exist.")); die;
            }
        }
            

        $arr = array("typ"=>$saler_type_txt, "id"=>$hdn_id, "name"=>$first_name." ".$last_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>ucfirst($saler_type_txt)." already exist. Please choose different name.")); die;
        }
        else
        {            
            $getarr = array("typ"=>"district", "id"=>$ccity_name);        
            $details = $this->common_model->get_detail($getarr);
            $state_name = $district_name = "";
            if(isset($details) && !empty($details))
            {
                $state_name = $details-> state_name;
                $district_name = $details-> district_name;
            }

            $data = array("saler_type" => $saler_type,                 
                "first_name" => $first_name, 
                "last_name" => $last_name, 
                "email" => $email, 
                "mobile" => $mobile,                 
                "state_id" => $cstates_name,
                "district_id" => $ccity_name,
                "address" => $caddress,
                "contact_person" => $contact_person, 
                "state_head" => $state_head, 
                "district_executive" => $district_executive, 
                "state_name" => $state_name,
                "district_name" => $district_name,                
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_by"] = $this->user_id;
                $data["created_date"] = date(DATETIME_DB);
                $data["saler_sys_id"] = $saler_sys_id;
                
                $this->dbaccess_model->insert("salers", $data);
            }
            else
            {   
                $this->dbaccess_model->update("salers", $data, array("saler_id ="=>$hdn_id));
            }            
                        
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function salers_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";         
        $saler_type = $_POST['saler_type'];
        
        if($saler_type == "" || $saler_type == 1) 
        {
            $saler_type = 1;
            $lbl = "wholesalers";
        }
        else
        {
            $saler_type = 2;
            $lbl = "dealers";
        }
        

        $quick_search .= " AND saler_type = $saler_type ";

        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.saler_sys_id','s.first_name', 's.last_name', 's.email', 's.mobile', 's.state_name', 's.district_name', 's.address');

        $sTable = "salers";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s
            WHERE s.is_deleted = 0 $quick_search $sWhere             
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.saler_id) as count
            FROM $sTable s
            WHERE s.is_deleted = 0 $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();  

            $row[] = $aRow-> saler_sys_id;          
            
            $row[] = $aRow-> first_name." ".$aRow-> last_name;

            $row[] = $aRow-> email;

            $row[] = $aRow-> mobile;

            $row[] = $aRow-> address;

            $row[] = $aRow-> district_name;

            $row[] = $aRow-> state_name;

            $row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    /*----------------------------------------------------------
    Rating Management
    ----------------------------------------------------------*/
    public function rating()
    {                
        $this->validate_permission(33, 4);

        $data['rating'] = $this->common_model->rating();

        $this->load->view('includes/main_header');
        $this->load->view('rating', $data);
        $this->load->view('includes/main_footer');
    }

    public function rating_save()
    {        
        $rating_from = $this->input->post('rating_from');
        $rating_to = $this->input->post('rating_to');
        $rating_star = $this->input->post('rating_star');

        if(count($rating_from) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter From Days."));            
            die;    
        }
        elseif(count($rating_to) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter To Days."));            
            die;    
        }
        elseif(count($rating_star) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Rating."));            
            die;    
        }
        
                            
        foreach($rating_from as $key => $val)
        {           
            if(!isset($val) || empty($val))
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter From Days."));
                die;
            }
            elseif(!isset($rating_to[$key]) || empty($rating_to[$key]))
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter To Days."));
                die;
            }
            elseif(!isset($rating_star[$key]))
            {
                echo json_encode(array("status"=>0,"message"=>"Please select Rating."));
                die;
            }

            $data = array("rating_from" => $val,
                "rating_to" => $rating_to[$key],
                "rating_star" => $rating_star[$key],
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );
            $this->dbaccess_model->update("rating", $data, array("rating_id ="=>$key));
        }
        
        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); 

        die;        
    
    }



    /*----------------------------------------------------------
    Upload Management
    ----------------------------------------------------------*/
    public function upload_acknowledgement1()
    {
        $this->validate_permission(3, 2);

        $data['details'] = $this->admin_model->last_upload_date('upload_acknowledgement1');

        $this->load->view('includes/main_header');
        $this->load->view('upload_acknowledgement1', $data);
        $this->load->view('includes/main_footer');
    }

    public function upload_acknowledgement2()
    {
        $this->validate_permission(4, 2);

        $data['details'] = $this->admin_model->last_upload_date('upload_acknowledgement2');

        $this->load->view('includes/main_header');
        $this->load->view('upload_acknowledgement2', $data);
        $this->load->view('includes/main_footer');
    }

    public function upload_wholesaler()
    {
        $this->validate_permission(5, 2);

        $data['details'] = $this->admin_model->last_upload_date('upload_wholesaler');

        $this->load->view('includes/main_header');
        $this->load->view('upload_wholesaler', $data);
        $this->load->view('includes/main_footer');
    }

    public function upload_retailer()
    {
        $this->validate_permission(6, 2);

        $data['details'] = $this->admin_model->last_upload_date('upload_retailer');

        $this->load->view('includes/main_header');
        $this->load->view('upload_retailer', $data);
        $this->load->view('includes/main_footer');
    }

    public function upload_retailer_pos()
    {
        $this->validate_permission(7, 2);

        $data['details'] = $this->admin_model->last_upload_date('upload_retailer_pos');

        $this->load->view('includes/main_header');
        $this->load->view('upload_retailer_pos', $data);
        $this->load->view('includes/main_footer');
    }

    public function upload_sales()
    {
        $this->validate_permission(9, 2);

        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $data['details'] = $this->admin_model->last_upload_date('upload_sales');

        $this->load->view('includes/main_header');
        $this->load->view('upload_sales', $data);
        $this->load->view('includes/main_footer');
    }


    public function upload_dispatch()
    {
        $this->validate_permission(10, 2);

        $data['details'] = $this->admin_model->last_upload_date('upload_dispatch');

        $this->load->view('includes/main_header');
        $this->load->view('upload_dispatch', $data);
        $this->load->view('includes/main_footer');
    }


    public function upload_data()
    {
        $upload_type = $this->input->post('upload_type');

        if($upload_type == "import_wholesalers" || $upload_type == "import_dealers" || $upload_type == "import_farmers")
        {
            $filename = $_FILES['upload_file']['name'];

            if($filename == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please select file."));
                die;    
            }
        }
        elseif($upload_type == "upload_sales")
        {            
            $cstates_name = $this->input->post('cstates_name');
            $ccity_name = $this->input->post('ccity_name');
            $upload_from_date = $this->input->post('upload_from_date');
            $upload_to_date = $this->input->post('upload_to_date');

            $filename = $_FILES['upload_file']['name'];

            if($cstates_name == "" || $cstates_name <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"Please select state."));
                die;    
            }
            elseif($ccity_name == "" || $ccity_name <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"Please select district."));
                die;    
            }
            elseif($upload_from_date == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter from date."));
                die;    
            }
            elseif($upload_to_date == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter to date."));
                die;    
            }
            elseif($filename == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please select file."));
                die;    
            }
        }
        elseif($upload_type == "upload_dispatch")
        {            
            $upload_from_date = $this->input->post('upload_from_date');
            $upload_to_date = $this->input->post('upload_to_date');

            $filename = $_FILES['upload_file']['name'];

            if($upload_from_date == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter from date."));
                die;    
            }
            elseif($upload_to_date == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter to date."));
                die;    
            }
            elseif($filename == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please select file."));
                die;    
            }
        }
        else
        {           
            $upload_date = $this->input->post('upload_date');
            $filename = $_FILES['upload_file']['name'];

            if($upload_date == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter date."));            
                die;    
            }
            elseif($filename == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please select file."));            
                die;    
            }
        }
            
        $ext = pathinfo($filename, PATHINFO_EXTENSION); 
        if(!in_array($ext, array("xls", "xlsx", "csv")))
        {
            echo json_encode(array("status"=>0,"message"=>"Invalid file format selected. Please choose xls, xlsx or csv file."));            
            die;
        }       
                            
        $path = "assets/uploads/import/";
        if(move_uploaded_file($_FILES['upload_file']['tmp_name'], $path.basename($filename)))
        {            
            if($upload_type == "import_wholesalers" || $upload_type == "import_dealers" || $upload_type == "import_farmers")
            {
                $this->import_records($upload_type, $path, $filename, 0);
            }
            else
            {    
                if($upload_type == "upload_sales")
                {
                    $dataup = array("ftype" => $upload_type,
                    "upload_state" => $cstates_name, 
                    "upload_district" => $ccity_name, 
                    "upload_from_date" => date(DATE_DB, strtotime($upload_from_date)), 
                    "upload_to_date" => date(DATE_DB, strtotime($upload_to_date)),                
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    ); 
                }
                elseif($upload_type == "upload_dispatch")
                {
                    $dataup = array("ftype" => $upload_type,                     
                    "upload_from_date" => date(DATE_DB, strtotime($upload_from_date)), 
                    "upload_to_date" => date(DATE_DB, strtotime($upload_to_date)),
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    ); 
                }
                else
                {
                    $dataup = array("ftype" => $upload_type, 
                    "upload_date" => date(DATE_DB, strtotime($upload_date)),
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                }    
                
                $upid = $this->dbaccess_model->insert("upload_files", $dataup);

                if($upid > 0)
                {
                    if($upload_type == "upload_sales")
                    {
                        $this->import_records($upload_type, $path, $filename, $upid, $cstates_name, $ccity_name, $upload_from_date, $upload_to_date);
                    }
                    elseif($upload_type == "upload_dispatch")
                    {
                        $this->import_records($upload_type, $path, $filename, $upid, "", "", $upload_from_date, $upload_to_date);
                    }
                    else
                    {
                        $this->import_records($upload_type, $path, $filename, $upid);
                    }                    
                }
                else
                {
                    echo json_encode(array("status"=>0,"message"=>"Error occur while uploading file. Please try again."));
                    die;
                }            
            }    
        }
        else
        {
            echo json_encode(array("status"=>0,"message"=>"Error occur while uploading file. Please try again."));            
            die;
        }            
    }


    public function import_records($upload_type, $path, $filename, $upid, $state_id="", $city_id="", $from_date="", $to_date="")
    {
        $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        if($ext == "csv") 
        {
            $this->import_records_csv($upload_type, $path, $filename, $upid, $state_id, $city_id, $from_date, $to_date);

            die;
        }    


        $this->load->library('excel_reader');
        $this->excel_reader->read($path.basename($filename));
        $worksheet = $this->excel_reader->sheets[0];
        $numRows = $worksheet['numRows'];
        $numCols = $worksheet['numCols'];
        $cells = $worksheet['cells'];
                
        $totalr = $ok = $skp = 0; $skp_str = "";

        /*if($upload_type == "upload_acknowledgement1")       $start = 6;
        elseif($upload_type == "upload_acknowledgement2")   $start = 6;
        elseif($upload_type == "upload_wholesaler")         $start = 7;
        elseif($upload_type == "upload_retailer")           $start = 3;
        elseif($upload_type == "upload_retailer_pos")       $start = 3; 
        elseif($upload_type == "upload_sales")              $start = 5;
        elseif($upload_type == "upload_dispatch")           $start = 4;
        elseif($upload_type == "import_wholesalers")        $start = 2;
        elseif($upload_type == "import_dealers")            $start = 2;
        elseif($upload_type == "import_farmers")            $start = 2;
        else  $start = 1;*/

        $start = 2;

        //Format Text-------------------------------------
        for ($i=$start; $i<=$numRows; $i++) 
        {                
            for($j=1; $j<=$numCols; $j++)
            {
               if(isset($worksheet['cells'][$i][$j]) && !empty($worksheet['cells'][$i][$j]))
                    $worksheet['cells'][$i][$j] = sanitize_text($worksheet['cells'][$i][$j]); 
                else
                    $worksheet['cells'][$i][$j] = "";
            }
        }    


        if($upload_type == "upload_acknowledgement1")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7]) && isset($worksheet['cells'][$i][14]) && !empty($worksheet['cells'][$i][14]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet['cells'][$i][14]);
                    if($master_id > 0)
                    {                                                      
                        $worksheet['cells'][$i][3] = format_date($worksheet['cells'][$i][3]);
                        $worksheet['cells'][$i][20] = format_date($worksheet['cells'][$i][20]);
                        $worksheet['cells'][$i][21] = format_date($worksheet['cells'][$i][21]);
                        $worksheet['cells'][$i][34] = format_date($worksheet['cells'][$i][34],DATETIME_DB);


                        if($worksheet['cells'][$i][9] == "RETAILER" || $worksheet['cells'][$i][9] == "WHOLESALER")
                        {
                            $sys_type = 1;
                            /*if($worksheet['cells'][$i][9] == "RETAILER")
                            {
                                $sys_type = 2;
                            }*/

                            $sysid = $worksheet['cells'][$i][7];
                            $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                            $dchk = $this->dbaccess_model->is_exist($sql);
                            
                            if($dchk == 0)
                            {                            
                                $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet['cells'][$i][13]);
                                $dd = $this->common_model->get_detail($arr2); 
                                
                                if(isset($dd) && !empty($dd))
                                {
                                    $sstate_id = $dd-> state_id;
                                    $ddistrict_id = $dd-> district_id;

                                    $dataset = array("saler_sys_id" => $sysid,
                                    "saler_type" => $sys_type,
                                    "first_name"   => $worksheet['cells'][$i][8],
                                    "mobile" => $worksheet['cells'][$i][11],                        
                                    "email" => "",
                                    "address" => "",
                                    "district_name" => $worksheet['cells'][$i][13],
                                    "state_name" => $worksheet['cells'][$i][12],
                                    "contact_person" => "",

                                    "state_id" => $sstate_id,
                                    "district_id" => $ddistrict_id,

                                    "created_date" => date(DATETIME_DB),                
                                    "created_by" => $this->user_id,
                                    "updated_date" => date(DATETIME_DB),                
                                    "updated_by" => $this->user_id
                                    );                                
                                    
                                    $this->dbaccess_model->insert("salers", $dataset);
                                }
                            }
                        }


                        $dataset = array("file_id" => $upid,
                        "transaction_id" => $worksheet['cells'][$i][1],
                        "invoice_no"        => $worksheet['cells'][$i][2],
                        "invoice_date" => $worksheet['cells'][$i][3],
                        "marketer" => $worksheet['cells'][$i][4],
                        "manufacturer" => $worksheet['cells'][$i][5],
                        "plant" => $worksheet['cells'][$i][6],
                        "dealer_id" => $worksheet['cells'][$i][7],
                        "product_id" => $master_id,
                        "unit" => $worksheet['cells'][$i][15],
                        "quantity" => $worksheet['cells'][$i][16],
                        "quantity_mt" => $worksheet['cells'][$i][17],
                        "received_quantity" => $worksheet['cells'][$i][18],
                        "status" => $worksheet['cells'][$i][19],
                        "entry_date" => $worksheet['cells'][$i][20],
                        "lock_date" => $worksheet['cells'][$i][21],
                        "txn_remark" => $worksheet['cells'][$i][22],
                        "ack_through" => $worksheet['cells'][$i][23],
                        "subsidy_year1" => $worksheet['cells'][$i][24],
                        "subsidy_month1" => $worksheet['cells'][$i][25],
                        "month1_qty" => $worksheet['cells'][$i][26],
                        "subsidy_month2" => $worksheet['cells'][$i][27],
                        "subsidy_year2" => $worksheet['cells'][$i][28],
                        "month2_qty" => $worksheet['cells'][$i][29],
                        "challan_no" => $worksheet['cells'][$i][30],
                        "dd_no" => $worksheet['cells'][$i][31],
                        "lorry_no" => $worksheet['cells'][$i][32],
                        "lorry_capacity" => $worksheet['cells'][$i][33],
                        "retailer_receipt_date" => $worksheet['cells'][$i][34],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][14]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Dealer ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_acknowledgement2")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7]) && isset($worksheet['cells'][$i][13]) && !empty($worksheet['cells'][$i][13]) && isset($worksheet['cells'][$i][18]) && !empty($worksheet['cells'][$i][18]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet['cells'][$i][18]);
                    if($master_id > 0)
                    {                                                      
                        $worksheet['cells'][$i][3] = format_date($worksheet['cells'][$i][3]);
                        $worksheet['cells'][$i][25] = format_date($worksheet['cells'][$i][25]);
                        $worksheet['cells'][$i][26] = format_date($worksheet['cells'][$i][26]);
                        $worksheet['cells'][$i][39] = format_date($worksheet['cells'][$i][39]);
                        


                        $sys_type = 1;
                        $sysid = $worksheet['cells'][$i][7];
                        $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                        $dchk = $this->dbaccess_model->is_exist($sql);                        
                        if($dchk == 0)
                        {                        
                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet['cells'][$i][11]);
                            $dd = $this->common_model->get_detail($arr2); 
                            
                            if(isset($dd) && !empty($dd))
                            {
                                $sstate_id = $dd-> state_id;
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $sysid,
                                "saler_type" => $sys_type,
                                "first_name"   => $worksheet['cells'][$i][8],
                                "mobile" => "",                        
                                "email" => "",
                                "address" => "",
                                "district_name" => $worksheet['cells'][$i][11],
                                "state_name" => $worksheet['cells'][$i][10],
                                "contact_person" => "",

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                                
                                $this->dbaccess_model->insert("salers", $dataset);
                            }
                        }


                        $sys_type = 2;
                        $sysid = $worksheet['cells'][$i][13];
                        $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                        $dchk = $this->dbaccess_model->is_exist($sql);                        
                        if($dchk == 0)
                        {                        
                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet['cells'][$i][11]);
                            $dd = $this->common_model->get_detail($arr2); 
                            
                            if(isset($dd) && !empty($dd))
                            {
                                $sstate_id = $dd-> state_id;
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $sysid,
                                "saler_type" => $sys_type,
                                "first_name"   => $worksheet['cells'][$i][15],
                                "mobile" => $worksheet['cells'][$i][17],                        
                                "email" => "",
                                "address" => "",
                                "district_name" => $worksheet['cells'][$i][12],
                                "state_name" => $worksheet['cells'][$i][10],
                                "contact_person" => "",

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                                
                                $this->dbaccess_model->insert("salers", $dataset);
                            }
                        }
                        

                        $dataset = array("file_id" => $upid,
                        "transaction_id" => $worksheet['cells'][$i][1],
                        "invoice_no"        => $worksheet['cells'][$i][2],
                        "invoice_date" => $worksheet['cells'][$i][3],
                        "marketer" => $worksheet['cells'][$i][4],
                        "manufacturer" => $worksheet['cells'][$i][5],
                        "plant" => $worksheet['cells'][$i][6],
                        "wholesaler_id" => $worksheet['cells'][$i][7],
                        "dealer_id" => $worksheet['cells'][$i][13],
                        "product_id" => $master_id,
                        "unit" => $worksheet['cells'][$i][19],
                        "quantity" => $worksheet['cells'][$i][20],
                        "quantity_mt" => $worksheet['cells'][$i][21],
                        "received_quantity" => $worksheet['cells'][$i][22],
                        "status" => $worksheet['cells'][$i][23],
                        "txn_type" => $worksheet['cells'][$i][24],
                        "entry_date" => $worksheet['cells'][$i][25],
                        "lock_date" => $worksheet['cells'][$i][26],
                        "ack_through" => $worksheet['cells'][$i][27],
                        "txn_remark" => $worksheet['cells'][$i][28],
                        "subsidy_month1" => $worksheet['cells'][$i][29],
                        "subsidy_year1" => $worksheet['cells'][$i][30],                        
                        "month1_qty" => $worksheet['cells'][$i][31],
                        "subsidy_month2" => $worksheet['cells'][$i][32],
                        "subsidy_year2" => $worksheet['cells'][$i][33],
                        "month2_qty" => $worksheet['cells'][$i][34],
                        "challan_no" => $worksheet['cells'][$i][35],
                        "lorry_no" => $worksheet['cells'][$i][36],
                        "lorry_capacity" => $worksheet['cells'][$i][37],
                        "dispatch_no" => $worksheet['cells'][$i][38],
                        "retailer_receipt_date" => $worksheet['cells'][$i][39],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][18]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }            

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Wholesaler ID, Dealer ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_wholesaler")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][4]) && !empty($worksheet['cells'][$i][4]) && isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet['cells'][$i][4]);
                    if($master_id > 0)
                    {                        

                        $sys_type = 1;
                        $sysid = $worksheet['cells'][$i][7];
                        $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                        $dchk = $this->dbaccess_model->is_exist($sql);                        
                        if($dchk == 0)
                        {                        
                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet['cells'][$i][6]);
                            $dd = $this->common_model->get_detail($arr2); 
                            
                            if(isset($dd) && !empty($dd))
                            {
                                $sstate_id = $dd-> state_id;
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $sysid,
                                "saler_type" => $sys_type,
                                "first_name"   => $worksheet['cells'][$i][9],
                                "mobile" => "",                        
                                "email" => "",
                                "address" => "",
                                "district_name" => $worksheet['cells'][$i][6],
                                "state_name" => $worksheet['cells'][$i][5],
                                "contact_person" => "",

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                                
                                $this->dbaccess_model->insert("salers", $dataset);
                            }
                        }




                        $dataset = array("file_id" => $upid,
                        "serial_number" => $worksheet['cells'][$i][1],
                        "company"        => $worksheet['cells'][$i][2],                        
                        "plant" => $worksheet['cells'][$i][3],
                        "product_id" => $master_id,                        
                        "dealer_id" => $worksheet['cells'][$i][7],                        
                        "wholesaler_ob" => $worksheet['cells'][$i][10],
                        "comp_ws_sale" => $worksheet['cells'][$i][11],
                        "comp_ws_sale_rcpt" => $worksheet['cells'][$i][12],
                        "received_from_ws" => $worksheet['cells'][$i][13],
                        "received_from_ws_ack" => $worksheet['cells'][$i][14],
                        "ws_rt_sale" => $worksheet['cells'][$i][15],
                        "ws_rt_sale_rcpt" => $worksheet['cells'][$i][16],
                        "ws_ws_sale" => $worksheet['cells'][$i][17],
                        "ws_ws_sale_rcpt" => $worksheet['cells'][$i][18],
                        "total_sales_by_ws" => $worksheet['cells'][$i][19],
                        "stock_transfer_from_ws_to_retailer" => $worksheet['cells'][$i][20],
                        "stock_transfer_from_ws_to_retailer_ack" => $worksheet['cells'][$i][21],
                        "balance_with_ws" => $worksheet['cells'][$i][22],
                        "total_ack_to_ws" => $worksheet['cells'][$i][23],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][4]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Product Name or Dealer ID inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_retailer")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][5]) && !empty($worksheet['cells'][$i][5]) && isset($worksheet['cells'][$i][11]) && !empty($worksheet['cells'][$i][11]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet['cells'][$i][11]);
                    if($master_id > 0)
                    {                                  
                        
                        $sys_type = 2;
                        $sysid = $worksheet['cells'][$i][5];
                        $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                        $dchk = $this->dbaccess_model->is_exist($sql);                        
                        if($dchk == 0)
                        {                        
                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet['cells'][$i][3]);
                            $dd = $this->common_model->get_detail($arr2); 
                            
                            if(isset($dd) && !empty($dd))
                            {
                                $sstate_id = $dd-> state_id;
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $sysid,
                                "saler_type" => $sys_type,
                                "first_name"   => $worksheet['cells'][$i][6],
                                "mobile" => $worksheet['cells'][$i][7],                        
                                "email" => "",
                                "address" => "",
                                "district_name" => $worksheet['cells'][$i][3],
                                "state_name" => $worksheet['cells'][$i][2],
                                "contact_person" => "",

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                                
                                $this->dbaccess_model->insert("salers", $dataset);
                            }
                        }


                        $dataset = array("file_id" => $upid,
                        "serial_number" => $worksheet['cells'][$i][1],
                        "retailer_id"   => $worksheet['cells'][$i][5],                        
                        "company" => $worksheet['cells'][$i][9],                        
                        "plant" => $worksheet['cells'][$i][10],
                        "product_id" => $master_id,
                        "opening_balance" => $worksheet['cells'][$i][12],
                        "received_quantity" => $worksheet['cells'][$i][13],
                        "sold_quantity" => $worksheet['cells'][$i][14],
                        "availabilty" => $worksheet['cells'][$i][15],
                        "closing_balance" => $worksheet['cells'][$i][16],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][11]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Retailer ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_retailer_pos")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][3]) && !empty($worksheet['cells'][$i][3]) && isset($worksheet['cells'][$i][4]) && !empty($worksheet['cells'][$i][4]) && isset($worksheet['cells'][$i][11]) && !empty($worksheet['cells'][$i][11]) && isset($worksheet['cells'][$i][14]) && !empty($worksheet['cells'][$i][14]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet['cells'][$i][11]);
                    if($master_id > 0)
                    {                                  
                        $worksheet['cells'][$i][3] = format_date($worksheet['cells'][$i][3]);


                        $sys_type = 2;
                        $sysid = $worksheet['cells'][$i][4];
                        $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                        $dchk = $this->dbaccess_model->is_exist($sql);                        
                        if($dchk == 0)
                        {                        
                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet['cells'][$i][7]);
                            $dd = $this->common_model->get_detail($arr2); 
                            
                            if(isset($dd) && !empty($dd))
                            {
                                $sstate_id = $dd-> state_id;
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $sysid,
                                "saler_type" => $sys_type,
                                "first_name"   => $worksheet['cells'][$i][5],
                                "mobile" => "",                        
                                "email" => "",
                                "address" => "",
                                "district_name" => $worksheet['cells'][$i][7],
                                "state_name" => $worksheet['cells'][$i][6],
                                "contact_person" => "",

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                                
                                $this->dbaccess_model->insert("salers", $dataset);
                            }
                        }

                        $dataset = array("file_id" => $upid,                        
                        "transaction_id" => $worksheet['cells'][$i][1],
                        "invoice_no"   => $worksheet['cells'][$i][2],                        
                        "invoice_date" => $worksheet['cells'][$i][3],                        
                        "retailer_id" => $worksheet['cells'][$i][4],
                        "company" => $worksheet['cells'][$i][9],
                        "plant" => $worksheet['cells'][$i][10],
                        "product_id" => $master_id,
                        "unit" => $worksheet['cells'][$i][12],
                        "quantity" => $worksheet['cells'][$i][13],
                        "quantity_mt" => $worksheet['cells'][$i][14],                        
                        
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][11]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Invoice date or Retailer ID or Product Name or Quantity in MT inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_sales")
        {            
            $from_date = date(DATE_DB, strtotime($from_date));
            $to_date = date(DATE_DB, strtotime($to_date));
            
            $sql = "DELETE FROM upload_sales 
                    WHERE from_date >= '$from_date' AND to_date <= '$to_date' AND state_id = '$state_id' AND district_id = '$city_id' ";
            $this->dbaccess_model->executeOnlySql($sql);             
            
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][1]) && !empty($worksheet['cells'][$i][1]) && isset($worksheet['cells'][$i][2]) && !empty($worksheet['cells'][$i][2]) && 
                    isset($worksheet['cells'][$i][3]) && !empty($worksheet['cells'][$i][3]) &&
                    isset($worksheet['cells'][$i][5]) && !empty($worksheet['cells'][$i][5]) && 
                    isset($worksheet['cells'][$i][6]) && !empty($worksheet['cells'][$i][6]))
                {
                    if($worksheet['cells'][$i][2] == "SSP-Zincated-")
                    $worksheet['cells'][$i][2] = "SSP-Zincated-Boronated";

                    $master_id = $this->admin_model->get_product_id($worksheet['cells'][$i][2]);
                    if($master_id > 0)
                    {                                  
                        if(!isset($worksheet['cells'][$i][6]) || empty($worksheet['cells'][$i][6]))
                        $worksheet['cells'][$i][6] = 0;

                        $company_type = 2;
                        if(strtoupper($worksheet['cells'][$i][1]) == "RMPCL")
                        {
                            $company_type = 1;
                        }

                        $worksheet['cells'][$i][5] = format_date($worksheet['cells'][$i][5], DATE_DB);

                        $dataset = array("file_id" => $upid,                        
                        "company" => $worksheet['cells'][$i][1],
                        "company_type" => $company_type,
                        "product_id" => $master_id,
                        "dealer_id" => $worksheet['cells'][$i][3],                        
                        "sales_date" => date(DATE_DB, strtotime($worksheet['cells'][$i][5])),
                        "quantity" => $worksheet['cells'][$i][6],
                        "agency_name" => $worksheet['cells'][$i][4],

                        "state_id" => $state_id,
                        "district_id" => $city_id,
                        "from_date" => $from_date,
                        "to_date" => $to_date,
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        //$skp_str .= "<tr><td colspan='2'></td></tr>";
                    }
                }
            }

            if(true)
            {
                $skp_str = "<tr><td colspan='2'>Only rows which having company products are added.</td></tr>";
            }            
        }
        elseif($upload_type == "upload_dispatch")
        {            
            $from_date = date(DATE_DB, strtotime($from_date));
            $to_date = date(DATE_DB, strtotime($to_date));

            $sql = "DELETE FROM upload_dispatch 
                    WHERE from_date >= '$from_date' AND to_date <= '$to_date' ";
            $this->dbaccess_model->executeOnlySql($sql);             
            
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7]) && 
                isset($worksheet['cells'][$i][9]) && !empty($worksheet['cells'][$i][9]) && 
                isset($worksheet['cells'][$i][23]) && !empty($worksheet['cells'][$i][23]) && 
                isset($worksheet['cells'][$i][16]) && !empty($worksheet['cells'][$i][16]))
                {
                    $master_id = $this->admin_model->get_product_id($worksheet['cells'][$i][16]);
                    if($master_id > 0)
                    {                                  
                        //echo "<pre>"; print_r($worksheet['cells'][$i]);
                        $bd = $worksheet['cells'][$i][1];
                        $od = $worksheet['cells'][$i][5];
                        $dd = $worksheet['cells'][$i][6];
                        //echo "<br/>$bd===$od===$dd........";

                        $worksheet['cells'][$i][9] = str_replace("WH", "", $worksheet['cells'][$i][9]);
                        $worksheet['cells'][$i][9] = str_replace("RE", "", $worksheet['cells'][$i][9]);


                        $worksheet['cells'][$i][11] = str_replace("WH", "", $worksheet['cells'][$i][11]);
                        $worksheet['cells'][$i][11] = str_replace("RE", "", $worksheet['cells'][$i][11]);


                        if(!isset($worksheet['cells'][$i][16]) || empty($worksheet['cells'][$i][16]))
                        {
                            $worksheet['cells'][$i][16] = 0;
                        }    

                        
                        if(isset($worksheet['cells'][$i][1]) && !empty($worksheet['cells'][$i][1]))
                        {
                            $worksheet['cells'][$i][1] = prepare_date($worksheet['cells'][$i][1], DATE_DB);
                        }


                        if(isset($worksheet['cells'][$i][5]) && !empty($worksheet['cells'][$i][5]))
                        {
                            $worksheet['cells'][$i][5] = prepare_date($worksheet['cells'][$i][5], DATE_DB);
                        }

                        if(isset($worksheet['cells'][$i][6]) && !empty($worksheet['cells'][$i][6]))
                        {
                            $worksheet['cells'][$i][6] = prepare_date($worksheet['cells'][$i][6], DATE_DB);
                        }

                        if(isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7]))
                        {
                            $worksheet['cells'][$i][7] = prepare_date($worksheet['cells'][$i][7], DATE_DB);
                        }


                        $worksheet['cells'][$i][23] = str_replace("D", "", $worksheet['cells'][$i][23]);

                        $dataset = array("file_id" => $upid,
                        "from_date" => $from_date,
                        "to_date" => $to_date,

                        "billing_month" => $worksheet['cells'][$i][1],
                        "billing_month_text" => $bd,
                        "invoice_no" => $worksheet['cells'][$i][2],
                        "state" => $worksheet['cells'][$i][3],
                        "sale_note" => $worksheet['cells'][$i][4],
                        "post_date" => $worksheet['cells'][$i][5],

                        "order_date" => $worksheet['cells'][$i][6],
                        "order_date_text" => $od,                        
                        "dispatch_date" => $worksheet['cells'][$i][7],
                        "dispatch_date_text" => $dd,
                        "tally_code" => $worksheet['cells'][$i][8],
                        "sold_to_id" => $worksheet['cells'][$i][9],
                        "sold_to_party" => $worksheet['cells'][$i][10],

                        "ship_to_id" => $worksheet['cells'][$i][11],
                        "ship_to_party" => $worksheet['cells'][$i][12],
                        "place" => $worksheet['cells'][$i][13],
                        "taluka_name" => $worksheet['cells'][$i][14],
                        "district_name" => $worksheet['cells'][$i][15],

                        "product_id" => $master_id,
                        "order_qty" => $worksheet['cells'][$i][17],
                        "quantity" => $worksheet['cells'][$i][18],
                        "lr_no" => $worksheet['cells'][$i][19],
                        "transporter_name" => $worksheet['cells'][$i][20],

                        "vehicle_number" => $worksheet['cells'][$i][21],
                        "batch_number" => $worksheet['cells'][$i][22],
                        "transaction_id" => $worksheet['cells'][$i][23],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                        
                        //echo "<pre>"; print_r($dataset);
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][16]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str)
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Dispatch date or FMS ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }     
        elseif($upload_type == "import_wholesalers")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(
                    isset($worksheet['cells'][$i][1]) && !empty($worksheet['cells'][$i][1]) && 
                    isset($worksheet['cells'][$i][2]) && !empty($worksheet['cells'][$i][2]) &&
                    isset($worksheet['cells'][$i][3]) && !empty($worksheet['cells'][$i][3]) &&
                    isset($worksheet['cells'][$i][6]) && !empty($worksheet['cells'][$i][6]) &&
                    isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7])
                )
                {
                    $worksheet['cells'][$i][1] = str_replace("WH", "", $worksheet['cells'][$i][1]);
                    $worksheet['cells'][$i][1] = str_replace("RE", "", $worksheet['cells'][$i][1]);

                    $arr = array("typ" => "wholesalersid", "id" => 0, "name" => $worksheet['cells'][$i][1]);
                    $chkm = $this->common_model->is_already_exist($arr);
                    if(!isset($chkm) || empty($chkm) || count($chkm) <= 0)
                    {                                  
                        $arr1 = array("typ" => "state_id_by_name", "id" => $worksheet['cells'][$i][7]);

                        $sd = $this->common_model->get_detail($arr1);
                        if(isset($sd) && !empty($sd))
                        {
                            $sstate_id = $sd-> state_id;

                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet['cells'][$i][6]);
                            $dd = $this->common_model->get_detail($arr2);
                            if(isset($dd) && !empty($dd))
                            {
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $worksheet['cells'][$i][1],
                                "saler_type" => 1,
                                "first_name"   => $worksheet['cells'][$i][2],
                                "mobile" => $worksheet['cells'][$i][3],                        
                                "email" => $worksheet['cells'][$i][4],
                                "address" => $worksheet['cells'][$i][5],
                                "district_name" => $worksheet['cells'][$i][6],
                                "state_name" => $worksheet['cells'][$i][7],
                                "contact_person" => $worksheet['cells'][$i][8],

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );
                            
                                $id = 0;
                                $id = $this->dbaccess_model->insert("salers", $dataset); 
                                
                                if($id) 
                                {
                                    $ok++;
                                }
                            }
                            else
                            {
                                $skp++;
                                $skp_str .= "<tr><td colspan='2'>District Name ".$worksheet['cells'][$i][6]." not exist in system.</td></tr>";
                            }
                        }
                        else
                        {
                            $skp++;
                            $skp_str .= "<tr><td colspan='2'>State Name ".$worksheet['cells'][$i][7]." not exist in system.</td></tr>";
                        }                                  
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>".$worksheet['cells'][$i][1]."=".$worksheet['cells'][$i][2]." Wholesaler ID already added.</td></tr>";
                    }
                }
                else
                {
                    $skp_str .= "<tr><td colspan='2'>Row: ".$i." Missing any one mandatory field.</td></tr>";
                }
            }

            if($skp_str != '')
            {
                $skp_str = $skp_str;
            }            
        }
        elseif($upload_type == "import_dealers")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(
                    isset($worksheet['cells'][$i][1]) && !empty($worksheet['cells'][$i][1]) && 
                    isset($worksheet['cells'][$i][2]) && !empty($worksheet['cells'][$i][2]) &&
                    isset($worksheet['cells'][$i][3]) && !empty($worksheet['cells'][$i][3]) &&
                    isset($worksheet['cells'][$i][6]) && !empty($worksheet['cells'][$i][6]) &&
                    isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7])
                )
                {
                    $worksheet['cells'][$i][1] = str_replace("WH", "", $worksheet['cells'][$i][1]);
                    $worksheet['cells'][$i][1] = str_replace("D", "", $worksheet['cells'][$i][1]);
                    $worksheet['cells'][$i][1] = str_replace("RT", "", $worksheet['cells'][$i][1]);
                    $worksheet['cells'][$i][1] = str_replace("RE", "", $worksheet['cells'][$i][1]);

                    $arr = array("typ" => "dealersid", "id" => 0, "name" => $worksheet['cells'][$i][1]);
                    $chkm = $this->common_model->is_already_exist($arr);
                    if(!isset($chkm) || empty($chkm) || count($chkm) <= 0)
                    {                                  
                        $arr1 = array("typ" => "state_id_by_name", "id" => $worksheet['cells'][$i][7]);                                                

                        $sd = $this->common_model->get_detail($arr1);
                        if(isset($sd) && !empty($sd))
                        {
                            $sstate_id = $sd-> state_id;

                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet['cells'][$i][6]);
                            $dd = $this->common_model->get_detail($arr2);
                            if(isset($dd) && !empty($dd))
                            {
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $worksheet['cells'][$i][1],
                                "saler_type" => 2,
                                "first_name"   => $worksheet['cells'][$i][2],
                                "mobile" => $worksheet['cells'][$i][3],                        
                                "email" => $worksheet['cells'][$i][4],
                                "address" => $worksheet['cells'][$i][5],
                                "district_name" => $worksheet['cells'][$i][6],
                                "state_name" => $worksheet['cells'][$i][7],
                                "contact_person" => $worksheet['cells'][$i][8],

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );
                            
                                $id = 0;
                                $id = $this->dbaccess_model->insert("salers", $dataset); 
                                
                                if($id) 
                                {
                                    $ok++;
                                }
                            }
                            else
                            {
                                $skp++;
                                $skp_str .= "<tr><td colspan='2'>District Name ".$worksheet['cells'][$i][6]." not exist in system.</td></tr>";
                            }
                        }
                        else
                        {
                            $skp++;
                            $skp_str .= "<tr><td colspan='2'>State Name ".$worksheet['cells'][$i][7]." not exist in system.</td></tr>";
                        }                                  
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>".$worksheet['cells'][$i][1]."=".$worksheet['cells'][$i][2]." Dealer ID already added.</td></tr>";
                    }
                }
                else
                {
                    $skp_str .= "<tr><td colspan='2'>Row: ".$i." Missing any one mandatory field.</td></tr>";
                }
            }

            if($skp_str != '')
            {
                $skp_str = $skp_str;
            }            
        }
        elseif($upload_type == "import_farmers")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(
                    isset($worksheet['cells'][$i][1]) && !empty($worksheet['cells'][$i][1]) && 
                    isset($worksheet['cells'][$i][2]) && !empty($worksheet['cells'][$i][2]) &&                    
                    isset($worksheet['cells'][$i][6]) && !empty($worksheet['cells'][$i][6])
                )
                {
                    $arr = array("typ" => "farmers", "id" => 0, "name" => $worksheet['cells'][$i][2]);
                    $chkm = $this->common_model->is_already_exist($arr);
                    if(!isset($chkm) || empty($chkm) || count($chkm) <= 0)
                    {                                  
                        //$arr2 = array("typ" => "_id_by_name", "id" => $worksheet['cells'][$i][6]);
                        //$sd = $this->common_model->get_detail($arr1);
                        //if(isset($sd) && !empty($sd))
                        if(true)
                        {
                            $sstate_id = $_POST['farmer_state'];
                            $ddistrict_id = $_POST['farmer_district'];
                            $ttaluka_id = $_POST['farmer_taluka'];
                            
                            if(true)
                            {
                                $farmer_reg_num = $this->common_model->get_farmer_reg_no("farmers");
                                $farmer_reg_num = regno($farmer_reg_num + 1, 5);

                                $data = array("farmer_reg_num" => $farmer_reg_num,
                                "farmer_name" => $worksheet['cells'][$i][1], 
                                "farmer_mobile" => $worksheet['cells'][$i][2], 
                                "farmer_email" => $worksheet['cells'][$i][3], 
                                "farmer_plot_no" => $worksheet['cells'][$i][4], 
                                "farmer_village" => $worksheet['cells'][$i][5], 
                                "farmer_taluka" => $ttaluka_id, 
                                "farmer_district" => $ddistrict_id, 
                                "farmer_state" => $sstate_id,
                                "on_or_off_site" => 'off_site',
                                "geo_lat" => 0,
                                "geo_long" => 0,                                
                                "farmer_land_area" => $worksheet['cells'][$i][6],
                                "farmer_reg_date" => date(DATE_DB),
                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                            
                                $id = 0;
                                $id = $this->dbaccess_model->insert("farmers", $data); 
                                
                                if($id) 
                                {
                                    $ok++;
                                }
                            }
                        }                                 
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>".$worksheet['cells'][$i][1]."=".$worksheet['cells'][$i][2]." Mobile is already registered.</td></tr>";
                    }
                }
                else
                {
                    $skp++;
                    $skp_str .= "<tr><td colspan='2'>Row: ".$i." Missing any one mandatory field.</td></tr>";
                }
            }

            if($skp_str != '')
            {
                $skp_str = $skp_str;
            }            
        }

        if($skp == $totalr)
        {
            $sql = "DELETE FROM upload_files WHERE file_id = '$upid' AND ftype = '$upload_type' LIMIT 1";
            $this->dbaccess_model->executeOnlySql($sql);             
        }


        $msg = "<tr><td colspan='2'>Total Records:&nbsp;<b>".$totalr."</b></td></tr>";
        $msg .= "<tr><td colspan='2'>Imported Successfully:&nbsp;<b>".$ok."</b></td></tr>";
        $msg .= "<tr><td colspan='2'>Skipped Records:&nbsp;<b>".$skp."</b></td></tr>";
        $msg .= $skp_str;
        echo json_encode(array("status"=>1,"message"=>"<table width='100%'>".$msg."</table>"));
        die;
    }



    public function import_records_csv($upload_type, $path, $filename, $upid, $state_id="", $city_id="", $from_date="", $to_date="")
    {
        $filepath = $path.$filename;
        $ptr = fopen($filepath, 'r');
        $datetime = date(DATETIME_DB);
        $sno = 0;        
                
        $totalr = $ok = $skp = 0; $skp_str = "";

        /*if($upload_type == "upload_acknowledgement1")       $start = 6;
        elseif($upload_type == "upload_acknowledgement2")   $start = 6;
        elseif($upload_type == "upload_wholesaler")         $start = 7;
        elseif($upload_type == "upload_retailer")           $start = 3;
        elseif($upload_type == "upload_retailer_pos")       $start = 3; 
        elseif($upload_type == "upload_sales")              $start = 5;
        elseif($upload_type == "upload_dispatch")           $start = 4;
        elseif($upload_type == "import_wholesalers")        $start = 2;
        elseif($upload_type == "import_dealers")            $start = 2;
        else  $start = 1;*/

        if($upload_type == "upload_acknowledgement1")       $num = 34;
        elseif($upload_type == "upload_acknowledgement2")   $num = 39;
        elseif($upload_type == "upload_wholesaler")         $num = 23;
        elseif($upload_type == "upload_retailer")           $num = 16;
        elseif($upload_type == "upload_retailer_pos")       $num = 14; 
        elseif($upload_type == "upload_sales")              $num = 6;
        elseif($upload_type == "upload_dispatch")           $num = 23;        
        else  $num = 1;

        $start = 2;

        $row = 1; $worksheet = array();
        while(($fdata = fgetcsv($ptr, 5000, ",")) !== FALSE)
        {
            //if($row == $start)      
            //$num = count($fdata);            
            
            //echo "\n row Row=$row.......Num=$num\n".print_r($fdata);
            for ($c=0; $c < $num; $c++) 
            {
                $worksheet[$row][($c+1)] = sanitize_text($fdata[$c]);
            }

            $row++;    
        }

        
        if($upload_type == "upload_acknowledgement1")
        {
            if($num < 34)
            {
                echo json_encode(array("status"=>0,"message"=>"Invalid csv file uploaded."));
                die;
            }

            $numRows = count($worksheet);

            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet[$i][7]) && !empty($worksheet[$i][7]) && isset($worksheet[$i][14]) && !empty($worksheet[$i][14]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet[$i][14]);
                    if($master_id > 0)
                    {                                                      
                        $worksheet[$i][3] = format_date($worksheet[$i][3]);                        
                        $worksheet[$i][20] = format_date($worksheet[$i][20]);
                        $worksheet[$i][21] = format_date($worksheet[$i][21]);
                        $worksheet[$i][34] = format_date($worksheet[$i][34],DATETIME_DB);


                        if($worksheet[$i][9] == "RETAILER" || $worksheet[$i][9] == "WHOLESALER")
                        {
                            $sys_type = 1;
                            /*if($worksheet[$i][9] == "RETAILER")
                            {
                                $sys_type = 2;
                            }*/

                            $sysid = $worksheet[$i][7];
                            $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                            $dchk = $this->dbaccess_model->is_exist($sql);
                            
                            if($dchk == 0)
                            {
                                
                                $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet[$i][13]);
                                $dd = $this->common_model->get_detail($arr2); 
                                
                                if(isset($dd) && !empty($dd))
                                {
                                    $sstate_id = $dd-> state_id;
                                    $ddistrict_id = $dd-> district_id;

                                    $dataset = array("saler_sys_id" => $sysid,
                                    "saler_type" => $sys_type,
                                    "first_name"   => $worksheet[$i][8],
                                    "mobile" => $worksheet[$i][11],                        
                                    "email" => "",
                                    "address" => "",
                                    "district_name" => $worksheet[$i][13],
                                    "state_name" => $worksheet[$i][12],
                                    "contact_person" => "",

                                    "state_id" => $sstate_id,
                                    "district_id" => $ddistrict_id,

                                    "created_date" => date(DATETIME_DB),                
                                    "created_by" => $this->user_id,
                                    "updated_date" => date(DATETIME_DB),                
                                    "updated_by" => $this->user_id
                                    );                                
                                    
                                    $this->dbaccess_model->insert("salers", $dataset);
                                }
                            }
                        }


                        $dataset = array("file_id" => $upid,
                        "transaction_id" => $worksheet[$i][1],
                        "invoice_no"        => $worksheet[$i][2],
                        "invoice_date" => $worksheet[$i][3],
                        "marketer" => $worksheet[$i][4],
                        "manufacturer" => $worksheet[$i][5],
                        "plant" => $worksheet[$i][6],
                        "dealer_id" => $worksheet[$i][7],
                        "product_id" => $master_id,
                        "unit" => $worksheet[$i][15],
                        "quantity" => $worksheet[$i][16],
                        "quantity_mt" => $worksheet[$i][17],
                        "received_quantity" => $worksheet[$i][18],
                        "status" => $worksheet[$i][19],
                        "entry_date" => $worksheet[$i][20],
                        "lock_date" => $worksheet[$i][21],
                        "txn_remark" => $worksheet[$i][22],
                        "ack_through" => $worksheet[$i][23],
                        "subsidy_year1" => $worksheet[$i][24],
                        "subsidy_month1" => $worksheet[$i][25],
                        "month1_qty" => $worksheet[$i][26],
                        "subsidy_month2" => $worksheet[$i][27],
                        "subsidy_year2" => $worksheet[$i][28],
                        "month2_qty" => $worksheet[$i][29],
                        "challan_no" => $worksheet[$i][30],
                        "dd_no" => $worksheet[$i][31],
                        "lorry_no" => $worksheet[$i][32],
                        "lorry_capacity" => $worksheet[$i][33],
                        "retailer_receipt_date" => $worksheet[$i][34],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet[$i][14]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Dealer ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_acknowledgement2")
        {
            if($num < 39)
            {
                echo json_encode(array("status"=>0,"message"=>"Invalid csv file uploaded."));
                die;
            }

            $numRows = count($worksheet);

            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet[$i][7]) && !empty($worksheet[$i][7]) && isset($worksheet[$i][13]) && !empty($worksheet[$i][13]) && isset($worksheet[$i][18]) && !empty($worksheet[$i][18]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet[$i][18]);
                    if($master_id > 0)
                    {                                                      
                        $worksheet[$i][3] = format_date($worksheet[$i][3]);
                        $worksheet[$i][25] = format_date($worksheet[$i][25]);
                        $worksheet[$i][26] = format_date($worksheet[$i][26]);
                        $worksheet[$i][39] = format_date($worksheet[$i][39]);
                        

                        $sys_type = 1;
                        $sysid = $worksheet[$i][7];
                        $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                        $dchk = $this->dbaccess_model->is_exist($sql);                        
                        if($dchk == 0)
                        {                        
                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet[$i][11]);
                            $dd = $this->common_model->get_detail($arr2); 
                            
                            if(isset($dd) && !empty($dd))
                            {
                                $sstate_id = $dd-> state_id;
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $sysid,
                                "saler_type" => $sys_type,
                                "first_name"   => $worksheet[$i][8],
                                "mobile" => "",                        
                                "email" => "",
                                "address" => "",
                                "district_name" => $worksheet[$i][11],
                                "state_name" => $worksheet[$i][10],
                                "contact_person" => "",

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                                
                                $this->dbaccess_model->insert("salers", $dataset);
                            }
                        }


                        $sys_type = 2;
                        $sysid = $worksheet[$i][13];
                        $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                        $dchk = $this->dbaccess_model->is_exist($sql);                        
                        if($dchk == 0)
                        {                        
                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet[$i][11]);
                            $dd = $this->common_model->get_detail($arr2); 
                            
                            if(isset($dd) && !empty($dd))
                            {
                                $sstate_id = $dd-> state_id;
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $sysid,
                                "saler_type" => $sys_type,
                                "first_name"   => $worksheet[$i][15],
                                "mobile" => $worksheet[$i][17],                        
                                "email" => "",
                                "address" => "",
                                "district_name" => $worksheet[$i][12],
                                "state_name" => $worksheet[$i][10],
                                "contact_person" => "",

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                                
                                $this->dbaccess_model->insert("salers", $dataset);
                            }
                        }


                        $dataset = array("file_id" => $upid,
                        "transaction_id" => $worksheet[$i][1],
                        "invoice_no"        => $worksheet[$i][2],
                        "invoice_date" => $worksheet[$i][3],
                        "marketer" => $worksheet[$i][4],
                        "manufacturer" => $worksheet[$i][5],
                        "plant" => $worksheet[$i][6],
                        "wholesaler_id" => $worksheet[$i][7],
                        "dealer_id" => $worksheet[$i][13],
                        "product_id" => $master_id,
                        "unit" => $worksheet[$i][19],
                        "quantity" => $worksheet[$i][20],
                        "quantity_mt" => $worksheet[$i][21],
                        "received_quantity" => $worksheet[$i][22],
                        "status" => $worksheet[$i][23],
                        "txn_type" => $worksheet[$i][24],
                        "entry_date" => $worksheet[$i][25],
                        "lock_date" => $worksheet[$i][26],
                        "ack_through" => $worksheet[$i][27],
                        "txn_remark" => $worksheet[$i][28],
                        "subsidy_month1" => $worksheet[$i][29],
                        "subsidy_year1" => $worksheet[$i][30],                        
                        "month1_qty" => $worksheet[$i][31],
                        "subsidy_month2" => $worksheet[$i][32],
                        "subsidy_year2" => $worksheet[$i][33],
                        "month2_qty" => $worksheet[$i][34],
                        "challan_no" => $worksheet[$i][35],
                        "lorry_no" => $worksheet[$i][36],
                        "lorry_capacity" => $worksheet[$i][37],
                        "dispatch_no" => $worksheet[$i][38],
                        "retailer_receipt_date" => $worksheet[$i][39],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet[$i][18]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }            

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Wholesaler ID, Dealer ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_wholesaler")
        {
            
            if($num < 23)
            {
                echo json_encode(array("status"=>0,"message"=>"Invalid csv file uploaded."));
                die;
            }

            $numRows = count($worksheet);


            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet[$i][4]) && !empty($worksheet[$i][4]) && isset($worksheet[$i][7]) && !empty($worksheet[$i][7]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet[$i][4]);
                    if($master_id > 0)
                    {                   
                        $sys_type = 1;
                        $sysid = $worksheet[$i][7];
                        $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                        $dchk = $this->dbaccess_model->is_exist($sql);                        
                        if($dchk == 0)
                        {                        
                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet[$i][6]);
                            $dd = $this->common_model->get_detail($arr2); 
                            
                            if(isset($dd) && !empty($dd))
                            {
                                $sstate_id = $dd-> state_id;
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $sysid,
                                "saler_type" => $sys_type,
                                "first_name"   => $worksheet[$i][9],
                                "mobile" => "",                        
                                "email" => "",
                                "address" => "",
                                "district_name" => $worksheet[$i][6],
                                "state_name" => $worksheet[$i][5],
                                "contact_person" => "",

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                                
                                $this->dbaccess_model->insert("salers", $dataset);
                            }
                        }



                        $dataset = array("file_id" => $upid,
                        "serial_number" => $worksheet[$i][1],
                        "company"        => $worksheet[$i][2],                        
                        "plant" => $worksheet[$i][3],
                        "product_id" => $master_id,                        
                        "dealer_id" => $worksheet[$i][7],                        
                        "wholesaler_ob" => $worksheet[$i][10],
                        "comp_ws_sale" => $worksheet[$i][11],
                        "comp_ws_sale_rcpt" => $worksheet[$i][12],
                        "received_from_ws" => $worksheet[$i][13],
                        "received_from_ws_ack" => $worksheet[$i][14],
                        "ws_rt_sale" => $worksheet[$i][15],
                        "ws_rt_sale_rcpt" => $worksheet[$i][16],
                        "ws_ws_sale" => $worksheet[$i][17],
                        "ws_ws_sale_rcpt" => $worksheet[$i][18],
                        "total_sales_by_ws" => $worksheet[$i][19],
                        "stock_transfer_from_ws_to_retailer" => $worksheet[$i][20],
                        "stock_transfer_from_ws_to_retailer_ack" => $worksheet[$i][21],
                        "balance_with_ws" => $worksheet[$i][22],
                        "total_ack_to_ws" => $worksheet[$i][23],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet[$i][4]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Product Name or Dealer ID inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_retailer")
        {
            if($num < 16)
            {
                echo json_encode(array("status"=>0,"message"=>"Invalid csv file uploaded."));
                die;
            }

            $numRows = count($worksheet);

            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet[$i][5]) && !empty($worksheet[$i][5]) && isset($worksheet[$i][11]) && !empty($worksheet[$i][11]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet[$i][11]);
                    if($master_id > 0)
                    {                                  
                        
                        $sys_type = 2;
                        $sysid = $worksheet[$i][5];
                        $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                        $dchk = $this->dbaccess_model->is_exist($sql);                        
                        if($dchk == 0)
                        {                        
                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet[$i][3]);
                            $dd = $this->common_model->get_detail($arr2); 
                            
                            if(isset($dd) && !empty($dd))
                            {
                                $sstate_id = $dd-> state_id;
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $sysid,
                                "saler_type" => $sys_type,
                                "first_name"   => $worksheet[$i][6],
                                "mobile" => $worksheet[$i][7],                        
                                "email" => "",
                                "address" => "",
                                "district_name" => $worksheet[$i][3],
                                "state_name" => $worksheet[$i][2],
                                "contact_person" => "",

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                                
                                $this->dbaccess_model->insert("salers", $dataset);
                            }
                        }


                        $dataset = array("file_id" => $upid,
                        "serial_number" => $worksheet[$i][1],
                        "retailer_id"   => $worksheet[$i][5],                        
                        "company" => $worksheet[$i][9],                        
                        "plant" => $worksheet[$i][10],
                        "product_id" => $master_id,
                        "opening_balance" => $worksheet[$i][12],
                        "received_quantity" => $worksheet[$i][13],
                        "sold_quantity" => $worksheet[$i][14],
                        "availabilty" => $worksheet[$i][15],
                        "closing_balance" => $worksheet[$i][16],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet[$i][11]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Retailer ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_retailer_pos")
        {
            if($num < 14)
            {
                echo json_encode(array("status"=>0,"message"=>"Invalid csv file uploaded."));
                die;
            }

            $numRows = count($worksheet);

            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet[$i][3]) && !empty($worksheet[$i][3]) && isset($worksheet[$i][4]) && !empty($worksheet[$i][4]) && isset($worksheet[$i][11]) && !empty($worksheet[$i][11]) && isset($worksheet[$i][14]) && !empty($worksheet[$i][14]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet[$i][11]);
                    if($master_id > 0)
                    {                                  
                        $worksheet[$i][3] = format_date($worksheet[$i][3]);


                        $sys_type = 2;
                        $sysid = $worksheet[$i][4];
                        $sql = "SELECT saler_id as id FROM salers WHERE saler_sys_id = $sysid AND saler_type = $sys_type LIMIT 1";
                        $dchk = $this->dbaccess_model->is_exist($sql);                        
                        if($dchk == 0)
                        {                        
                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet[$i][7]);
                            $dd = $this->common_model->get_detail($arr2); 
                            
                            if(isset($dd) && !empty($dd))
                            {
                                $sstate_id = $dd-> state_id;
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $sysid,
                                "saler_type" => $sys_type,
                                "first_name"   => $worksheet[$i][5],
                                "mobile" => "",                        
                                "email" => "",
                                "address" => "",
                                "district_name" => $worksheet[$i][7],
                                "state_name" => $worksheet[$i][6],
                                "contact_person" => "",

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );                                
                                
                                $this->dbaccess_model->insert("salers", $dataset);
                            }
                        }

                        $dataset = array("file_id" => $upid,                        
                        "transaction_id" => $worksheet[$i][1],
                        "invoice_no"   => $worksheet[$i][2],                        
                        "invoice_date" => $worksheet[$i][3],                        
                        "retailer_id" => $worksheet[$i][4],
                        "company" => $worksheet[$i][9],
                        "plant" => $worksheet[$i][10],
                        "product_id" => $master_id,
                        "unit" => $worksheet[$i][12],
                        "quantity" => $worksheet[$i][13],
                        "quantity_mt" => $worksheet[$i][14],                        
                        
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet[$i][11]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Invoice date or Retailer ID or Product Name or Quantity in MT inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_sales")
        {                
            if($num < 6)
            {
                echo json_encode(array("status"=>0,"message"=>"Invalid csv file uploaded."));
                die;
            }

            $numRows = count($worksheet);

            $from_date = date(DATE_DB, strtotime($from_date));
            $to_date = date(DATE_DB, strtotime($to_date));

            $sql = "DELETE FROM upload_sales 
                    WHERE from_date >= '$from_date' AND to_date <= '$to_date' AND state_id = '$state_id' AND district_id = '$city_id' ";
            $this->dbaccess_model->executeOnlySql($sql);             
            
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet[$i][1]) && !empty($worksheet[$i][1]) && isset($worksheet[$i][2]) && !empty($worksheet[$i][2]) && 
                    isset($worksheet[$i][3]) && !empty($worksheet[$i][3]) &&
                    isset($worksheet[$i][5]) && !empty($worksheet[$i][5]) && 
                    isset($worksheet[$i][6]) && !empty($worksheet[$i][6]))
                {
                    if($worksheet[$i][2] == "SSP-Zincated-")
                    $worksheet[$i][2] = "SSP-Zincated-Boronated";

                    $master_id = $this->admin_model->get_product_id($worksheet[$i][2]);
                    if($master_id > 0)
                    {                                  
                        if(!isset($worksheet[$i][6]) || empty($worksheet[$i][6]))
                        $worksheet[$i][6] = 0;

                        $company_type = 2;
                        if(strtoupper($worksheet[$i][1]) == "RMPCL")
                        {
                            $company_type = 1;
                        }

                        $worksheet[$i][5] = format_date($worksheet[$i][5], DATE_DB);

                        $dataset = array("file_id" => $upid,                        
                        "company" => $worksheet[$i][1],
                        "company_type" => $company_type,
                        "product_id" => $master_id,
                        "dealer_id" => $worksheet[$i][3],                        
                        "sales_date" => date(DATE_DB, strtotime($worksheet[$i][5])),
                        "quantity" => $worksheet[$i][6],
                        "agency_name" => $worksheet[$i][4],

                        "state_id" => $state_id,
                        "district_id" => $city_id,
                        "from_date" => $from_date,
                        "to_date" => $to_date,
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        //$skp_str .= "<tr><td colspan='2'></td></tr>";
                    }
                }
            }

            if(true)
            {
                $skp_str = "<tr><td colspan='2'>Only rows which having company products are added.</td></tr>";
            }            
        }
        elseif($upload_type == "upload_dispatch")
        {           
            if($num < 23)
            {
                echo json_encode(array("status"=>0,"message"=>"Invalid csv file uploaded."));
                die;
            }

            $numRows = count($worksheet);


            $from_date = date(DATE_DB, strtotime($from_date));
            $to_date = date(DATE_DB, strtotime($to_date));

            $sql = "DELETE FROM upload_dispatch 
                    WHERE from_date >= '$from_date' AND to_date <= '$to_date' ";
            $this->dbaccess_model->executeOnlySql($sql);             
            
            for ($i=$start; $i<=$numRows; $i++) 
            {                
                
                $totalr++;
                if(isset($worksheet[$i][7]) && !empty($worksheet[$i][7]) && 
                isset($worksheet[$i][9]) && !empty($worksheet[$i][9]) && 
                isset($worksheet[$i][23]) && !empty($worksheet[$i][23]) && 
                isset($worksheet[$i][16]) && !empty($worksheet[$i][16]))
                {
                    $master_id = $this->admin_model->get_product_id($worksheet[$i][16]);
                    if($master_id > 0)
                    {                                  
                        
                        $bd = $worksheet[$i][1];
                        $od = $worksheet[$i][5];
                        $dd = $worksheet[$i][6];
                        //echo "<br/>$bd===$od===$dd........";

                        $worksheet[$i][9] = str_replace("WH", "", $worksheet[$i][9]);
                        $worksheet[$i][9] = str_replace("RE", "", $worksheet[$i][9]);


                        $worksheet[$i][11] = str_replace("WH", "", $worksheet[$i][11]);
                        $worksheet[$i][11] = str_replace("RE", "", $worksheet[$i][11]);


                        if(!isset($worksheet[$i][16]) || empty($worksheet[$i][16]))
                        {
                            $worksheet[$i][16] = 0;
                        }    

                        
                        if(isset($worksheet[$i][1]) && !empty($worksheet[$i][1]))
                        {
                            $worksheet[$i][1] = prepare_date($worksheet[$i][1], DATE_DB);
                        }


                        if(isset($worksheet[$i][5]) && !empty($worksheet[$i][5]))
                        {
                            $worksheet[$i][5] = prepare_date($worksheet[$i][5], DATE_DB);
                        }

                        if(isset($worksheet[$i][6]) && !empty($worksheet[$i][6]))
                        {
                            $worksheet[$i][6] = prepare_date($worksheet[$i][6], DATE_DB);
                        }

                        if(isset($worksheet[$i][7]) && !empty($worksheet[$i][7]))
                        {
                            $worksheet[$i][7] = prepare_date($worksheet[$i][7], DATE_DB);
                        }


                        $worksheet[$i][23] = str_replace("D", "", $worksheet[$i][23]);

                        $dataset = array("file_id" => $upid,
                        "from_date" => $from_date,
                        "to_date" => $to_date,

                        "billing_month" => $worksheet[$i][1],
                        "billing_month_text" => $bd,
                        "invoice_no" => $worksheet[$i][2],
                        "state" => $worksheet[$i][3],
                        "sale_note" => $worksheet[$i][4],
                        "post_date" => $worksheet[$i][5],

                        "order_date" => $worksheet[$i][6],
                        "order_date_text" => $od,                        
                        "dispatch_date" => $worksheet[$i][7],
                        "dispatch_date_text" => $dd,
                        "tally_code" => $worksheet[$i][8],
                        "sold_to_id" => $worksheet[$i][9],
                        "sold_to_party" => $worksheet[$i][10],

                        "ship_to_id" => $worksheet[$i][11],
                        "ship_to_party" => $worksheet[$i][12],
                        "place" => $worksheet[$i][13],
                        "taluka_name" => $worksheet[$i][14],
                        "district_name" => $worksheet[$i][15],

                        "product_id" => $master_id,
                        "order_qty" => $worksheet[$i][17],
                        "quantity" => $worksheet[$i][18],
                        "lr_no" => $worksheet[$i][19],
                        "transporter_name" => $worksheet[$i][20],

                        "vehicle_number" => $worksheet[$i][21],
                        "batch_number" => $worksheet[$i][22],
                        "transaction_id" => $worksheet[$i][23],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                        
                        //echo "<pre>"; print_r($dataset);
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet[$i][16]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            
            }

            if($skp_str)
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Dispatch date or FMS ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        } 
        

        if($skp == $totalr)
        {
            $sql = "DELETE FROM upload_files WHERE file_id = '$upid' AND ftype = '$upload_type' LIMIT 1";
            $this->dbaccess_model->executeOnlySql($sql);             
        }


        $msg = "<tr><td colspan='2'>Total Records:&nbsp;<b>".$totalr."</b></td></tr>";
        $msg .= "<tr><td colspan='2'>Imported Successfully:&nbsp;<b>".$ok."</b></td></tr>";
        $msg .= "<tr><td colspan='2'>Skipped Records:&nbsp;<b>".$skp."</b></td></tr>";
        $msg .= $skp_str;
        echo json_encode(array("status"=>1,"message"=>"<table width='100%'>".$msg."</table>"));
        die;
    }


    /*----------------------------------------------------------
    Inventory Upload Listing Management
    ----------------------------------------------------------*/
    public function inventory_acknowledgement1()
    {
        $this->validate_permission(3, 4);

        $data['details'] = $this->admin_model->last_upload_date('upload_acknowledgement1');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $data['fsid'] = $this->uri->segment(3);
        $data['fdid'] = $this->uri->segment(4);

        $this->load->view('includes/main_header');
        $this->load->view('inventory_acknowledgement1', $data);
        $this->load->view('includes/main_footer');
    }


    public function inventory_acknowledgement1_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = $this-> set_limit("salers");


        $file_id = $_POST['file_id'];        
        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_product = $_POST['fby_product'];
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        $fby_keyword = $_POST['fby_keyword'];

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND ss.state_id = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND ss.district_id = '$fby_city' "; 
        }

        if(isset($fby_product) && !empty($fby_product))
        {
           $quick_search .= " AND p.product_id = '$fby_product' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND s.invoice_date >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND s.invoice_date <= '$fby_to_date' "; 
        }

        if(isset($fby_keyword) && !empty($fby_keyword))
        {
            $quick_search .= " AND (saler_sys_id LIKE '$fby_keyword%' OR ss.mobile LIKE '$fby_keyword%' OR quantity LIKE '$fby_keyword%' OR dd_no LIKE '$fby_keyword%' OR ss.first_name LIKE '$fby_keyword%' OR ss.last_name LIKE '$fby_keyword%' OR ss.state_name LIKE '$fby_keyword%' OR ss.district_name LIKE '$fby_keyword%') ";
        }

        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.invoice_date', 's.dealer_id','ss.first_name', 'ss.last_name', 'ss.mobile', 'ss.state_name', 'ss.district_name', 's.product_id', 's.unit', 's.quantity', 's.dd_no', 'p.product_name');

        $sTable = "upload_acknowledgement1";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,ss.mobile,ss.state_name, ss.district_name
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();              
            //$row[] = $aRow-> invoice_no;
            $row[] = date(DATE, strtotime($aRow-> invoice_date));
            

            $row[] = $aRow-> dealer_id;          
            
            $row[] = $aRow-> first_name." ".$aRow-> last_name;            
            $row[] = $aRow-> mobile;
            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name;
            $row[] = $aRow-> product_name;
            $row[] = $aRow-> quantity." ".$aRow-> unit;
            $row[] = $aRow-> dd_no;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    public function inventory_acknowledgement2()
    {
        $this->validate_permission(4, 4);

        $data['details'] = $this->admin_model->last_upload_date('upload_acknowledgement2');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $data['fsid'] = $this->uri->segment(3);
        $data['fdid'] = $this->uri->segment(4);

        $this->load->view('includes/main_header');
        $this->load->view('inventory_acknowledgement2', $data);
        $this->load->view('includes/main_footer');
    }


    public function inventory_acknowledgement2_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = $this-> set_limit("salers");

        $file_id = $_POST['file_id']; 
        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_product = $_POST['fby_product'];
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        $fby_keyword = $_POST['fby_keyword'];

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND (ss.state_id = '$fby_state' OR w.state_id = '$fby_state' ) "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND (ss.district_id = '$fby_city' OR w.district_id = '$fby_city' ) "; 
        }

        if(isset($fby_product) && !empty($fby_product))
        {
           $quick_search .= " AND p.product_id = '$fby_product' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND s.invoice_date >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND s.invoice_date <= '$fby_to_date' "; 
        }

        if(isset($fby_keyword) && !empty($fby_keyword))
        {
            $_POST['search']['value'] = $fby_keyword;
        }

        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.invoice_date', 's.wholesaler_id', 's.dealer_id','ss.first_name', 'ss.last_name', 'ss.mobile', 'ss.state_name', 'ss.district_name', 'w.first_name', 'w.last_name', 'w.mobile', 'w.state_name', 'w.district_name', 's.product_id', 's.unit', 's.quantity', 's.dispatch_no', 'p.product_name');

        $sTable = "upload_acknowledgement2";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
                

        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,ss.mobile,ss.state_name, ss.district_name,w.first_name as wfirst_name,w.last_name as wlast_name,w.mobile as wmobile,w.state_name as wstate_name, w.district_name as wdistrict_name
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();                          
            $row[] = date(DATE, strtotime($aRow-> invoice_date));
            
            $row[] = $aRow-> wholesaler_id;                      
            $row[] = $aRow-> wfirst_name." ".$aRow-> wlast_name;            
            //$row[] = $aRow-> wmobile;
            $row[] = $aRow-> wstate_name;
            $row[] = $aRow-> wdistrict_name;

            $row[] = $aRow-> dealer_id;                      
            $row[] = $aRow-> first_name." ".$aRow-> last_name;            
            $row[] = $aRow-> mobile;
            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name;

            $row[] = $aRow-> product_name;
            $row[] = $aRow-> quantity." ".$aRow-> unit;
            $row[] = $aRow-> dispatch_no;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    public function inventory_wholesaler()
    {
        $this->validate_permission(5, 4);

        $data['details'] = $this->admin_model->last_upload_date('upload_wholesaler');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $data['fsid'] = $this->uri->segment(3);
        $data['fdid'] = $this->uri->segment(4);

        $this->load->view('includes/main_header');
        $this->load->view('inventory_wholesaler', $data);
        $this->load->view('includes/main_footer');
    }


    public function inventory_wholesaler_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = $this-> set_limit("salers");

        $file_id = $_POST['file_id']; 
        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_product = $_POST['fby_product'];
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        $fby_keyword = $_POST['fby_keyword'];

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND ss.state_id = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND ss.district_id = '$fby_city' "; 
        }

        if(isset($fby_product) && !empty($fby_product))
        {
           $quick_search .= " AND p.product_id = '$fby_product' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND s.created_date >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND s.created_date <= '$fby_to_date' "; 
        }

        if(isset($fby_keyword) && !empty($fby_keyword))
        {
            $_POST['search']['value'] = $fby_keyword;
        }

        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.dealer_id','ss.first_name', 'ss.last_name', 'ss.state_name', 'ss.district_name', 's.balance_with_ws', 'p.product_name');

        $sTable = "upload_wholesaler";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
                

        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,ss.mobile,ss.state_name, ss.district_name
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1        
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1        
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();              
            $row[] = $aRow-> product_name;

            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name;

            $row[] = $aRow-> dealer_id;                      
            $row[] = $aRow-> first_name." ".$aRow-> last_name;            
            
            $row[] = $aRow-> balance_with_ws;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    public function inventory_retailer()
    {
        $this->validate_permission(6, 4);

        $data['details'] = $this->admin_model->last_upload_date('upload_retailer');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $data['fsid'] = $this->uri->segment(3);
        $data['fdid'] = $this->uri->segment(4);

        $this->load->view('includes/main_header');
        $this->load->view('inventory_retailer', $data);
        $this->load->view('includes/main_footer');
    }


    public function inventory_retailer_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = $this-> set_limit("salers");

        $file_id = $_POST['file_id'];
        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_product = $_POST['fby_product'];
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        $fby_keyword = $_POST['fby_keyword'];

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND ss.state_id = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND ss.district_id = '$fby_city' "; 
        }

        if(isset($fby_product) && !empty($fby_product))
        {
           $quick_search .= " AND p.product_id = '$fby_product' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND s.created_date >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND s.created_date <= '$fby_to_date' "; 
        }

        if(isset($fby_keyword) && !empty($fby_keyword))
        {
            $_POST['search']['value'] = $fby_keyword;
            //$quick_search .= " AND (saler_sys_id LIKE '$fby_keyword%' OR ss.mobile LIKE '$fby_keyword%' OR quantity LIKE '$fby_keyword%' OR dd_no LIKE '$fby_keyword%' OR ss.first_name LIKE '$fby_keyword%' OR ss.last_name LIKE '$fby_keyword%' OR ss.state_name LIKE '$fby_keyword%' OR ss.district_name LIKE '$fby_keyword%') ";
        } 
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.retailer_id','ss.first_name', 'ss.last_name', 'ss.state_name', 'ss.district_name', 'ss.mobile', 's.availabilty', 'p.product_name');
        $sTable = "upload_retailer";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
                

        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,ss.mobile,ss.state_name, ss.district_name
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 
            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name;
            $row[] = $aRow-> retailer_id;                      
            $row[] = $aRow-> first_name." ".$aRow-> last_name; 
            $row[] = $aRow-> mobile;
            $row[] = $aRow-> product_name;            
            $row[] = $aRow-> availabilty;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }




    public function sales_report()
    {
        $this->validate_permission(9, 4);

        $data['details'] = $this->admin_model->last_upload_date('upload_sales');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $this->load->view('includes/main_header');
        $this->load->view('sales_report', $data);
        $this->load->view('includes/main_footer');
    }


    public function sales_report_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = $this-> set_limit("salers");

        $file_id = $_POST['file_id']; 
        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_product = $_POST['fby_product'];
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        $fby_keyword = $_POST['fby_keyword'];

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND ss.state_id = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND ss.district_id = '$fby_city' "; 
        }

        if(isset($fby_product) && !empty($fby_product))
        {
           $quick_search .= " AND p.product_id = '$fby_product' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND ss.sales_date >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND ss.sales_date <= '$fby_to_date' "; 
        }

        if(isset($fby_keyword) && !empty($fby_keyword))
        {
            $_POST['search']['value'] = $fby_keyword;
        }
                           
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('ss.dealer_id', 'ss.company', 'ss.sales_date', 'ss.quantity', 'st.state_name', 'dis.district_name', 'p.product_name', 'ss.quantity');
        $sTable = "upload_sales";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
              

        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ss.*,p.product_name,st.state_name, dis.district_name
            FROM $sTable ss
            INNER JOIN states st ON st.state_id = ss.state_id
            INNER JOIN districts dis ON dis.district_id = ss.district_id
            
            INNER JOIN products p ON p.product_id = ss.product_id
            WHERE ss.file_id = '$file_id'  $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(ss.id) as count
            FROM $sTable ss
            INNER JOIN states st ON st.state_id = ss.state_id
            INNER JOIN districts dis ON dis.district_id = ss.district_id
            
            INNER JOIN products p ON p.product_id = ss.product_id
            WHERE ss.file_id = '$file_id' $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
         

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 
            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name;
            $row[] = $aRow-> company;
            $row[] = $aRow-> product_name;
            $row[] = $aRow-> dealer_id;                      
            $row[] = $aRow-> agency_name; 
            $row[] = format_date($aRow-> sales_date, DATE);            
            $row[] = $aRow-> quantity;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Farmers Management
    ----------------------------------------------------------*/
    public function farmers()
    {
        $this->validate_permission(19, 4);

        $this->load->view('includes/main_header');
        $this->load->view('farmers');
        $this->load->view('includes/main_footer');
    }

    public function farmers_add()
    {
        $this->validate_permission(19, 2);

        $edit_id = $this->uri->segment(3);
        $arr = array("id"=>$edit_id, "typ" => "farmers_temp");
        $data['details'] = $this->common_model->get_detail($arr);        
        if(!isset($data['details']) || empty($data['details']))
        {            
            $data['details'] = array();
        } 


        $farmer_reg_num = $this->common_model->get_farmer_reg_no();
        $data['farmer_reg_num'] = "F".regno($farmer_reg_num + 1, 5);

        $arr = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arr);

        $master_list = array("crops", "crop_variety");

        $data['masters'] = $this->webservice_model->get_masters($master_list);
        //p($data);
        $this->load->view('includes/main_header');
        $this->load->view('farmers_add', $data);
        $this->load->view('includes/main_footer');
    }

    public function farmers_edit()
    {   
        $this->validate_permission(19, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("farmer_id"=>$edit_id);        
        $data['details'] = $this->webservice_model->get_farmer_detail($arr);
        
        if(!isset($data['details']['basic']) || empty($data['details']['basic']))
        {            
            echo "<script> window.location.href = '".base_url()."admin/farmers'; </script>";
            die;
        }                
        
        $arr = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arr);

        $master_list = array("crops");

        $data['masters'] = $this->webservice_model->get_masters($master_list);
        
        $this->load->view('includes/main_header');
        $this->load->view('farmers_edit', $data);
        $this->load->view('includes/main_footer');
    }

    public function farmers_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = $this-> set_limit("farmers");

        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_executive = $_POST['fby_executive'];        
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        $fby_keyword = $_POST['fby_keyword'];        

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND f.farmer_state = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND f.farmer_district = '$fby_city' "; 
        }

        if(isset($fby_executive) && !empty($fby_executive))
        {
           $quick_search .= " AND f.created_by = '$fby_executive' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND f.created_date >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND f.created_date <= '$fby_to_date' "; 
        }

        if(isset($fby_keyword) && !empty($fby_keyword))
        {
            $rgns = ltrim(str_replace("F", "", $fby_keyword),"0");

            $quick_search .= " AND (f.farmer_name LIKE '$fby_keyword%' OR 
            f.farmer_mobile LIKE '$fby_keyword%' OR 
            s.state_name LIKE '$fby_keyword%' OR 
            d.district_name LIKE '$fby_keyword%' OR 
            f.farmer_reg_num LIKE '$rgns%' OR 
            f.farmer_plot_no LIKE '$fby_keyword%' OR 
            f.farmer_village LIKE '$fby_keyword%' OR 
            t.taluka_name LIKE '$fby_keyword%') "; 
        }


        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('f.farmer_name', 'f.farmer_mobile', 's.state_name', 'd.district_name', 'f.farmer_photo_url', 'f.farmer_reg_num', 'f.farmer_plot_no', 'f.farmer_village', 't.taluka_name', 'u.first_name', 'u.last_name');

        $sTable = "farmers";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {            
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                if($search_columns[$i] == "f.farmer_reg_num")
                {
                    $_POST['search']['value'] = trim(str_replace("F", "", $_POST['search']['value']));
                    $_POST['search']['value'] = ltrim($_POST['search']['value'], "0");
                }

                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
                
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS f.*, s.state_name, d.district_name, t.taluka_name, u.first_name, u.last_name 
            FROM $sTable f             
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            LEFT JOIN users u ON u.user_id = f.created_by
            WHERE f.is_deleted = 0 $quick_search $sWhere             
            $sOrder
            $sLimit ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "SELECT COUNT(f.farmer_id) as count
            FROM $sTable f             
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            LEFT JOIN users u ON u.user_id = f.created_by
            WHERE f.is_deleted = 0 $quick_search $sWhere ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = "<img src='".$aRow-> farmer_photo_url."' width='50px' height='50px'>";

            $row[] = "F".regno($aRow-> farmer_reg_num, 5);

            $row[] = ucwords($aRow-> farmer_name);

            $row[] = $aRow-> farmer_mobile;

            $row[] = $aRow-> farmer_plot_no." ".$aRow-> farmer_village;

            $row[] = $aRow-> taluka_name;

            $row[] = $aRow-> district_name;

            $row[] = $aRow-> state_name;

            $row[] = ucwords($aRow-> first_name." ".$aRow-> last_name);

            $row[] = '<a class="action_link" href="'.base_url().'admin/farmers_view/'.$aRow -> farmer_id.'"><i class="fa fa-eye"></i></a>&nbsp;<a class="action_link" href="'.base_url().'admin/farmers_edit/'.$aRow -> farmer_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("farmers",'.$aRow -> farmer_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    public function farmers_quick()
    {
        $this->validate_permission(19, 4);

        $this->load->view('includes/main_header');
        $this->load->view('farmers_quick');
        $this->load->view('includes/main_footer');
    }

    public function farmers_quick_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = $this-> set_limit("farmers");

        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_executive = $_POST['fby_executive'];        
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        $fby_keyword = $_POST['fby_keyword'];        

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND f.farmer_state = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND f.farmer_district = '$fby_city' "; 
        }

        if(isset($fby_executive) && !empty($fby_executive))
        {
           $quick_search .= " AND f.created_by = '$fby_executive' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND f.created_date >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND f.created_date <= '$fby_to_date' "; 
        }

        if(isset($fby_keyword) && !empty($fby_keyword))
        {
            $quick_search .= " AND (f.farmer_name LIKE '$fby_keyword%' OR 
            f.farmer_mobile LIKE '$fby_keyword%' OR 
            s.state_name LIKE '$fby_keyword%' OR 
            d.district_name LIKE '$fby_keyword%' OR             
            f.farmer_village LIKE '$fby_keyword%' OR 
            t.taluka_name LIKE '$fby_keyword%') "; 
        }


        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('f.farmer_name', 'f.farmer_mobile', 's.state_name', 'd.district_name', 'f.farmer_land_area', 'f.farmer_village', 't.taluka_name', 'u.first_name', 'u.last_name');

        $sTable = "farmers_temp";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {            
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                
                $_POST['search']['value'] = ltrim($_POST['search']['value'], "0");

                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
                
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS f.*, s.state_name, d.district_name, t.taluka_name, u.first_name, u.last_name 
            FROM $sTable f             
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            LEFT JOIN users u ON u.user_id = f.created_by
            WHERE f.is_deleted = 0 $quick_search $sWhere             
            $sOrder
            $sLimit ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        
        $aColumns = $search_columns = array('f.farmer_name', 'f.farmer_mobile', 's.state_name', 'd.district_name', 'f.farmer_land_area', 'f.farmer_village', 't.taluka_name');
        $sQuery = "SELECT COUNT(f.farmer_id) as count
            FROM $sTable f             
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            LEFT JOIN users u ON u.user_id = f.created_by
            WHERE f.is_deleted = 0 $quick_search $sWhere ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            

            $row[] = ucwords($aRow-> farmer_name);

            $row[] = $aRow-> farmer_mobile;

            $row[] = $aRow-> farmer_village;

            $row[] = $aRow-> taluka_name;

            $row[] = $aRow-> district_name;

            $row[] = $aRow-> state_name;

            $row[] = $aRow-> farmer_land_area;

            $row[] = ucwords($aRow-> first_name." ".$aRow-> last_name);

            $row[] = '<a title="Complete The Registration" class="action_link" href="'.base_url().'admin/farmers_add/'.$aRow -> farmer_id.'"><i class="fa fa-plus"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("farmers_temp",'.$aRow -> farmer_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    public function farmers_import()
    {
        $this->validate_permission(19, 2);

        $arr = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arr);

        $this->load->view('includes/main_header');
        $this->load->view('farmers_import', $data);
        $this->load->view('includes/main_footer');
    }


    public function farmers_view()
    {
        $this->validate_permission(19, 4);

        $id = $this->uri->segment(3);

        if(isset($id) && !empty($id) && $id > 0)
        {
            $this->load->model("Webservice_model");
            $data = $this->Webservice_model->get_farmer_detail(array("farmer_id"=>$id));
            
            //p($data);
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."farmers'; </script>"; die;
        }

        $this->load->view('includes/main_header');
        $this->load->view('farmers_view', $data);
        $this->load->view('includes/main_footer');
    }

    



    /*----------------------------------------------------------
    Demos Management
    ----------------------------------------------------------*/
    public function demos()
    {
        $this->validate_permission(20, 4);

        $this->load->view('includes/main_header');
        $this->load->view('demos');
        $this->load->view('includes/main_footer');
    }


    public function demos_view()
    {
        $this->validate_permission(20, 4);

        $id = $this->uri->segment(3);

        if(isset($id) && !empty($id) && $id > 0)
        {
            $this->load->model("Webservice_model");
            $data = $this->Webservice_model->get_demo_farmer_detail(array("farmer_id"=>$id, "farmer_type" => 2));
            //p($data);
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."demos'; </script>"; die;
        }

        $this->load->view('includes/main_header');
        $this->load->view('demos_view', $data);
        $this->load->view('includes/main_footer');
    }

    public function demos_add()
    {
        $this->validate_permission(20, 2);

        $arr = array("typ"=>"farmers");
        $data['farmers'] = $this->common_model->get_dd_list($arr);


        $farmer_reg_num = $this->common_model->get_farmer_reg_no("demos");
        $data['farmer_reg_num'] = "D".regno($farmer_reg_num + 1, 5);

        $arr = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arr);

        $master_list = array("crops", "crop_variety", "products", "soil_type", "irrigation_method", "fertilizer_used", "micro_nutrients", "weedicides");
        $data['masters'] = $this->webservice_model->get_masters($master_list);
        
        $this->load->view('includes/main_header');
        $this->load->view('demos_add', $data);
        $this->load->view('includes/main_footer');
    }

    public function demos_add2()
    {
        $this->validate_permission(20, 2);

        $arr = array("typ"=>"demos");
        $data['farmers'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arr);

        $master_list = array("crops", "crop_variety", "products", "soil_type", "irrigation_method", "fertilizer_used", "micro_nutrients", "weedicides");
        $data['masters'] = $this->webservice_model->get_masters($master_list);
        
        $this->load->view('includes/main_header');
        $this->load->view('demos_add2', $data);
        $this->load->view('includes/main_footer');
    }

    public function demos_add3()
    {
        $this->validate_permission(20, 2);

        $arr = array("typ"=>"demos");
        $data['farmers'] = $this->common_model->get_dd_list($arr);


        $arr = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arr);

        $master_list = array("crops", "crop_variety", "products", "soil_type", "irrigation_method", "fertilizer_used", "micro_nutrients", "weedicides");
        $data['masters'] = $this->webservice_model->get_masters($master_list);
        
        $this->load->view('includes/main_header');
        $this->load->view('demos_add3', $data);
        $this->load->view('includes/main_footer');
    }


    public function demos_add4()
    {
        $this->validate_permission(20, 2);

        $arr = array("typ"=>"demos");
        $data['farmers'] = $this->common_model->get_dd_list($arr);

        
        $arr = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arr);

        $master_list = array("crops", "crop_variety", "products", "soil_type", "irrigation_method", "fertilizer_used", "micro_nutrients", "weedicides");
        $data['masters'] = $this->webservice_model->get_masters($master_list);
        
        $this->load->view('includes/main_header');
        $this->load->view('demos_add4', $data);
        $this->load->view('includes/main_footer');
    }


    public function demos_edit()
    {   
        $this->validate_permission(20, 3);

        $edit_id = $this->uri->segment(3);

        $arr = array("farmer_id"=>$edit_id, "farmer_type" => 2);          
        $data['details'] = $this->webservice_model->get_demo_farmer_detail($arr);
        
        if(!isset($data['details']['basic']) || empty($data['details']['basic']))
        {            
            echo "<script> window.location.href = '".base_url()."admin/farmers'; </script>";
            die;
        }        
        
        //p($data['details']);
        $arr = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arr);

        $master_list = array("crops", "crop_variety", "products", "soil_type", "irrigation_method", "fertilizer_used", "micro_nutrients", "weedicides");

        $data['masters'] = $this->webservice_model->get_masters($master_list);
        //p($data);
        $this->load->view('includes/main_header');
        $this->load->view('demos_edit', $data);
        $this->load->view('includes/main_footer');
    }

    public function demos_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = $this-> set_limit("farmers");

        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_executive = $_POST['fby_executive'];        
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        $fby_keyword = $_POST['fby_keyword'];        
        $fby_is_approved = $_POST['fby_is_approved'];        

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND f.farmer_state = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND f.farmer_district = '$fby_city' "; 
        }

        if(isset($fby_executive) && !empty($fby_executive))
        {
           $quick_search .= " AND f.created_by = '$fby_executive' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND f.created_date >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND f.created_date <= '$fby_to_date' "; 
        }

        if(isset($fby_is_approved) && !empty($fby_is_approved) && $fby_is_approved == 1)
        {
            $quick_search .= " AND f.is_approved = '1' ";
        }
        elseif($fby_is_approved == 0)
        {
            $quick_search .= " AND f.is_approved = '0' ";
        }

        if(isset($fby_keyword) && !empty($fby_keyword))
        {
            $quick_search .= " AND (f.farmer_name LIKE '$fby_keyword%' OR 
            f.farmer_mobile LIKE '$fby_keyword%' OR 
            s.state_name LIKE '$fby_keyword%' OR 
            d.district_name LIKE '$fby_keyword%' OR 
            f.farmer_reg_num LIKE '$fby_keyword%' OR             
            f.farmer_village LIKE '$fby_keyword%' OR 
            t.taluka_name LIKE '$fby_keyword%') "; 
        }

        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('f.farmer_name', 'f.farmer_mobile', 's.state_name', 'd.district_name', 'f.farmer_photo_url', 'f.farmer_reg_num', 'f.farmer_village', 't.taluka_name', 'u.first_name', 'u.last_name');

        $sTable = "demos";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {            
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                if($search_columns[$i] == "f.farmer_reg_num")
                {
                    $_POST['search']['value'] = trim(str_replace("D", "", $_POST['search']['value']));
                    $_POST['search']['value'] = ltrim($_POST['search']['value'], "0");
                }

                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
                
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS f.*, s.state_name, d.district_name, t.taluka_name, u.first_name, u.last_name
            FROM $sTable f             
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            LEFT JOIN users u ON u.user_id = f.created_by
            WHERE f.is_deleted = 0 $quick_search $sWhere             
            $sOrder
            $sLimit ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "SELECT COUNT(f.farmer_id) as count
            FROM $sTable f             
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            LEFT JOIN users u ON u.user_id = f.created_by
            WHERE f.is_deleted = 0 $quick_search $sWhere ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = "<img src='".$aRow-> farmer_photo_url."' width='50px' height='50px'>";

            $lbl = "success";
            if($aRow-> is_approved == 0) $lbl = "danger";
            $row[] = "<label class='badge label-$lbl'>D".regno($aRow-> farmer_reg_num, 5)."</label>";

            $row[] = ucwords($aRow-> farmer_name);

            $row[] = $aRow-> farmer_mobile;

            $row[] = $aRow-> farmer_village;

            $row[] = $aRow-> taluka_name;

            $row[] = $aRow-> district_name;

            $row[] = $aRow-> state_name;

            $aRow-> approved_reason = htmlentities($aRow-> approved_reason);

            $row[] = ucwords($aRow-> first_name." ".$aRow-> last_name);

            $defbtn = "";
            if($aRow->is_approved == 0)
            {
                $defbtn = '<a class="action_link" href="javascript:void(0);" name="'.$aRow-> approved_reason.'" title="Approve Demo" onclick="show_popup('.$aRow -> farmer_id.',this);"><label class="badge bg-blue bg-primary"><i class="fa fa-plus"></i>&nbsp;Approve</a>';
            }
            else
            {
                $defbtn = '&nbsp;<a class="action_link" href="javascript:void(0);" name="'.$aRow-> approved_reason.'" title="Edit Approved Demo" onclick="show_popup('.$aRow -> farmer_id.',this);"><label class="badge bg-green bg-success"><i class="fa fa-edit"></i>&nbsp;Approved</label></a>';
            }
            
            $row[] = '<a class="action_link" href="'.base_url().'admin/demos_view/'.$aRow -> farmer_id.'"><i class="fa fa-eye"></i></a>&nbsp;<a class="action_link" href="'.base_url().'admin/demos_edit/'.$aRow -> farmer_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("demos",'.$aRow -> farmer_id.');><i class="fa fa-trash"></i></a>'.$defbtn;/*
            $row[] = '<a class="action_link" href="'.base_url().'admin/demos_view/'.$aRow -> farmer_id.'"><i class="fa fa-eye"></i></a>'.$defbtn;*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }





    public function inventory_retailer_pos()
    {
        $this->validate_permission(7, 4);

        $data['details'] = $this->admin_model->last_upload_date('upload_retailer_pos');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $this->load->view('includes/main_header');
        $this->load->view('inventory_retailer_pos', $data);
        $this->load->view('includes/main_footer');
    }


    public function inventory_retailer_pos_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = $this-> set_limit("salers");

        $file_id = $_POST['file_id']; 
        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_product = $_POST['fby_product'];
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        $fby_keyword = $_POST['fby_keyword'];

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND ss.state_id = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND ss.district_id = '$fby_city' "; 
        }

        if(isset($fby_product) && !empty($fby_product))
        {
           $quick_search .= " AND p.product_id = '$fby_product' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND s.invoice_date >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND s.invoice_date <= '$fby_to_date' "; 
        }

        if(isset($fby_keyword) && !empty($fby_keyword))
        {
            $_POST['search']['value'] = $fby_keyword;
            //$quick_search .= " AND (saler_sys_id LIKE '$fby_keyword%' OR ss.mobile LIKE '$fby_keyword%' OR quantity LIKE '$fby_keyword%' OR dd_no LIKE '$fby_keyword%' OR ss.first_name LIKE '$fby_keyword%' OR ss.last_name LIKE '$fby_keyword%' OR ss.state_name LIKE '$fby_keyword%' OR ss.district_name LIKE '$fby_keyword%') ";
        }

        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.invoice_date','s.retailer_id', 'ss.first_name', 'ss.last_name', 'ss.state_name', 'ss.district_name', 'p.product_name', 's.quantity_mt');
        $sTable = "upload_retailer_pos";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
                
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,ss.state_name, ss.district_name
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 

            $row[] = format_date($aRow-> invoice_date,DATE);
            $row[] = $aRow-> retailer_id;            
            $row[] = $aRow-> first_name." ".$aRow-> last_name; 
            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name; 
            $row[] = $aRow-> product_name;            
            $row[] = $aRow-> quantity_mt;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    public function profile()
    {        
        if($this->user_role_id == 1 && $this->user_id == 1)
        {
            $arr = array("typ"=>'states_aop');        
            $data['states_all'] = $this->common_model->get_dd_list($arr);

            $arr = array("typ" => "users", "id" => $this->user_id);
            $data['details'] = $this->common_model->get_detail($arr);

            if(isset($data['details']->company_details) && !empty($data['details']->company_details))
            {
                $data['details']->company_details = json_decode($data['details']->company_details);
            }
            else
            {
                $data['details']->company_details = array();
            }

            $data['details']->company_details = (array) $data['details']->company_details;
            
            error_reporting(0);
            $this->load->view('includes/main_header');
            $this->load->view('profile', $data);
            $this->load->view('includes/main_footer');
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."'; </script>"; die;
        }    
    }


    public function profile_user()
    {        
        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ" => "users", "id" => $this->user_id);
        $data['details'] = $this->common_model->get_detail($arr);
        
        $this->load->view('includes/main_header');
        $this->load->view('profile_user', $data);
        $this->load->view('includes/main_footer');
            
    }

    public function profile_save()
    {
        extract($_POST);

        if((!isset($_FILES['logo']['name']) || empty($_FILES['logo']['name'])) && $photo_url == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Logo."));
            die; 
        }
        elseif($company_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Company Name."));
            die;    
        }        
        elseif($cstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select State."));
            die;    
        }
        elseif($ccity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select City."));
            die;    
        }
        elseif($caddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Address."));
            die;    
        }        
        elseif($email == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
            die;    
        }
        elseif($mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Mobile Number."));
            die;    
        }
        elseif(strlen($mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }

        

        $logo = $this->input->post('logo');        
        if(isset($_FILES['logo']['name']) && !empty($_FILES['logo']['name']))
        {
            $logo = $_FILES['logo'];            
            if(isset($logo) && !empty($logo))
            {                
                $ext = pathinfo($logo['name'], PATHINFO_EXTENSION);                
                
                $config['file_name'] = "logo.png";
                $config['upload_path'] = "assets/images";
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['overwrite'] = TRUE;
                
                $this->load->library('upload');
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('logo')) 
                {
                    $error = $this->upload->display_errors(); 

                    echo json_encode(array("status"=>0,"message"=>$error)); 
                    die;       
                }
                else
                {
                    $dataup = $this->upload->data();                    
                    $unm = $dataup['file_name'];

                    $photo_url = "assets/images/".$unm;
                }
            }
        }


        $data = array( 
            "photo_url" => $photo_url, 
            "company_details" => json_encode($_POST),
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id
        );

        $this->dbaccess_model->update("users", $data, array("user_id" => $this->user_id));
        
        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        
    }



    public function profile_user_save()
    {        
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
     
        $address = $this->input->post('address');
        $gender = $this->input->post('gender');
        $dob = $this->input->post('dob');
        $qualification = $this->input->post('qualification');     
        $experience = $this->input->post('experience');

        $cstates_name = $this->input->post('cstates_name');
        $ccity_name = $this->input->post('ccity_name');
        $caddress = $this->input->post('caddress');
        $caddress2 = $this->input->post('caddress2');

        $pstates_name = $this->input->post('pstates_name');
        $pcity_name = $this->input->post('pcity_name');
        $paddress = $this->input->post('paddress');
        $paddress2 = $this->input->post('paddress2');

        $user_photo_url = $this->input->post('user_photo_url');
        $hdn_id = $this->input->post('hdn_id');

        if($first_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter First Name."));
            die;    
        }
        elseif($last_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Last Name."));
            die;    
        }        
        elseif($email == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
            die;    
        }
        elseif($mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Mobile Number."));
            die;    
        }
        elseif(strlen($mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        elseif($cstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address State Name."));
            die;
        }
        elseif($ccity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address City Name."));
            die;
        }
        elseif($caddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Current Address Details."));
            die;
        }
        elseif($pstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address State Name."));
            die;
        }
        elseif($pcity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address City Name."));
            die;
        }
        elseif($paddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Permanent Address Details."));
            die;
        } 

        
        if(isset($_FILES['user_photo']['name']) && !empty($_FILES['user_photo']['name']))
        {
            $logo = $_FILES['user_photo'];            
            if(isset($logo) && !empty($logo))
            {                
                $ext = pathinfo($logo['name'], PATHINFO_EXTENSION);                
                
                $config['file_name'] = $this->user_id."_user_photo.png";
                $config['upload_path'] = "assets/images";
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['overwrite'] = TRUE;
                
                $this->load->library('upload');
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('user_photo')) 
                {
                    $error = $this->upload->display_errors(); 

                    echo json_encode(array("status"=>0,"message"=>$error)); 
                    die;       
                }
                else
                {
                    $dataup = $this->upload->data();                    
                    $unm = $dataup['file_name'];

                    $user_photo_url = "assets/images/".$unm;
                }
            }
        }       

        if(true)
        {            
            $data = array("user_photo_url" => $user_photo_url,
                "first_name" => $first_name, 
                "last_name" => $last_name, 
                "email" => $email, 
                "mobile" => $mobile, 
                
                "cstate" => $cstates_name,
                "ccity" => $ccity_name,
                "caddress" => $caddress,
                "caddress2" => $caddress2,

                "pstate" => $pstates_name,
                "pcity" => $pcity_name,
                "paddress" => $paddress,
                "paddress2" => $paddress2,

                "gender" => $gender, 
                "dob" => date(DATE_DB, strtotime($dob)), 
                "qualification" => $qualification, 
                
                "experience" => $experience,                

                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

                            
            $this->dbaccess_model->update("users", $data, array("user_id" => $this->user_id));
            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }




    public function dispatch_report()
    {
        $this->validate_permission(10, 4);

        $data['details'] = $this->admin_model->last_upload_date('upload_dispatch');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $this->load->view('includes/main_header');
        $this->load->view('dispatch_report', $data);
        $this->load->view('includes/main_footer');
    }


    public function dispatch_report_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = $this-> set_limit("dispatch_report");

        $file_id = $_POST['file_id'];
        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_product = $_POST['fby_product'];
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        $fby_keyword = $_POST['fby_keyword'];

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND ss.state_id = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND d.district_id = '$fby_city' "; 
        }

        if(isset($fby_product) && !empty($fby_product))
        {
           $quick_search .= " AND p.product_id = '$fby_product' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND s.dispatch_date >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND s.dispatch_date <= '$fby_to_date' "; 
        }

        if(isset($fby_keyword) && !empty($fby_keyword))
        {
            $_POST['search']['value'] = $fby_keyword;
        } 
                           
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.dispatch_date_text', 's.sold_to_party', 's.ship_to_id', 's.ship_to_party','s.place', 's.district_name', 's.quantity', 'p.product_code');
        $sTable = "upload_dispatch";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
              
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS DISTINCT s.id as sid, s.*,p.product_code
            FROM $sTable s            
            INNER JOIN products p ON p.product_id = s.product_id
            INNER JOIN states ss ON ss.state_name = s.state
            INNER JOIN districts d ON d.district_name = s.district_name
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            GROUP BY s.id
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(DISTINCT s.id) as count
            FROM $sTable s            
            INNER JOIN products p ON p.product_id = s.product_id
            INNER JOIN states ss ON ss.state_name = s.state
            INNER JOIN districts d ON d.district_name = s.district_name
            WHERE s.file_id = '$file_id' $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
         

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 
            $row[] = $aRow-> dispatch_date_text;
            $row[] = $aRow-> sold_to_party;
            $row[] = $aRow-> ship_to_id;
            $row[] = $aRow-> ship_to_party;
            $row[] = $aRow-> place;                      
            $row[] = $aRow-> district_name; 
            $row[] = $aRow-> product_code;            
            $row[] = $aRow-> quantity;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Album Management
    ----------------------------------------------------------*/
    public function album_farmer()
    {
        $this->validate_permission(38, 4);

        $arr = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arr);

        $this->load->view('includes/main_header');
        $this->load->view('album_farmer', $data);
        $this->load->view('includes/main_footer');
    }

    public function album_farmer_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_executive = $_POST['fby_executive'];
        

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND s.farmer_state = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND s.farmer_district = '$fby_city' "; 
        }

        if(isset($fby_executive) && !empty($fby_executive))
        {
           $quick_search .= " AND u.user_id = '$fby_executive' "; 
        }
                           
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.farmer_id', 's.farmer_name', 's.farmer_mobile', 'u.first_name', 'u.last_name', 'u.mobile', 's.farmer_reg_num');
        $sTable = "farmers";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
              
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.farmer_id, s.farmer_name, s.farmer_mobile, u.first_name, u.last_name, u.mobile
            FROM $sTable s            
            INNER JOIN users u ON u.user_id = s.created_by
            INNER JOIN farmers_plot_photo fp ON fp.farmer_id = s.farmer_id AND fp.farmer_type = 1 AND fp.demo_type = 1           
            WHERE fp.farmer_type = 1 AND fp.demo_type = 1 $quick_search $sWhere
            GROUP BY s.farmer_id            
            $sOrder            
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.farmer_id) as count
            FROM $sTable s            
            INNER JOIN users u ON u.user_id = s.created_by
            INNER JOIN farmers_plot_photo fp ON fp.farmer_id = s.farmer_id AND fp.farmer_type = 1 AND fp.demo_type = 1           
            WHERE fp.farmer_type = 1 AND fp.demo_type = 1 $quick_search $sWhere
            GROUP BY s.farmer_id
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        if(isset($rResultTotal) && !empty($rResultTotal))
        $iTotal = $rResultTotal[0]->count;
        else
        $iTotal = 0;    
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
         

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 
            
            $row[] = $aRow-> farmer_name."<br/><small>".$aRow-> farmer_mobile."</small>";
            
            $row[] = $aRow-> first_name." ".$aRow-> last_name."<br/><small>".$aRow-> mobile."</small>";
            
            $row[] = count_plot_images($aRow-> farmer_id, 1, 2, 1);            

            $row[] = '<a class="action_link" onclick="load_photo_gallery('.$aRow-> farmer_id.',1,2);"><i class="fa fa-eye"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    public function album_demo()
    {
        $this->validate_permission(38, 4);

        $arr = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arr);

        $this->load->view('includes/main_header');
        $this->load->view('album_demo', $data);
        $this->load->view('includes/main_footer');
    }

    public function album_demo_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        $fby_state = $_POST['fby_state'];
        $fby_city = $_POST['fby_city'];
        $fby_executive = $_POST['fby_executive'];
        $fby_stage = $_POST['fby_stage'];
        

        if(isset($fby_state) && !empty($fby_state))
        {
           $quick_search .= " AND s.farmer_state = '$fby_state' "; 
        }

        if(isset($fby_city) && !empty($fby_city))
        {
           $quick_search .= " AND s.farmer_district = '$fby_city' "; 
        }

        if(isset($fby_executive) && !empty($fby_executive))
        {
           $quick_search .= " AND u.user_id = '$fby_executive' "; 
        }

        if(isset($fby_stage) && !empty($fby_stage))
        {
           $quick_search .= " AND fp.for_stage = '$fby_stage' "; 
        }
                           
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.farmer_id', 's.farmer_name', 's.farmer_mobile', 'u.first_name', 'u.last_name', 'u.mobile', 's.farmer_reg_num');
        $sTable = "demos";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
              
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ANY_VALUE(s.farmer_id) as farmer_id, ANY_VALUE(fp.farmer_type) as farmer_type, ANY_VALUE(fp.for_stage) as for_stage, ANY_VALUE(s.farmer_name) as farmer_name, ANY_VALUE(s.farmer_mobile) as farmer_mobile, ANY_VALUE(u.first_name) as first_name, ANY_VALUE(u.last_name) as last_name, ANY_VALUE(u.mobile) as mobile, ANY_VALUE(fp.demo_type) as demo_type 
            FROM $sTable s            
            INNER JOIN users u ON u.user_id = s.created_by
            INNER JOIN farmers_plot_photo fp ON fp.farmer_id = s.farmer_id AND fp.farmer_type = 2            
            WHERE fp.farmer_type = 2 $quick_search $sWhere
            GROUP BY s.farmer_id, fp.farmer_type, fp.for_stage            
            $sOrder            
            $sLimit
        ";

        /*$sQuery = "
            SELECT SQL_CALC_FOUND_ROWS (s.farmer_id) as farmer_id, (fp.farmer_type) as farmer_type, (fp.for_stage) as for_stage, (s.farmer_name) as farmer_name, (s.farmer_mobile) as farmer_mobile, (u.first_name) as first_name, (u.last_name) as last_name, (u.mobile) as mobile, (fp.demo_type) as demo_type 
            FROM $sTable s            
            INNER JOIN users u ON u.user_id = s.created_by
            INNER JOIN farmers_plot_photo fp ON fp.farmer_id = s.farmer_id AND fp.farmer_type = 2            
            WHERE fp.farmer_type = 2 $quick_search $sWhere
            GROUP BY s.farmer_id, fp.farmer_type, fp.for_stage            
            $sOrder            
            $sLimit
        ";*/
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.farmer_id) as count
            FROM $sTable s            
            INNER JOIN users u ON u.user_id = s.created_by
            INNER JOIN farmers_plot_photo fp ON fp.farmer_id = s.farmer_id AND fp.farmer_type = 2            
            WHERE fp.farmer_type = 2 $quick_search $sWhere
            GROUP BY s.farmer_id, fp.farmer_type, fp.for_stage
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        if(isset($rResultTotal) && !empty($rResultTotal))
        $iTotal = $rResultTotal[0]->count;
        else
        $iTotal = 0;    
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 
            
            $row[] = $aRow-> farmer_name."<br/><small>".$aRow-> farmer_mobile."</small>";
            
            $row[] = $aRow-> first_name." ".$aRow-> last_name."<br/><small>".$aRow-> mobile."</small>";
            
            $row[] = $aRow-> for_stage;

            $row[] = count_plot_images($aRow-> farmer_id, 2, $aRow-> for_stage, 1);

            $row[] = count_plot_images($aRow-> farmer_id, 2, $aRow-> for_stage, 2);            

            
            $row[] = '<a class="action_link" onclick="load_photo_gallery('.$aRow-> farmer_id.',2,'.$aRow-> for_stage.');"><i class="fa fa-eye"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    public function album_stock()
    {        
        $this->validate_permission(38, 4);

        $data['wholesalers'] = $this->common_model->get_dd_list(array("typ"=>"wholesalers"));
        $data['retailers'] = $this->common_model->get_dd_list(array("typ"=>"retailers"));
        $data['products'] = $this->common_model->get_dd_list(array("typ"=>"products"));

        $this->load->view('includes/main_header');
        $this->load->view('album_stock', $data);
        $this->load->view('includes/main_footer');
    }

    public function album_stock_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        $fby_saler = $_POST['fby_saler'];
        $fby_product = $_POST['fby_product'];
        $fby_from_date = $_POST['fby_from_date'];
        $fby_to_date = $_POST['fby_to_date'];
        

        if(isset($fby_saler) && !empty($fby_saler))
        {
           $quick_search .= " AND ss.saler_id = '$fby_saler' "; 
        }

        if(isset($fby_product) && !empty($fby_product))
        {
           $quick_search .= " AND s.product_id = '$fby_product' "; 
        }

        if(isset($fby_from_date) && !empty($fby_from_date))
        {
           $fby_from_date = format_date($fby_from_date, DATE_DB);
           $quick_search .= " AND date(s.created_date) >= '$fby_from_date' "; 
        }

        if(isset($fby_to_date) && !empty($fby_to_date))
        {
           $fby_to_date = format_date($fby_to_date, DATE_DB);
           $quick_search .= " AND date(s.created_date) <= '$fby_to_date' "; 
        }
                           
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('ss.saler_id', 'ss.first_name', 'ss.last_name', 'p.product_name', 's.stock_current_quantity', 's.stock_verify_quantity');
        $sTable = "stock_verification";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
              
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.id, ss.saler_id, ss.first_name, ss.last_name, p.product_name, s.stock_photo_url, s.created_date, s.stock_current_quantity, s.stock_verify_quantity
            FROM $sTable s            
            INNER JOIN salers ss ON ss.saler_id = s.stock_saler_id
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.is_deleted = 0 $quick_search $sWhere            
            $sOrder            
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s            
            INNER JOIN salers ss ON ss.saler_id = s.stock_saler_id
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        if(isset($rResultTotal) && !empty($rResultTotal))
        $iTotal = $rResultTotal[0]->count;
        else
        $iTotal = 0;    
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
         
        
        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 
            
            $row[] = $aRow-> first_name." ".$aRow-> last_name;
            
            $row[] = $aRow-> product_name."<br/><label class='bg-danger btn-xs'><i class='fa fa-cubes'></i>".$aRow-> stock_current_quantity."</label>&nbsp;<label class='bg-success btn-xs'><i class='fa fa-check-square-o'></i>".$aRow-> stock_verify_quantity."</label>";            
            
            $row[] = format_date($aRow-> created_date, DATE);

            $count = 0;
            if(isset($aRow-> stock_photo_url) && !empty($aRow-> stock_photo_url))
                $count = count(json_decode($aRow-> stock_photo_url));

            $row[] = $count; 

            $row[] = '<a class="action_link" onclick="load_photo_gallery_stock('.$aRow-> id.');"><i class="fa fa-eye"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }

    

    public function load_photo_gallery()
    {        
        $farmer_id = $this->input->post('farmer_id');
        $farmer_type = $this->input->post('farmer_type');
        $for_stage = $this->input->post('for_stage');

        if($farmer_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select farmer."));
            die;    
        }
                            

        $data = $this->common_model->farmer_plot_photo(array("farmer_id" => $farmer_id, "farmer_type" => $farmer_type, "for_stage" => $for_stage));
        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>1,"message"=>"", "data"=>$data)); die;            
        }
        else
        {
            echo json_encode(array("status"=>0,"message"=>"No image found."));
            die;
        }        
    }


    public function load_photo_gallery_stock()
    {        
        $id = $this->input->post('id');       

        if($id == "" || $id <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select wholesaler/retailer type."));
            die;    
        }                            

        $data = $this->common_model->load_photo_gallery_stock(array("id" => $id));

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>1,"message"=>"", "data"=>$data)); 
            die;            
        }
        else
        {
            echo json_encode(array("status"=>0,"message"=>"No image found."));
            die;
        }        
    }



    public function reports()
    {
        $this->validate_permission(21, 4);

        $this->load->view('includes/main_header');
        $this->load->view('reports');
        $this->load->view('includes/main_footer');
    }


    /*----------------------------------------------------------
    Export Management
    ----------------------------------------------------------*/
    public function export_data()
    {
        $user_id = $this->user_id;        
        $export_type = $this->uri->segment(3);
        $report_type = $this->uri->segment(4);
        $file_id = $this->uri->segment(5); 
        
        $fby_state = $this->uri->segment(6);
        $fby_city = $this->uri->segment(7);
        $fby_product = $this->uri->segment(8);
        $fby_from_date = $this->uri->segment(9);
        $fby_to_date = $this->uri->segment(10);        
        $fby_keyword = urldecode($this->uri->segment(11));
        $fby_executive = urldecode($this->uri->segment(12));

        $header = $content = array();
        $quick_search = "";
        
        if($report_type == "upload_acknowledgement1")
        {
            if(isset($fby_state) && !empty($fby_state))
            {
               $quick_search .= " AND ss.state_id = '$fby_state' "; 
            }

            if(isset($fby_city) && !empty($fby_city))
            {
               $quick_search .= " AND ss.district_id = '$fby_city' "; 
            }

            if(isset($fby_product) && !empty($fby_product))
            {
               $quick_search .= " AND p.product_id = '$fby_product' "; 
            }

            if(isset($fby_from_date) && !empty($fby_from_date))
            {
               $fby_from_date = format_date($fby_from_date, DATE_DB);
               $quick_search .= " AND s.invoice_date >= '$fby_from_date' "; 
            }

            if(isset($fby_to_date) && !empty($fby_to_date))
            {
               $fby_to_date = format_date($fby_to_date, DATE_DB);
               $quick_search .= " AND s.invoice_date <= '$fby_to_date' "; 
            }

            if(isset($fby_keyword) && !empty($fby_keyword))
            {
                $quick_search .= " AND (saler_sys_id LIKE '$fby_keyword%' OR ss.mobile LIKE '$fby_keyword%' OR quantity LIKE '$fby_keyword%' OR dd_no LIKE '$fby_keyword%' OR ss.first_name LIKE '$fby_keyword%' OR ss.last_name LIKE '$fby_keyword%' OR ss.state_name LIKE '$fby_keyword%' OR ss.district_name LIKE '$fby_keyword%') "; 
            }

            $sql = "
            SELECT s.invoice_date, ss.saler_sys_id,CONCAT(ss.first_name,' ',ss.last_name) as wsname, ss.mobile,ss.state_name, ss.district_name, p.product_name, CONCAT(s.quantity,' ', s.unit) as quantity, s.dd_no
            FROM upload_acknowledgement1 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search
            ";


            $header = array('invoice_date'=>"Invoice Date", 
                'saler_sys_id'=>"WS ID",
                'wsname' => "Name",                 
                'mobile' => "Mobile", 
                'state_name' => "State", 
                'district_name' => "District",
                'product_name' => "Product", 
                'quantity' => "Quantity",                
                'dd_no' => "DD No.");            

            $result = $this->dbaccess_model->executeSql($sql);
            
            if(isset($result) && !empty($result))
            {
                foreach($result as $obj)
                {
                    $content[] = (array)$obj;
                }                
            }

            $rname = "Acknowledgement 1";
        }
        elseif($report_type == "upload_acknowledgement2")
        {
            if(isset($fby_state) && !empty($fby_state))
            {
               $quick_search .= " AND (ss.state_id = '$fby_state' OR w.state_id = '$fby_state' ) "; 
            }

            if(isset($fby_city) && !empty($fby_city))
            {
               $quick_search .= " AND (ss.district_id = '$fby_city' OR w.district_id = '$fby_city' ) "; 
            }

            if(isset($fby_product) && !empty($fby_product))
            {
               $quick_search .= " AND p.product_id = '$fby_product' "; 
            }

            if(isset($fby_from_date) && !empty($fby_from_date))
            {
               $fby_from_date = format_date($fby_from_date, DATE_DB);
               $quick_search .= " AND s.invoice_date >= '$fby_from_date' "; 
            }

            if(isset($fby_to_date) && !empty($fby_to_date))
            {
               $fby_to_date = format_date($fby_to_date, DATE_DB);
               $quick_search .= " AND s.invoice_date <= '$fby_to_date' "; 
            }

            if(isset($fby_keyword) && !empty($fby_keyword))
            {
                $_POST['search']['value'] = $fby_keyword;
            }

            if(isset($fby_keyword) && !empty($fby_keyword))
            {
                $quick_search .= " AND (w.saler_sys_id LIKE '$fby_keyword%' OR ss.mobile LIKE '$fby_keyword%' OR s.quantity LIKE '$fby_keyword%' OR dispatch_no LIKE '$fby_keyword%' OR ss.first_name LIKE '$fby_keyword%' OR ss.last_name LIKE '$fby_keyword%' OR ss.state_name LIKE '$fby_keyword%' OR ss.district_name LIKE '$fby_keyword%'  OR w.first_name LIKE '$fby_keyword%' OR w.last_name LIKE '$fby_keyword%' OR w.state_name LIKE '$fby_keyword%' OR w.district_name LIKE '$fby_keyword%' OR ss.saler_sys_id LIKE '$fby_keyword%') "; 
            }

            
            $sql = "
            SELECT s.invoice_date, w.saler_sys_id as wsaler_sys_id,CONCAT(w.first_name,' ',w.last_name) as wsname, w.state_name as wstate_name, w.district_name as wdistrict_name, ss.saler_sys_id,CONCAT(ss.first_name,' ',ss.last_name) as ssname, ss.mobile,ss.state_name, ss.district_name, p.product_name, CONCAT(s.quantity,' ', s.unit) as quantity, s.dispatch_no
            FROM upload_acknowledgement2 s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search
            ";


            $header = array('invoice_date'=>"Invoice Date", 
                'wsaler_sys_id'=>"WS ID",
                'wsname' => "WS Name",                 
                'wstate_name' => "State", 
                'wdistrict_name' => "District",

                'saler_sys_id'=>"Reatiler ID",
                'ssname' => "Name",
                'mobile' => "Mobile",
                'state_name' => "State", 
                'district_name' => "District",

                'product_name' => "Product", 
                'quantity' => "Quantity",                
                'dispatch_no' => "Dispatch No.");            

            $result = $this->dbaccess_model->executeSql($sql);
            
            if(isset($result) && !empty($result))
            {
                foreach($result as $obj)
                {
                    $content[] = (array)$obj;
                }                
            }

            $rname = "Acknowledgement 2";
        }
        elseif($report_type == "upload_retailer")
        {
            if(isset($fby_state) && !empty($fby_state))
            {
               $quick_search .= " AND ss.state_id = '$fby_state' "; 
            }

            if(isset($fby_city) && !empty($fby_city))
            {
               $quick_search .= " AND ss.district_id = '$fby_city' "; 
            }

            if(isset($fby_product) && !empty($fby_product))
            {
               $quick_search .= " AND p.product_id = '$fby_product' "; 
            }

            if(isset($fby_from_date) && !empty($fby_from_date))
            {
               $fby_from_date = format_date($fby_from_date, DATE_DB);
               $quick_search .= " AND s.created_date >= '$fby_from_date' "; 
            }

            if(isset($fby_to_date) && !empty($fby_to_date))
            {
               $fby_to_date = format_date($fby_to_date, DATE_DB);
               $quick_search .= " AND s.created_date <= '$fby_to_date' "; 
            }

            if(isset($fby_keyword) && !empty($fby_keyword))
            {
                $quick_search .= " AND (saler_sys_id LIKE '$fby_keyword%' OR ss.mobile LIKE '$fby_keyword%' OR quantity LIKE '$fby_keyword%'  OR ss.first_name LIKE '$fby_keyword%' OR ss.last_name LIKE '$fby_keyword%' OR ss.state_name LIKE '$fby_keyword%' OR ss.district_name LIKE '$fby_keyword%' OR product_name LIKE '$fby_keyword%') "; 
            }            

            $sql = "
            SELECT ss.state_name, ss.district_name, ss.saler_sys_id,CONCAT(ss.first_name,' ',ss.last_name) as name, ss.mobile, p.product_name, s.sold_quantity as quantity
            FROM upload_retailer s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search
            ";


            $header = array('state_name'=>"State", 
                'district_name'=>"District",
                'saler_sys_id' => "Reatiler ID",                 
                'name' => "Name", 
                'mobile' => "Mobile", 
                'product_name' => "Product",                
                'quantity' => "Quantity");            

            $result = $this->dbaccess_model->executeSql($sql);
            
            if(isset($result) && !empty($result))
            {
                foreach($result as $obj)
                {
                    $content[] = (array)$obj;
                }                
            }

            $rname = "Retailer Stock";
        }
        elseif($report_type == "upload_retailer_pos")
        {
            if(isset($fby_state) && !empty($fby_state))
            {
               $quick_search .= " AND ss.state_id = '$fby_state' "; 
            }

            if(isset($fby_city) && !empty($fby_city))
            {
               $quick_search .= " AND ss.district_id = '$fby_city' "; 
            }

            if(isset($fby_product) && !empty($fby_product))
            {
               $quick_search .= " AND p.product_id = '$fby_product' "; 
            }

            if(isset($fby_from_date) && !empty($fby_from_date))
            {
               $fby_from_date = format_date($fby_from_date, DATE_DB);
               $quick_search .= " AND s.invoice_date >= '$fby_from_date' "; 
            }

            if(isset($fby_to_date) && !empty($fby_to_date))
            {
               $fby_to_date = format_date($fby_to_date, DATE_DB);
               $quick_search .= " AND s.invoice_date <= '$fby_to_date' "; 
            }

            if(isset($fby_keyword) && !empty($fby_keyword))
            {
                $quick_search .= " AND (saler_sys_id LIKE '$fby_keyword%' OR quantity LIKE '$fby_keyword%' OR ss.first_name LIKE '$fby_keyword%' OR ss.last_name LIKE '$fby_keyword%' OR ss.state_name LIKE '$fby_keyword%' OR ss.district_name LIKE '$fby_keyword%' OR product_name LIKE '$fby_keyword%') "; 
            }

            $sql = "
            SELECT s.invoice_date, ss.saler_sys_id,CONCAT(ss.first_name,' ',ss.last_name) as name, ss.state_name, ss.district_name, p.product_name, s.quantity
            FROM upload_retailer_pos s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search
            ";


            $header = array('invoice_date'=>"Invoice Date", 
                'saler_sys_id'=>"Reatiler ID",
                'rname' => "Name",                                  
                'state_name' => "State", 
                'district_name' => "District",
                'product_name' => "Product", 
                'quantity' => "Quantity" );            

            $result = $this->dbaccess_model->executeSql($sql);
            
            if(isset($result) && !empty($result))
            {
                foreach($result as $obj)
                {
                    $content[] = (array)$obj;
                }                
            }

            $rname = "Retailer POS Sale";
        }
        elseif($report_type == "upload_sales")
        {
            if(isset($fby_state) && !empty($fby_state))
            {
               $quick_search .= " AND s.state_id = '$fby_state' "; 
            }

            if(isset($fby_city) && !empty($fby_city))
            {
               $quick_search .= " AND s.district_id = '$fby_city' "; 
            }

            if(isset($fby_product) && !empty($fby_product))
            {
               $quick_search .= " AND p.product_id = '$fby_product' "; 
            }

            if(isset($fby_from_date) && !empty($fby_from_date))
            {
               $fby_from_date = format_date($fby_from_date, DATE_DB);
               $quick_search .= " AND s.sales_date >= '$fby_from_date' "; 
            }

            if(isset($fby_to_date) && !empty($fby_to_date))
            {
               $fby_to_date = format_date($fby_to_date, DATE_DB);
               $quick_search .= " AND s.sales_date <= '$fby_to_date' "; 
            }

            if(isset($fby_keyword) && !empty($fby_keyword))
            {
                $quick_search .= " AND (saler_sys_id LIKE '$fby_keyword%' OR ss.mobile LIKE '$fby_keyword%' OR quantity LIKE '$fby_keyword%' OR s.agency_name LIKE '$fby_keyword%' OR st.state_name LIKE '$fby_keyword%' OR dis.district_name LIKE '$fby_keyword%') "; 
            }

            $sql = "
            SELECT st.state_name, dis.district_name, s.company, p.product_name, s.dealer_id,s.agency_name as wsname, s.sales_date,s.quantity
            FROM upload_sales s
            INNER JOIN states st ON st.state_id = s.state_id
            INNER JOIN districts dis ON dis.district_id = s.district_id            
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search
            ";


            $header = array('state_name' => "State", 
                'district_name' => "District",
                'company'=>"Company", 
                'product_name'=>"Product",
                'saler_sys_id'=>"ID",
                'wsname' => "Name",                 
                'sales_date' => "Sales Date", 
                'quantity' => "Quantity");            

            $result = $this->dbaccess_model->executeSql($sql);
            
            if(isset($result) && !empty($result))
            {
                foreach($result as $obj)
                {
                    $content[] = (array)$obj;
                }                
            }

            $rname = "Sales Report";
        }
        elseif($report_type == "upload_dispatch")
        {
            if(isset($fby_state) && !empty($fby_state))
            {
               $quick_search .= " AND ss.state_id = '$fby_state' "; 
            }

            if(isset($fby_city) && !empty($fby_city))
            {
               $quick_search .= " AND d.district_id = '$fby_city' "; 
            }

            if(isset($fby_product) && !empty($fby_product))
            {
               $quick_search .= " AND p.product_id = '$fby_product' "; 
            }

            if(isset($fby_from_date) && !empty($fby_from_date))
            {
               $fby_from_date = format_date($fby_from_date, DATE_DB);
               $quick_search .= " AND s.dispatch_date >= '$fby_from_date' "; 
            }

            if(isset($fby_to_date) && !empty($fby_to_date))
            {
               $fby_to_date = format_date($fby_to_date, DATE_DB);
               $quick_search .= " AND s.dispatch_date <= '$fby_to_date' "; 
            }

            if(isset($fby_keyword) && !empty($fby_keyword))
            {
                $quick_search .= " AND (s.dispatch_date_text LIKE '$fby_keyword%' OR s.sold_to_party LIKE '$fby_keyword%' OR s.ship_to_id LIKE '$fby_keyword%' OR s.place LIKE '$fby_keyword%' OR s.district_name LIKE '$fby_keyword%' OR s.quantity LIKE '$fby_keyword%' OR p.product_code LIKE '$fby_keyword%') "; 
            }
            

            $sql = "
            SELECT s.dispatch_date_text, s.sold_to_party,s.ship_to_id, s.ship_to_party, s.place, s.district_name, p.product_code, s.quantity            
            FROM upload_dispatch s
            INNER JOIN products p ON p.product_id = s.product_id
            INNER JOIN states ss ON ss.state_name = s.state
            INNER JOIN districts d ON d.district_name = s.district_name
            WHERE s.file_id = '$file_id' $quick_search
            ";


            $header = array('dispatch_date_text'=>"Dispatch Date", 
                'sold_to_party'=>"Sold to Party",
                'ship_to_id' => "Location",                 
                'ship_to_party' => "Ship to Party",                 
                'place' => "Place", 
                'district_name' => "District", 
                'product_code' => "Product",
                'quantity' => "Quantity");            

            $result = $this->dbaccess_model->executeSql($sql);
            
            if(isset($result) && !empty($result))
            {
                foreach($result as $obj)
                {
                    $content[] = (array)$obj;
                }                
            }

            $rname = "Dispatch Report";
        }
        elseif($report_type == "upload_wholesaler")
        {
            if(isset($fby_state) && !empty($fby_state))
            {
               $quick_search .= " AND ss.state_id = '$fby_state' "; 
            }

            if(isset($fby_city) && !empty($fby_city))
            {
               $quick_search .= " AND ss.district_id = '$fby_city' "; 
            }

            if(isset($fby_product) && !empty($fby_product))
            {
               $quick_search .= " AND p.product_id = '$fby_product' "; 
            }

            if(isset($fby_from_date) && !empty($fby_from_date))
            {
               $fby_from_date = format_date($fby_from_date, DATE_DB);
               $quick_search .= " AND s.invoice_date >= '$fby_from_date' "; 
            }

            if(isset($fby_to_date) && !empty($fby_to_date))
            {
               $fby_to_date = format_date($fby_to_date, DATE_DB);
               $quick_search .= " AND s.invoice_date <= '$fby_to_date' "; 
            }

            if(isset($fby_keyword) && !empty($fby_keyword))
            {
                $quick_search .= " AND (saler_sys_id LIKE '$fby_keyword%'  OR balance_with_ws LIKE '$fby_keyword%' OR ss.first_name LIKE '$fby_keyword%' OR ss.last_name LIKE '$fby_keyword%' OR ss.state_name LIKE '$fby_keyword%' OR ss.district_name LIKE '$fby_keyword%' OR product_name LIKE '$fby_keyword%') "; 
            }

            $sql = "SELECT p.product_name, ss.state_name, ss.district_name, ss.saler_sys_id, CONCAT(ss.first_name,' ',ss.last_name) as wsname, s.balance_with_ws 
            FROM upload_wholesaler s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search
            ";


            $header = array('product_name' => "Product", 
                'state_name' => "State",
                'district_name' => "District",
                'saler_sys_id'=>"WS ID",
                'wsname' => "Name",                
                'balance_with_ws' => "Quantity");            

            $result = $this->dbaccess_model->executeSql($sql);
            
            if(isset($result) && !empty($result))
            {
                foreach($result as $obj)
                {
                    $content[] = (array)$obj;
                }                
            }

            $rname = "Wholesaler Stock";
        }
        elseif($report_type == "farmers")
        {
            if(isset($fby_state) && !empty($fby_state))
            {
               $quick_search .= " AND f.farmer_state = '$fby_state' "; 
            }

            if(isset($fby_city) && !empty($fby_city))
            {
               $quick_search .= " AND f.farmer_district = '$fby_city' "; 
            }

            if(isset($fby_executive) && !empty($fby_executive))
            {
               $quick_search .= " AND f.created_by = '$fby_executive' "; 
            }

            if(isset($fby_from_date) && !empty($fby_from_date))
            {
               $fby_from_date = format_date($fby_from_date, DATE_DB);
               $quick_search .= " AND f.created_date >= '$fby_from_date' "; 
            }

            if(isset($fby_to_date) && !empty($fby_to_date))
            {
               $fby_to_date = format_date($fby_to_date, DATE_DB);
               $quick_search .= " AND f.created_date <= '$fby_to_date' "; 
            }
            
            if(isset($fby_keyword) && !empty($fby_keyword))
            {
                $quick_search .= " AND (f.farmer_name LIKE '$fby_keyword%' OR 
                f.farmer_mobile LIKE '$fby_keyword%' OR 
                s.state_name LIKE '$fby_keyword%' OR 
                d.district_name LIKE '$fby_keyword%' OR 
                f.farmer_reg_num LIKE '$fby_keyword%' OR 
                f.farmer_plot_no LIKE '$fby_keyword%' OR 
                f.farmer_village LIKE '$fby_keyword%' OR 
                t.taluka_name LIKE '$fby_keyword%' OR
                u.last_name LIKE '$fby_keyword%' OR
                u.first_name LIKE '$fby_keyword%' OR 
                f.farmer_taluka LIKE '$fby_keyword%') "; 
            }


            $sql = "SELECT f.farmer_reg_num, f.farmer_name, f.farmer_mobile, CONCAT(f.farmer_plot_no,' ',f.farmer_village) as address, t.taluka_name as farmer_taluka, d.district_name, s.state_name, CONCAT(u.first_name,' ', u.last_name) as executive_name
            FROM farmers f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            LEFT JOIN users u ON u.user_id = f.created_by
            WHERE f.is_deleted = 0 $quick_search
            ORDER BY f.created_date DESC, f.farmer_name
            ";
            
            $header = array('farmer_reg_num' => "Reg. No.", 
                'farmer_name' => "Farmer Name",
                'farmer_mobile' => "Mobile",
                'address' => "Address",
                'farmer_taluka'=>"Taluka",
                'district_name' => "District",                
                'state_name' => "State",
                'executive_name' => "Executive Name"
                );            

            $result = $this->dbaccess_model->executeSql($sql);
            
            if(isset($result) && !empty($result))
            {
                foreach($result as $obj)
                {
                    $obj-> farmer_reg_num = "F".regno($obj-> farmer_reg_num, 5);

                    $content[] = (array)$obj;
                }                
            }

            $rname = "Farmers";
        }
        elseif($report_type == "demos")
        {
            if(isset($fby_state) && !empty($fby_state))
            {
               $quick_search .= " AND f.farmer_state = '$fby_state' "; 
            }

            if(isset($fby_city) && !empty($fby_city))
            {
               $quick_search .= " AND f.farmer_district = '$fby_city' "; 
            }

            if(isset($fby_executive) && !empty($fby_executive))
            {
               $quick_search .= " AND f.created_by = '$fby_executive' "; 
            }

            if(isset($fby_from_date) && !empty($fby_from_date))
            {
               $fby_from_date = format_date($fby_from_date, DATE_DB);
               $quick_search .= " AND f.created_date >= '$fby_from_date' "; 
            }

            if(isset($fby_to_date) && !empty($fby_to_date))
            {
               $fby_to_date = format_date($fby_to_date, DATE_DB);
               $quick_search .= " AND f.created_date <= '$fby_to_date' "; 
            }
            
            if(isset($fby_keyword) && !empty($fby_keyword))
            {
                $quick_search .= " AND (f.farmer_name LIKE '$fby_keyword%' OR 
                f.farmer_mobile LIKE '$fby_keyword%' OR 
                s.state_name LIKE '$fby_keyword%' OR 
                d.district_name LIKE '$fby_keyword%' OR 
                f.farmer_reg_num LIKE '$fby_keyword%' OR                 
                f.farmer_village LIKE '$fby_keyword%' OR 
                u.last_name LIKE '$fby_keyword%' OR
                u.first_name LIKE '$fby_keyword%' OR
                t.taluka_name LIKE '$fby_keyword%' OR
                f.farmer_taluka LIKE '$fby_keyword%') "; 
            }


            $sql = "SELECT f.farmer_reg_num, f.farmer_name, f.farmer_mobile, CONCAT(f.farmer_village) as address, t.taluka_name as farmer_taluka, d.district_name, s.state_name, CONCAT(u.first_name,' ', u.last_name) as executive_name 
            FROM demos f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            LEFT JOIN users u ON u.user_id = f.created_by
            WHERE f.is_deleted = 0 $quick_search
            ORDER BY f.created_date DESC, f.farmer_name
            ";
            
            $header = array('farmer_reg_num' => "Reg. No.", 
                'farmer_name' => "Farmer Name",
                'farmer_mobile' => "Mobile",
                'address' => "Address",
                'farmer_taluka'=>"Taluka",
                'district_name' => "District",                
                'state_name' => "State",
                'executive_name' => "Executive Name");            

            $result = $this->dbaccess_model->executeSql($sql);
            
            if(isset($result) && !empty($result))
            {
                foreach($result as $obj)
                {
                    $obj-> farmer_reg_num = "D".regno($obj-> farmer_reg_num, 5);

                    $content[] = (array)$obj;
                }                
            }

            $rname = "Demos";
        }
        elseif($report_type == "demos_details")
        {
            
            if(isset($fby_state) && !empty($fby_state))
                $for_stage = $fby_state;
            else
                $for_stage = "2";

            $this->load->model("Webservice_model");
            $data = $this->Webservice_model->get_demo_farmer_detail(array("farmer_id"=>$file_id, "farmer_type" => 2));

            $data['for_stage_type'] = $for_stage;
            $html = $this->load->view("demos_stage_pdf", $data, true);
            
            //Download--------------------------------------
            $down_filename = "Demo_Stage_".$for_stage."_".date('d-M-Y').".pdf";

            $this->load->library('m_pdf');        

            $this->m_pdf->pdf->setFooter($down_filename." (".'{PAGENO}'.")");

            $this->m_pdf->pdf->WriteHTML($html);

            $this->m_pdf->pdf->Output($down_filename, "I");

            die;
        }

        
        
        if($export_type == "xls")
        {
            export_excel($rname, $report_type, $header, $content);    
        }
        elseif($export_type == "pdf")
        {
            export_pdf($rname, $report_type, $header, $content);    
        }
        else
        {
            export_csv($rname, $report_type, $header, $content);
        }

    }



    /*----------------------------------------------------------
    Target Settings
    ----------------------------------------------------------*/
    public function target()
    {        
        $this->validate_permission(39, 4);

        $target_id = $this->uri->segment(3);
        if(!isset($target_id) || empty($target_id)) $target_id = 0;

        $arrs = array("typ"=>"states_aop");
        $data['states'] = $this->common_model->get_dd_list($arrs);

        $data['details'] = $this->admin_model->get_target(array("target_id"=>$target_id));
        $data['target_id'] = $target_id;

        $this->load->view('includes/main_header');
        $this->load->view('target', $data);
        $this->load->view('includes/main_footer');
    }

    
    public function target_save()
    {
        $tyear = $this->input->post('tyear');
        $tmonth = $this->input->post('tmonth');
        $tstate = $this->input->post('tstate');
        $tvalue = $this->input->post('tvalue');
        $tpercent = $this->input->post('tpercent');

        if($tyear < date("Y"))
        {
            echo json_encode(array("status"=>0,"message"=>"You can not edit previous year target."));            
            die;    
        }       
        elseif($tyear == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Year."));
            die;    
        }
        elseif($tmonth == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Month."));
            die;    
        }
        elseif($tstate == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select State."));
            die;    
        }
        elseif($tpercent == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Target Percentage."));
            die;    
        }
        elseif($tpercent <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Target Percentage greater than zero."));
            die;    
        }        
        elseif($tvalue == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Target Value."));
            die;    
        }

        if(strlen($tmonth) == 1) $tmonth = "0".$tmonth;
        $sql = "DELETE FROM target WHERE state_id = '$tstate' AND target_year = '$tyear' AND target_month = '$tmonth' LIMIT 1";
        $this->dbaccess_model->executeOnlySql($sql);

        
        $data = array("state_id" => $tstate,
            "target_year" => $tyear,
            "target_month" => $tmonth,
            "target_percent" => $tpercent,
            "target_value" => $tvalue,
            "created_date" => date(DATETIME_DB),
            "created_by" => $this->user_id
        );
        
        $this->dbaccess_model->insert("target", $data);               

        
        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;        
    }


    public function target_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.state_name', 't.target_year', 't.target_month', 't.target_percent', 't.target_value');

        $sTable = "target";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS t.*, s.state_name
            FROM $sTable t
            INNER JOIN states s ON s.state_id = t.state_id AND s.is_state_active = 1           
            WHERE 1 = 1 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(t.target_id) as count
            FROM $sTable t
            INNER JOIN states s ON s.state_id = t.state_id AND s.is_state_active = 1
            WHERE 1 = 1 $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 

            $row[] = $aRow -> state_name;

            $row[] = $aRow -> target_year;

            $row[] = date("M", strtotime($aRow -> target_year."-".$aRow -> target_month."-01"));

            $row[] = $aRow -> target_percent;             

            $row[] = $aRow -> target_value;             

            if($aRow -> target_year >= date("Y"))
            {
                $row[] = '<a class="action_link" href="'.base_url().'admin/target/'.$aRow -> target_id.'"><i class="fa fa-pencil"></i></a>';
            }
            else
            {
                $row[] = "";
            }   

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    public function get_old_target()
    {
        $tyear = $this->input->post('tyear');
        $tmonth = $this->input->post('tmonth');
        $tstate = $this->input->post('tstate');        

        if($tyear == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Year."));
            die;    
        }
        elseif($tmonth == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Month."));
            die;    
        }
        elseif($tstate == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select State."));
            die;    
        }

        
        $sfile_id = $file_id_ack1 = $file_id_ack2 = $file_id_wstk = $file_id_rstk = "";
        $details = $this->admin_model->last_upload_date('upload_acknowledgement1');        
        if(isset($details) && !empty($details))
        {
            $file_id_ack1 = $details-> file_id;
        }

        $details = $this->admin_model->last_upload_date('upload_acknowledgement2');        
        if(isset($details) && !empty($details))
        {
            $file_id_ack2 = $details-> file_id;
        }

        $details = $this->admin_model->last_upload_date('upload_wholesaler');        
        if(isset($details) && !empty($details))
        {
            $file_id_wstk = $details-> file_id;
        }

        $details = $this->admin_model->last_upload_date('upload_retailer');        
        if(isset($details) && !empty($details))
        {
            $file_id_rstk = $details-> file_id;
        }


        if(strlen($tmonth) == 1) $tmonth = "0".$tmonth;
        
        $arr = array("year"=>$tyear, "month"=>$tmonth, "state"=>$tstate, "file_id_ack1" => $file_id_ack1, "file_id_ack2" => $file_id_ack2, "file_id_wstk" => $file_id_wstk, "file_id_rstk" => $file_id_rstk);
        $details = $this->admin_model->get_old_target($arr);

        $data = array();
        if(isset($details) && !empty($details))
        {
            foreach($details as $obj)
            {                                
                $data[] = array("quantity" => number_format($obj-> quantity, "2", ".", ""),
                    "type" => $obj-> type
                );                
            }       

            echo json_encode(array("status"=>1,"message"=>"Saved successfully.","data"=>$data)); die;        
        }
        else
        {    
            echo json_encode(array("status"=>0,"message"=>"","data"=>$data)); die;        
        }        
    }



    /*----------------------------------------------------------
    SMS Management
    ----------------------------------------------------------*/
    public function sms_template()
    {
        $this->validate_permission(40, 4);

        $this->load->view('includes/main_header');
        $this->load->view('sms_template');
        $this->load->view('includes/main_footer');
    }


    public function sms_template_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        $template_for = $_POST['template_for'];
        if(isset($template_for) && !empty($template_for))
            $quick_search = " AND t.template_for = '$template_for' ";
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('t.template_for', 't.content', 't.days', 't.copy_to');

        $sTable = "templates";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS t.*
            FROM $sTable t            
            WHERE t.is_deleted = 0 AND t.template_type = 1 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(t.template_id) as count
            FROM $sTable t            
            WHERE t.is_deleted = 0 AND t.template_type = 1 $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 

            $row[] = $aRow -> content;

            $row[] = $aRow -> days;

            $row[] = $aRow -> copy_to;

            $tfor = "Acknowledgement 1";
            if($aRow -> template_for == 2) $tfor = "Stock Movement";
            elseif($aRow -> template_for == 3) $tfor = "Acknowledgement 2";
            $row[] = $tfor; 

            $l = "sms_template_edit_w";
            if($aRow -> template_for == 2) $l = "sms_template_edit_r";
            elseif($aRow -> template_for == 3) $l = "sms_template_edit_r1";

            
            $defbtn = "";
            if($aRow->is_default == 0)
            {
                $defbtn = '&nbsp;<a class="action_link" href="javascript:void(0);" title="Set as Default" onclick=set_as_default("1",'.$aRow -> template_for.','.$aRow -> template_id.');><label class="badge bg-blue bg-primary">Set as Default</label></a>';
            }
            else
            {
                $defbtn = "&nbsp;<label class='badge bg-green bg-success'>default</label>";
            }

            $row[] = '<a class="action_link" href="'.base_url().'admin/'.$l.'/'.$aRow -> template_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("templates",'.$aRow -> template_id.');><i class="fa fa-trash"></i></a>'.$defbtn;              

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }

    public function sms_template_add_w()
    {
        $this->validate_permission(40, 2);

        $this->load->view('includes/main_header');
        $this->load->view('sms_template_add_w');
        $this->load->view('includes/main_footer');
    }

    public function sms_template_edit_w()
    {
        $this->validate_permission(40, 3);

        $template_id = $this->uri->segment(3);
        if(isset($template_id) && !empty($template_id) && $template_id > 0)
        {
            $arr = array("typ"=>"templates", "id"=>$template_id);        
            $data['details'] = $this->common_model->get_detail($arr);
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."admin/sms_template'; </script>";
        }

        $this->load->view('includes/main_header');
        $this->load->view('sms_template_edit_w', $data);
        $this->load->view('includes/main_footer');
    }


    public function sms_template_add_r1()
    {
        $this->validate_permission(40, 2);

        $this->load->view('includes/main_header');
        $this->load->view('sms_template_add_r1');
        $this->load->view('includes/main_footer');
    }

    public function sms_template_edit_r1()
    {
        $this->validate_permission(40, 3);

        $template_id = $this->uri->segment(3);
        if(isset($template_id) && !empty($template_id) && $template_id > 0)
        {
            $arr = array("typ"=>"templates", "id"=>$template_id);        
            $data['details'] = $this->common_model->get_detail($arr);
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."admin/sms_template'; </script>";
        }

        $this->load->view('includes/main_header');
        $this->load->view('sms_template_edit_r1', $data);
        $this->load->view('includes/main_footer');
    }

    public function sms_template_add_r()
    {
        $this->validate_permission(40, 2);

        $this->load->view('includes/main_header');
        $this->load->view('sms_template_add_r');
        $this->load->view('includes/main_footer');
    }

    public function sms_template_edit_r()
    {
        $this->validate_permission(40, 3);

        $template_id = $this->uri->segment(3);
        if(isset($template_id) && !empty($template_id) && $template_id > 0)
        {
            $arr = array("typ"=>"templates", "id"=>$template_id);        
            $data['details'] = $this->common_model->get_detail($arr);
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."admin/sms_template'; </script>";
        }

        $this->load->view('includes/main_header');
        $this->load->view('sms_template_edit_r', $data);
        $this->load->view('includes/main_footer');
    }


    public function sms_template_save()
    {
        $sms_for = $this->input->post('sms_for');        
        $content = $this->input->post('content');
        $days = $this->input->post('days');
        $sms_to = $this->input->post('sms_to');
        $template_id = $this->input->post('template_id');

        if($content == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter sms content."));
            die;    
        }
        elseif(strlen($content)>100)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter sms content less than 100 characters."));
            die; 
        }
        elseif($days == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter over days."));
            die;    
        }       
        
        if(!isset($sms_for) || empty($sms_for)) $sms_for = 1;
        if(isset($sms_to) && !empty($sms_to)) $sms_to = implode(",", $sms_to);
        
        if($template_id > 0)
        {
            $data = array("content" => $content,
            "days" => $days,
            "copy_to" => $sms_to,
            
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id);

            $this->dbaccess_model->update("templates", $data, array("template_id ="=>$template_id));
        }
        else
        {
            $data = array("template_type" => 1, 
            "template_for" => $sms_for,
            "content" => $content,
            "days" => $days,
            "copy_to" => $sms_to,

            "created_date" => date(DATETIME_DB),                
            "created_by" => $this->user_id,
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id);

            $this->dbaccess_model->insert("templates", $data);
        }

        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;     
    }



    /*----------------------------------------------------------
    Email Management
    ----------------------------------------------------------*/
    public function email_template()
    {
        $this->validate_permission(41, 4);

        $this->load->view('includes/main_header');
        $this->load->view('email_template');
        $this->load->view('includes/main_footer');
    }


    public function email_template_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        $template_for = $_POST['template_for'];
        if(isset($template_for) && !empty($template_for))
            $quick_search = " AND t.template_for = '$template_for' ";

        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('t.template_for', 't.content', 't.days', 't.copy_to');

        $sTable = "templates";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS t.*
            FROM $sTable t            
            WHERE t.is_deleted = 0 AND t.template_type = 2 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(t.template_id) as count
            FROM $sTable t            
            WHERE t.is_deleted = 0 AND t.template_type = 2 $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 

            $row[] = $aRow -> content;

            $row[] = $aRow -> days;

            $row[] = $aRow -> copy_to;

            $tfor = "Acknowledgement 1";
            if($aRow -> template_for == 2) $tfor = "Stock Movement";
            elseif($aRow -> template_for == 3) $tfor = "Acknowledgement 2";
            $row[] = $tfor; 

            $l = "email_template_edit_w";
            if($aRow -> template_for == 2) $l = "email_template_edit_r";
            elseif($aRow -> template_for == 3) $l = "email_template_edit_r1";


            $defbtn = "";
            if($aRow->is_default == 0)
            {
                $defbtn = '&nbsp;<a class="action_link" href="javascript:void(0);" title="Set as Default" onclick=set_as_default("2",'.$aRow -> template_for.','.$aRow -> template_id.');><label class="badge bg-blue bg-primary">Set as Default</label></a>';
            }
            else
            {
                $defbtn = "&nbsp;<label class='badge bg-green bg-success'>default</label>";
            }


            $row[] = '<a class="action_link" href="'.base_url().'admin/'.$l.'/'.$aRow -> template_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("templates",'.$aRow -> template_id.');><i class="fa fa-trash"></i></a>'.$defbtn;              

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }

    public function email_template_add_w()
    {        
        $this->validate_permission(41, 2);

        $data['email_signatures'] = $this->common_model->get_dd_list(array("typ"=>"email_signature"));
     
        $this->load->view('includes/main_header');
        $this->load->view('email_template_add_w', $data);
        $this->load->view('includes/main_footer');
    }

    public function email_template_edit_w()
    {
        $this->validate_permission(41, 3);

        $template_id = $this->uri->segment(3);
        if(isset($template_id) && !empty($template_id) && $template_id > 0)
        {
            $arr = array("typ"=>"templates", "id"=>$template_id);        
            $data['details'] = $this->common_model->get_detail($arr);
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."admin/email_template'; </script>";
        }

        $data['email_signatures'] = $this->common_model->get_dd_list(array("typ"=>"email_signature"));

        $this->load->view('includes/main_header');
        $this->load->view('email_template_edit_w', $data);
        $this->load->view('includes/main_footer');
    }

    public function email_template_add_r1()
    {
        $this->validate_permission(41, 2);

        $data['email_signatures'] = $this->common_model->get_dd_list(array("typ"=>"email_signature"));

        $this->load->view('includes/main_header');
        $this->load->view('email_template_add_r1', $data);
        $this->load->view('includes/main_footer');
    }

    public function email_template_edit_r1()
    {
        $this->validate_permission(41, 3);

        $template_id = $this->uri->segment(3);
        if(isset($template_id) && !empty($template_id) && $template_id > 0)
        {
            $arr = array("typ"=>"templates", "id"=>$template_id);        
            $data['details'] = $this->common_model->get_detail($arr);
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."admin/email_template'; </script>";
        }

        $data['email_signatures'] = $this->common_model->get_dd_list(array("typ"=>"email_signature"));

        $this->load->view('includes/main_header');
        $this->load->view('email_template_edit_r1', $data);
        $this->load->view('includes/main_footer');
    }

    public function email_template_add_r()
    {
        $this->validate_permission(41, 2);

        $data['email_signatures'] = $this->common_model->get_dd_list(array("typ"=>"email_signature"));

        $this->load->view('includes/main_header');
        $this->load->view('email_template_add_r', $data);
        $this->load->view('includes/main_footer');
    }

    public function email_template_edit_r()
    {
        $this->validate_permission(41, 3);

        $template_id = $this->uri->segment(3);
        if(isset($template_id) && !empty($template_id) && $template_id > 0)
        {
            $arr = array("typ"=>"templates", "id"=>$template_id);        
            $data['details'] = $this->common_model->get_detail($arr);
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."admin/email_template'; </script>";
        }

        $data['email_signatures'] = $this->common_model->get_dd_list(array("typ"=>"email_signature"));

        $this->load->view('includes/main_header');
        $this->load->view('email_template_edit_r', $data);
        $this->load->view('includes/main_footer');
    }


    public function email_template_save()
    {
        $email_for = $this->input->post('email_for');        
        $content = $this->input->post('content');
        $days = $this->input->post('days');
        $email_to = $this->input->post('email_to');
        $template_id = $this->input->post('template_id');
        $email_sign = $this->input->post('email_sign');

        if($content == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter email content."));
            die;    
        }
        elseif(strlen($content)>100)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter email content less than 100 characters."));
            die; 
        }
        elseif($days == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter over days."));
            die;    
        }       
        
        if(!isset($email_for) || empty($email_for)) $email_for = 1;
        if(isset($email_to) && !empty($email_to)) $email_to = implode(",", $email_to);
        
        if($template_id > 0)
        {
            $data = array("content" => $content,
            "days" => $days,
            "copy_to" => $email_to,
            "email_signature_id" => $email_sign,
            
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id);

            $this->dbaccess_model->update("templates", $data, array("template_id ="=>$template_id));
        }
        else
        {
            $data = array("template_type" => 2, 
            "template_for" => $email_for,
            "content" => $content,
            "days" => $days,
            "copy_to" => $email_to,
            "email_signature_id" => $email_sign,

            "created_date" => date(DATETIME_DB),                
            "created_by" => $this->user_id,
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id);

            $this->dbaccess_model->insert("templates", $data);
        }

        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;     
    }


    public function set_as_default()
    {
        $template_type = $this->input->post('template_type'); 
        $template_for = $this->input->post('template_for');               
        $template_id = $this->input->post('template_id');        

        if($template_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select template."));
            die;    
        }
        elseif($template_type == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Template type missing SMS or Email."));
            die;    
        }

        $sql = "UPDATE templates SET is_default = 0 WHERE template_type = '$template_type' AND template_for = '$template_for' ";
        $this->dbaccess_model->executeOnlySql($sql);

        $sql = "UPDATE templates SET is_default = 1 WHERE template_id = '$template_id' AND template_type = '$template_type' AND template_for = '$template_for' LIMIT 1";
        $i = $this->dbaccess_model->executeOnlySql($sql);

       
        if(isset($i) && !empty($i))
        {
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;        
        }
        else
        {    
            echo json_encode(array("status"=>0,"message"=>"Error occur while processing your request. Please try again.")); die;        
        }        
    }


    public function approve_reason_save()
    {
        $approve_reason = $this->input->post('approve_reason');
        
        $demo_id = $this->input->post('demo_id');

        if($approve_reason == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter approve reason."));
            die;    
        }
        elseif($demo_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select any demo first."));
            die;    
        }
        
        $sql = "UPDATE demos SET is_approved = 1, approved_reason = '$approve_reason', approved_date = '".date(DATETIME_DB)."' WHERE farmer_id = '$demo_id' LIMIT 1";

        $this->dbaccess_model->executeOnlySql($sql);


        echo json_encode(array("status"=>1,"message"=>"Saved Successfully."));
        die;
    }


    public function farmers_add_save()
    {        
        $farmer_name = $this->input->post('farmer_name');
        $farmer_mobile = $this->input->post('farmer_mobile');
        $farmer_email = "";//$this->input->post('farmer_email');
        $farmer_taluka = $this->input->post('farmer_taluka');
        $farmer_district = $this->input->post('farmer_district');
        $farmer_state = $this->input->post('farmer_state');

        $visit_date = $this->input->post('visit_date');

        $farmer_land_area = $this->input->post('farmer_land_area');

        if($visit_date == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Registration Date required.")); die;
        }
        elseif($farmer_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Name required.")); die;
        }       
        elseif($farmer_mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number required."));
            die;    
        }
        elseif(strlen($farmer_mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        elseif($farmer_state == "" || $farmer_state <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer State required."));
            die;    
        }
        elseif($farmer_district == "" || $farmer_district <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer District required."));
            die;    
        }
        elseif($farmer_taluka == "" || $farmer_taluka <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Taluka required."));
            die;    
        }        
        elseif($farmer_land_area == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Total Acrage required."));
            die;    
        }
        
                
        $arr = array("typ"=>"farmers", "id"=>0, "name"=>$farmer_mobile);
        $data = $this->common_model->is_already_exist($arr);
        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number already exist. Please enter different Mobile Number.")); die;
        }

        $rf_temp = array();
        $farmer_refrence_name = $this->input->post('farmer_refrence_name');
        $farmer_refrence_mobile = $this->input->post('farmer_refrence_mobile');
        if(isset($farmer_refrence_name) && !empty($farmer_refrence_name))
        {            
            foreach($farmer_refrence_name as $rid => $rnm)
            {
                $rf_temp[] = array("name" => $rnm, 
                    "mobile" => $farmer_refrence_mobile[$rid]
                );
            }
        }


               
        
        $data = array("farmer_name" => $farmer_name, 
        "farmer_mobile" => $farmer_mobile, 
        "farmer_email" => $this->input->post('farmer_email'), 
        "farmer_plot_no" => $this->input->post('farmer_plot_no'), 
        "farmer_village" => $this->input->post('farmer_village'), 
        "farmer_taluka" => $this->input->post('farmer_taluka'), 
        "farmer_district" => $this->input->post('farmer_district'), 
        "farmer_state" => $this->input->post('farmer_state'),
        "on_or_off_site" => "off_site",
        "geo_lat" => 0,
        "geo_long" => 0,        
        "farmer_land_area" => $this->input->post('farmer_land_area'),                
        "farmer_willing_demo" => $this->input->post('farmer_willing_demo'), 
        "farmer_plot_area_consultancy" => $this->input->post('farmer_plot_area_consultancy'),
        "farmer_reg_date" => date(DATE_DB, strtotime($visit_date)),                
        

        "kharif_crop" => $this->input->post('kharif_crop'), 
        "is_ziron_used" => $this->input->post('is_ziron_used'), 
        "ziron_bought_from" => $this->input->post('ziron_bought_from'), 
        "ziron_feedback" => $this->input->post('ziron_feedback'), 
        "farmer_refrence" => json_encode($rf_temp), 

        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id,
        "updated_date" => date(DATETIME_DB),                
        "updated_by" => $this->user_id
        );

        $farmerid = $this->dbaccess_model->insert("farmers", $data);            
        if($farmerid > 0)
        {
            $farmer_reg_num = $this->common_model->get_farmer_reg_no();
            $farmer_reg_num = $farmer_reg_num + 1;
            $this->dbaccess_model->update("farmers", array("farmer_reg_num"=>$farmer_reg_num), array("farmer_id" => $farmerid));



            $sdarr = array("typ"=>'district', "id" => $this->input->post('farmer_district'));
            $sd = $this->common_model->get_detail($sdarr);
            $fstate_name = strtolower(trim($sd-> state_name));
            $fdistrict_name = strtolower(trim($sd-> district_name));
            $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/farmer";

            if(isset($_FILES['farmer_photo']['name']) && !empty($_FILES['farmer_photo']['name']))
            {
                $farmer_photo = $_FILES['farmer_photo'];            
                if(isset($farmer_photo) && !empty($farmer_photo))
                {                
                    $ext = pathinfo($farmer_photo['name'], PATHINFO_EXTENSION);

                    $chk = $this->upload_image($path, 'farmer_photo', $farmerid.'_'.$this->user_id."_fp_".$farmer_photo['name']);
                    if(isset($chk) && !empty($chk))
                    {
                       $this->webservice_model->remove_farmer($farmerid, 1);                   
                       echo json_encode(array("status"=>0,"message"=>$chk)); 
                       die; 
                    }
                    else
                    {
                        $spath = BASE_PATH.'/'.$path."/".$farmerid.'_'.$this->user_id."_fp_".$farmer_photo['name'];
                        $this->dbaccess_model->update("farmers", array("farmer_photo" => $farmerid.'_'.$this->user_id."_fp_".$farmer_photo['name'],"farmer_photo_url"=>$spath), array("farmer_id" => $farmerid));
                    }
                }
            }    
            
            
            $crop_last_year = ($this->input->post('crop_last_year'));
            $crop_current_year = ($this->input->post('crop_next_year'));
            

            if(isset($crop_last_year) && !empty($crop_last_year))
            {
                foreach($crop_last_year as $k => $val)
                {
                    if(isset($val) && !empty($val))
                    {
                        $datain = array("farmer_id" => $farmerid,
                        "last_year_crop_id" => $val,
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $this->dbaccess_model->insert("farmers_crop_last_year", $datain);
                    }    
                }    
            }


            if(isset($crop_current_year) && !empty($crop_current_year))
            {
                foreach($crop_current_year as $k => $val)
                {
                    if(isset($val) && !empty($val))
                    {
                        $datain = array("farmer_id" => $farmerid,
                        "current_year_crop_id" => $val,
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $this->dbaccess_model->insert("farmers_crop_current_year", $datain);
                    }    
                }    
            }  
            

                       
                          
            if(isset($_FILES['plot_photos']['name'][0]) && !empty($_FILES['plot_photos']['name'][0]))
            {
                $files = $_FILES['plot_photos'];

                $config = array(
                    'upload_path'   => $path,
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'overwrite'     => 1                       
                );
                $this->load->library('upload', $config);

                foreach ($files['name'] as $key => $image) 
                {
                    $_FILES['plot_photos[]']['name']= $files['name'][$key];
                    $_FILES['plot_photos[]']['type']= $files['type'][$key];
                    $_FILES['plot_photos[]']['tmp_name']= $files['tmp_name'][$key];
                    $_FILES['plot_photos[]']['error']= $files['error'][$key];
                    $_FILES['plot_photos[]']['size']= $files['size'][$key];
                    
                    $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_pp_".$image;

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('plot_photos[]')) 
                    {
                        $dataup = $this->upload->data();
                        $ext = $dataup['file_ext'];
                        $unm = $dataup['file_name'];
                        
                        $datain = array("farmer_id" => $farmerid,
                        "plot_photo_name" => $unm,
                        "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                    } 
                    else 
                    {
                        $this->webservice_model->remove_farmer($farmerid, 1);
                        echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                        die;
                    }
                }
            } 


            $hdn_quick_id = $this->input->post('hdn_quick_id');
            if(isset($hdn_quick_id) && !empty($hdn_quick_id) && $hdn_quick_id > 0)
            {
                $sql = "UPDATE farmers_temp SET is_deleted = 1 WHERE farmer_id = $hdn_quick_id AND farmer_mobile = '$farmer_mobile' LIMIT 1";   
                $this->dbaccess_model->executeOnlySql($sql);
            }    
        }


        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;
    }
    


    public function farmers_edit_save()
    {        
        $farmer_name = $this->input->post('farmer_name');
        $farmer_mobile = $this->input->post('farmer_mobile');
        $farmer_email = "";//$this->input->post('farmer_email');
        $farmer_taluka = $this->input->post('farmer_taluka');
        $farmer_district = $this->input->post('farmer_district');
        $farmer_state = $this->input->post('farmer_state');
        $visit_date = $this->input->post('visit_date');

        $farmer_hdn_id = $this->input->post('hdn_id');

        $farmer_land_area = $this->input->post('farmer_land_area');

        if($visit_date == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Registration Date required.")); die;
        } 
        elseif($farmer_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Name required.")); die;
        }       
        elseif($farmer_mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number required."));
            die;    
        }
        elseif(strlen($farmer_mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        elseif($farmer_state == "" || $farmer_state <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer State required."));
            die;    
        }
        elseif($farmer_district == "" || $farmer_district <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer District required."));
            die;    
        }
        elseif($farmer_taluka == "" || $farmer_taluka <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Taluka required."));
            die;    
        }        
        elseif($farmer_land_area == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Total Acrage required."));
            die;    
        }
        
                
        $arr = array("typ"=>"farmers", "id"=>$farmer_hdn_id, "name"=>$farmer_mobile);
        $data = $this->common_model->is_already_exist($arr);
        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number already exist. Please enter different Mobile Number.")); die;
        }

        $rf_temp = array();
        $farmer_refrence_name = $this->input->post('farmer_refrence_name');
        $farmer_refrence_mobile = $this->input->post('farmer_refrence_mobile');
        if(isset($farmer_refrence_name) && !empty($farmer_refrence_name))
        {            
            foreach($farmer_refrence_name as $rid => $rnm)
            {
                $rf_temp[] = array("name" => $rnm, 
                    "mobile" => $farmer_refrence_mobile[$rid]
                );
            }
        }

        $farmer_reg_num = $this->input->post('farmer_reg_num');
               
        
        $data = array("farmer_name" => $farmer_name, 
        "farmer_mobile" => $farmer_mobile, 
        "farmer_email" => $this->input->post('farmer_email'), 
        "farmer_plot_no" => $this->input->post('farmer_plot_no'), 
        "farmer_village" => $this->input->post('farmer_village'), 
        "farmer_taluka" => $this->input->post('farmer_taluka'), 
        "farmer_district" => $this->input->post('farmer_district'), 
        "farmer_state" => $this->input->post('farmer_state'),
        "on_or_off_site" => "off_site",
        "geo_lat" => 0,
        "geo_long" => 0,        
        "farmer_land_area" => $this->input->post('farmer_land_area'),                
        "farmer_willing_demo" => $this->input->post('farmer_willing_demo'), 
        "farmer_plot_area_consultancy" => $this->input->post('farmer_plot_area_consultancy'),

        "kharif_crop" => $this->input->post('kharif_crop'), 
        "is_ziron_used" => $this->input->post('is_ziron_used'), 
        "ziron_bought_from" => $this->input->post('ziron_bought_from'), 
        "ziron_feedback" => $this->input->post('ziron_feedback'), 
        "farmer_refrence" => json_encode($rf_temp), 
        
        "updated_date" => date(DATETIME_DB),                
        "updated_by" => $this->user_id
        );


        $farmerid = $farmer_hdn_id; 
        $this->dbaccess_model->update("farmers", $data, array("farmer_id"=>$farmer_hdn_id));            
        if($farmerid > 0)
        {
            $sdarr = array("typ"=>'district', "id" => $this->input->post('farmer_district'));
            $sd = $this->common_model->get_detail($sdarr);
            $fstate_name = strtolower(trim($sd-> state_name));
            $fdistrict_name = strtolower(trim($sd-> district_name));
            $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/farmer";

            if(isset($_FILES['farmer_photo']['name']) && !empty($_FILES['farmer_photo']['name']))
            {
                $farmer_photo = $_FILES['farmer_photo'];            
                if(isset($farmer_photo) && !empty($farmer_photo))
                {                
                    $ext = pathinfo($farmer_photo['name'], PATHINFO_EXTENSION);

                    $chk = $this->upload_image($path, 'farmer_photo', $farmerid.'_'.$this->user_id."_fp_".$farmer_photo['name']);
                    if(isset($chk) && !empty($chk))
                    {                       
                       echo json_encode(array("status"=>0,"message"=>$chk)); 
                       die; 
                    }
                    else
                    {
                        $spath = BASE_PATH.'/'.$path."/".$farmerid.'_'.$this->user_id."_fp_".$farmer_photo['name'];
                        $this->dbaccess_model->update("farmers", array("farmer_photo" => $farmerid.'_'.$this->user_id."_fp_".$farmer_photo['name'],"farmer_photo_url"=>$spath), array("farmer_id" => $farmerid));
                    }
                }
            }    
            
            
            $crop_last_year = ($this->input->post('crop_last_year'));
            $crop_current_year = ($this->input->post('crop_next_year'));
            

            if(isset($crop_last_year) && !empty($crop_last_year))
            {
                $del = "DELETE FROM farmers_crop_last_year WHERE farmer_id = $farmerid";
                $this->dbaccess_model->executeOnlySql($del);

                foreach($crop_last_year as $k => $val)
                {
                    if(isset($val) && !empty($val))
                    {
                        $datain = array("farmer_id" => $farmerid,
                        "last_year_crop_id" => $val,
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );

                        
                    
                        $this->dbaccess_model->insert("farmers_crop_last_year", $datain);
                    }    
                }    
            }


            if(isset($crop_current_year) && !empty($crop_current_year))
            {
                $del = "DELETE FROM farmers_crop_current_year WHERE farmer_id = $farmerid";
                $this->dbaccess_model->executeOnlySql($del);

                foreach($crop_current_year as $k => $val)
                {
                    if(isset($val) && !empty($val))
                    {
                        $datain = array("farmer_id" => $farmerid,
                        "current_year_crop_id" => $val,
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $this->dbaccess_model->insert("farmers_crop_current_year", $datain);
                    }    
                }    
            }  
            

                       
                          
            if(isset($_FILES['plot_photos']['name'][0]) && !empty($_FILES['plot_photos']['name'][0]))
            {
                $files = $_FILES['plot_photos'];

                $config = array(
                    'upload_path'   => $path,
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'overwrite'     => 1                       
                );
                $this->load->library('upload', $config);

                foreach ($files['name'] as $key => $image) 
                {
                    $_FILES['plot_photos[]']['name']= $files['name'][$key];
                    $_FILES['plot_photos[]']['type']= $files['type'][$key];
                    $_FILES['plot_photos[]']['tmp_name']= $files['tmp_name'][$key];
                    $_FILES['plot_photos[]']['error']= $files['error'][$key];
                    $_FILES['plot_photos[]']['size']= $files['size'][$key];
                    
                    $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_pp_".$image;

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('plot_photos[]')) 
                    {
                        $dataup = $this->upload->data();
                        $ext = $dataup['file_ext'];
                        $unm = $dataup['file_name'];
                        
                        $datain = array("farmer_id" => $farmerid,
                        "plot_photo_name" => $unm,
                        "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                    } 
                    else 
                    {                        
                        echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                        die;
                    }
                }
            }    
        }

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;
    } 

    function upload_image($path, $filename, $newfilename)
    { 
        $config['file_name'] = $newfilename;
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        
        $this->load->library('upload');
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) 
        {
            $error = $this->upload->display_errors();        
        } 
        else 
        {            
            $error = "";
        }

        return $error;
    }




    /*----------------------------------------------------------
    Email Signature Management
    ----------------------------------------------------------*/
    public function email_template_signature()
    {
        $this->validate_permission(41, 4);

        $email_signature_id = $this->uri->segment(3);

        if(isset($email_signature_id) && !empty($email_signature_id) && $email_signature_id > 0)
        {
            $arr = array("typ" => "email_signature", "id" => $email_signature_id);
            $data['details'] = (array)$this->common_model->get_detail($arr);
        }
        else
        {
            $data['details']['email_signature_id'] = 0;
            $data['details']['signature_name'] = "";
            $data['details']['signature_from'] = "";
            $data['details']['signature_content'] = "";
        }    

        $this->load->view('includes/main_header');
        $this->load->view('email_template_signature', $data);
        $this->load->view('includes/main_footer');
    }


    public function email_template_signature_save()
    {
        extract($_POST);

        if($signature_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Send From Name."));
            die;    
        }
        elseif($signature_from == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Send Email From."));
            die;    
        }        
        elseif($signature_content == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Signature."));
            die;    
        }

        if($hdn_id > 0)
        {
            $data = array("signature_name" => $signature_name,
            "signature_from" => $signature_from,
            "signature_content" => $signature_content,
            
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id);

            $this->dbaccess_model->update("email_signature", $data, array("email_signature_id ="=>$hdn_id));
        }
        else
        {
            $data = array("signature_name" => $signature_name,
            "signature_from" => $signature_from,
            "signature_content" => $signature_content,

            "created_date" => date(DATETIME_DB),                
            "created_by" => $this->user_id,
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id);

            $this->dbaccess_model->insert("email_signature", $data);
        }

        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
    }

    public function email_signature_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('signature_name', 'signature_from', 'signature_content');

        $sTable = "email_signature";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.email_signature_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        
        
        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 

            $row[] = $aRow -> signature_name;

            $row[] = $aRow -> signature_from;

            $row[] = $aRow -> signature_content;                                   

            $row[] = '<a class="action_link" href="'.base_url().'admin/email_template_signature/'.$aRow -> email_signature_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("email_signature",'.$aRow -> email_signature_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    public function get_sales_yearly()
    {
        $year = $this->input->post('year'); 
        
        if($year == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select year."));
            die;    
        }

        $details = $this->admin_model->last_upload_date('upload_sales');        
        if(isset($details) && !empty($details))
        {
            $sfile_id = $details-> file_id;
        }
        else
        {
            $sfile_id = 0;
        }

        $data = $this->common_model->get_sales_yearly($sfile_id, $year);

        if(isset($data) && !empty($data))
        {
            $arr = $arr1 = array();
            foreach($data as $obj)
            {
                if($obj-> type == "sales")
                {
                    $m = date("m", strtotime($obj-> sales_date));

                    if(isset($arr[$obj-> state_name][$m]) && !empty($arr[$obj-> state_name][$m]))
                    {
                        $arr[$obj-> state_name][$m] = $arr[$obj-> state_name][$m] + $obj-> quantity;
                    }
                    else
                    {
                        $arr[$obj-> state_name][$m] = $obj-> quantity;
                    }
                }
                elseif($obj-> type == "target")
                {
                    $m = $obj-> sales_date;

                    $arr1[$obj-> state_name][$m] = $obj-> quantity;
                }    
            }


            //Sales Data-----------------------------
            $t = $t1 = array();
            foreach($arr as $snm => $temp)
            {                
                for($ii=1; $ii<=12; $ii++)
                {
                    if(strlen($ii) == 1) $jj = "0".$ii;
                    else $jj = $ii;

                    if(isset($temp[$jj]) && !empty($temp[$jj]))
                    {
                        $t[$snm][$jj] = $temp[$jj];
                    }
                    else
                    {
                        $t[$snm][$jj] = 0;
                    }


                    if(isset($arr1[$snm][$jj]) && !empty($arr1[$snm][$jj]))
                    {
                        $ts = $t[$snm][$jj];
                        $tt = $arr1[$snm][$jj];
                        $per = number_format(($ts * 100) / $tt, "2", ".", "");
                        $t1[] = array("state_name"=>$snm, "month"=>$jj, "total_sales"=>$ts, "total_target"=>$tt, "percent"=>$per);
                    }
                }
            }

            echo json_encode(array("status"=>1, "data"=>array("sales"=>$t, "target"=>$t1))); die;        
        }
        else
        {    
            echo json_encode(array("status"=>0,"message"=>"Error occur while processing your request. Please try again.")); die;        
        }        
    }


    public function get_target_achievement()
    {
        $year = $this->input->post('year'); 
        
        if($year == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select year."));
            die;    
        }

        $details = $this->admin_model->last_upload_date('upload_sales');        
        if(isset($details) && !empty($details))
        {
            $sfile_id = $details-> file_id;
        }
        else
        {
            $sfile_id = 0;
        }

        $data = $this->common_model->get_target_achievement($sfile_id, $year);

        if(isset($data) && !empty($data))
        {
            $arr = array();
            foreach($data as $obj)
            {
                $m = date("m", strtotime($obj-> sales_date));

                if(isset($arr[$obj-> state_name][$m]) && !empty($arr[$obj-> state_name][$m]))
                {
                    $arr[$obj-> state_name][$m] = $arr[$obj-> state_name][$m] + $obj-> quantity;
                }
                else
                {
                    $arr[$obj-> state_name][$m] = $obj-> quantity;
                }
            }


            $t = array();
            foreach($arr as $snm => $temp)
            {                
                for($ii=1; $ii<=12; $ii++)
                {
                    if(strlen($ii) == 1) $jj = "0".$ii;
                    else $jj = $ii;

                    if(isset($temp[$jj]) && !empty($temp[$jj]))
                    {
                        $t[$snm][$jj] = $temp[$jj];
                    }
                    else
                    {
                        $t[$snm][$jj] = 0;
                    }
                }
            }

            echo json_encode(array("status"=>1, "data"=>$t)); die;        
        }
        else
        {    
            echo json_encode(array("status"=>0,"message"=>"Error occur while processing your request. Please try again.")); die;        
        }        
    }  


}//End Of Class
?>