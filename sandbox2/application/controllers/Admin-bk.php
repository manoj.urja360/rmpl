<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{
	public $user_first_name = "";
    public $user_last_name = "";
    public $user_fullname = "";
    public $user_email = "";
    public $user_mobile = "";
    public $user_role_id = "";
    public $user_role_name = "";
    public $user_id = "";
    public $all_modules = array();    
    public $user_photo_url = "";

	public function __construct()
    {
        parent::__construct();
        
        $this->is_logged_in();
        
        $this->load->model("admin_model");

        /*$all_modules =

        
        $data['modules'] = $this->common_model->get_modules();
        $data['permissions'] = $this->common_model->get_permissions();

        
        $arr = array("role_id"=>$this-> user_role_id);
        $data['permitted_modules'] = $this->common_model->get_permitted_modules($arr); */
    }


    public function is_logged_in()
    {
        $data = $this->session->all_userdata();
        
        if(isset($data['user_id']) && !empty($data['user_id']))
        {
            $this->user_first_name = ucfirst($data['first_name']);

            $this->user_last_name = ucfirst($data['last_name']);

            $this->user_fullname = ucwords($data['first_name']." ".$data['last_name']);

            $this->user_email = $data['email'];

            $this->user_mobile = $data['mobile'];

            $this->user_role_id = $data['role_id'];

            $this->user_role_name = $data['role_name'];

            $this->user_id = $data['user_id'];            

            $this->user_photo_url = $data['user_photo_url'];

            $this->user_created_date = date("M, Y", strtotime($data['created_date']));

            return true;
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."login/logout'; </script>"; die;
        }

    }


	public function index()
	{
		/*$modules = $this->common_model->get_permitted_modules(array("user_id"=>$this->user_id, "role_id"=>$this->user_role_id));

        p($modules);*/

        $this->load->view('includes/main_header');
		$this->load->view('dashboard');
		$this->load->view('includes/main_footer');
	}


    function get_dd_list()
    {
        $typ = $this->input->post('typ');
        $id = $this->input->post('parent_id');
        $uid = $this->input->post('uid');

        $arr = array("typ"=>$typ, "id"=>$id, "uid"=>$uid);

        $data = $this->common_model->get_dd_list($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>1,"message"=>"","records"=>$data)); 
            die;
        }
        else
        {
            echo json_encode(array("status"=>0,"message"=>"")); 
            die;
        }    
    }


    /*----------------------------------------------------------
    Unit Management
    ----------------------------------------------------------*/
    public function units()
    {
        $this->load->view('includes/main_header');
        $this->load->view('units');
        $this->load->view('includes/main_footer');
    }


    public function units_add()
    {
        $this->load->view('includes/main_header');
        $this->load->view('units_add');
        $this->load->view('includes/main_footer');
    }


    public function units_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"units", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);

        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/units'; </script>";
            die;
        }

        $arr = array("typ"=>"subunits", "id"=>$edit_id);        
        $data['sub_units_details'] = $this->admin_model->get_sub_units($edit_id);
        
        $this->load->view('includes/main_header');
        $this->load->view('units_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function units_save()
    {
        $unit_name = $this->input->post('unit_name');
        $sub_unit_name = $this->input->post('sub_unit_name');
        $hdn_id = $this->input->post('hdn_id');

        if($unit_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Unit Name."));
            
            die;    
        }
        elseif(count($sub_unit_name) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter atleast one Sub Unit Name."));
            
            die;    
        }

        $arr = array("typ"=>"units", "id"=>$hdn_id, "name"=>$unit_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Unit name already exist.")); 
            die;
        }
        else
        {            
            $data = array("unit_name" => $unit_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $unit_pid = $this->dbaccess_model->insert("units", $data);

                foreach($sub_unit_name as $snm)
                {
                    $data = array("unit_id" => $unit_pid, 
                    "subunit_name" => $snm,                    
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id,
                    "updated_date" => date(DATETIME_DB),                
                    "updated_by" => $this->user_id);

                    $this->dbaccess_model->insert("subunits", $data);
                }
            }
            else
            {   
                $this->dbaccess_model->update("units", $data, array("unit_id ="=>$hdn_id));

                $unit_pid = $hdn_id;

                $sql = "UPDATE subunits SET is_deleted = 1 WHERE unit_id = $hdn_id ";
                $this->dbaccess_model->executeOnlySql($sql);

                foreach($sub_unit_name as $snm)
                {
                    $data = array("unit_id" => $unit_pid, 
                    "subunit_name" => $snm,                    
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id,
                    "updated_date" => date(DATETIME_DB),                
                    "updated_by" => $this->user_id);

                    $this->dbaccess_model->insert("subunits", $data);
                }                
            }                

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function delete_record()
    {
        $user_id = $this->user_id;
        
        $typ = $this->input->post('typ');
        $rec_id = $this->input->post('rec_id');
        
        $arr = array("typ" => $typ, "id" => $rec_id);

        if($typ == "" || $rec_id <= 0)
        {
            echo json_encode(array("status"=>1,"message"=>"Illegal request made."));
            die;
        }
        elseif(count($this->common_model->is_exist($arr)) <= 0)
        {
            echo json_encode(array("status"=>1,"message"=>"Selected record not found in the system."));
            die;
        }
        /*elseif(count($this->common_model->is_owner($services_id, $user_id, 'services')) <= 0)
        {
            echo json_encode(array("status"=>"fail","message"=> OWNER_MSG));

            die;
        }*/
        
        
        $data = array("is_deleted" => 1, 
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id
        );
        
        if($typ == "units") 
        {
            $table = $typ;
            $field = "unit_id";
        } 
        elseif($typ == "soil_type") 
        {
            $table = $typ;
            $field = "soil_type_id";
        } 
        elseif($typ == "crops") 
        {
            $table = $typ;
            $field = "crop_id";
        } 
        elseif($typ == "crop_variety") 
        {
            $table = $typ;
            $field = "crop_variety_id";
        } 
        elseif($typ == "stock_verification_reason") 
        {
            $table = $typ;
            $field = "stock_verification_reason_id";
        }
        elseif($typ == "area_of_operation") 
        {
            $this->admin_model->reset_aop($rec_id);
            echo json_encode(array("status"=>1,"message"=>"Removed successfully."));        
            die;
        }
        elseif($typ == "product_category") 
        {
            $table = $typ;
            $field = "product_category_id";
        }
        elseif($typ == "staff") 
        {
            $table = "users";
            $field = "user_id";
        }
        elseif($typ == "roles") 
        {
            $table = $typ;
            $field = "role_id";
        }
        elseif($typ == "wholesalers" || $typ == "dealers") 
        {
            $table = "salers";
            $field = "saler_id";
        }

        $this->dbaccess_model->update($table, $data, array($field." ="=>$rec_id));

        echo json_encode(array("status"=>1,"message"=>"Deleted successfully."));            
        
        die;
    }

    public function units_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
                
        //-----------------------------------------------------------------------------
        $aColumns = array('unit_name');
        $search_columns = array('unit_name');

        $sTable = "units";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*, GROUP_CONCAT(su.subunit_name) as uom
            FROM $sTable s            
            LEFT JOIN subunits su ON su.unit_id = s.unit_id AND su.is_deleted = 0
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            GROUP BY s.unit_id
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.unit_id) as count
            FROM $sTable s 
            LEFT JOIN subunits su ON su.unit_id = s.unit_id AND su.is_deleted = 0
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            GROUP BY s.unit_id
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> unit_name;

            $row[] = $aRow -> uom;            
            
            $row[] = '<a class="action_link" href="'.base_url().'admin/units_edit/'.$aRow -> unit_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("units",'.$aRow -> unit_id.');><i class="fa fa-trash"></i></a>';                       

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );
    
    
    }



    /*----------------------------------------------------------
    Soils Management
    ----------------------------------------------------------*/
    public function soil_type()
    {
        $this->load->view('includes/main_header');
        $this->load->view('soil_type');
        $this->load->view('includes/main_footer');
    }


    public function soil_type_add()
    {
        $this->load->view('includes/main_header');
        $this->load->view('soil_type_add');
        $this->load->view('includes/main_footer');
    }


    public function soil_type_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"soil_type", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/soil_type'; </script>";
            die;
        }

        $this->load->view('includes/main_header');
        $this->load->view('soil_type_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function soil_type_save()
    {
        $soil_name = $this->input->post('soil_type_name');
        $hdn_id = $this->input->post('hdn_id');

        if($soil_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Soil Type."));
            
            die;    
        }

        $arr = array("typ"=>"soil_type", "id"=>$hdn_id, "name"=>$soil_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Soil Type already exist.")); die;
        }
        else
        {            
            $data = array("soil_type_name" => $soil_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("soil_type", $data);
            }
            else
            {   
                $this->dbaccess_model->update("soil_type", $data, array("soil_type_id ="=>$hdn_id));
            }

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function soil_type_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        //$qs_name = $_POST['qs_name'];
        
        //if($qs_name != "") $quick_search .= " AND state_name LIKE '$qs_name%' ";
        
        
        //-----------------------------------------------------------------------------
        $aColumns = array('soil_type_name', 'created_date');
        $search_columns = array('soil_type_name', 'created_date');

        $sTable = "soil_type";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.soil_type_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            /*$row[] = '<div class="btn-group">
                        <button type="button" class="btn btn_action dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="'.base_url().'admin/soil_type_edit/'.$aRow -> soil_type_id.'"><i class="fa fa-pencil"></i> Edit</a></li>
                          <li><a href="javascript:void(0);" onclick=delete_record("soil_type",'.$aRow -> soil_type_id.');><i class="fa fa-trash"></i> Delete</a></li>
                        </ul>
                      </div>';*/

            $row[] = $aRow -> soil_type_name;                       

            $row[] = date(DATE, strtotime($aRow -> created_date));

            $row[] = '<a class="action_link" href="'.base_url().'admin/soil_type_edit/'.$aRow -> soil_type_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("soil_type",'.$aRow -> soil_type_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    /*----------------------------------------------------------
    State Management
    ----------------------------------------------------------*/
    public function states()
    {
        $this->load->view('includes/main_header');
        $this->load->view('states');
        $this->load->view('includes/main_footer');
    }


	public function states_list()
    {        
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        //$qs_name = $_POST['qs_name'];
        
        //if($qs_name != "") $quick_search .= " AND state_name LIKE '$qs_name%' ";
        
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('state_id','state_name');

        $sTable = "states";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.state_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
                        
            $row[] = $aRow -> state_id;

            $row[] = $aRow -> state_name;

            $row[] = "";
            
            //$row[] = $aRow -> status;
            /*if($aRow -> is_discharged == 1 && $aRow -> discharge_date!="")
            {
                $url1 = base_url()."opd/patient_details/".$aRow->patient_id."/".$aRow->id; 
                $url2 = base_url()."opd/opd_print_patient_details/".$aRow->patient_id."/".$aRow->id;                
                $row[] = '<a href="'.$url1.'"><button type="button" class="btn btn-'.BTN_VIEW.' actionbtn" title="View Details"><i class="fa fa-fw fa-eye"></i>View</button></a>&nbsp;<a href="'.$url2.'" target="_blank"><button type="button" class="btn btn-'.BTN_PRINT.' actionbtn" title="Print Report"><i class="fa fa-fw fa-print"></i>Print</button></a>';
            }
            else
            {
                $url1 = base_url()."opd/patient_details/".$aRow->patient_id."/".$aRow->id; 
                $url2 = base_url()."opd/opd_print_patient_details/".$aRow->patient_id."/".$aRow->id;
                $url3 = base_url()."opd/opd_edit/".$aRow->patient_id."/".$aRow->id."/02";                               
                $url5 = base_url()."opd/discharge/".$aRow->patient_id."/".$aRow->id;

                $row[] = '<a href="'.$url1.'"><button type="button" class="btn btn-'.BTN_VIEW.' actionbtn" title="View Details"><i class="fa fa-fw fa-eye"></i>View</button></a>&nbsp;<a href="'.$url3.'"><button type="button" class="btn btn-primary actionbtn" title="Edit Details"><i class="fa fa-fw fa-eye"></i>Edit</button></a>&nbsp;<a href="'.$url2.'" target="_blank"><button type="button" class="btn btn-'.BTN_PRINT.' actionbtn" title="Print Report"><i class="fa fa-fw fa-print"></i>Print</button></a>';
            }*/            

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );
    
    } 



    /*----------------------------------------------------------
    Roles and Permissions
    ----------------------------------------------------------*/
    public function roles_and_permissions()
    {
        $this->load->view('includes/main_header');
        $this->load->view('roles_and_permissions');
        $this->load->view('includes/main_footer');
    }

    public function roles_add()
    {        
        $this->load->view('includes/main_header');
        $this->load->view('roles_add');
        $this->load->view('includes/main_footer');
    }

    public function roles_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"roles", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);

        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/roles_and_permissions'; </script>";
            die;
        }
        
        $this->load->view('includes/main_header');
        $this->load->view('roles_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function roles_save()
    {
        $role_name = $this->input->post('role_name');
        $hdn_id = $this->input->post('hdn_id');

        if($role_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Role Name."));
            
            die;    
        }

        $arr = array("typ"=>"roles", "id"=>$hdn_id, "name"=>$role_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Role Name already exist.")); die;
        }
        else
        {            
            $data = array("role_name" => $role_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("roles", $data);
            }
            else
            {   
                $this->dbaccess_model->update("roles", $data, array("role_id ="=>$hdn_id));
            }

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function roles_and_permissions_set()
    {
        $sel_role_id = $this->uri->segment(3);

        if(!isset($sel_role_id) || $sel_role_id <= 0)
        {
            echo "<script> window.location.href = '".base_url()."roles_and_permissions'; </script>"; 
            die;
        }

        $data['sel_role_id'] = $sel_role_id;

        $data['roles'] = $this->common_model->get_roles();
        $data['modules'] = $this->common_model->get_modules();
        $data['permissions'] = $this->common_model->get_permissions();

        
        $arr = array("role_id"=>$sel_role_id);
        $data['permitted_modules'] = $this->common_model->get_permitted_modules($arr);
        
        
        $this->load->view('includes/main_header');
        $this->load->view('roles_and_permissions_set', $data);
        $this->load->view('includes/main_footer');
    }

    
    public function roles_and_permissions_save()
    {
        $role_name = $this->input->post('role_name');
        $chkbox = $this->input->post('chkbox');

        if($role_name == "" || $role_name <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Role Name."));
            
            die;    
        }
        elseif(count($chkbox) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please set atleast one permission to any module."));
            
            die;    
        }
        
        $this->common_model->clear_permisions(array("role_id" => $role_name));
                    
        foreach($chkbox as $module_id => $arr)
        {
            foreach($arr as $permission_id)
            {
                $data = array("role_id" => $role_name, 
                    "module_id" => $module_id, 
                    "permission_id" => $permission_id, 
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                );

                $this->dbaccess_model->insert("access_modules", $data);
            }
        }


        foreach($chkbox as $module_id => $arr)
        {
            $this->common_model->find_and_clear_permisions(array("role_id" => $role_name, "module_id" => $module_id));
        }
        
        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); 

        die;        
    }


    public function roles_and_permissions_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        //$qs_name = $_POST['qs_name'];
        
        //if($qs_name != "") $quick_search .= " AND state_name LIKE '$qs_name%' ";
        
        
        //-----------------------------------------------------------------------------
        $aColumns = array('role_name');
        $search_columns = array('role_name');

        $sTable = "roles";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.role_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> role_name; 

            $btn = "";
            if($aRow-> role_id >= 5)
            {
                $btn = '<a class="action_link" href="'.base_url().'admin/roles_edit/'.$aRow -> role_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("roles",'.$aRow -> role_id.');><i class="fa fa-trash"></i></a>&nbsp;';
            }

            $row[] = $btn.'<a class="action_link" href="'.base_url().'admin/roles_and_permissions_set/'.$aRow -> role_id.'"><i class="fa fa-cog"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Crops Management
    ----------------------------------------------------------*/
    public function crops()
    {
        $this->load->view('includes/main_header');
        $this->load->view('crops');
        $this->load->view('includes/main_footer');
    }


    public function crops_add()
    {
        $this->load->view('includes/main_header');
        $this->load->view('crops_add');
        $this->load->view('includes/main_footer');
    }


    public function crops_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"crops", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);

        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/crops'; </script>";
            die;
        }
        
        $this->load->view('includes/main_header');
        $this->load->view('crops_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function crops_save()
    {
        $crop_name = $this->input->post('crop_name');
        $hdn_id = $this->input->post('hdn_id');

        if($crop_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Crop Name."));
            
            die;    
        }

        $arr = array("typ"=>"crops", "id"=>$hdn_id, "name"=>$crop_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Crop Name already exist.")); die;
        }
        else
        {            
            $data = array("crop_name" => $crop_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("crops", $data);
            }
            else
            {   
                $this->dbaccess_model->update("crops", $data, array("crop_id ="=>$hdn_id));
            }
            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function crops_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        
        //-----------------------------------------------------------------------------
        $aColumns = array('crop_name', 'created_date');
        $search_columns = array('crop_name', 'created_date');

        $sTable = "crops";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.crop_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 

            $row[] = $aRow -> crop_name;                       

            $row[] = date(DATE, strtotime($aRow -> created_date));

            $row[] = '<a class="action_link" href="'.base_url().'admin/crops_edit/'.$aRow -> crop_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("crops",'.$aRow -> crop_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    /*----------------------------------------------------------
    Crops Variety Management
    ----------------------------------------------------------*/
    public function crops_variety()
    {
        $this->load->view('includes/main_header');
        $this->load->view('crops_variety');
        $this->load->view('includes/main_footer');
    }


    public function crops_variety_add()
    {
        $data['crops'] = $this->common_model->get_dd_list(array('typ' => 'crops'));

        $this->load->view('includes/main_header');
        $this->load->view('crops_variety_add', $data);
        $this->load->view('includes/main_footer');
    }


    public function crops_variety_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"crop_variety", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/crops_variety'; </script>";
            die;
        }

        $data['crops'] = $this->common_model->get_dd_list(array('typ' => 'crops'));
        $data['crop_variety_id'] = $edit_id;

        $this->load->view('includes/main_header');
        $this->load->view('crops_variety_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function crops_variety_save()
    {
        $crop_name = $this->input->post('crop_name');
        $sub_name = $this->input->post('sub_name');
        $hdn_id = $this->input->post('hdn_id');

        if($crop_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Crop Name."));
            
            die;    
        }
        elseif(count($sub_name) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter atleast one Crop Variety Name."));
            
            die;    
        }

        
        foreach($sub_name as $vnm)
        {    
            $arr = array("typ"=>"crop_variety_mas", "id"=>$crop_name, "name"=>$vnm);
            $data = $this->common_model->is_already_exist($arr);

            if(isset($data) && !empty($data))
            {
                echo json_encode(array("status"=>0,"message"=>"<b>".$vnm."</b> Variety Name already exist.")); 
                die;
            }
        }


        foreach($sub_name as $vnm)
        {
            $data = array("crop_id" => $crop_name, 
            "crop_variety_name" => $vnm,                    
            "created_date" => date(DATETIME_DB),                
            "created_by" => $this->user_id,
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id);

            $this->dbaccess_model->insert("crop_variety", $data);
        }

        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;     
    }


    public function crops_variety_edit_save()
    {
        $crop_name = $this->input->post('crop_name');
        $sub_name = $this->input->post('sub_name');
        $hdn_id = $this->input->post('hdn_id');

        if($crop_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Crop Name."));
            
            die;    
        }
        elseif($sub_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Crop Variety Name."));
            
            die;    
        }

        
        $arr = array("typ"=>"crop_variety", "id"=>$hdn_id, "name"=>$sub_name, "parent_id"=>$crop_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Crop Variety Name already exist.")); 
            die;
        }
        
        $data = array("crop_variety_name" => $sub_name,                    
        "updated_date" => date(DATETIME_DB),                
        "updated_by" => $this->user_id);
        
        $this->dbaccess_model->update("crop_variety", $data, array("crop_variety_id ="=>$hdn_id));

        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;     
    }


    public function crops_variety_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        
        //-----------------------------------------------------------------------------
        $aColumns = array('s.crop_variety_name', 'c.crop_name');
        $search_columns = array('s.crop_variety_name', 'c.crop_name');

        $sTable = "crop_variety";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*, c.crop_name
            FROM $sTable s
            INNER JOIN crops c ON c.crop_id = s.crop_id             
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.crop_variety_id) as count
            FROM $sTable s            
            INNER JOIN crops c ON c.crop_id = s.crop_id
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 

            $row[] = $aRow -> crop_variety_name;                       

            $row[] = $aRow -> crop_name;       

            $row[] = '<a class="action_link" href="'.base_url().'admin/crops_variety_edit/'.$aRow -> crop_variety_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("crop_variety",'.$aRow -> crop_variety_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Stock Verification Reason Management
    ----------------------------------------------------------*/
    public function stock_verification_reason()
    {
        $this->load->view('includes/main_header');
        $this->load->view('stock_verification_reason');
        $this->load->view('includes/main_footer');
    }


    public function stock_verification_reason_add()
    {
        $this->load->view('includes/main_header');
        $this->load->view('stock_verification_reason_add');
        $this->load->view('includes/main_footer');
    }


    public function stock_verification_reason_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"stock_verification_reason", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/stock_verification_reason'; </script>";
            die;
        }

        $this->load->view('includes/main_header');
        $this->load->view('stock_verification_reason_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function stock_verification_reason_save()
    {
        $stock_verification_reason_name = $this->input->post('stock_verification_reason_name');
        $hdn_id = $this->input->post('hdn_id');

        if($stock_verification_reason_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Reason Name."));
            
            die;    
        }

        $arr = array("typ"=>"stock_verification_reason", "id"=>$hdn_id, "name"=>$stock_verification_reason_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Stock Verification Reason already exist.")); die;
        }
        else
        {            
            $data = array("stock_verification_reason_name" => $stock_verification_reason_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("stock_verification_reason", $data);
            }
            else
            {   
                $this->dbaccess_model->update("stock_verification_reason", $data, array("stock_verification_reason_id ="=>$hdn_id));
            }

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function stock_verification_reason_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";        
        //$qs_name = $_POST['qs_name'];
        
        //if($qs_name != "") $quick_search .= " AND state_name LIKE '$qs_name%' ";
        
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('stock_verification_reason_name');

        $sTable = "stock_verification_reason";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.stock_verification_reason_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> stock_verification_reason_name;                       

            $row[] = '<a class="action_link" href="'.base_url().'admin/stock_verification_reason_edit/'.$aRow -> stock_verification_reason_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("stock_verification_reason",'.$aRow -> stock_verification_reason_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    /*----------------------------------------------------------
    Product Category Management
    ----------------------------------------------------------*/
    public function product_category()
    {
        $this->load->view('includes/main_header');
        $this->load->view('product_category');
        $this->load->view('includes/main_footer');
    }


    public function product_category_add()
    {
        $this->load->view('includes/main_header');
        $this->load->view('product_category_add');
        $this->load->view('includes/main_footer');
    }


    public function product_category_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"product_category", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/product_category'; </script>";
            die;
        }

        $this->load->view('includes/main_header');
        $this->load->view('product_category_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function product_category_save()
    {
        $product_category_name = $this->input->post('product_category_name');
        $hdn_id = $this->input->post('hdn_id');

        if($product_category_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Product Category."));
            
            die;    
        }

        $arr = array("typ"=>"product_category", "id"=>$hdn_id, "name"=>$product_category_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Product Category already exist.")); die;
        }
        else
        {            
            $data = array("product_category_name" => $product_category_name, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("product_category", $data);
            }
            else
            {   
                $this->dbaccess_model->update("product_category", $data, array("product_category_id ="=>$hdn_id));
            }

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function product_category_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";         
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('product_category_name');

        $sTable = "product_category";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.product_category_id) as count
            FROM $sTable s            
            WHERE s.is_deleted = 0 $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> product_category_name;                       

            $row[] = '<a class="action_link" href="'.base_url().'admin/product_category_edit/'.$aRow -> product_category_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("product_category",'.$aRow -> product_category_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Products Management
    ----------------------------------------------------------*/
    public function products()
    {
        $this->load->view('includes/main_header');
        $this->load->view('products');
        $this->load->view('includes/main_footer');
    }


    public function products_add()
    {
        $arr = array("typ"=>'product_category');        
        $data['product_category'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>'subunits');        
        $data['subunits'] = ddg($this->common_model->get_dd_list($arr));

        $this->load->view('includes/main_header');
        $this->load->view('products_add', $data);
        $this->load->view('includes/main_footer');
    }


    public function products_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"products", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/products'; </script>";
            die;
        }

        $arr = array("typ"=>'product_category');        
        $data['product_category'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>'subunits');        
        $data['subunits'] = ddg($this->common_model->get_dd_list($arr));
        
        $this->load->view('includes/main_header');
        $this->load->view('products_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function products_save()
    {
        $product_category_name = $this->input->post('product_category_name');
        $product_name = $this->input->post('product_name');
        $product_subunit = $this->input->post('product_subunit');
        $product_cost = $this->input->post('product_cost');

        $hdn_id = $this->input->post('hdn_id');

        if($product_category_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Product Category."));
            die;    
        }
        elseif($product_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Product Name."));
            die;    
        }
        elseif($product_subunit == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select UoM."));
            die;    
        }
        elseif($product_cost == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Product Cost."));
            die;    
        }
        elseif($product_cost <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Product Cost greater than zero."));
            die;    
        }

        $arr = array("typ"=>"products", "id"=>$hdn_id, "name"=>$product_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Product Name already exist.")); die;
        }
        else
        {            
            $data = array("product_category_id" => $product_category_name, 
                "product_name" => $product_name, 
                "subunit_id" => $product_subunit, 
                "cost" => $product_cost, 
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_date"] = date(DATETIME_DB);
                $data["created_by"] = $this->user_id;
                
                $this->dbaccess_model->insert("products", $data);
            }
            else
            {   
                $this->dbaccess_model->update("products", $data, array("product_id ="=>$hdn_id));
            }

            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function products_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";         
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('p.product_name', 'pc.product_category_name', 's.subunit_name', 'p.cost');

        $sTable = "products";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS p.*, pc.product_category_name, s.subunit_name
            FROM $sTable p 
            INNER JOIN product_category pc ON p.product_category_id = pc.product_category_id
            INNER JOIN subunits s ON p.subunit_id = s.subunit_id
            WHERE p.is_deleted = 0 $quick_search $sWhere 
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(p.product_id) as count
            FROM $sTable p 
            INNER JOIN product_category pc ON p.product_category_id = pc.product_category_id
            INNER JOIN subunits s ON p.subunit_id = s.subunit_id
            WHERE p.is_deleted = 0 $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> product_name;

            $row[] = $aRow -> product_category_name;                       

            $row[] = $aRow -> subunit_name;

            $row[] = $aRow -> cost;

            $row[] = '<a class="action_link" href="'.base_url().'admin/products_edit/'.$aRow -> product_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("products",'.$aRow -> product_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Area Of Operation Management
    ----------------------------------------------------------*/
    public function area_of_operation()
    {                
        $this->load->view('includes/main_header');
        $this->load->view('area_of_operation');
        $this->load->view('includes/main_footer');
    }

    public function area_of_operation_set()
    {
        $arr = array("typ"=>'states_all');
        
        $data['states_all'] = $this->common_model->get_dd_list($arr);
                
        $this->load->view('includes/main_header');
        $this->load->view('area_of_operation_set', $data);
        $this->load->view('includes/main_footer');
    }


        
    public function area_of_operation_save()
    {
        $role_name = $this->input->post('dchk');
        $chkbox = $this->input->post('chkbox');

        if(count($chkbox) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please set atleast one District Name."));
            
            die;    
        }
        
                            
        foreach($chkbox as $pid => $arr)
        {
            $this->admin_model->reset_aop($pid);

            foreach($arr as $cid)
            {
                $data = array("is_district_active" => 1,                     
                    "updated_date" => date(DATETIME_DB),                
                    "updated_by" => $this->user_id
                );

                $this->dbaccess_model->update("districts", $data, array("district_id ="=>$cid, "state_id ="=>$pid));
            }

            $data = array("is_state_active" => 1,                     
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );
            $this->dbaccess_model->update("states", $data, array("state_id ="=>$pid));
        }

        $arr = array("typ"=>'states_aop');        
        $states = $this->common_model->get_dd_list($arr);
        if(isset($states) && !empty($states))
        {
            foreach($states as $obj)
            {
                $snm = strtolower(trim($obj-> state_name));

                $path = PATH_ALBUM.$snm;
                if(!file_exists($path))
                {
                    mkdir($path, 0777);
                }


                $arr = array("typ"=>'district_by_state_aop', "id" => $obj-> state_id);        
                $districts = $this->common_model->get_dd_list($arr);
                if(isset($districts) && !empty($districts))
                {
                    foreach($districts as $obj1)
                    {
                        $dnm = strtolower(trim($obj1-> district_name));

                        $dpath = PATH_ALBUM.$snm."/".$dnm;
                        if(!file_exists($dpath))
                        {
                            mkdir($dpath, 0777);

                            mkdir($dpath."/farmer", 0777);
                            mkdir($dpath."/demo", 0777);
                        }
                    }
                }
            }
        }        
        
        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); 

        die;        
    }


    public function area_of_operation_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = ""; 
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.state_name', 'd.district_name');

        $sTable = "states";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*, GROUP_CONCAT(d.district_name SEPARATOR ', ') as district_name
            FROM $sTable s 
            INNER JOIN districts d ON s.state_id = d.state_id AND d.is_deleted = 0 AND d.is_district_active = 1
            WHERE s.is_deleted = 0 AND s.is_state_active = 1 $quick_search $sWhere 
            GROUP BY s.state_id
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.state_id) as count
            FROM $sTable s            
            INNER JOIN districts d ON s.state_id = d.state_id AND d.is_deleted = 0 AND d.is_district_active = 1
            WHERE s.is_deleted = 0 AND s.is_state_active = 1 $quick_search $sWhere
            GROUP BY s.state_id
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = $aRow -> state_name;

            $row[] = $aRow -> district_name;

            $row[] = '<a class="action_link" href="javascript:void(0);" onclick=delete_record("area_of_operation",'.$aRow -> state_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Staff Management
    ----------------------------------------------------------*/
    public function staff()
    {
        $this->load->view('includes/main_header');
        $this->load->view('staff');
        $this->load->view('includes/main_footer');
    }


    public function staff_add()
    {
        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>'roles');        
        $data['roles'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>'reporting');        
        $data['reporting'] = $this->common_model->get_dd_list($arr);
        
        $data['keycode'] = $this->common_model->generate_key(5);

        $this->load->view('includes/main_header');
        $this->load->view('staff_add', $data);
        $this->load->view('includes/main_footer');
    }


    public function staff_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"staff", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/staff'; </script>";
            die;
        }

        $arr = array("typ"=>'states_aop_user', "uid"=>$edit_id);        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ"=>'roles');        
        $data['roles'] = $this->common_model->get_dd_list($arr);
        
        $arr = array("typ"=>'reporting');        
        $data['reporting'] = $this->common_model->get_dd_list($arr);

        $this->load->view('includes/main_header');
        $this->load->view('staff_edit', $data);
        $this->load->view('includes/main_footer');
    }


    public function staff_save()
    {
        $role_id = $this->input->post('role_name');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $admin_access = $this->input->post('admin_access');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $cpassword = $this->input->post('cpassword');
        $app_access = $this->input->post('app_access');
        $keycode = $this->input->post('keycode');
        $chkbox = $this->input->post('chkbox');

        
        $gender = $this->input->post('gender');
        $dob = $this->input->post('dob');
        $qualification = $this->input->post('qualification');
        $doj = $this->input->post('doj');
        $experience = $this->input->post('experience');

        $cstates_name = $this->input->post('cstates_name');
        $ccity_name = $this->input->post('ccity_name');
        $caddress = $this->input->post('caddress');

        $pstates_name = $this->input->post('pstates_name');
        $pcity_name = $this->input->post('pcity_name');
        $paddress = $this->input->post('paddress');

        $reporting_to = $this->input->post('reporting_to');

        $hdn_id = $this->input->post('hdn_id');

        if($role_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Role Name."));
            die;    
        }
        elseif($first_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter First Name."));
            die;    
        }
        elseif($last_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Last Name."));
            die;    
        }        
        elseif($email == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
            die;    
        }
        elseif($mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Mobile Number."));
            die;    
        }
        elseif(strlen($mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        elseif($username == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Username."));
            die;    
        }
        elseif($password == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Password."));
            die;    
        }
        elseif($cpassword == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Confirm Password."));
            die;    
        }
        elseif($password != $cpassword)
        {
            echo json_encode(array("status"=>0,"message"=>"Password and Confirm Password not matches."));
            die;    
        }
        elseif($keycode == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Key Code."));
            die;    
        }
        elseif($cstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address State Name."));
            die;
        }
        elseif($ccity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address City Name."));
            die;
        }
        elseif($caddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Current Address Details."));
            die;
        }
        elseif($pstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address State Name."));
            die;
        }
        elseif($pcity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address City Name."));
            die;
        }
        elseif($paddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Permanent Address Details."));
            die;
        }
        elseif($reporting_to == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Reporting Manager."));
            die;
        }
        elseif(count($chkbox) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Area Of Operation."));
            die;    
        }

        $arr = array("typ"=>"staff", "id"=>$hdn_id, "name"=>$username);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Username already exist. Please choose different Username.")); die;
        }
        else
        {            
            $data = array("role_id" => $role_id, 
                "reporting_to" => $reporting_to, 
                "username" => $username, 
                "keycode" => $keycode, 
                "first_name" => $first_name, 
                "last_name" => $last_name, 
                "email" => $email, 
                "mobile" => $mobile, 
                
                "cstate" => $cstates_name,
                "ccity" => $ccity_name,
                "caddress" => $caddress,
                "pstate" => $pstates_name,
                "pcity" => $pcity_name,
                "paddress" => $paddress,

                "gender" => $gender, 
                "dob" => $dob, 
                "qualification" => $qualification, 
                "doj" => $doj, 
                "experience" => $experience, 
                "admin_access" => $admin_access, 
                "app_access" => $app_access, 

                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id,
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

                            
            $userid = $this->dbaccess_model->insert("users", $data);
            
            if($userid)
            {
                //Set Password-----------------------------------------
                $data = array("user_id" => $userid,
                    "password" => md5($password),
                    "password_for" => 1,
                    "created_date" => date(DATETIME_DB),
                    "updated_date" => date(DATETIME_DB)
                );
                $this->dbaccess_model->insert("users_login", $data);

                $data = array("user_id" => $userid,
                    "password" => md5($keycode),
                    "password_for" => 2,
                    "created_date" => date(DATETIME_DB),
                    "updated_date" => date(DATETIME_DB)
                );
                $this->dbaccess_model->insert("users_login", $data);


                //Set Area Of Operation-----------------------------------------
                $this->admin_model->reset_aop_of_user($userid);

                foreach($chkbox as $pid => $arr)
                {
                    foreach($arr as $cid)
                    {   
                        $getarr = array("typ"=>"district", "id"=>$cid);        
                        $details = $this->common_model->get_detail($getarr);
                        $state_name = $district_name = "";
                        if(isset($details) && !empty($details))
                        {
                            $state_name = $details-> state_name;
                            $district_name = $details-> district_name;
                        }

                        $data = array("user_id" => $userid,
                            "state_id" => $pid,
                            "district_id" => $cid,
                            "state_name" => $state_name,
                            "district_name" => $district_name,                     
                            "created_date" => date(DATETIME_DB),                
                            "created_by" => $this->user_id
                        );

                        $this->dbaccess_model->insert("users_area", $data);
                    }
                }
            }
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }



    public function staff_save_edit()
    {
        $role_id = $this->input->post('role_name');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $admin_access = $this->input->post('admin_access');
        $app_access = $this->input->post('app_access');        
        $chkbox = $this->input->post('chkbox');

        $address = $this->input->post('address');
        $gender = $this->input->post('gender');
        $dob = $this->input->post('dob');
        $qualification = $this->input->post('qualification');
        $doj = $this->input->post('doj');
        $experience = $this->input->post('experience');

        $cstates_name = $this->input->post('cstates_name');
        $ccity_name = $this->input->post('ccity_name');
        $caddress = $this->input->post('caddress');

        $pstates_name = $this->input->post('pstates_name');
        $pcity_name = $this->input->post('pcity_name');
        $paddress = $this->input->post('paddress');

        $reporting_to = $this->input->post('reporting_to');

        $hdn_id = $this->input->post('hdn_id');

        if($role_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Role Name."));
            die;    
        }
        elseif($first_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter First Name."));
            die;    
        }
        elseif($last_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Last Name."));
            die;    
        }        
        elseif($email == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
            die;    
        }
        elseif($mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Mobile Number."));
            die;    
        }
        elseif(strlen($mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        elseif($cstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address State Name."));
            die;
        }
        elseif($ccity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address City Name."));
            die;
        }
        elseif($caddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Current Address Details."));
            die;
        }
        elseif($pstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address State Name."));
            die;
        }
        elseif($pcity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address City Name."));
            die;
        }
        elseif($paddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Permanent Address Details."));
            die;
        }
        elseif($reporting_to == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Reporting Manager."));
            die;
        }
        elseif(count($chkbox) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Area Of Operation."));
            die;    
        }

        if(true)
        {            
            $data = array("reporting_to" => $reporting_to,
                "first_name" => $first_name, 
                "last_name" => $last_name, 
                "email" => $email, 
                "mobile" => $mobile, 
                
                "cstate" => $cstates_name,
                "ccity" => $ccity_name,
                "caddress" => $caddress,
                "pstate" => $pstates_name,
                "pcity" => $pcity_name,
                "paddress" => $paddress,

                "gender" => $gender, 
                "dob" => date(DATE_DB, strtotime($dob)), 
                "qualification" => $qualification, 
                "doj" => date(DATE_DB, strtotime($doj)), 
                "experience" => $experience, 
                "admin_access" => $admin_access, 
                "app_access" => $app_access, 

                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

                            
            $this->dbaccess_model->update("users", $data, array("user_id" => $hdn_id));
            $userid = $hdn_id;

            if($userid)
            {
                //Set Area Of Operation-----------------------------------------
                $this->admin_model->reset_aop_of_user($userid);

                foreach($chkbox as $pid => $arr)
                {
                    foreach($arr as $cid)
                    {   
                        $getarr = array("typ"=>"district", "id"=>$cid);        
                        $details = $this->common_model->get_detail($getarr);
                        $state_name = $district_name = "";
                        if(isset($details) && !empty($details))
                        {
                            $state_name = $details-> state_name;
                            $district_name = $details-> district_name;
                        }

                        $data = array("user_id" => $userid,
                            "state_id" => $pid,
                            "district_id" => $cid,
                            "state_name" => $state_name,
                            "district_name" => $district_name,                     
                            "created_date" => date(DATETIME_DB),                
                            "created_by" => $this->user_id
                        );

                        $this->dbaccess_model->insert("users_area", $data);
                    }
                }
            }
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function staff_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";         
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.first_name', 's.last_name', 's.email', 's.mobile', 'ua.state_name', 'ua.district_name', 'r.role_name', 's.keycode', 's.app_access');

        $sTable = "users";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*, GROUP_CONCAT(CONCAT(ua.district_name) SEPARATOR ', ') as aop, GROUP_CONCAT(CONCAT(ua.state_name) SEPARATOR ',') as state_name, r.role_name, r.role_id
            FROM $sTable s             
            LEFT JOIN users_area ua ON ua.user_id = s.user_id
            LEFT JOIN roles r ON r.role_id = s.role_id
            WHERE s.is_deleted = 0 $quick_search $sWhere 
            GROUP BY s.user_id
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.user_id) as count
            FROM $sTable s             
            LEFT JOIN users_area ua ON ua.user_id = s.user_id
            LEFT JOIN roles r ON r.role_id = s.role_id
            WHERE s.is_deleted = 0 $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = ucwords($aRow-> first_name." ".$aRow-> last_name)."&nbsp;<small>(".$aRow-> role_name.")</small>";

            $row[] = $aRow-> email;

            $row[] = $aRow-> mobile;


            $arr = explode(",", $aRow-> state_name);
            $arr = array_unique($arr);
            $str = implode(", ", $arr);
            $row[] = $str;     


            if($aRow-> role_id == 2) $txt = "All Districts"; 
            elseif($aRow-> role_id != 3)  $txt = "HO"; 
            else $txt = $aRow-> aop;

            $row[] = $txt;

            $row[] = $aRow-> keycode."&nbsp;<small>(".ucfirst($aRow-> app_access).")</small>";

            $row[] = '<a class="action_link" href="'.base_url().'admin/staff_edit/'.$aRow -> user_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("staff",'.$aRow -> user_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    /*----------------------------------------------------------
    Wholesaler Management
    ----------------------------------------------------------*/
    public function wholesalers()
    {
        $this->load->view('includes/main_header');
        $this->load->view('wholesalers');
        $this->load->view('includes/main_footer');
    }


    public function wholesalers_add()
    {
        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $this->load->view('includes/main_header');
        $this->load->view('wholesalers_add', $data);
        $this->load->view('includes/main_footer');
    }


    public function wholesalers_import()
    {
        $this->load->view('includes/main_header');
        $this->load->view('wholesalers_import');
        $this->load->view('includes/main_footer');
    }


    public function wholesalers_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"wholesalers", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/wholesalers'; </script>";
            die;
        }

        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);
        
        $this->load->view('includes/main_header');
        $this->load->view('wholesalers_edit', $data);
        $this->load->view('includes/main_footer');
    }


    /*----------------------------------------------------------
    Dealers Management
    ----------------------------------------------------------*/
    public function dealers()
    {
        $this->load->view('includes/main_header');
        $this->load->view('dealers');
        $this->load->view('includes/main_footer');
    }


    public function dealers_add()
    {
        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $this->load->view('includes/main_header');
        $this->load->view('dealers_add', $data);
        $this->load->view('includes/main_footer');
    }


    public function dealers_import()
    {
        $this->load->view('includes/main_header');
        $this->load->view('dealers_import');
        $this->load->view('includes/main_footer');
    }


    public function dealers_edit()
    {   
        $edit_id = $this->uri->segment(3);

        $arr = array("typ"=>"dealers", "id"=>$edit_id);        
        $data['details'] = $this->common_model->get_detail($arr);
        
        if(!isset($data['details']) || empty($data['details']))
        {
            //echo "<script> alert('Selected record not found in the system.'); </script>";  
            echo "<script> window.location.href = '".base_url()."admin/dealers'; </script>";
            die;
        }

        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);
        
        $this->load->view('includes/main_header');
        $this->load->view('dealers_edit', $data);
        $this->load->view('includes/main_footer');
    }

    public function salers_save()
    {
        $hdn_id = $this->input->post('hdn_id');
        $saler_sys_id = $this->input->post('saler_sys_id');
        $saler_type = $this->input->post('saler_type');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');

        $cstates_name = $this->input->post('cstates_name');
        $ccity_name = $this->input->post('ccity_name');
        $caddress = $this->input->post('caddress');

        $contact_person = $this->input->post('contact_person');
        $state_head = $this->input->post('state_head');
        $district_executive = $this->input->post('district_executive');

        $saler_type_txt = "wholesalers";
        if($saler_type == 2)
        {
            $saler_type_txt = "dealers";
        }

        if($saler_sys_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter ".ucfirst($saler_type_txt)." ID."));
            die;    
        }
        elseif($first_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter First Name."));
            die;    
        }
        elseif($last_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Last Name."));
            die;    
        }        
        elseif($email == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
            die;    
        }
        elseif($mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Mobile Number."));
            die;    
        }
        elseif(strlen($mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }        
        elseif($cstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select State Name."));
            die;
        }
        elseif($ccity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select District Name."));
            die;
        }
        elseif($caddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Address."));
            die;
        }        
        elseif($state_head == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select State Head."));
            die;
        }
        elseif($district_executive == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select District Executive."));
            die;
        }

        if($hdn_id <= 0)
        {
            $arr = array("typ"=>$saler_type_txt."id", "id"=>$hdn_id, "name"=>$saler_sys_id);
            $data = $this->common_model->is_already_exist($arr);    
            if(isset($data) && !empty($data))
            {
                echo json_encode(array("status"=>0,"message"=>ucfirst($saler_type_txt)." ID already exist.")); die;
            }
        }
            

        $arr = array("typ"=>$saler_type_txt, "id"=>$hdn_id, "name"=>$first_name." ".$last_name);
        $data = $this->common_model->is_already_exist($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>ucfirst($saler_type_txt)." already exist. Please choose different name.")); die;
        }
        else
        {            
            $getarr = array("typ"=>"district", "id"=>$ccity_name);        
            $details = $this->common_model->get_detail($getarr);
            $state_name = $district_name = "";
            if(isset($details) && !empty($details))
            {
                $state_name = $details-> state_name;
                $district_name = $details-> district_name;
            }

            $data = array("saler_type" => $saler_type,                 
                "first_name" => $first_name, 
                "last_name" => $last_name, 
                "email" => $email, 
                "mobile" => $mobile,                 
                "state_id" => $cstates_name,
                "district_id" => $ccity_name,
                "address" => $caddress,
                "contact_person" => $contact_person, 
                "state_head" => $state_head, 
                "district_executive" => $district_executive, 
                "state_name" => $state_name,
                "district_name" => $district_name,                
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

            if($hdn_id <= 0)
            {
                $data["created_by"] = $this->user_id;
                $data["created_date"] = date(DATETIME_DB);
                $data["saler_sys_id"] = $saler_sys_id;
                
                $this->dbaccess_model->insert("salers", $data);
            }
            else
            {   
                $this->dbaccess_model->update("salers", $data, array("saler_id ="=>$hdn_id));
            }            
                        
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }


    public function salers_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";         
        $saler_type = $_POST['saler_type'];
        
        if($saler_type == "" || $saler_type == 1) 
        {
            $saler_type = 1;
            $lbl = "wholesalers";
        }
        else
        {
            $saler_type = 2;
            $lbl = "dealers";
        }
        

        $quick_search .= " AND saler_type = $saler_type ";

        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.saler_sys_id','s.first_name', 's.last_name', 's.email', 's.mobile', 's.state_name', 's.district_name', 's.address');

        $sTable = "salers";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*
            FROM $sTable s
            WHERE s.is_deleted = 0 $quick_search $sWhere             
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.saler_id) as count
            FROM $sTable s
            WHERE s.is_deleted = 0 $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();  

            $row[] = $aRow-> saler_sys_id;          
            
            $row[] = $aRow-> first_name." ".$aRow-> last_name;

            $row[] = $aRow-> email;

            $row[] = $aRow-> mobile;

            $row[] = $aRow-> address;

            $row[] = $aRow-> district_name;

            $row[] = $aRow-> state_name;

            $row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    /*----------------------------------------------------------
    Rating Management
    ----------------------------------------------------------*/
    public function rating()
    {                
        $data['rating'] = $this->common_model->rating();

        $this->load->view('includes/main_header');
        $this->load->view('rating', $data);
        $this->load->view('includes/main_footer');
    }

    public function rating_save()
    {        
        $rating_from = $this->input->post('rating_from');
        $rating_to = $this->input->post('rating_to');
        $rating_star = $this->input->post('rating_star');

        if(count($rating_from) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter From Days."));            
            die;    
        }
        elseif(count($rating_to) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter To Days."));            
            die;    
        }
        elseif(count($rating_star) <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Rating."));            
            die;    
        }
        
                            
        foreach($rating_from as $key => $val)
        {           
            if(!isset($val) || empty($val))
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter From Days."));
                die;
            }
            elseif(!isset($rating_to[$key]) || empty($rating_to[$key]))
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter To Days."));
                die;
            }
            elseif(!isset($rating_star[$key]))
            {
                echo json_encode(array("status"=>0,"message"=>"Please select Rating."));
                die;
            }

            $data = array("rating_from" => $val,
                "rating_to" => $rating_to[$key],
                "rating_star" => $rating_star[$key],
                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );
            $this->dbaccess_model->update("rating", $data, array("rating_id ="=>$key));
        }
        
        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); 

        die;        
    
    }



    /*----------------------------------------------------------
    Upload Management
    ----------------------------------------------------------*/
    public function upload_acknowledgement1()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_acknowledgement1');

        $this->load->view('includes/main_header');
        $this->load->view('upload_acknowledgement1', $data);
        $this->load->view('includes/main_footer');
    }

    public function upload_acknowledgement2()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_acknowledgement2');

        $this->load->view('includes/main_header');
        $this->load->view('upload_acknowledgement2', $data);
        $this->load->view('includes/main_footer');
    }

    public function upload_wholesaler()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_wholesaler');

        $this->load->view('includes/main_header');
        $this->load->view('upload_wholesaler', $data);
        $this->load->view('includes/main_footer');
    }

    public function upload_retailer()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_retailer');

        $this->load->view('includes/main_header');
        $this->load->view('upload_retailer', $data);
        $this->load->view('includes/main_footer');
    }

    public function upload_retailer_pos()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_retailer_pos');

        $this->load->view('includes/main_header');
        $this->load->view('upload_retailer_pos', $data);
        $this->load->view('includes/main_footer');
    }

    public function upload_sales()
    {
        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $data['details'] = $this->admin_model->last_upload_date('upload_sales');

        $this->load->view('includes/main_header');
        $this->load->view('upload_sales', $data);
        $this->load->view('includes/main_footer');
    }


    public function upload_dispatch()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_dispatch');

        $this->load->view('includes/main_header');
        $this->load->view('upload_dispatch', $data);
        $this->load->view('includes/main_footer');
    }


    public function upload_data()
    {
        $upload_type = $this->input->post('upload_type');

        if($upload_type == "import_wholesalers" || $upload_type == "import_dealers")
        {
            $filename = $_FILES['upload_file']['name'];

            if($filename == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please select file."));
                die;    
            }
        }
        elseif($upload_type == "upload_sales")
        {            
            $cstates_name = $this->input->post('cstates_name');
            $ccity_name = $this->input->post('ccity_name');
            $upload_from_date = $this->input->post('upload_from_date');
            $upload_to_date = $this->input->post('upload_to_date');

            $filename = $_FILES['upload_file']['name'];

            if($cstates_name == "" || $cstates_name <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"Please select state."));
                die;    
            }
            elseif($ccity_name == "" || $ccity_name <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"Please select district."));
                die;    
            }
            elseif($upload_from_date == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter from date."));
                die;    
            }
            elseif($upload_to_date == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter to date."));
                die;    
            }
            elseif($filename == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please select file."));
                die;    
            }
        }
        elseif($upload_type == "upload_dispatch")
        {            
            $upload_from_date = $this->input->post('upload_from_date');
            $upload_to_date = $this->input->post('upload_to_date');

            $filename = $_FILES['upload_file']['name'];

            if($upload_from_date == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter from date."));
                die;    
            }
            elseif($upload_to_date == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter to date."));
                die;    
            }
            elseif($filename == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please select file."));
                die;    
            }
        }
        else
        {           
            $upload_date = $this->input->post('upload_date');
            $filename = $_FILES['upload_file']['name'];

            if($upload_date == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please enter date."));            
                die;    
            }
            elseif($filename == "")
            {
                echo json_encode(array("status"=>0,"message"=>"Please select file."));            
                die;    
            }
        }
            
        $ext = pathinfo($filename, PATHINFO_EXTENSION); 
        if($ext != 'xls')
        {
            echo json_encode(array("status"=>0,"message"=>"Invalid file format selected. Please choose xls file."));            
            die;
        }       
                            
        $path = "assets/uploads/import/";
        if(move_uploaded_file($_FILES['upload_file']['tmp_name'], $path.basename($filename)))
        {            
            if($upload_type == "import_wholesalers" || $upload_type == "import_dealers")
            {
                $this->import_records($upload_type, $path, $filename, 0);
            }
            else
            {    
                if($upload_type == "upload_sales")
                {
                    $dataup = array("ftype" => $upload_type,
                    "upload_state" => $cstates_name, 
                    "upload_district" => $ccity_name, 
                    "upload_from_date" => date(DATE_DB, strtotime($upload_from_date)), 
                    "upload_to_date" => date(DATE_DB, strtotime($upload_to_date)),                
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    ); 
                }
                elseif($upload_type == "upload_dispatch")
                {
                    $dataup = array("ftype" => $upload_type,                     
                    "upload_from_date" => date(DATE_DB, strtotime($upload_from_date)), 
                    "upload_to_date" => date(DATE_DB, strtotime($upload_to_date)),
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    ); 
                }
                else
                {
                    $dataup = array("ftype" => $upload_type, 
                    "upload_date" => date(DATE_DB, strtotime($upload_date)),
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                }    
                
                $upid = $this->dbaccess_model->insert("upload_files", $dataup);

                if($upid > 0)
                {
                    if($upload_type == "upload_sales")
                    {
                        $this->import_records($upload_type, $path, $filename, $upid, $cstates_name, $ccity_name, $upload_from_date, $upload_to_date);
                    }
                    elseif($upload_type == "upload_dispatch")
                    {
                        $this->import_records($upload_type, $path, $filename, $upid, "", "", $upload_from_date, $upload_to_date);
                    }
                    else
                    {
                        $this->import_records($upload_type, $path, $filename, $upid);
                    }                    
                }
                else
                {
                    echo json_encode(array("status"=>0,"message"=>"Error occur while uploading file. Please try again."));
                    die;
                }            
            }    
        }
        else
        {
            echo json_encode(array("status"=>0,"message"=>"Error occur while uploading file. Please try again."));            
            die;
        }            
    }


    public function import_records($upload_type, $path, $filename, $upid, $state_id="", $city_id="", $from_date="", $to_date="")
    {
        $this->load->library('excel_reader');
        $this->excel_reader->read($path.basename($filename));
        $worksheet = $this->excel_reader->sheets[0];
        $numRows = $worksheet['numRows'];
        $numCols = $worksheet['numCols'];
        $cells = $worksheet['cells'];
                
        $totalr = $ok = $skp = 0; $skp_str = "";

        if($upload_type == "upload_acknowledgement1")       $start = 6;
        elseif($upload_type == "upload_acknowledgement2")   $start = 6;
        elseif($upload_type == "upload_wholesaler")         $start = 7;
        elseif($upload_type == "upload_retailer")           $start = 3;
        elseif($upload_type == "upload_retailer_pos")       $start = 3; 
        elseif($upload_type == "upload_sales")              $start = 5;
        elseif($upload_type == "upload_dispatch")           $start = 4;
        elseif($upload_type == "import_wholesalers")        $start = 2;
        elseif($upload_type == "import_dealers")            $start = 2;
        else  $start = 1;

        //Format Text-------------------------------------
        for ($i=$start; $i<=$numRows; $i++) 
        {                
            for($j=1; $j<=$numCols; $j++)
            {
               if(isset($worksheet['cells'][$i][$j]) && !empty($worksheet['cells'][$i][$j]))
                    $worksheet['cells'][$i][$j] = sanitize_text($worksheet['cells'][$i][$j]); 
                else
                    $worksheet['cells'][$i][$j] = "";
            }
        }    


        if($upload_type == "upload_acknowledgement1")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7]) && isset($worksheet['cells'][$i][14]) && !empty($worksheet['cells'][$i][14]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet['cells'][$i][14]);
                    if($master_id > 0)
                    {                                                      
                        $worksheet['cells'][$i][3] = format_date($worksheet['cells'][$i][3]);
                        $worksheet['cells'][$i][20] = format_date($worksheet['cells'][$i][20]);
                        $worksheet['cells'][$i][21] = format_date($worksheet['cells'][$i][21]);
                        $worksheet['cells'][$i][34] = format_date($worksheet['cells'][$i][34],DATETIME_DB);

                        $dataset = array("file_id" => $upid,
                        "transaction_id" => $worksheet['cells'][$i][1],
                        "invoice_no"        => $worksheet['cells'][$i][2],
                        "invoice_date" => $worksheet['cells'][$i][3],
                        "marketer" => $worksheet['cells'][$i][4],
                        "manufacturer" => $worksheet['cells'][$i][5],
                        "plant" => $worksheet['cells'][$i][6],
                        "dealer_id" => $worksheet['cells'][$i][7],
                        "product_id" => $master_id,
                        "unit" => $worksheet['cells'][$i][15],
                        "quantity" => $worksheet['cells'][$i][16],
                        "quantity_mt" => $worksheet['cells'][$i][17],
                        "received_quantity" => $worksheet['cells'][$i][18],
                        "status" => $worksheet['cells'][$i][19],
                        "entry_date" => $worksheet['cells'][$i][20],
                        "lock_date" => $worksheet['cells'][$i][21],
                        "txn_remark" => $worksheet['cells'][$i][22],
                        "ack_through" => $worksheet['cells'][$i][23],
                        "subsidy_year1" => $worksheet['cells'][$i][24],
                        "subsidy_month1" => $worksheet['cells'][$i][25],
                        "month1_qty" => $worksheet['cells'][$i][26],
                        "subsidy_month2" => $worksheet['cells'][$i][27],
                        "subsidy_year2" => $worksheet['cells'][$i][28],
                        "month2_qty" => $worksheet['cells'][$i][29],
                        "challan_no" => $worksheet['cells'][$i][30],
                        "dd_no" => $worksheet['cells'][$i][31],
                        "lorry_no" => $worksheet['cells'][$i][32],
                        "lorry_capacity" => $worksheet['cells'][$i][33],
                        "retailer_receipt_date" => $worksheet['cells'][$i][34],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][14]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Dealer ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_acknowledgement2")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7]) && isset($worksheet['cells'][$i][13]) && !empty($worksheet['cells'][$i][13]) && isset($worksheet['cells'][$i][18]) && !empty($worksheet['cells'][$i][18]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet['cells'][$i][18]);
                    if($master_id > 0)
                    {                                                      
                        $worksheet['cells'][$i][3] = format_date($worksheet['cells'][$i][3]);
                        $worksheet['cells'][$i][25] = format_date($worksheet['cells'][$i][25]);
                        $worksheet['cells'][$i][26] = format_date($worksheet['cells'][$i][26]);
                        $worksheet['cells'][$i][39] = format_date($worksheet['cells'][$i][39]);
                  

                        $dataset = array("file_id" => $upid,
                        "transaction_id" => $worksheet['cells'][$i][1],
                        "invoice_no"        => $worksheet['cells'][$i][2],
                        "invoice_date" => $worksheet['cells'][$i][3],
                        "marketer" => $worksheet['cells'][$i][4],
                        "manufacturer" => $worksheet['cells'][$i][5],
                        "plant" => $worksheet['cells'][$i][6],
                        "wholesaler_id" => $worksheet['cells'][$i][7],
                        "dealer_id" => $worksheet['cells'][$i][13],
                        "product_id" => $master_id,
                        "unit" => $worksheet['cells'][$i][19],
                        "quantity" => $worksheet['cells'][$i][20],
                        "quantity_mt" => $worksheet['cells'][$i][21],
                        "received_quantity" => $worksheet['cells'][$i][22],
                        "status" => $worksheet['cells'][$i][23],
                        "txn_type" => $worksheet['cells'][$i][24],
                        "entry_date" => $worksheet['cells'][$i][25],
                        "lock_date" => $worksheet['cells'][$i][26],
                        "ack_through" => $worksheet['cells'][$i][27],
                        "txn_remark" => $worksheet['cells'][$i][28],
                        "subsidy_month1" => $worksheet['cells'][$i][29],
                        "subsidy_year1" => $worksheet['cells'][$i][30],                        
                        "month1_qty" => $worksheet['cells'][$i][31],
                        "subsidy_month2" => $worksheet['cells'][$i][32],
                        "subsidy_year2" => $worksheet['cells'][$i][33],
                        "month2_qty" => $worksheet['cells'][$i][34],
                        "challan_no" => $worksheet['cells'][$i][35],
                        "lorry_no" => $worksheet['cells'][$i][36],
                        "lorry_capacity" => $worksheet['cells'][$i][37],
                        "dispatch_no" => $worksheet['cells'][$i][38],
                        "retailer_receipt_date" => $worksheet['cells'][$i][39],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][18]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }            

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Wholesaler ID, Dealer ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_wholesaler")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][4]) && !empty($worksheet['cells'][$i][4]) && isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet['cells'][$i][4]);
                    if($master_id > 0)
                    {                   
                        $dataset = array("file_id" => $upid,
                        "serial_number" => $worksheet['cells'][$i][1],
                        "company"        => $worksheet['cells'][$i][2],                        
                        "plant" => $worksheet['cells'][$i][3],
                        "product_id" => $master_id,                        
                        "dealer_id" => $worksheet['cells'][$i][7],                        
                        "wholesaler_ob" => $worksheet['cells'][$i][10],
                        "comp_ws_sale" => $worksheet['cells'][$i][11],
                        "comp_ws_sale_rcpt" => $worksheet['cells'][$i][12],
                        "received_from_ws" => $worksheet['cells'][$i][13],
                        "received_from_ws_ack" => $worksheet['cells'][$i][14],
                        "ws_rt_sale" => $worksheet['cells'][$i][15],
                        "ws_rt_sale_rcpt" => $worksheet['cells'][$i][16],
                        "ws_ws_sale" => $worksheet['cells'][$i][17],
                        "ws_ws_sale_rcpt" => $worksheet['cells'][$i][18],
                        "total_sales_by_ws" => $worksheet['cells'][$i][19],
                        "stock_transfer_from_ws_to_retailer" => $worksheet['cells'][$i][20],
                        "stock_transfer_from_ws_to_retailer_ack" => $worksheet['cells'][$i][21],
                        "balance_with_ws" => $worksheet['cells'][$i][22],
                        "total_ack_to_ws" => $worksheet['cells'][$i][23],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][4]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Product Name or Dealer ID inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_retailer")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][5]) && !empty($worksheet['cells'][$i][5]) && isset($worksheet['cells'][$i][11]) && !empty($worksheet['cells'][$i][11]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet['cells'][$i][11]);
                    if($master_id > 0)
                    {                                  
                        $dataset = array("file_id" => $upid,
                        "serial_number" => $worksheet['cells'][$i][1],
                        "retailer_id"   => $worksheet['cells'][$i][5],                        
                        "company" => $worksheet['cells'][$i][9],                        
                        "plant" => $worksheet['cells'][$i][10],
                        "product_id" => $master_id,
                        "opening_balance" => $worksheet['cells'][$i][12],
                        "received_quantity" => $worksheet['cells'][$i][13],
                        "sold_quantity" => $worksheet['cells'][$i][14],
                        "availabilty" => $worksheet['cells'][$i][15],
                        "closing_balance" => $worksheet['cells'][$i][16],
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][11]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Retailer ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_retailer_pos")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][3]) && !empty($worksheet['cells'][$i][3]) && isset($worksheet['cells'][$i][4]) && !empty($worksheet['cells'][$i][4]) && isset($worksheet['cells'][$i][11]) && !empty($worksheet['cells'][$i][11]) && isset($worksheet['cells'][$i][14]) && !empty($worksheet['cells'][$i][14]))
                {
                    $master_id = $this->admin_model->get_master_id('products','product_id', 'product_name', $worksheet['cells'][$i][11]);
                    if($master_id > 0)
                    {                                  
                        $worksheet['cells'][$i][3] = format_date($worksheet['cells'][$i][3]);


                        $dataset = array("file_id" => $upid,
                        
                        "transaction_id" => $worksheet['cells'][$i][1],
                        "invoice_no"   => $worksheet['cells'][$i][2],                        
                        "invoice_date" => $worksheet['cells'][$i][3],                        
                        "retailer_id" => $worksheet['cells'][$i][4],
                        "company" => $worksheet['cells'][$i][9],
                        "plant" => $worksheet['cells'][$i][10],
                        "product_id" => $master_id,
                        "unit" => $worksheet['cells'][$i][12],
                        "quantity" => $worksheet['cells'][$i][13],
                        "quantity_mt" => $worksheet['cells'][$i][14],                        
                        
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][11]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str != '')
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Invoice date or Retailer ID or Product Name or Quantity in MT inside excel file.</td></tr>".$skp_str;
            }            
        }
        elseif($upload_type == "upload_sales")
        {            
            $from_date = date(DATE_DB, strtotime($from_date));
            $to_date = date(DATE_DB, strtotime($to_date));

            $sql = "DELETE FROM upload_sales 
                    WHERE from_date >= '$from_date' AND to_date <= '$to_date' ";
            $this->dbaccess_model->executeOnlySql($sql);             
            
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][2]) && !empty($worksheet['cells'][$i][2]) && isset($worksheet['cells'][$i][4]) && !empty($worksheet['cells'][$i][4]) && 
                    isset($worksheet['cells'][$i][6]) && !empty($worksheet['cells'][$i][6]) &&
                    isset($worksheet['cells'][$i][13]) && !empty($worksheet['cells'][$i][13]) && 
                    isset($worksheet['cells'][$i][15]) && !empty($worksheet['cells'][$i][15]))
                {
                    if($worksheet['cells'][$i][4] == "SSP-Zincated-")
                    $worksheet['cells'][$i][4] = "SSP-Zincated-Boronated";

                    $master_id = $this->admin_model->get_product_id($worksheet['cells'][$i][4]);
                    if($master_id > 0)
                    {                                  
                        if(!isset($worksheet['cells'][$i][15]) || empty($worksheet['cells'][$i][15]))
                        $worksheet['cells'][$i][15] = 0;

                        $company_type = 2;
                        if(strtoupper($worksheet['cells'][$i][2]) == "RMPCL")
                        {
                            $company_type = 1;
                        }

                        $worksheet['cells'][$i][13] = format_date($worksheet['cells'][$i][13], DATE_DB);

                        $dataset = array("file_id" => $upid,                        
                        "company" => $worksheet['cells'][$i][2],
                        "company_type" => $company_type,
                        "product_id" => $master_id,
                        "dealer_id" => $worksheet['cells'][$i][6],                        
                        "sales_date" => date(DATE_DB, strtotime($worksheet['cells'][$i][13])),
                        "quantity" => $worksheet['cells'][$i][15],

                        "state_id" => $state_id,
                        "district_id" => $city_id,
                        "from_date" => $from_date,
                        "to_date" => $to_date,
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        //$skp_str .= "<tr><td colspan='2'></td></tr>";
                    }
                }
            }

            if(true)
            {
                $skp_str = "<tr><td colspan='2'>Only rows which having company products are added.</td></tr>";
            }            
        }
        elseif($upload_type == "upload_dispatch")
        {            
            $from_date = date(DATE_DB, strtotime($from_date));
            $to_date = date(DATE_DB, strtotime($to_date));

            $sql = "DELETE FROM upload_dispatch 
                    WHERE from_date >= '$from_date' AND to_date <= '$to_date' ";
            $this->dbaccess_model->executeOnlySql($sql);             
            
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(isset($worksheet['cells'][$i][6]) && !empty($worksheet['cells'][$i][6]) && 
                isset($worksheet['cells'][$i][8]) && !empty($worksheet['cells'][$i][8]) && 
                isset($worksheet['cells'][$i][15]) && !empty($worksheet['cells'][$i][15]))
                {
                    $master_id = $this->admin_model->get_product_id($worksheet['cells'][$i][15]);
                    if($master_id > 0)
                    {                                  
                        //echo "<pre>"; print_r($worksheet['cells'][$i]);
                        $bd = $worksheet['cells'][$i][1];
                        $od = $worksheet['cells'][$i][5];
                        $dd = $worksheet['cells'][$i][6];
                        //echo "<br/>$bd===$od===$dd........";

                        $worksheet['cells'][$i][8] = str_replace("WH", "", $worksheet['cells'][$i][8]);
                        $worksheet['cells'][$i][8] = str_replace("RE", "", $worksheet['cells'][$i][8]);


                        $worksheet['cells'][$i][10] = str_replace("WH", "", $worksheet['cells'][$i][10]);
                        $worksheet['cells'][$i][10] = str_replace("RE", "", $worksheet['cells'][$i][10]);


                        if(!isset($worksheet['cells'][$i][16]) || empty($worksheet['cells'][$i][16]))
                        {
                            $worksheet['cells'][$i][16] = 0;
                        }    

                        
                        if(isset($worksheet['cells'][$i][1]) && !empty($worksheet['cells'][$i][1]))
                        {
                            $worksheet['cells'][$i][1] = prepare_date($worksheet['cells'][$i][1], DATE_DB);
                        }


                        if(isset($worksheet['cells'][$i][5]) && !empty($worksheet['cells'][$i][5]))
                        {
                            $worksheet['cells'][$i][5] = prepare_date($worksheet['cells'][$i][5], DATE_DB);
                        }

                        if(isset($worksheet['cells'][$i][6]) && !empty($worksheet['cells'][$i][6]))
                        {
                            $worksheet['cells'][$i][6] = prepare_date($worksheet['cells'][$i][6], DATE_DB);
                        }

                        $dataset = array("file_id" => $upid,
                        "from_date" => $from_date,
                        "to_date" => $to_date,

                        "billing_month" => $worksheet['cells'][$i][1],
                        "billing_month_text" => $bd,

                        "invoice_no" => $worksheet['cells'][$i][2],
                        "state" => $worksheet['cells'][$i][3],
                        "sale_note" => $worksheet['cells'][$i][4],

                        "order_date" => $worksheet['cells'][$i][5],
                        "order_date_text" => $od,
                        
                        "dispatch_date" => $worksheet['cells'][$i][6],
                        "dispatch_date_text" => $dd,

                        "tally_code" => $worksheet['cells'][$i][7],
                        "sold_to_id" => $worksheet['cells'][$i][8],
                        "sold_to_party" => $worksheet['cells'][$i][9],
                        "ship_to_id" => $worksheet['cells'][$i][10],
                        "ship_to_party" => $worksheet['cells'][$i][11],
                        "place" => $worksheet['cells'][$i][12],
                        "taluka_name" => $worksheet['cells'][$i][13],
                        "district_name" => $worksheet['cells'][$i][14],
                        "product_id" => $master_id,
                        "quantity" => $worksheet['cells'][$i][16],
                        "lr_no" => $worksheet['cells'][$i][17],
                        "transporter_name" => $worksheet['cells'][$i][18],
                        "vehicle_number" => $worksheet['cells'][$i][19],
                        "batch_number" => $worksheet['cells'][$i][20],

                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                        
                        //echo "<pre>"; print_r($dataset);
                        $id = 0;
                        $id = $this->dbaccess_model->insert($upload_type, $dataset); 
                        
                        if($id) 
                        {
                            $ok++;
                        }          
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>Row Number:".$i."&nbsp;Product Name (<b>".$worksheet['cells'][$i][11]."</b>) is not setup in inside Setup->Products. Please add this product name.</td></tr>";
                    }
                }
            }

            if($skp_str)
            {
                $skp_str = "<tr><td colspan='2'>Skipped Records that either missing Dispatch date or FMS ID or Product Name inside excel file.</td></tr>".$skp_str;
            }            
        }     
        elseif($upload_type == "import_wholesalers")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(
                    isset($worksheet['cells'][$i][1]) && !empty($worksheet['cells'][$i][1]) && 
                    isset($worksheet['cells'][$i][2]) && !empty($worksheet['cells'][$i][2]) &&
                    isset($worksheet['cells'][$i][3]) && !empty($worksheet['cells'][$i][3]) &&
                    isset($worksheet['cells'][$i][6]) && !empty($worksheet['cells'][$i][6]) &&
                    isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7])
                )
                {
                    $arr = array("typ" => "wholesalersid", "id" => 0, "name" => $worksheet['cells'][$i][1]);
                    $chkm = $this->common_model->is_already_exist($arr);
                    if(!isset($chkm) || empty($chkm) || count($chkm) <= 0)
                    {                                  
                        $arr1 = array("typ" => "state_id_by_name", "id" => $worksheet['cells'][$i][7]);                                                

                        $sd = $this->common_model->get_detail($arr1);
                        if(isset($sd) && !empty($sd))
                        {
                            $sstate_id = $sd-> state_id;

                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet['cells'][$i][6]);
                            $dd = $this->common_model->get_detail($arr2);
                            if(isset($dd) && !empty($dd))
                            {
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $worksheet['cells'][$i][1],
                                "saler_type" => 1,
                                "first_name"   => $worksheet['cells'][$i][2],
                                "mobile" => $worksheet['cells'][$i][3],                        
                                "email" => $worksheet['cells'][$i][4],
                                "address" => $worksheet['cells'][$i][5],
                                "district_name" => $worksheet['cells'][$i][6],
                                "state_name" => $worksheet['cells'][$i][7],
                                "contact_person" => $worksheet['cells'][$i][8],

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );
                            
                                $id = 0;
                                $id = $this->dbaccess_model->insert("salers", $dataset); 
                                
                                if($id) 
                                {
                                    $ok++;
                                }
                            }
                            else
                            {
                                $skp++;
                                $skp_str .= "<tr><td colspan='2'>District Name ".$worksheet['cells'][$i][6]." not exist in system.</td></tr>";
                            }
                        }
                        else
                        {
                            $skp++;
                            $skp_str .= "<tr><td colspan='2'>State Name ".$worksheet['cells'][$i][7]." not exist in system.</td></tr>";
                        }                                  
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>".$worksheet['cells'][$i][1]." Wholesaler ID already added.</td></tr>";
                    }
                }
                else
                {
                    $skp_str .= "<tr><td colspan='2'>Row: ".$i." Missing any one mandatory field.</td></tr>";
                }
            }

            if($skp_str != '')
            {
                $skp_str = $skp_str;
            }            
        }
        elseif($upload_type == "import_dealers")
        {
            for ($i=$start; $i<=$numRows; $i++) 
            {
                $totalr++;
                if(
                    isset($worksheet['cells'][$i][1]) && !empty($worksheet['cells'][$i][1]) && 
                    isset($worksheet['cells'][$i][2]) && !empty($worksheet['cells'][$i][2]) &&
                    isset($worksheet['cells'][$i][3]) && !empty($worksheet['cells'][$i][3]) &&
                    isset($worksheet['cells'][$i][6]) && !empty($worksheet['cells'][$i][6]) &&
                    isset($worksheet['cells'][$i][7]) && !empty($worksheet['cells'][$i][7])
                )
                {
                    $arr = array("typ" => "dealersid", "id" => 0, "name" => $worksheet['cells'][$i][1]);
                    $chkm = $this->common_model->is_already_exist($arr);
                    if(!isset($chkm) || empty($chkm) || count($chkm) <= 0)
                    {                                  
                        $arr1 = array("typ" => "state_id_by_name", "id" => $worksheet['cells'][$i][7]);                                                

                        $sd = $this->common_model->get_detail($arr1);
                        if(isset($sd) && !empty($sd))
                        {
                            $sstate_id = $sd-> state_id;

                            $arr2 = array("typ" => "district_id_by_name", "id" => $worksheet['cells'][$i][6]);
                            $dd = $this->common_model->get_detail($arr2);
                            if(isset($dd) && !empty($dd))
                            {
                                $ddistrict_id = $dd-> district_id;

                                $dataset = array("saler_sys_id" => $worksheet['cells'][$i][1],
                                "saler_type" => 2,
                                "first_name"   => $worksheet['cells'][$i][2],
                                "mobile" => $worksheet['cells'][$i][3],                        
                                "email" => $worksheet['cells'][$i][4],
                                "address" => $worksheet['cells'][$i][5],
                                "district_name" => $worksheet['cells'][$i][6],
                                "state_name" => $worksheet['cells'][$i][7],
                                "contact_person" => $worksheet['cells'][$i][8],

                                "state_id" => $sstate_id,
                                "district_id" => $ddistrict_id,

                                "created_date" => date(DATETIME_DB),                
                                "created_by" => $this->user_id,
                                "updated_date" => date(DATETIME_DB),                
                                "updated_by" => $this->user_id
                                );
                            
                                $id = 0;
                                $id = $this->dbaccess_model->insert("salers", $dataset); 
                                
                                if($id) 
                                {
                                    $ok++;
                                }
                            }
                            else
                            {
                                $skp++;
                                $skp_str .= "<tr><td colspan='2'>District Name ".$worksheet['cells'][$i][6]." not exist in system.</td></tr>";
                            }
                        }
                        else
                        {
                            $skp++;
                            $skp_str .= "<tr><td colspan='2'>State Name ".$worksheet['cells'][$i][7]." not exist in system.</td></tr>";
                        }                                  
                    }
                    else
                    {
                        $skp++;
                        $skp_str .= "<tr><td colspan='2'>".$worksheet['cells'][$i][1]." Dealer ID already added.</td></tr>";
                    }
                }
                else
                {
                    $skp_str .= "<tr><td colspan='2'>Row: ".$i." Missing any one mandatory field.</td></tr>";
                }
            }

            if($skp_str != '')
            {
                $skp_str = $skp_str;
            }            
        }

        if($skp == $totalr)
        {
            $sql = "DELETE FROM upload_files WHERE file_id = '$upid' AND ftype = '$upload_type' LIMIT 1";
            $this->dbaccess_model->executeOnlySql($sql);             
        }


        $msg = "<tr><td colspan='2'>Total Records:&nbsp;<b>".$totalr."</b></td></tr>";
        $msg .= "<tr><td colspan='2'>Imported Successfully:&nbsp;<b>".$ok."</b></td></tr>";
        $msg .= "<tr><td colspan='2'>Skipped Records:&nbsp;<b>".$skp."</b></td></tr>";
        $msg .= $skp_str;
        echo json_encode(array("status"=>1,"message"=>"<table width='100%'>".$msg."</table>"));
        die;
    }



    /*----------------------------------------------------------
    Inventory Upload Listing Management
    ----------------------------------------------------------*/
    public function inventory_acknowledgement1()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_acknowledgement1');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $this->load->view('includes/main_header');
        $this->load->view('inventory_acknowledgement1', $data);
        $this->load->view('includes/main_footer');
    }


    public function inventory_acknowledgement1_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        $file_id = $_POST['file_id']; 
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.invoice_date', 's.dealer_id','ss.first_name', 'ss.last_name', 'ss.mobile', 'ss.state_name', 'ss.district_name', 's.product_id', 's.unit', 's.quantity', 's.dd_no', 'p.product_name');

        $sTable = "upload_acknowledgement1";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,ss.mobile,ss.state_name, ss.district_name
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();              
            //$row[] = $aRow-> invoice_no;
            $row[] = date(DATE, strtotime($aRow-> invoice_date));
            $row[] = $aRow-> dealer_id;          
            
            $row[] = $aRow-> first_name." ".$aRow-> last_name;            
            $row[] = $aRow-> mobile;
            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name;
            $row[] = $aRow-> product_name;
            $row[] = $aRow-> quantity." ".$aRow-> unit;
            $row[] = $aRow-> dd_no;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


    public function inventory_acknowledgement2()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_acknowledgement2');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $this->load->view('includes/main_header');
        $this->load->view('inventory_acknowledgement2', $data);
        $this->load->view('includes/main_footer');
    }


    public function inventory_acknowledgement2_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        $file_id = $_POST['file_id']; 
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.invoice_date', 's.wholesaler_id', 's.dealer_id','ss.first_name', 'ss.last_name', 'ss.mobile', 'ss.state_name', 'ss.district_name', 'w.first_name', 'w.last_name', 'w.mobile', 'w.state_name', 'w.district_name', 's.product_id', 's.unit', 's.quantity', 's.dispatch_no', 'p.product_name');

        $sTable = "upload_acknowledgement2";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
                

        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,ss.mobile,ss.state_name, ss.district_name,w.first_name as wfirst_name,w.last_name as wlast_name,w.mobile as wmobile,w.state_name as wstate_name, w.district_name as wdistrict_name
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON ss.saler_sys_id = s.wholesaler_id AND ss.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere 
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();                          
            $row[] = date(DATE, strtotime($aRow-> invoice_date));
            
            $row[] = $aRow-> wholesaler_id;                      
            $row[] = $aRow-> wfirst_name." ".$aRow-> wlast_name;            
            //$row[] = $aRow-> wmobile;
            $row[] = $aRow-> wstate_name;
            $row[] = $aRow-> wdistrict_name;

            $row[] = $aRow-> dealer_id;                      
            $row[] = $aRow-> first_name." ".$aRow-> last_name;            
            $row[] = $aRow-> mobile;
            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name;

            $row[] = $aRow-> product_name;
            $row[] = $aRow-> quantity." ".$aRow-> unit;
            $row[] = $aRow-> dispatch_no;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    public function inventory_wholesaler()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_wholesaler');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $this->load->view('includes/main_header');
        $this->load->view('inventory_wholesaler', $data);
        $this->load->view('includes/main_footer');
    }


    public function inventory_wholesaler_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        $file_id = $_POST['file_id']; 
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.dealer_id','ss.first_name', 'ss.last_name', 'ss.state_name', 'ss.district_name', 's.balance_with_ws', 'p.product_name');

        $sTable = "upload_wholesaler";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
                

        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,ss.mobile,ss.state_name, ss.district_name
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1        
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1        
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();              
            $row[] = $aRow-> product_name;

            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name;

            $row[] = $aRow-> dealer_id;                      
            $row[] = $aRow-> first_name." ".$aRow-> last_name;            
            
            $row[] = $aRow-> balance_with_ws;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    public function inventory_retailer()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_retailer');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $this->load->view('includes/main_header');
        $this->load->view('inventory_retailer', $data);
        $this->load->view('includes/main_footer');
    }


    public function inventory_retailer_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        $file_id = $_POST['file_id']; 
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.retailer_id','ss.first_name', 'ss.last_name', 'ss.state_name', 'ss.district_name', 'ss.mobile', 's.availabilty', 'p.product_name');
        $sTable = "upload_retailer";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
                

        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,ss.mobile,ss.state_name, ss.district_name
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 
            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name;
            $row[] = $aRow-> retailer_id;                      
            $row[] = $aRow-> first_name." ".$aRow-> last_name; 
            $row[] = $aRow-> mobile;
            $row[] = $aRow-> product_name;            
            $row[] = $aRow-> availabilty;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }




    public function sales_report()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_sales');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $this->load->view('includes/main_header');
        $this->load->view('sales_report', $data);
        $this->load->view('includes/main_footer');
    }


    public function sales_report_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        $file_id = $_POST['file_id']; 
                           
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.dealer_id', 's.company', 's.sales_date', 's.quantity','ss.first_name', 'ss.last_name', 'st.state_name', 'dis.district_name', 'p.product_name');
        $sTable = "upload_sales";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
              

        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,st.state_name, dis.district_name
            FROM $sTable s
            INNER JOIN states st ON st.state_id = s.state_id
            INNER JOIN districts dis ON dis.district_id = s.district_id
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id 
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN states st ON st.state_id = s.state_id
            INNER JOIN districts dis ON dis.district_id = s.district_id
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id 
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
         

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 
            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name;
            $row[] = $aRow-> company;
            $row[] = $aRow-> product_name;
            $row[] = $aRow-> dealer_id;                      
            $row[] = $aRow-> first_name." ".$aRow-> last_name; 
            $row[] = format_date($aRow-> sales_date, DATE);            
            $row[] = $aRow-> quantity;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Farmers Management
    ----------------------------------------------------------*/
    public function farmers()
    {
        $this->load->view('includes/main_header');
        $this->load->view('farmers');
        $this->load->view('includes/main_footer');
    }

    public function farmers_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";         
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('f.farmer_name', 'f.farmer_mobile', 's.state_name', 'd.district_name', 'f.farmer_photo_url', 'f.farmer_reg_num', 'f.farmer_plot_no', 'f.farmer_village', 'f.farmer_taluka');

        $sTable = "farmers";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {            
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                if($search_columns[$i] == "f.farmer_reg_num")
                {
                    $_POST['search']['value'] = trim(str_replace("F", "", $_POST['search']['value']));
                    $_POST['search']['value'] = ltrim($_POST['search']['value'], "0");
                }

                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
                
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS f.*, s.state_name, d.district_name
            FROM $sTable f             
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            WHERE f.is_deleted = 0 $quick_search $sWhere             
            $sOrder
            $sLimit ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "SELECT COUNT(f.farmer_id) as count
            FROM $sTable f             
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            WHERE f.is_deleted = 0 $quick_search $sWhere ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = "<img src='".$aRow-> farmer_photo_url."' width='50px' height='50px'>";

            $row[] = "F".regno($aRow-> farmer_reg_num, 5);

            $row[] = ucwords($aRow-> farmer_name);

            $row[] = $aRow-> farmer_mobile;

            $row[] = $aRow-> farmer_plot_no." ".$aRow-> farmer_village." ".$aRow-> farmer_taluka;

            $row[] = $aRow-> district_name;

            $row[] = $aRow-> state_name;

            $row[] = '<a class="action_link" href="'.base_url().'admin/farmers_view/'.$aRow -> farmer_id.'"><i class="fa fa-eye"></i></a>';//&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("farmers",'.$aRow -> farmer_id.');><i class="fa fa-trash"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    public function farmers_view()
    {
        $id = $this->uri->segment(3);

        if(isset($id) && !empty($id) && $id > 0)
        {
            $this->load->model("Webservice_model");
            $data = $this->Webservice_model->get_farmer_detail(array("farmer_id"=>$id));
            //p($data);
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."farmers'; </script>"; die;
        }

        $this->load->view('includes/main_header');
        $this->load->view('farmers_view', $data);
        $this->load->view('includes/main_footer');
    }


    public function demos_view()
    {       

        $this->load->view('includes/main_header');
        $this->load->view('demos_view');
        $this->load->view('includes/main_footer');
    }


    /*----------------------------------------------------------
    Demos Management
    ----------------------------------------------------------*/
    public function demos()
    {
        $this->load->view('includes/main_header');
        $this->load->view('demos');
        $this->load->view('includes/main_footer');
    }

    public function demos_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";         
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('f.farmer_name', 'f.farmer_mobile', 's.state_name', 'd.district_name', 'f.farmer_photo_url', 'f.farmer_reg_num', 'f.farmer_village', 'f.farmer_taluka');

        $sTable = "demos";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {            
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                if($search_columns[$i] == "f.farmer_reg_num")
                {
                    $_POST['search']['value'] = trim(str_replace("D", "", $_POST['search']['value']));
                    $_POST['search']['value'] = ltrim($_POST['search']['value'], "0");
                }

                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
                
        if($sWhere!="") $sWhere = " AND $sWhere";                    
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS f.*, s.state_name, d.district_name
            FROM $sTable f             
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            WHERE f.is_deleted = 0 $quick_search $sWhere             
            $sOrder
            $sLimit ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "SELECT COUNT(f.farmer_id) as count
            FROM $sTable f             
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            WHERE f.is_deleted = 0 $quick_search $sWhere ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array();            
            
            $row[] = "<img src='".$aRow-> farmer_photo_url."' width='50px' height='50px'>";

            $row[] = "D".regno($aRow-> farmer_reg_num, 5);

            $row[] = ucwords($aRow-> farmer_name);

            $row[] = $aRow-> farmer_mobile;

            $row[] = $aRow-> farmer_village.", ".$aRow-> farmer_taluka;

            $row[] = $aRow-> district_name;

            $row[] = $aRow-> state_name;

            $row[] = '<a class="action_link" href="'.base_url().'admin/demos_view/'.$aRow -> farmer_id.'"><i class="fa fa-eye"></i></a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }





    public function inventory_retailer_pos()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_retailer_pos');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $this->load->view('includes/main_header');
        $this->load->view('inventory_retailer_pos', $data);
        $this->load->view('includes/main_footer');
    }


    public function inventory_retailer_pos_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        $file_id = $_POST['file_id']; 
        
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.invoice_date','s.retailer_id', 'ss.first_name', 'ss.last_name', 'ss.state_name', 'ss.district_name', 'p.product_name', 'p.quantity_mt');
        $sTable = "upload_retailer_pos";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
                
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_name,ss.first_name,ss.last_name,ss.state_name, ss.district_name
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
        

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 

            $row[] = format_date($aRow-> invoice_date,DATE);
            $row[] = $aRow-> retailer_id;            
            $row[] = $aRow-> first_name." ".$aRow-> last_name; 
            $row[] = $aRow-> state_name;
            $row[] = $aRow-> district_name; 
            $row[] = $aRow-> product_name;            
            $row[] = $aRow-> quantity_mt;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    public function profile()
    {        
        if($this->user_role_id == 1 && $this->user_id == 1)
        {
            $arr = array("typ"=>'states_aop');        
            $data['states_all'] = $this->common_model->get_dd_list($arr);

            $arr = array("typ" => "users", "id" => $this->user_id);
            $data['details'] = $this->common_model->get_detail($arr);

            if(isset($data['details']->company_details) && !empty($data['details']->company_details))
            {
                $data['details']->company_details = json_decode($data['details']->company_details);
            }
            else
            {
                $data['details']->company_details = array();
            }

            $data['details']->company_details = (array) $data['details']->company_details;
            
            error_reporting(0);
            $this->load->view('includes/main_header');
            $this->load->view('profile', $data);
            $this->load->view('includes/main_footer');
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."'; </script>"; die;
        }    
    }


    public function profile_user()
    {        
        $arr = array("typ"=>'states_aop');        
        $data['states_all'] = $this->common_model->get_dd_list($arr);

        $arr = array("typ" => "users", "id" => $this->user_id);
        $data['details'] = $this->common_model->get_detail($arr);
        
        $this->load->view('includes/main_header');
        $this->load->view('profile_user', $data);
        $this->load->view('includes/main_footer');
            
    }

    public function profile_save()
    {
        extract($_POST);

        if((!isset($_FILES['logo']['name']) || empty($_FILES['logo']['name'])) && $photo_url == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Logo."));
            die; 
        }
        elseif($cstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select State."));
            die;    
        }
        elseif($ccity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select City."));
            die;    
        }
        elseif($caddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Address."));
            die;    
        }        
        elseif($email == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
            die;    
        }
        elseif($mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Mobile Number."));
            die;    
        }
        elseif(strlen($mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }

        

        $logo = $this->input->post('logo');        
        if(isset($_FILES['logo']['name']) && !empty($_FILES['logo']['name']))
        {
            $logo = $_FILES['logo'];            
            if(isset($logo) && !empty($logo))
            {                
                $ext = pathinfo($logo['name'], PATHINFO_EXTENSION);                
                
                $config['file_name'] = "logo.png";
                $config['upload_path'] = "assets/images";
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['overwrite'] = TRUE;
                
                $this->load->library('upload');
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('logo')) 
                {
                    $error = $this->upload->display_errors(); 

                    echo json_encode(array("status"=>0,"message"=>$error)); 
                    die;       
                }
                else
                {
                    $dataup = $this->upload->data();                    
                    $unm = $dataup['file_name'];

                    $photo_url = "assets/images/".$unm;
                }
            }
        }


        $data = array( 
            "photo_url" => $photo_url, 
            "company_details" => json_encode($_POST),
            "updated_date" => date(DATETIME_DB),                
            "updated_by" => $this->user_id
        );

        $this->dbaccess_model->update("users", $data, array("user_id" => $this->user_id));
        
        echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        
    }



    public function profile_user_save()
    {        
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
     
        $address = $this->input->post('address');
        $gender = $this->input->post('gender');
        $dob = $this->input->post('dob');
        $qualification = $this->input->post('qualification');     
        $experience = $this->input->post('experience');

        $cstates_name = $this->input->post('cstates_name');
        $ccity_name = $this->input->post('ccity_name');
        $caddress = $this->input->post('caddress');

        $pstates_name = $this->input->post('pstates_name');
        $pcity_name = $this->input->post('pcity_name');
        $paddress = $this->input->post('paddress');

        $user_photo_url = $this->input->post('user_photo_url');
        $hdn_id = $this->input->post('hdn_id');

        if($first_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter First Name."));
            die;    
        }
        elseif($last_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Last Name."));
            die;    
        }        
        elseif($email == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
            die;    
        }
        elseif($mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Mobile Number."));
            die;    
        }
        elseif(strlen($mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        elseif($cstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address State Name."));
            die;
        }
        elseif($ccity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Current Address City Name."));
            die;
        }
        elseif($caddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Current Address Details."));
            die;
        }
        elseif($pstates_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address State Name."));
            die;
        }
        elseif($pcity_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select Permanent Address City Name."));
            die;
        }
        elseif($paddress == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter Permanent Address Details."));
            die;
        } 

        
        if(isset($_FILES['user_photo']['name']) && !empty($_FILES['user_photo']['name']))
        {
            $logo = $_FILES['user_photo'];            
            if(isset($logo) && !empty($logo))
            {                
                $ext = pathinfo($logo['name'], PATHINFO_EXTENSION);                
                
                $config['file_name'] = $this->user_id."_user_photo.png";
                $config['upload_path'] = "assets/images";
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['overwrite'] = TRUE;
                
                $this->load->library('upload');
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('user_photo')) 
                {
                    $error = $this->upload->display_errors(); 

                    echo json_encode(array("status"=>0,"message"=>$error)); 
                    die;       
                }
                else
                {
                    $dataup = $this->upload->data();                    
                    $unm = $dataup['file_name'];

                    $user_photo_url = "assets/images/".$unm;
                }
            }
        }       

        if(true)
        {            
            $data = array("user_photo_url" => $user_photo_url,
                "first_name" => $first_name, 
                "last_name" => $last_name, 
                "email" => $email, 
                "mobile" => $mobile, 
                
                "cstate" => $cstates_name,
                "ccity" => $ccity_name,
                "caddress" => $caddress,
                "pstate" => $pstates_name,
                "pcity" => $pcity_name,
                "paddress" => $paddress,

                "gender" => $gender, 
                "dob" => date(DATE_DB, strtotime($dob)), 
                "qualification" => $qualification, 
                
                "experience" => $experience,                

                "updated_date" => date(DATETIME_DB),                
                "updated_by" => $this->user_id
            );

                            
            $this->dbaccess_model->update("users", $data, array("user_id" => $this->user_id));
            
            echo json_encode(array("status"=>1,"message"=>"Saved successfully.")); die;
        }
    }




    public function dispatch_report()
    {
        $data['details'] = $this->admin_model->last_upload_date('upload_dispatch');

        if(!isset($data['details']) || empty($data['details']))
        {
            $data['details']->file_id = 0;
        }

        $this->load->view('includes/main_header');
        $this->load->view('dispatch_report', $data);
        $this->load->view('includes/main_footer');
    }


    public function dispatch_report_list()
    {                
        $user_id = $this->user_id;
                
        //Quick Search-----------------------------------------------------------------
        $quick_search = "";
        $file_id = $_POST['file_id']; 
                           
        //-----------------------------------------------------------------------------
        $aColumns = $search_columns = array('s.dispatch_date_text', 's.sold_to_party', 's.ship_to_id', 's.ship_to_party','s.place', 's.district_name', 's.quantity', 'p.product_code');
        $sTable = "upload_dispatch";       

        $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];        
        
        $sOrder = "ORDER BY ".$aColumns[0];
        if ( isset( $_POST['order'] ) )
        {
            $sOrder = "ORDER BY  ".$aColumns[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'];
        }   

        $sWhere = "";
        if ( $_POST['search']['value'] != "" )
        {
            $sWhere = " (";
            for ( $i=0 ; $i<count($search_columns) ; $i++ )
            {
                $sWhere .= $search_columns[$i]." LIKE '%".( $_POST['search']['value'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
              
        
        if($sWhere!="") $sWhere = " AND $sWhere";                            
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS s.*,p.product_code
            FROM $sTable s            
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
            $sOrder
            $sLimit
        ";
        
        $rResult = $this->dbaccess_model->executeSql($sQuery);
       
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = $this->dbaccess_model->executeSql($sQuery);
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        

        $sQuery = "
            SELECT COUNT(s.id) as count
            FROM $sTable s            
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id = '$file_id' $quick_search $sWhere
        ";
        $rResultTotal = $this->dbaccess_model->executeSql($sQuery);
        $iTotal = $rResultTotal[0]->count;
        
        
       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iFilteredTotal,
            "recordsFiltered" => $iTotal,
            "data" => array()
        );
         

        $sno=0;
        foreach($rResult as $aRow)
        {
            $row = array(); 
            $row[] = $aRow-> dispatch_date_text;
            $row[] = $aRow-> sold_to_party;
            $row[] = $aRow-> ship_to_id;
            $row[] = $aRow-> ship_to_party;
            $row[] = $aRow-> place;                      
            $row[] = $aRow-> district_name; 
            $row[] = $aRow-> product_code;            
            $row[] = $aRow-> quantity;            

            /*$row[] = '<a class="action_link" href="'.base_url().'admin/'.$lbl.'_edit/'.$aRow -> saler_id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a class="action_link" href="javascript:void(0);" onclick=delete_record("'.$lbl.'",'.$aRow -> saler_id.');><i class="fa fa-trash"></i></a>';*/

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }



    /*----------------------------------------------------------
    Album Management
    ----------------------------------------------------------*/
    public function album()
    {
        $this->load->view('includes/main_header');
        $this->load->view('demos');
        $this->load->view('includes/main_footer');
    }



    public function reports()
    {
        $this->load->view('includes/main_header');
        $this->load->view('reports');
        $this->load->view('includes/main_footer');
    }

}//End Of Class
?>