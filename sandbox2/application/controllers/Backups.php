<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Backups extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();
		
		$this->is_logged_in();
				
		$this->load->model('backup_model', 'backup');
	}


    public function is_logged_in()
    {
        $data = $this->session->all_userdata();
        
        if(isset($data['user_id']) && !empty($data['user_id']))
        {
            $this->user_first_name = ucfirst($data['first_name']);

            $this->user_last_name = ucfirst($data['last_name']);

            $this->user_fullname = ucwords($data['first_name']." ".$data['last_name']);

            $this->user_email = $data['email'];

            $this->user_mobile = $data['mobile'];

            $this->user_role_id = $data['role_id'];

            $this->user_role_name = $data['role_name'];

            $this->user_id = $data['user_id'];            

            $this->user_photo_url = $data['user_photo_url']; 

            $this->user_created_date = date("M, Y", strtotime($data['created_date']));


            if($this->user_role_id == 2 || $this->user_role_id == 3)
            {
                $this->user_state_aop = $this->common_model->user_state_aop($this->user_id);

                $this->user_district_aop = $this->common_model->user_district_aop($this->user_id);
            }

            return true;
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."login/logout'; </script>"; die;
        }

    }


    public function validate_permission($module_id, $permission_id)
    {
        $chk_pm = $this->common_model->check_permission($this->user_role_id, $module_id, $permission_id);
        
        if(!isset($chk_pm) || empty($chk_pm))
        {            
            $this->load->view('noaccess');            
        }

        return true;
    }


    public function backups_list()
    {                
        $user_id = $this->user_id;        
        
        $dir = "./assets/backups/databases/";

        $data = array();

        $allfiles = scandir($dir);

        /*if (is_dir($dir))
        {
          if ($dh = opendir($dir))
          {
            while (($file = scandir($dir)) !== false)
            {
                if($file != "." && $file!="..")
                $data[] = $file;
            }

            closedir($dh);
          }
        }*/

        if (isset($allfiles) && !empty($allfiles))
        {          
            foreach($allfiles as $file)
            {
                if($file != "." && $file!=".." && $file!="index.php")
                $data[] = $file;
            }          
        }

       
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => count($data),
            "recordsFiltered" => count($data),
            "data" => array()
        );       

        
        foreach($data as $file)
        {
            $row = array(); 

            $filename = $file;

            $row[] = $filename;

            $filesize = filesize($dir.$filename);
            $filesize = number_format($filesize / (1024 * 1024), 2, ".", "");
            $row[] = $filesize." MB";

            $row[] = '<a title="Restore Backup" class="btn btn-primary action_link" href="javascript:void(0);" onclick=restore_backup("'.$filename.'");><i class="fa fa-download"></i>&nbsp;Restore</a>&nbsp;<a class="btn btn-danger action_link" href="javascript:void(0);" onclick="delete_record(\'db_backup\',\''.$filename.'\');"><i class="fa fa-trash"></i>&nbsp;Remove</a>';

            $output['data'][] = $row;
        }
        
        echo json_encode( $output );    
    }


	function index() 
	{		
		$this->validate_permission(50, 2);

        $this->load->view('includes/main_header');
        $this->load->view('backup');
        $this->load->view('includes/main_footer');		
	}


	public function restore_backup()
	{
		$this->validate_permission(50, 2);

		$backup_id = $this->input->post('backup_id');

		if($backup_id == "" || $backup_id <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select any backup."));            
            die;    
        }

		$this->backup->restore_backup($backup_id);

        echo json_encode(array("status"=>1,"message"=>"Backup has been restored successfully."));            
            die;
	}


}

?>