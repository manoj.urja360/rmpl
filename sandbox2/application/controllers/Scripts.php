<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scripts extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("admin_model");
    }

    function sendsms()
    {
        $arr = array("Text"=>"First SMS for Testing");

        sendSMS($arr);
    }

    function salers() 
    {    
        $filepath = "./assets/import/salers.csv";
        $file = fopen($filepath, 'r');
        $datetime = date(DATETIME_DB);
        $sno = 0;        
        
        $columns = array("Software Unique ID", "Dealer ID", "Name", "Dealership Type", "State Name", "District Name");
        $results = array();
        while(($data = fgetcsv($file, 1000, ",")) !== FALSE)
        {
            if(isset($data) && !empty($data)) 
            {
                $saler_id = $data[0];
                $saler_sys_id = $data[1];
                $saler_type = $data[2];                
                $first_name = $data[5];
                $state_id = $data[9];
                $district_id = $data[10];

                $state_name = $data[13]; 
                $district_name = $data[14];                                 
                
                $mstype = "Wholesaler";
                if($saler_type == 2) $mstype = "Retailer"; 

                $datachk = $this->common_model->dealersid_for_other_stateid($saler_sys_id, $state_id, $district_id);
                 
                if(isset($datachk) && !empty($datachk))
                {
                    $results[] = $columns;
                    $results[] = array($saler_id,$saler_sys_id, $first_name, $mstype, $state_name, $district_name);
                    foreach($datachk as $obj)
                    {
                        $stype = "Wholesaler";
                        if($obj-> saler_sys_id == 2) $stype = "Retailer"; 

                        $results[] = array($obj->saler_id,$obj->saler_sys_id, $obj->first_name, $stype, $obj->state_name, $obj->district_name);
                     
                    } 

                    $results[] =  array("", "", "", "", "", "");  
                } 

                
            }                      
        }

        export_excel("Duplicate Ids", "Dealers", array(), $results);

        fclose($file);        
    }


    function d() 
    {    
        $filepath = "./assets/import/retailer1.csv";
        $file = fopen($filepath, 'r');
        $datetime = date(DATETIME_DB);
        $sno = 0;        
        while(($data = fgetcsv($file, 1000, ",")) !== FALSE)
        {
            if(isset($data) && !empty($data)) 
            {
                $stt = $data[0];
                $dis = $data[1];
                $id = $data[2];                
                $nm = $data[3];
                $mob = $data[4];

                $id = sanitize_text($id);
                $nm = sanitize_text($nm);
                $mob = sanitize_text($mob);
                $stt = sanitize_text($stt);
                $dis = sanitize_text($dis);                               
                
                $arr = array("typ"=>"dealersid", "id"=>0, "name"=>$id);
                $datachk = $this->common_model->is_already_exist($arr);
                 
                if(!isset($datachk) || empty($datachk))
                {
                    $sno++;

                    $datain = array( 
                    "saler_type" => 2,                    
                    "state_name" => $stt,
                    "district_name" => $dis,
                    "saler_sys_id" => $id,                    
                    "first_name" => $nm,                    
                    "mobile" => $mob,
                    "created_date" => $datetime,                
                    "created_by" => 1,
                    "updated_date" => $datetime,                
                    "updated_by" => 1);
                    
                    $this->dbaccess_model->insert("salers", $datain);
                }                
            }                      
        }       

        echo "Total=".$sno; 

        fclose($file);        
    }



    function w() 
    {    
        $filepath = "./assets/import/whosalerlist4.csv";
        $file = fopen($filepath, 'r');
        $datetime = date(DATETIME_DB);
        $sno = 0;
        while(($data = fgetcsv($file, 1000, ",")) !== FALSE)
        {
            if(isset($data) && !empty($data)) 
            {
                $id = $data[0];
                $nm = $data[1];                
                $mob = $data[2];
                $stt = $data[3];
                $dis = $data[4]; 

                $id = sanitize_text($id);
                $nm = sanitize_text($nm);
                $mob = sanitize_text($mob);
                $stt = sanitize_text($stt);
                $dis = sanitize_text($dis);
                
                $arr = array("typ"=>"wholesalersid", "id"=>0, "name"=>$id);
                $datachk = $this->common_model->is_already_exist($arr);                 
                if(!isset($datachk) || empty($datachk))
                {
                    echo "<pre>"; print_r($datachk);
                    $sno++;

                    $datain = array( 
                    "saler_type" => 1,                    
                    "state_name" => $stt,
                    "district_name" => $dis,
                    "saler_sys_id" => $id,                    
                    "first_name" => $nm,                    
                    "mobile" => $mob,
                    "created_date" => $datetime,                
                    "created_by" => 1,
                    "updated_date" => $datetime,                
                    "updated_by" => 1);

                    $this->dbaccess_model->insert("salers", $datain);
                }                
            }                      
        }        

        echo "Total=".$sno;
        fclose($file);       
        
    }

    

    public function set_s()
    {
        $sql = "SELECT state_name 
        FROM salers
        GROUP BY state_name
        ORDER BY state_name ASC
        ";
        
        $data = $this->dbaccess_model->executeSql($sql);

        if(isset($data) && !empty($data))
        {
            $sarr = array();
            foreach($data as $obj)
            {
                $state_name = strtoupper(trim($obj-> state_name));
                
                $arr = array("typ"=>"state_id_by_name", "id"=>$state_name);        
                $details = $this->common_model->get_detail($arr);

                if(isset($details) && !empty($details))
                {
                    $sarr[$state_name] = $details-> state_id;
                }
            }

            if(isset($sarr) && !empty($sarr))
            {
                foreach($sarr as $snm => $sid)
                {
                    $this->dbaccess_model->update("salers", array("state_id" => $sid), array("state_name ="=>$snm));
                }
            }

        }
    }


    public function set_d()
    {
        $sql = "SELECT district_name 
        FROM salers
        GROUP BY district_name
        ORDER BY district_name ASC
        ";
        
        $data = $this->dbaccess_model->executeSql($sql);

        if(isset($data) && !empty($data))
        {
            $sarr = array();
            foreach($data as $obj)
            {
                $district_name = strtoupper(trim($obj-> district_name));
                
                $arr = array("typ"=>"district_id_by_name", "id"=>$district_name);        
                $details = $this->common_model->get_detail($arr);

                if(isset($details) && !empty($details))
                {
                    $sarr[$district_name] = $details-> district_id;
                }
            }

            if(isset($sarr) && !empty($sarr))
            {
                foreach($sarr as $snm => $sid)
                {
                    $this->dbaccess_model->update("salers", array("district_id" => $sid), array("district_name ="=>$snm));
                }
            }
        }
    }


    function taluka() 
    {    
        $filepath = "./assets/uploads/import/TalukaDataFormat.csv";
        $file = fopen($filepath, 'r');
        $datetime = date(DATETIME_DB);
        $sno = 0;        
        while(($data = fgetcsv($file, 1000, ",")) !== FALSE)
        {
            if(isset($data) && !empty($data)) 
            {                
                $one = $data[1];
                $two = $data[2];                                

                $one = sanitize_text($one);
                $two = sanitize_text($two);                           
                
                
                $sno++;

                $datain = array( 
                "district_id" => $one,                    
                "taluka_name" => $two,
                "is_taluka_active" => 1,                
                "created_date" => $datetime,                
                "created_by" => 1,
                "updated_date" => $datetime,                
                "updated_by" => 1);
                    
                $this->dbaccess_model->insert("taluka", $datain);                                
            }                      
        }       

        echo "Total=".$sno; 

        fclose($file);        
    }



    function staff() 
    {    
        $filepath = "./assets/uploads/import/staff.csv";
        $file = fopen($filepath, 'r');
        $datetime = date(DATETIME_DB);
        $sno = 0;        
        while(($data = fgetcsv($file, 1000, ",")) !== FALSE)
        {
            if(isset($data) && !empty($data)) 
            {                
                $one1 = $data[0];
                $one2 = $data[1];
                $one3 = $data[2];
                $one4 = $data[3];
                $one5 = $data[4];
                $one6 = $data[5];
                $one7 = $data[6];                

                $one1 = sanitize_text($one1);
                $one2 = sanitize_text($one2);
                $one3 = sanitize_text($one3);
                $one4 = sanitize_text($one4);
                $one5 = sanitize_text($one5);
                $one6 = sanitize_text($one6);
                $one7 = sanitize_text($one7); 


                $one8 = strtolower(trim($one1)."".trim($one2));
                
                $sno++;

                $kcode = generate_random_string(5);

                $datain = array( 
                "role_id" => $one7,                    
                "first_name" => $one1,
                "last_name" => $one2,
                "email" => $one3,
                "mobile" => $one4,                
                "ccity" => $one5,
                "cstate" => $one6,                
                "pcity" => $one5,
                "pstate" => $one6,

                "username" => $one8,
                "keycode" => $kcode,                               

                "created_date" => $datetime,                
                "created_by" => 1,
                "updated_date" => $datetime,                
                "updated_by" => 1);
                    
                $id = $this->dbaccess_model->insert("users", $datain);                                

                if($id > 0)
                {
                    $datain = array( 
                    "user_id" => $id,                    
                    "password" => md5("123456"),
                    "password_for" => 1,
                    "created_date" => $datetime);                        
                    $this->dbaccess_model->insert("users_login", $datain);


                    $datain = array( 
                    "user_id" => $id,                    
                    "password" => md5($kcode),
                    "password_for" => 2,
                    "created_date" => $datetime);                        
                    $this->dbaccess_model->insert("users_login", $datain);


                    $datain = array( 
                    "user_id" => $id,                    
                    "state_id" => $one6,
                    "district_id" => $one5,
                    "created_by" => 1,
                    "created_date" => $datetime);                        
                    $this->dbaccess_model->insert("users_area", $datain); 
                }
            }                      
        }       

        echo "Total=".$sno; 

        fclose($file);        
    }


    function all() 
    {    
        $fnm = $_REQUEST['fnm'];

        $filepath = "./assets/uploads/import/$fnm.csv";
        $file = fopen($filepath, 'r');
        $datetime = date(DATETIME_DB);
        $sno = 0;
        $state_id = 27;
        while(($data = fgetcsv($file, 1000, ",")) !== FALSE)
        {
            if(isset($data) && !empty($data)) 
            {                
                $d = $data[3];
                $t = $data[5];
                
                $d = sanitize_text($d);
                $t = sanitize_text($t);
                                
                $sql = "SELECT district_id as id FROM districts WHERE state_id = $state_id AND district_name = '$d' LIMIT 1";
                $dchk = $this->dbaccess_model->is_exist($sql);  

                if($dchk == 0)
                {
                    $datain = array( 
                    "state_id" => $state_id,                    
                    "district_name" => $d,
                    "is_district_active" => 0,                
                    "created_date" => $datetime,                
                    "created_by" => 1,
                    "updated_date" => $datetime,                
                    "updated_by" => 1);
                        
                    //$did = $this->dbaccess_model->insert("districts", $datain);
                }
                else
                {
                    $did = $dchk;
                }


                if($did > 0)
                {
                    $sql = "SELECT taluka_id as id FROM taluka WHERE district_id = $did AND taluka_name = '$t' LIMIT 1";
                    $tchk = $this->dbaccess_model->is_exist($sql);
                    if($tchk == 0)
                    {
                        $sno++;
                        $datain = array( 
                        "district_id" => $did,                    
                        "taluka_name" => $t,
                        "is_taluka_active" => 0,                
                        "created_date" => $datetime,                
                        "created_by" => 1,
                        "updated_date" => $datetime,                
                        "updated_by" => 1);
                            
                        //$this->dbaccess_model->insert("taluka", $datain);
                    }                        
                }
                                                                    
            }                      
        }       

        echo "Total Taluka=".$sno; 

        fclose($file);        
    }



}//End Of Class