<?php
class Excel {

    private $excel;

    public function __construct() {
        //echo APPPATH.'third_party/phpexcel/PHPExcel.php';
        // initialise the reference to the codeigniter instance
        require_once APPPATH.'third_party/PHPExcel/PHPExcel.php';
        $this->excel = new PHPExcel();    
    }

    public function load($path) {
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $this->excel = $objReader->load($path);
    }

    public function save($path) {
        // Write out as the new file
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save($path);
    }

    public function stream($filename) {       
        header('Content-type: application/ms-excel');
        header("Content-Disposition: attachment; filename=\"".$filename."\""); 
        header("Cache-control: private");        
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');    
    }


    public function excel_to_array($inputFileName,$row_callback=null){
        if (!class_exists('PHPExcel')) return false;
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            return ('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();
        $keys = array();
        $results = array();
        if(is_callable($row_callback)){
            for ($row = 1; $row <= $highestRow; $row++){ 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,null,true,false);
                if ($row === 1){
                    $keys = $rowData[0];
                } else {
                    $record = array();
                    foreach($rowData[0] as $pos=>$value) $record[$keys[$pos]] = $value; 
                    $row_callback($record);           
                }
            } 
        } else {            
            for ($row = 1; $row <= $highestRow; $row++){ 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,null,true,false);
                if ($row === 1){
                    $keys = $rowData[0];
                } else {
                    $record = array();
                    foreach($rowData[0] as $pos=>$value) $record[$keys[$pos]] = $value; 
                    $results[] = $record;           
                }
            } 
            return $results;
        }
    }

    public function  __call($name, $arguments) {  
        // make sure our child object has this method  
        if(method_exists($this->excel, $name)) {  
            // forward the call to our child object  
            return call_user_func_array(array($this->excel, $name), $arguments);  
        }  
        return null;  
    }  
}
?>