<section class="content-header">
<h1>
Set Area Of Operation
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/area_of_operation';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/area_of_operation_set';" data-toggle="tab" aria-expanded="false">Set Area Of Operation</a></li>
</ul>


<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
  <div class="form-group">            
    <div class="col-sm-10">
      <label for="states_name"><?php echo MANDATORY;?>Select State Name</label>
      <select name="states_name" id="states_name" class="form-control" onchange="get_dd_list(this.value, 'district_by_state', 'dd_list');">
        <option value="">Select State Name</option>
        <?php
          foreach($states_all as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>
  </div>
</div>  


<div id="dd_list"></div>  


<br/><br/>
<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/area_of_operation';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="0" type="hidden">
    </div>
  </div> 
</div> 

</form>
</div>
</div>  
</div>
</div>
</section>

<link href="<?php echo base_url();?>assets/bower_components/select2/dist/css/select2.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.js"></script>

<script type="text/javascript">

$(document).ready(function()
{
    $("#states_name").select2();

    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/area_of_operation_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/area_of_operation'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>