<section class="content-header">
<h1>
Add New Unit
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<!-- <div class="box-header with-border">
<i class="fa fa-warning"></i>
<h3 class="box-title">Listing</h3>
</div>
 -->

<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/units';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/units_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>
</ul>

<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
  <div class="form-group">        
    <div class="col-sm-6">
      <label for="unit_name"><?php echo MANDATORY;?>Unit Name</label>
      <input class="form-control" name="unit_name" id="unit_name" value="" type="text" maxlength="50">
    </div>
  </div>
</div>  

<div class="row">
  <div class="form-group">        
    <div class="col-sm-6">
      <label for="sub_unit_name"><?php echo MANDATORY;?>Sub Unit Name</label>
      <input class="form-control" name="sub_unit_name[]" value="" type="text" maxlength="50">
    </div>

    <div class="col-sm-6"><br/>      
      <button type="button" class="btn btn-default" onclick="add_more('sub_unit');"><i class="fa fa-plus"></i>&nbsp;Add More</button>
    </div>
  </div>
</div>

<div id="addmorecontent"></div>

<br/>
<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/units';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="0" type="hidden">
    </div>
  </div> 
</div> 

</form>
</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(document).ready(function()
{
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/units_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/units'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>