<?php
$arrs = array("products_only");
$filters = $this->webservice_model->get_masters($arrs);

$arrs = array("typ"=>"states_aop");
$states = $this->common_model->get_dd_list($arrs);


$lbl_date = " ";
if($report_type == "upload_acknowledgement1" || $report_type == "upload_acknowledgement2" || $report_type == "upload_retailer_pos")
	$lbl_date = " Invoice ";
elseif($report_type == "upload_retailer" || $report_type == "upload_wholesaler")
	$lbl_date = " Upload ";
elseif($report_type == "upload_sales")
	$lbl_date = " Sales ";
elseif($report_type == "upload_dispatch")
	$lbl_date = " Dispatch ";
?>

<div class="row" style="margin-top:2%; margin-bottom:2%;">
  <div class="form-group">    
    <div class="col-sm-4">
      <label for="fby_state">State Name</label>
      <select name="fby_state" id="fby_state" class="form-control select2 fby" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'fby_city');">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-4">
      <label for="fby_city">District Name</label>
      <select name="fby_city" id="fby_city" class="form-control select2 fby"></select>
    </div>


    <div class="col-sm-4">
      <label for="fby_product">Product Name</label>
      <select name="fby_product" id="fby_product" class="form-control select2 fby">
      <option value="">Select</option>
      <?php
      if(isset($filters['products_only']) && !empty($filters['products_only']))
      {
      	foreach($filters['products_only'] as $obj)
      	{
      ?>
      		<option value="<?php echo $obj->id;?>"><?php echo $obj->name;?></option>
      <?php		
      	}		
      }
      ?>	
      </select>      
    </div>  
    
  </div>
</div>


<div class="row">
  <div class="form-group">
    <div class="col-sm-4">
      <label for="fby_from_date">From<?php echo $lbl_date;?>Date</label>
      <input class="form-control fby" name="fby_from_date" id="fby_from_date" type="text" maxlength="50" value="" readonly>  
    </div>

    <div class="col-sm-4">
      <label for="fby_to_date">To<?php echo $lbl_date;?>Date</label>
      <input class="form-control fby" name="fby_to_date" id="fby_to_date" type="text" maxlength="50" value="" readonly>  
    </div>


    <div class="col-sm-4">
      <label for="fby_keyword">Searh by Keyword</label>
      <input class="form-control fby" name="fby_keyword" id="fby_keyword" type="text">  
    </div>

  </div>
</div>


<div class="row btn_row">
    <div class="form-group">
    <div class="col-sm-4">
      <button type="submit" name="btn_fby" id="btn_fby" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;Search</button>&nbsp;
      <button type="button" name="btn_reset" id="btn_reset" class="btn btn-danger"><i class="fa fa-empty"></i>&nbsp;Clear Search</button>
    </div>

    <div class="col-sm-8">
    	
      <div class="btn-group" style="float: right; margin-right: 2%;">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="fa fa-cloud-download"></span>&nbsp;Export</button>
        
        <ul class="dropdown-menu" role="menu">
          <li><a href="#" onclick="export_data('csv');"><i class="fa fa-file-text-o"></i>&nbsp;CSV</a></li>
          

          <li><a href="#" onclick="export_data('xls');"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</a></li>
          

          <li><a href="#" onclick="export_data('pdf');"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a></li>
        </ul>
      </div>      
    </div>	
  </div> 
</div>
<br/>
<hr/>

<!-- <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.css">
<script src="<?php //echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.js"></script> -->
<script type="text/javascript">

$(document).ready(function()
{
    $(".select2").select2();    


    $("#btn_fby").click(function()
    {
        table.ajax.reload(null, false);
    });


    $("#btn_reset").click(function()
    {
        $(".fby").val("");
        $(".select2").select2().trigger("chosen:updated");

        $("#btn_fby").trigger("click");
    });


    <?php
    if(isset($fsid) && !empty($fsid))
    {
    ?>
        $("#fby_state").val("<?php echo $fsid;?>");
        get_dd_list("<?php echo $fsid;?>", 'district_by_state_aop_dd', 'fby_city');
        $(".select2").select2().trigger("chosen:updated");
    <?php 
        if(!isset($fdid) || empty($fdid)) 
        {
    ?>
            $("#btn_fby").trigger("click");
    <?php      
        }
    }
    ?>

    <?php
    if(isset($fdid) && !empty($fdid))
    {
    ?>
        setTimeout(function()
        {
            $("#fby_city").val("<?php echo $fdid;?>");
            
            $(".select2").select2().trigger("chosen:updated");

            $("#btn_fby").trigger("click");

        }, "2000");
        
    <?php  
    }
    ?>

}); 


function export_data(typ)
{
	var file_id = "<?php echo $details-> file_id;?>";
	var report_type = "<?php echo $report_type;?>";

    var fby_state = $("#fby_state").val();
    var fby_city = $("#fby_city").val();
    var fby_product = $("#fby_product").val();
    var fby_from_date = $("#fby_from_date").val();
    var fby_to_date = $("#fby_to_date").val();
    var fby_keyword = $("#fby_keyword").val();

    if(fby_state == "" || fby_state == null) fby_state = 0;
    if(fby_city == "" || fby_city == null) fby_city = 0;
    if(fby_product == "" || fby_product == null) fby_product = 0;
    if(fby_from_date == "" || fby_from_date == null) fby_from_date = 0;
    if(fby_to_date == "" || fby_to_date == null) fby_to_date = 0;
    if(fby_keyword == "" || fby_keyword == null) fby_keyword = 0;


    var url = base_url + "admin/export_data/"+ typ + "/" + report_type + "/" + file_id + "/" + fby_state + "/" + fby_city + "/" + fby_product + "/" + fby_from_date + "/" + fby_to_date + "/" + fby_keyword;

    window.open(url,"");
}
</script>   