<section class="content-header">
<h1>
Sales Report
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sales_report';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/upload_sales';" data-toggle="tab" aria-expanded="false">Upload New</a></li>
</ul>


<div class="">
<?php 
$report_type = "upload_sales";
include('filters.php'); 
?>
</div>


<fieldset style="overflow: auto;">
<div id="message_box"></div>
<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_100;?>">State</th>
<th style="<?php echo COL_100;?>">District</th>
<th style="<?php echo COL_100;?>">Company</th>
<th style="<?php echo COL_300;?>">Product</th>

<th style="<?php echo COL_100;?>">Dealer ID</th>
<th style="<?php echo COL_100;?>">Name</th>
<th style="<?php echo COL_100;?>">Sales Date</th>
<th style="<?php echo COL_100;?>">Qty.(MT)</th>
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;
 
$(document).ready(function() {
 
    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "asc" ]],       
    "aoColumns": [        
    { "bSortable": true },
    { "bSortable": true },        
    { "bSortable": true },        
    { "bSortable": true },        
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/sales_report_list",
            "type": "POST",
            "data": function ( data ) {
                data.file_id = "<?php echo $details-> file_id;?>";

                data.fby_state = $("#fby_state").val();
                data.fby_city = $("#fby_city").val();
                data.fby_product = $("#fby_product").val();
                data.fby_from_date = $("#fby_from_date").val();
                data.fby_to_date = $("#fby_to_date").val();
                data.fby_keyword = $("#fby_keyword").val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ],
 
    });
 
});
</script>