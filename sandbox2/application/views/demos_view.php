<style type="text/css">
.modal.and.carousel, .carousel-control 
{
  position: fixed; 
} 
.modal-content
{
  /*min-height: 400px;
  max-height: 400px;*/
} 

.plot_photos
{
  height: 100px;
  width: 100px;
  border: 3px solid #ccc;
  text-align: center;
  border-radius: 3px;
  
}

.modal-body ul li
{
  margin-right: 1%;
  margin-bottom: 1%;
}

.full_image
{
  /*height: auto;
  max-height: auto;
  width: 100%;*/
}
</style>

<section class="content-header">
<h1>
Demo Details
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">


<ul class="nav nav-tabs">
<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>
</ul>


<fieldset style="overflow: auto;">
<table class="table no-border" border="0" width="100%">    
<tr><td colspan="2" align="right"><input type="button" name="export_btn" class="btn btn-danger" onclick="export_data('<?php echo $basic->farmer_id;?>', 'all');" value="Export All Stage"></td></tr>

<tr>
    <td valign="top" width="10%"><img src="<?php echo $basic-> farmer_photo_url;?>" width="70px" height="70px"></td>

    <td width="90%" valign="top">
        <small><?php echo "D".regno($basic-> farmer_reg_num, 5);?></small>
        <h4><?php echo $basic->farmer_name;?></h4>
        <?php echo $basic->district_name.", ".$basic->state_name;?>
    </td>
</tr>
</table>

<table class="table table-bordered" width="100%">    


<tr>
    <td>Registration Number: <?php echo "D".regno($basic-> farmer_reg_num, 5);?></td>

    <td>Date: <?php echo format_date($basic->visit_date,DATE);?></td>
    
    <td>By: <?php echo ucwords($basic->first_name." ".$basic->last_name);?></td>
</tr>        

<tr><td colspan="3" align="center"><b>Farmer Information</b></td></tr>

<tr>
    <td>Farmer Name: <?php echo $basic->farmer_name;?></td>    
    
    <td>Village: <?php echo $basic->farmer_village;?></td>
</tr>

<tr>
    <td>Taluka: <?php echo $basic->taluka_name;?></td>

    <td>District: <?php echo $basic->district_name;?></td>
    
    <td>State: <?php echo $basic->state_name;?></td>
</tr>
  

<tr>
    <td>Mobile: <?php echo $basic->farmer_mobile;?></td>

    <td>Email: <?php echo $basic->farmer_email;?></td>
</tr>


<tr>
    <td>Total Acrage: <?php echo $basic->total_acrage;?></td>

    <td>Crop 1: <?php echo get_crop_name('crops',$basic->crop_1);?></td>
    
    <td>Crop 2: <?php echo get_crop_name('crops',$basic->crop_2);?></td>
</tr>


<tr>
    <td>GPS Location: <?php echo $basic->gps_location;?>&nbsp;(Lat:<?php echo $basic->geo_lat;?> and Long:<?php echo $basic->geo_long;?>)</td>

    <td>Soil Test: <?php echo ucfirst($basic->soil_test);?></td>
    
    <td>Irrigated: <?php echo ucfirst($basic->irrigated);?></td>
</tr>
</table>



<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item active">
    <a class="nav-link active" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="true">Stage 1</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false">Stage 2</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="false">Stage 3</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="tab4-tab" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false">Stage 4</a>
  </li>

</ul>


<div class="tab-content" id="myTabContent">

<div class="tab-pane active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
<table class="table table-bordered" width="100%">    
<thead>
  <tr style="background-color: #3c8dbc;color:#fff;">
    <td colspan="2" align="center"><h4>Stage 1 - Demo Application</h4></td>
  </tr>
</thead>

<tr>
  <td colspan="2" align="right"><input type="button" name="export_btn" class="btn btn-success" onclick="export_data('<?php echo $basic->farmer_id;?>', 1);" value="Export Stage"></td>
</tr>

<tr>
    <td>Crop Selected: <?php echo $basic->crop_name;?></td>

    <td>Variety: <?php echo $basic->crop_variety_name;?></td>
</tr>            

<tr>
    <td>Acrage for Demo: <?php echo $basic->acrage_for_demo;?></td>

    <td>Reason: <?php echo $basic->demo_crop_reason;?></td>
</tr>


<tr>
  <td>Last Year Fertiliser Application for Same Crop:</td>

  <?php
  $td1 = "";
  if(isset($crop_last_year) && !empty($crop_last_year))
  {
      foreach($crop_last_year as $obj)
      {            
          $td1 .= $obj->crop_name.": ".$obj->last_year_crop_qty."<br/>";          
      }
  }
  ?>
  <td><?php echo $td1;?></td>
</tr>


<tr>
  <td>Plan for This Year Fertiliser Application:</td>

  <?php
  $td1 = "";
  if(isset($crop_current_year) && !empty($crop_current_year))
  {
      foreach($crop_current_year as $obj)
      {            
          $td1 .= $obj->crop_name.": ".$obj->current_year_crop_qty."<br/>";          
      }
  }
  ?>
  <td><?php echo $td1;?></td>
</tr>


<tr>
  <td>Fertiliser Applied by RMPCL in Demo:</td>
  <?php
  $td1 = "";
  if(isset($demos_product) && !empty($demos_product))
  {
      foreach($demos_product as $obj)
      {            
          if($obj->for_stage == 1 && $obj->demo_type == 1 && $obj->product_category_id == 1)
          {
              $td1 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          }
      }
  }
  ?>
  <td><?php echo $td1;?></td>
</tr>


<tr>
  <td colspan="2">Demo Start Date: <?php echo format_date($basic->demo_start_date,DATE);?></td>
</tr>

<tr>
  <td colspan="2">Stage 2 Visit Date: 
  <?php
  if(isset($demos_stage) && !empty($demos_stage))
  {
      foreach($demos_stage as $obj)
      {            
          if($obj->for_stage == 2)
          {
              echo format_date($obj->next_stage_date, DATE);
          }
      }
  }
  ?>
  </td>
</tr>
</table>
</div>



<div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
<table class="table table-bordered" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;color:#fff;">
    <td colspan="3" align="center"><h4>Stage 2 - Demo Application</h4></td>
  </tr>
</thead>

<tr>
  <td colspan="3" align="right"><input type="button" name="export_btn" class="btn btn-success" onclick="export_data('<?php echo $basic->farmer_id;?>', 2);" value="Export Stage"></td>
</tr>


<tr>
  <td colspan="3">Date: <?php echo format_date($basic->visit_date_stage_2,DATE);?></td>
</tr>


<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>


<tr>
  <td>Acrage</td>
  <td><?php echo $basic->acrage_demo;?></td>
  <td><?php echo $basic->acrage_comparative;?></td>
</tr>  


<tr>
  <td>Fertiliser Application</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_product) && !empty($demos_product))
  {
      foreach($demos_product as $obj)
      {            
          if($obj->for_stage == 2 && $obj->demo_type == 1 && $obj->product_category_id == 1)
          {
              $td1 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          }
          elseif($obj->for_stage == 2 && $obj->demo_type == 2 && $obj->product_category_id == 1)
          {
              $td2 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Other Fertiliser Application</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_product) && !empty($demos_product))
  {
      foreach($demos_product as $obj)
      {            
          if($obj->for_stage == 2 && $obj->demo_type == 1 && $obj->product_category_id == 2)
          {
              $td1 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          }
          elseif($obj->for_stage == 2 && $obj->demo_type == 2 && $obj->product_category_id == 2)
          {
              $td2 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Micro Nutrients</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_micro_nutrients) && !empty($demos_micro_nutrients))
  {
      foreach($demos_micro_nutrients as $obj)
      {            
          if($obj->for_stage == 2 && $obj->demo_type == 1)
          {
              $td1 .= $obj->micro_nutrient_name.": ".$obj->micro_nutrient_qty."<br/>";
          }
          elseif($obj->for_stage == 2 && $obj->demo_type == 2)
          {
              $td2 .= $obj->micro_nutrient_name.": ".$obj->micro_nutrient_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Weedicides</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_weedicides) && !empty($demos_weedicides))
  {
      foreach($demos_weedicides as $obj)
      {            
          if($obj->for_stage == 2 && $obj->demo_type == 1)
          {
              $td1 .= $obj->weedicide_name.": ".$obj->weedicide_qty."<br/>";
          }
          elseif($obj->for_stage == 2 && $obj->demo_type == 2)
          {
              $td2 .= $obj->weedicide_name.": ".$obj->weedicide_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>

<tr>
  <td colspan="3">Stage 3 Visit Date: 
  <?php
  $td1 = $td2 = "";
  if(isset($demos_stage) && !empty($demos_stage))
  {
      foreach($demos_stage as $obj)
      {            
          if($obj->for_stage == 3)
          {
              echo format_date($obj->next_stage_date, DATE);
          }
      }
  }
  ?> 
  </td>
</tr>
</table>

<?php
if(isset($plot_photos) && !empty($plot_photos))
{    
?>
    <div class="view_gallery_model">
    <div class="modal-header">    
    <h4 class="modal-title font-weight-semibold txt-blue ">Stage 2 Photos</h4>      
    </div>
    <div class="modal-body">
    <div class="form-area"> 
    <div class="row">
      <!-- <div class="col-md-6"><h4>Demo Plot</h4>
        <ul id="gallerylist_demo">
          <?php //echo prepare_plot_photo($plot_photos, 'thmb', 2, 1);?>          
        </ul>
      </div> -->  

      <div class="col-md-12"><!-- <h4>Comparative Plot</h4> -->
        <ul>
          <?php echo prepare_plot_photo($plot_photos, 'thmb', 2);?>
        </ul>
      </div>  
    </div>
    </div>   
    </div>    
    </div>

<div class="modal fade" id="lightbox_2">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span></button>        
</div>
<div class="modal-body">
<p>
<ol class="carousel-indicators">
<?php echo prepare_plot_photo($plot_photos, 'pop_li', 2);?>
</ol>
<div class="carousel-inner">
<?php echo prepare_plot_photo($plot_photos, 'pop_zom', 2);?>
</div>
<a class="left carousel-control" href="#lightbox_2" role="button" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" href="#lightbox_2" role="button" data-slide="next">
<span class="glyphicon glyphicon-chevron-right"></span>
</a>
</p>
</div>      
</div>    
</div>  
</div>

<?php
}
?>

</div>




<div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
<table class="table table-bordered" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;color:#fff;">
    <td colspan="3" align="center"><h4>Stage 3 - Flowering</h4></td>
  </tr>
</thead>

<tr>
  <td colspan="3" align="right"><input type="button" name="export_btn" class="btn btn-success" onclick="export_data('<?php echo $basic->farmer_id;?>', 3);" value="Export Stage"></td>
</tr>

<tr>
  <td colspan="3">Date: <?php echo format_date($basic->visit_date_stage_3,DATE);?></td>
</tr>


<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>


<tr>
  <td>Fertiliser Application</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_product) && !empty($demos_product))
  {
      foreach($demos_product as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1 && $obj->product_category_id == 1)
          {
              $td1 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2 && $obj->product_category_id == 1)
          {
              $td2 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Other Fertiliser Application</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_product) && !empty($demos_product))
  {
      foreach($demos_product as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1 && $obj->product_category_id == 2)
          {
              $td1 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2 && $obj->product_category_id == 2)
          {
              $td2 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Micro Nutrients</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_micro_nutrients) && !empty($demos_micro_nutrients))
  {
      foreach($demos_micro_nutrients as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 .= $obj->micro_nutrient_name.": ".$obj->micro_nutrient_qty."<br/>";
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 .= $obj->micro_nutrient_name.": ".$obj->micro_nutrient_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Weedicides</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_weedicides) && !empty($demos_weedicides))
  {
      foreach($demos_weedicides as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 .= $obj->weedicide_name.": ".$obj->weedicide_qty."<br/>";
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 .= $obj->weedicide_name.": ".$obj->weedicide_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>


<tr>
  <td>Crop Condition</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 = $obj->crop_condition;
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 = $obj->crop_condition;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Vegetative Growth</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 = $obj->vegitative_growth;
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 = $obj->vegitative_growth;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>


<tr>
  <td>Flowering Growth</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 = $obj->flowering_growth;
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 = $obj->flowering_growth;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>


<tr>
  <td>Fruitning Growth</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 = $obj->fruitning_growth;
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 = $obj->fruitning_growth;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>


<tr>
  <td>Quality</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 = $obj->quality;
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 = $obj->quality;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>

<tr>
  <td colspan="3">Stage 4 Visit Date: 
  <?php
  $td1 = $td2 = "";
  if(isset($demos_stage) && !empty($demos_stage))
  {
      foreach($demos_stage as $obj)
      {            
          if($obj->for_stage == 4)
          {
              echo format_date($obj->next_stage_date, DATE);
          }
      }
  }
  ?> 
  </td>
</tr>
</table>

<?php
if(isset($plot_photos) && !empty($plot_photos))
{    
?>
    <div class="view_gallery_model">
    <div class="modal-header">
    <h4 class="modal-title font-weight-semibold txt-blue ">Stage 3 Photos</h4>      
    </div>
    <div class="modal-body">
    <div class="form-area"> 
    <div class="row">
      <!-- <div class="col-md-6"><h4>Demo Plot</h4>
        <ul id="gallerylist_demo">
          <?php //echo prepare_plot_photo($plot_photos, 'thmb', 2, 1);?>          
        </ul>
      </div> -->  

      <div class="col-md-12"><!-- <h4>Comparative Plot</h4> -->
        <ul>
          <?php echo prepare_plot_photo($plot_photos, 'thmb', 3);?>
        </ul>
      </div>  
    </div>
    </div>   
    </div>    
    </div>

    <div class="modal fade" id="lightbox_3">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>        
    </div>
    <div class="modal-body">
    <p>
    <ol class="carousel-indicators">
    <?php echo prepare_plot_photo($plot_photos, 'pop_li', 3);?>
    </ol>
    <div class="carousel-inner">
    <?php echo prepare_plot_photo($plot_photos, 'pop_zom', 3);?>
    </div>
    <a class="left carousel-control" href="#lightbox_3" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#lightbox_3" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
    </p>
    </div>      
    </div>    
    </div>  
    </div>

    <!-- <div class="container">  
    <div class="modal fade and carousel slide" id="lightbox_3">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-body">
    <ol class="carousel-indicators">
      <?php //echo prepare_plot_photo($plot_photos, 'pop_li', 3);?>
    </ol>
    <div class="carousel-inner">
      <?php //echo prepare_plot_photo($plot_photos, 'pop_zom', 3);?>
    </div>
    <a class="left carousel-control" href="#lightbox_3" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#lightbox_3" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
    </div>    
    </div>
    </div>
    </div>
    </div> -->
<?php
}
?>

</div>

  


<div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
<table class="table table-bordered" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;color:#fff;">
    <td colspan="3" align="center"><h4>Stage 4 - Harvesting & Analysis Report</h4></td>
  </tr>
</thead>

<tr>
  <td colspan="3" align="right"><input type="button" name="export_btn" class="btn btn-success" onclick="export_data('<?php echo $basic->farmer_id;?>', 4);" value="Export Stage"></td>
</tr>

<tr>
  <td colspan="3">Date: <?php echo format_date($basic->visit_date_stage_4,DATE);?></td>
</tr>

<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>

<tr>
  <td>Yield (KGs/Acre)</td>  
  <td><?php echo $basic->yields_demo;?></td>
  <td><?php echo $basic->yields_comparative;?></td>
</tr>

<tr>
  <td>Quality</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 4 && $obj->demo_type == 1)
          {
              $td1 = $obj->quality;
          }
          elseif($obj->for_stage == 4 && $obj->demo_type == 2)
          {
              $td2 = $obj->quality;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>


<tr>
  <td>Market Price INR</td>  
  <td colspan="2"><?php echo $basic->market_price;?></td>
</tr>

<tr>
  <td>Yield Difference</td>  
  <td colspan="2"><?php echo $basic->yield_difference;?></td>
</tr>

<tr>
  <td>Produce Value / Acre</td>  
  <td colspan="2"><?php echo $basic->produce_value;?></td>
</tr>

<tr>
  <td>Cost of Production/Acre (fertilisers)</td>  
  <td colspan="2"><?php echo $basic->cost_of_production;?></td>
</tr>

<tr>
  <td>Yield Difference Justification</td>  
  <td colspan="2"><?php echo $basic->yield_difference_justification;?></td>
</tr>


<tr>
  <td>Analysis Report</td>  
  <td colspan="2">
    Comparative analysis of yield: <?php echo $basic->analysis_report_1;?><br/>
    Comparative time for life cycle: <?php echo $basic->analysis_report_2;?><br/>
    Requirement of irrigation analysis: <?php echo $basic->analysis_report_3;?><br/>
    Effect on growth, colour, yield and cost benefitation: <?php echo $basic->analysis_report_4;?>
  </td>
</tr>


<tr>
  <td>Farmers Feedback</td>  
  <td colspan="2"><?php echo $basic->farmers_feedback;?></td>
</tr>

<tr>
  <td>Executive Remark</td>  
  <td colspan="2"><?php echo $basic->executive_remark;?></td>
</tr>

<tr>
  <td>Guest</td>  
  <td colspan="2"><img class="img-circle" src="<?php echo $basic->guest_photo_url;?>" alt="<?php echo $basic->guest_name;?>"><?php echo $basic->guest_name;?></td>
</tr>


<tr>
  <td>Approve Reason</td>  
  <td colspan="2"><img class="img-circle" src="<?php echo $basic->guest_photo_url;?>" alt="<?php echo $basic->guest_name;?>"><?php echo $basic->approved_reason;?><br/><br/>Approve Date: <?php if(isset($basic->approved_date) && !empty($basic->approved_date)) echo date('d-M-Y', strtotime($basic->approved_date)); ?></td>
</tr>

</table>

<?php
if(isset($plot_photos) && !empty($plot_photos))
{    
?>
    <div class="view_gallery_model">
    <div class="modal-header">
    <h4 class="modal-title font-weight-semibold txt-blue ">Stage 4 Photos</h4>      
    </div>
    <div class="modal-body">
    <div class="form-area"> 
    <div class="row">
      <!-- <div class="col-md-6"><h4>Demo Plot</h4>
        <ul id="gallerylist_demo">
          <?php //echo prepare_plot_photo($plot_photos, 'thmb', 2, 1);?>          
        </ul>
      </div> -->  

      <div class="col-md-12"><!-- <h4>Comparative Plot</h4> -->
        <ul>
          <?php echo prepare_plot_photo($plot_photos, 'thmb', 4);?>
        </ul>
      </div>  
    </div>
    </div>   
    </div>    
    </div>


    <div class="modal fade" id="lightbox_4">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>        
    </div>
    <div class="modal-body">
    <p>
    <ol class="carousel-indicators">
    <?php echo prepare_plot_photo($plot_photos, 'pop_li', 4);?>
    </ol>
    <div class="carousel-inner">
    <?php echo prepare_plot_photo($plot_photos, 'pop_zom', 4);?>
    </div>
    <a class="left carousel-control" href="#lightbox_4" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#lightbox_4" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
    </p>
    </div>      
    </div>    
    </div>  
    </div>

    <!-- <div class="container">  
    <div class="modal fade and carousel slide" id="lightbox_4">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-body">
    <ol class="carousel-indicators">
      <?php //echo prepare_plot_photo($plot_photos, 'pop_li', 4);?>
    </ol>
    <div class="carousel-inner">
      <?php //echo prepare_plot_photo($plot_photos, 'pop_zom', 4);?>
    </div>
    <a class="left carousel-control" href="#lightbox_4" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#lightbox_4" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
    </div>    
    </div>
    </div>
    </div>
    </div> -->
<?php
}
?>

</div>
</div>
</fieldset> 






</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
function export_data(demo_id, for_stage)
{
    if(demo_id > 0 && for_stage == 'all')
    {
        var url = base_url + "admin/export_data/pdf/demos_details/"+ demo_id + "/" + for_stage;

        window.open(url,"");
    }
    else if(demo_id > 0 && for_stage > 0 && for_stage == 1)
    {
        var url = base_url + "admin/export_data/pdf/demos_details/"+ demo_id + "/" + for_stage;

        window.open(url,"");
    }
    else if(demo_id > 0 && for_stage > 1)
    {
        var url = base_url + "demos/export_data/demos_details/"+ demo_id + "/" + for_stage;

        window.open(url,"");
    }  
}  
</script>