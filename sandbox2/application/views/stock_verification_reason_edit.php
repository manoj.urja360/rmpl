<section class="content-header">
<h1>
Edit Stock Verification Reason
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<!-- <div class="box-header with-border">
<i class="fa fa-warning"></i>
<h3 class="box-title">Listing</h3>
</div>
 -->

<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/stock_verification_reason';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/stock_verification_reason_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>
</ul>

<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
  <div class="form-group">        
    <div class="col-sm-12">
      <label for="stock_verification_reason_name"><?php echo MANDATORY;?>Stock Verification Reason</label>
      <input class="form-control" name="stock_verification_reason_name" value="<?php echo $details->stock_verification_reason_name;?>" type="text" maxlength="255">
    </div>
  </div>
</div>  

<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/stock_verification_reason';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="<?php echo $details->stock_verification_reason_id;?>" type="hidden">
    </div>
  </div> 
</div> 

</form>
</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(document).ready(function()
{
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/stock_verification_reason_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/stock_verification_reason'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>