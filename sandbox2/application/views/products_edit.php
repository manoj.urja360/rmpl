<section class="content-header">
<h1>
Edit Product
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/products';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/products_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>
</ul>

<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
  <div class="form-group">        
    <div class="col-sm-8">
      <label for="product_category_name"><?php echo MANDATORY;?>Product Category</label>
      <select class="form-control select2" name="product_category_name">
          <option value="">Select</option>
          <?php
          foreach($product_category as $obj)
          {
              $sel = "";
              if($details-> product_category_id == $obj->product_category_id) $sel = "selected";
          ?>
            <option value="<?php echo $obj->product_category_id;?>" <?php echo $sel;?>><?php echo $obj->product_category_name;?></option>
          <?php  
          }
          ?>
      </select>  
    </div>    
  </div>
</div> 


<div class="row">
  <div class="form-group">        
    <div class="col-sm-8">
      <label for="product_name"><?php echo MANDATORY;?>Product Name</label>
      <input class="form-control" name="product_name" value="<?php echo $details->product_name;?>" type="text" maxlength="255">
    </div>
  </div>
</div> 

<div class="row">
  <div class="form-group">        
    <div class="col-sm-4">
      <label for="product_code"><?php echo MANDATORY;?>Product Code</label>
      <input class="form-control" name="product_code" value="<?php echo $details->product_code;?>" type="text" maxlength="100">
    </div>
  </div>
</div>  

<div class="row">
  <div class="form-group">        
    <div class="col-sm-4">
      <label for="product_subunit"><?php echo MANDATORY;?>UoM</label>
      <select class="form-control select2" name="product_subunit">
          <option value="">Select</option>
          <?php
          foreach($subunits as $pnm => $arr)
          {                  
          ?>
              <optgroup label="<?php echo $pnm;?>">
          <?php
              foreach($arr as $cid => $cnm) 
              {
                  $sel = "";
                  if($details-> subunit_id == $cid) $sel = "selected";
          ?>
                <option value="<?php echo $cid;?>" <?php echo $sel;?>><?php echo $cnm;?></option>
          <?php      
              }
          ?>
              </optgroup >              
          <?php  
          }
          ?>
      </select>  
    </div>

    <div class="col-sm-4">
      <label for="product_cost"><?php echo MANDATORY;?>Product Cost</label>
      <input class="form-control numericonly" name="product_cost" value="<?php echo $details->cost;?>" type="text" maxlength="7">
    </div>
  </div>
</div>



<div class="row btn_row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/products';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="<?php echo $details-> product_id;?>" type="hidden">
    </div>
  </div> 
</div> 



</form>
</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(document).ready(function()
{
    $(".select2").select2();

    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/products_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/products'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>