<section class="content-header">
<h1>
Edit Staff
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/staff';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/staff_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>
</ul>

<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row"><h4>General Details</h4>
  <div class="form-group">        
    <div class="col-sm-3">
      <label for="role_name"><?php echo MANDATORY;?>Select Role</label>
      <select class="form-control select2" name="role_name">
          <option value="">Select Role</option>
          <?php
          foreach($roles as $obj)
          {
          ?>
            <option value="<?php echo $obj->role_id;?>"><?php echo $obj->role_name;?></option>
          <?php  
          }
          ?>
      </select>  
    </div>    
  </div>
</div> 


<div class="row">
  <div class="form-group">        
    <div class="col-sm-3">
      <label for="first_name"><?php echo MANDATORY;?>First Name</label>
      <input class="form-control" name="first_name" value="" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="last_name"><?php echo MANDATORY;?>Last Name</label>
      <input class="form-control" name="last_name" value="" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="email"><?php echo MANDATORY;?>Email</label>
      <input class="form-control" name="email" value="" type="email" maxlength="150">
    </div>

    <div class="col-sm-3">
      <label for="mobile"><?php echo MANDATORY;?>Mobile</label>
      <input class="form-control" name="mobile" value="" type="text" maxlength="10">
    </div>
  </div>
</div> 


<div class="row">
  <div class="form-group">        
    <div class="col-sm-3">
      <label for="gender"><?php echo MANDATORY;?>Gender</label>
      <select class="form-control select2" name="gender">
          <option value="">Select Gender</option>
          <option value="male">Male</option>
          <option value="female">Female</option>
          <option value="other">Other</option>
      </select>
    </div>
      
    <div class="col-sm-9">
      <label for="address"><?php echo MANDATORY;?>Full Address</label>
      <input class="form-control" name="address" value="" type="text" maxlength="50">
    </div>
  </div>
</div>


<div class="row">
  <div class="form-group">        
    <div class="col-sm-3">
      <label for="admin_access"><?php echo MANDATORY;?>Allow Admin Access</label>
      <select class="form-control select2" name="admin_access">          
          <option value="yes">Yes</option>
          <option value="no">No</option>
      </select>
    </div>
  </div>
</div>

<div class="row">
  <div class="form-group">  
    <div class="col-sm-3">
      <label for="dob">Date Of Birth</label>
      <input class="form-control" name="dob" id="dob" value="" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="qualification">Highest Qualification</label>
      <input class="form-control" name="qualification" value="" type="text" maxlength="255">
    </div>

    <div class="col-sm-3">
      <label for="experience">Total Experience</label>
      <input class="form-control" name="experience" value="" type="text" maxlength="100">
    </div>

    <div class="col-sm-3">
      <label for="dob">Date Of Joining</label>
      <input class="form-control" name="doj" id="doj" value="" type="text" maxlength="50">
    </div>
  </div>
</div>



<div class="row"><h4>Admin Panel Login Details</h4>
  <div class="form-group">  
    <div class="col-sm-4">
      <label for="username"><?php echo MANDATORY;?>Username</label>
      <input class="form-control" name="username" value="" type="text" maxlength="50">
    </div>

    <div class="col-sm-4">
      <label for="password"><?php echo MANDATORY;?>Password</label>
      <input class="form-control" name="password" value="" type="password" maxlength="255">
    </div>

    <div class="col-sm-4">
      <label for="cpassword"><?php echo MANDATORY;?>Confirm Password</label>
      <input class="form-control" name="cpassword" value="" type="text" maxlength="255">
    </div>

  </div>
</div>



<div class="row"><h4>Mobile App Login Details</h4>
  <div class="form-group">  
    <div class="col-sm-4">
      <label for="keycode"><?php echo MANDATORY;?>Key Code</label>
      <input class="form-control" name="keycode" value="" type="text" maxlength="50">
    </div>
  </div>
</div>


<div class="row btn_row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/products';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="0" type="hidden">
    </div>
  </div> 
</div> 



</form>
</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(document).ready(function()
{
    $(".select2").select2();

    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/staff_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/staff'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>