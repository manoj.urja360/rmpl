<div class="col-sm-8">
	
  <div class="btn-group" style="float: right; margin-right: 2%;">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="fa fa-cloud-download"></span>&nbsp;Export</button>
    
    <ul class="dropdown-menu" role="menu">
      <li><a href="#" onclick="export_data('csv');"><i class="fa fa-file-text-o"></i>&nbsp;CSV</a></li>
      

      <li><a href="#" onclick="export_data('xls');"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</a></li>
      

      <li><a href="#" onclick="export_data('pdf');"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a></li>
    </ul>
  </div>      
</div>


<script type="text/javascript">
function export_data(typ)
{
	var file_id = 0;
	var report_type = "<?php echo $report_type;?>";

    var fby_state = $("#fby_state").val();
    var fby_city = $("#fby_city").val();
    var fby_product = $("#fby_product").val();
    var fby_dealer = $("#fby_dealer").val();
    var fby_to_date = 0;
    var fby_keyword = 0;
    

    if(fby_state == "" || fby_state == null) fby_state = 0;
    if(fby_city == "" || fby_city == null) fby_city = 0;
    if(fby_product == "" || fby_product == null) fby_product = 0;
    if(fby_dealer == "" || fby_dealer == null) fby_dealer = 0;
    

    var url = base_url + "admin/export_data/"+ typ + "/" + report_type + "/" + file_id + "/" + fby_state + "/" + fby_city + "/" + fby_product + "/" + fby_dealer + "/" + fby_to_date + "/" + fby_keyword;

    window.open(url,"");
}
</script>   