<!DOCTYPE html>
<html>
<head>
  
  <?php $this->load->view('includes/css'); ?>

  <?php $this->load->view("includes/js") ;?>
  
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition <?php echo THEME_COLOR;?> fixed sidebar-mini">
<div class="wrapper" style="background-color: #eee !important;">
  
  <header class="main-header">  
    <?php $this->load->view('includes/topmenu'); ?> 
  </header> 

  
  <aside class="main-sidebar">
    <?php $this->load->view("includes/leftmenu") ;?>    
  </aside>

  
  
  <div class="content-wrapper">