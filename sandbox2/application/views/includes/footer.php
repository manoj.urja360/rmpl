<div class="pull-right hidden-xs">
  <a href="https://delraf.com"><img src="<?php echo base_url(); ?>assets/images/delraf.png" class="footer_img" alt="Delref"> Powered by Delraf</a>
</div>

<strong>Copyright &copy; <?php echo date("Y");?> <a href="<?php echo base_url();?>">Fertiliser Inventory and Sales Dashboard</a>.</strong> All rights reserved.