<?php
$udata = $this->common_model->get_detail(array("typ" => "users", "id" => 1));
$hd_address = $hd_contact = "";
$compname = PROJECT_NAME;
if(isset($udata) && !empty($udata) && isset($udata-> company_details) && !empty($udata-> company_details))
{
    $carr = json_decode($udata-> company_details);
    $carr = (array)$carr;
    $hd_address = "<tr><td align='center'>".$carr['caddress'].", ".$carr['ccity_name'].", ".$carr['cstates_name']."</td></tr>";
    $hd_contact = "<tr><td align='center'>".$carr['mobile'].", ".$carr['email']."</td></tr>";

    $compname = $carr['company_name'];
}

?>
<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title><?php echo $compname;?></title><style type="text/css">table,tr,td{font-size:10px !important; border-collapse:collapse; } td { font-family: freeserif; } .table, .table tr, .table td, .table th {border:1px solid #bbb; padding:5px; color:#222;}#gallerylist_demo_1, #gallerylist_demo_1 li, #gallerylist_comp_1, #gallerylist_comp_1 li
{
  list-style: none;  
  cursor: move;
  margin-bottom: 2%;
}
.plot_photos_sort
{
    width: 100px;
    height:50px;
    border:3px solid #ddd;    
    border-radius: 5px;
}
.full_image
{
  height: auto;
  max-height: 300px;
  width: auto;
}
</style></head><body id="body_print"><table align="center" width="100%" border="0"><tr><td align="center"><img src="<?php echo base_url();?>assets/images/logo.png" width="50px" height="50px" alt="<?php echo $compname;?>"><h2><?php echo $compname;?></h2></td></tr><?php echo $hd_address.$hd_contact;?><tr><td align="center"><h3>Demo Stage <?php echo $for_stage_type;?></h3></td></tr></table>

<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">


<fieldset style="overflow: auto;">
<table class="table no-border" border="0" width="100%">    
<tr>
    <td valign="top" width="10%"><img src="<?php echo $basic-> farmer_photo_url;?>" width="70px" height="70px"></td>

    <td width="90%" valign="top">
        <small><?php echo "D".regno($basic-> farmer_reg_num, 5);?></small>
        <h4><?php echo $basic->farmer_name;?></h4>
        <?php echo $basic->district_name.", ".$basic->state_name;?>
    </td>
</tr>
</table>

<table class="table table-bordered" width="100%">    
<tr>
    <td>Registration Number: <?php echo "D".regno($basic-> farmer_reg_num, 5);?></td>

    <td>Date: <?php echo format_date($basic->visit_date,DATE);?></td>
    
    <td>By: <?php echo ucwords($basic->first_name." ".$basic->last_name);?></td>
</tr>        

<tr><td colspan="3" align="center"><b>Farmer Information</b></td></tr>

<tr>
    <td>Farmer Name: <?php echo $basic->farmer_name;?></td>    
    
    <td>Village: <?php echo $basic->farmer_village;?></td>
</tr>

<tr>
    <td>Taluka: <?php echo $basic->taluka_name;?></td>

    <td>District: <?php echo $basic->district_name;?></td>
    
    <td>State: <?php echo $basic->state_name;?></td>
</tr>
  

<tr>
    <td>Mobile: <?php echo $basic->farmer_mobile;?></td>

    <td>Email: <?php echo $basic->farmer_email;?></td>
</tr>


<tr>
    <td>Total Acrage: <?php echo $basic->total_acrage;?></td>

    <td>Crop 1: <?php echo $basic->crop_1;?></td>
    
    <td>Crop 2: <?php echo $basic->crop_2;?></td>
</tr>


<tr>
    <td>GPS Location: <?php echo $basic->gps_location;?></td>

    <td>Soil Test: <?php echo ucfirst($basic->soil_test);?></td>
    
    <td>Irrigated: <?php echo ucfirst($basic->irrigated);?></td>
</tr>
</table>



<div class="tab-content" id="myTabContent">
<?php
if($for_stage_type == 1)
{
?>  
<div class="tab-pane active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
<table class="table table-bordered" width="100%">    
<thead>
  <tr style="background-color: #3c8dbc;color:#fff;">
    <td colspan="2" align="center"><h4>Stage 1 - Demo Application</h4></td>
  </tr>
</thead>
<tr>
    <td>Crop Selected: <?php echo $basic->crop_name;?></td>

    <td>Variety: <?php echo $basic->crop_variety_name;?></td>
</tr>            

<tr>
    <td>Acrage for Demo: <?php echo $basic->acrage_for_demo;?></td>

    <td>Reason: <?php echo $basic->demo_crop_reason;?></td>
</tr>


<tr>
  <td>Last Year Fertiliser Application for Same Crop:</td>

  <?php
  $td1 = "";
  if(isset($crop_last_year) && !empty($crop_last_year))
  {
      foreach($crop_last_year as $obj)
      {            
          $td1 .= $obj->crop_name.": ".$obj->last_year_crop_qty."<br/>";          
      }
  }
  ?>
  <td><?php echo $td1;?></td>
</tr>


<tr>
  <td>Plan for This Year Fertiliser Application:</td>

  <?php
  $td1 = "";
  if(isset($crop_current_year) && !empty($crop_current_year))
  {
      foreach($crop_current_year as $obj)
      {            
          $td1 .= $obj->crop_name.": ".$obj->current_year_crop_qty."<br/>";          
      }
  }
  ?>
  <td><?php echo $td1;?></td>
</tr>


<tr>
  <td>Fertiliser Applied by RMPCL in Demo:</td>
  <?php
  $td1 = "";
  if(isset($demos_product) && !empty($demos_product))
  {
      foreach($demos_product as $obj)
      {            
          if($obj->for_stage == 1 && $obj->demo_type == 1 && $obj->product_category_id == 1)
          {
              $td1 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          }
      }
  }
  ?>
  <td><?php echo $td1;?></td>
</tr>


<tr>
  <td colspan="2">Demo Start Date: <?php echo format_date($basic->demo_start_date,DATE);?></td>
</tr>

<tr>
  <td colspan="2">Stage 2 Visit Date: 
  <?php
  if(isset($demos_stage) && !empty($demos_stage))
  {
      foreach($demos_stage as $obj)
      {            
          if($obj->for_stage == 2)
          {
              echo format_date($obj->next_stage_date, DATE);
          }
      }
  }
  ?>
  </td>
</tr>
</table>
</div>
<?php
}
elseif($for_stage_type == 2)
{
?>

<div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
<table class="table table-bordered" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;color:#fff;">
    <td colspan="3" align="center"><h4>Stage 2 - Demo Application</h4></td>
  </tr>
</thead>


<tr>
  <td colspan="3">Date: <?php echo format_date($basic->visit_date_stage_2,DATE);?></td>
</tr>


<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>


<tr>
  <td>Acrage</td>
  <td><?php echo $basic->acrage_demo;?></td>
  <td><?php echo $basic->acrage_comparative;?></td>
</tr>  


<tr>
  <td>Fertiliser Application</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_product) && !empty($demos_product))
  {
      foreach($demos_product as $obj)
      {            
          if($obj->for_stage == 2 && $obj->demo_type == 1 && $obj->product_category_id == 1)
          {
              $td1 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          }
          elseif($obj->for_stage == 2 && $obj->demo_type == 2 && $obj->product_category_id == 1)
          {
              $td2 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Other Fertiliser Application</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_product) && !empty($demos_product))
  {
      foreach($demos_product as $obj)
      {            
          if($obj->for_stage == 2 && $obj->demo_type == 1 && $obj->product_category_id == 2)
          {
              $td1 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          }
          elseif($obj->for_stage == 2 && $obj->demo_type == 2 && $obj->product_category_id == 2)
          {
              $td2 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Micro Nutrients</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_micro_nutrients) && !empty($demos_micro_nutrients))
  {
      foreach($demos_micro_nutrients as $obj)
      {            
          if($obj->for_stage == 2 && $obj->demo_type == 1)
          {
              $td1 .= $obj->micro_nutrient_name.": ".$obj->micro_nutrient_qty."<br/>";
          }
          elseif($obj->for_stage == 2 && $obj->demo_type == 2)
          {
              $td2 .= $obj->micro_nutrient_name.": ".$obj->micro_nutrient_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Weedicides</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_weedicides) && !empty($demos_weedicides))
  {
      foreach($demos_weedicides as $obj)
      {            
          if($obj->for_stage == 2 && $obj->demo_type == 1)
          {
              $td1 .= $obj->weedicide_name.": ".$obj->weedicide_qty."<br/>";
          }
          elseif($obj->for_stage == 2 && $obj->demo_type == 2)
          {
              $td2 .= $obj->weedicide_name.": ".$obj->weedicide_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>

<tr>
  <td colspan="3">Stage 3 Visit Date: 
  <?php
  $td1 = $td2 = "";
  if(isset($demos_stage) && !empty($demos_stage))
  {
      foreach($demos_stage as $obj)
      {            
          if($obj->for_stage == 3)
          {
              echo format_date($obj->next_stage_date, DATE);
          }
      }
  }
  ?> 
  </td>
</tr>
</table>

<?php
if(isset($plot_photos) && !empty($plot_photos))
{    
?>
<div class="">
<div class="form-area"> 
<div class="row">
<table width="100%">
<tr><td align="center"><h2>Demo Plot</h2></td> <td align="center"><h2>Comparative Plot</h2></td></tr>
<tr>
<td valign="top">
<ul id="gallerylist_demo_1">
  <?php echo prepare_plot_photo($plot_photos, 'in_html', 2, 1);?>
</ul>
</td>

<td valign="top">
<ul id="gallerylist_comp_1">
<?php echo prepare_plot_photo($plot_photos, 'in_html', 2, 2);?>  
</ul>
</td>
</tr>
</table>  
</div>
</div>   
</div>
<?php
}
?>


</div>

<?php
}
elseif($for_stage_type == 3)
{
?>
<div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
<table class="table table-bordered" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;">
    <td colspan="3" align="center"><h4>Stage 3 - Flowering</h4></td>
  </tr>
</thead>


<tr>
  <td colspan="3">Date: <?php echo format_date($basic->visit_date_stage_3,DATE);?></td>
</tr>


<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>


<tr>
  <td>Fertiliser Application</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_product) && !empty($demos_product))
  {
      foreach($demos_product as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1 && $obj->product_category_id == 1)
          {
              $td1 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2 && $obj->product_category_id == 1)
          {
              $td2 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Other Fertiliser Application</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_product) && !empty($demos_product))
  {
      foreach($demos_product as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1 && $obj->product_category_id == 2)
          {
              $td1 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2 && $obj->product_category_id == 2)
          {
              $td2 .= $obj->product_name.": ".$obj->product_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Micro Nutrients</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_micro_nutrients) && !empty($demos_micro_nutrients))
  {
      foreach($demos_micro_nutrients as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 .= $obj->micro_nutrient_name.": ".$obj->micro_nutrient_qty."<br/>";
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 .= $obj->micro_nutrient_name.": ".$obj->micro_nutrient_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Weedicides</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_weedicides) && !empty($demos_weedicides))
  {
      foreach($demos_weedicides as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 .= $obj->weedicide_name.": ".$obj->weedicide_qty."<br/>";
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 .= $obj->weedicide_name.": ".$obj->weedicide_qty."<br/>";
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>


<tr>
  <td>Crop Condition</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 = $obj->crop_condition;
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 = $obj->crop_condition;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>



<tr>
  <td>Vegetative Growth</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 = $obj->vegitative_growth;
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 = $obj->vegitative_growth;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>


<tr>
  <td>Flowering Growth</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 = $obj->flowering_growth;
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 = $obj->flowering_growth;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>


<tr>
  <td>Fruitning Growth</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 = $obj->fruitning_growth;
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 = $obj->fruitning_growth;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>


<tr>
  <td>Quality</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 3 && $obj->demo_type == 1)
          {
              $td1 = $obj->quality;
          }
          elseif($obj->for_stage == 3 && $obj->demo_type == 2)
          {
              $td2 = $obj->quality;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>

<tr>
  <td colspan="3">Stage 4 Visit Date: 
  <?php
  $td1 = $td2 = "";
  if(isset($demos_stage) && !empty($demos_stage))
  {
      foreach($demos_stage as $obj)
      {            
          if($obj->for_stage == 4)
          {
              echo format_date($obj->next_stage_date, DATE);
          }
      }
  }
  ?> 
  </td>
</tr>
</table>


<?php
if(isset($plot_photos) && !empty($plot_photos))
{    
?>
<div class="">
<div class="form-area"> 
<div class="row">
<table width="100%">
<tr><td align="center"><h2>Demo Plot</h2></td> <td align="center"><h2>Comparative Plot</h2></td></tr>
<tr>
<td valign="top">
<ul id="gallerylist_demo_1">
  <?php echo prepare_plot_photo($plot_photos, 'in_html', 3, 1);?>
</ul>
</td>

<td valign="top">
<ul id="gallerylist_comp_1">
<?php echo prepare_plot_photo($plot_photos, 'in_html', 3, 2);?>  
</ul>
</td>
</tr>
</table>  
</div>
</div>   
</div>
<?php
}
?>


</div>
<?php
}
elseif($for_stage_type == 4)
{
?>
<div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
<table class="table table-bordered" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;">
    <td colspan="3" align="center"><h4>Stage 4 - Harvesting & Analysis Report</h4></td>
  </tr>
</thead>


<tr>
  <td colspan="3">Date: <?php echo format_date($basic->visit_date_stage_4,DATE);?></td>
</tr>

<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>

<tr>
  <td>Yield (KGs/Acre)</td>  
  <td><?php echo $basic->yields_demo;?></td>
  <td><?php echo $basic->yields_comparative;?></td>
</tr>

<tr>
  <td>Quality</td>
  <?php
  $td1 = $td2 = "";
  if(isset($demos_crop_quality) && !empty($demos_crop_quality))
  {
      foreach($demos_crop_quality as $obj)
      {            
          if($obj->for_stage == 4 && $obj->demo_type == 1)
          {
              $td1 = $obj->quality;
          }
          elseif($obj->for_stage == 4 && $obj->demo_type == 2)
          {
              $td2 = $obj->quality;
          } 
      }
  }
  ?>
  <td><?php echo $td1;?></td>
  <td><?php echo $td2;?></td>
</tr>


<tr>
  <td>Market Price INR</td>  
  <td colspan="2"><?php echo $basic->market_price;?></td>
</tr>

<tr>
  <td>Yield Difference</td>  
  <td colspan="2"><?php echo $basic->yield_difference;?></td>
</tr>

<tr>
  <td>Produce Value / Acre</td>  
  <td colspan="2"><?php echo $basic->produce_value;?></td>
</tr>

<tr>
  <td>Cost of Production/Acre (fertilisers)</td>  
  <td colspan="2"><?php echo $basic->cost_of_production;?></td>
</tr>

<tr>
  <td>Yield Difference Justification</td>  
  <td colspan="2"><?php echo $basic->yield_difference_justification;?></td>
</tr>


<tr>
  <td>Analysis Report</td>  
  <td colspan="2">
    Comparative analysis of yield: <?php echo $basic->analysis_report_1;?><br/>
    Comparative time for life cycle: <?php echo $basic->analysis_report_2;?><br/>
    Requirement of irrigation analysis: <?php echo $basic->analysis_report_3;?><br/>
    Effect on growth, colour, yield and cost benefitation: <?php echo $basic->analysis_report_4;?>
  </td>
</tr>


<tr>
  <td>Farmers Feedback</td>  
  <td colspan="2"><?php echo $basic->farmers_feedback;?></td>
</tr>

<tr>
  <td>Executive Remark</td>  
  <td colspan="2"><?php echo $basic->executive_remark;?></td>
</tr>

<tr>
  <td>Guest</td>  
  <td colspan="2"><img class="img-circle" src="<?php echo $basic->guest_photo_url;?>" alt="<?php echo $basic->guest_name;?>"><?php echo $basic->guest_name;?></td>
</tr>

</table>


<?php
if(isset($plot_photos) && !empty($plot_photos))
{    
?>
<div class="">
<div class="form-area"> 
<div class="row">
<table width="100%">
<tr><td align="center"><h2>Demo Plot</h2></td> <td align="center"><h2>Comparative Plot</h2></td></tr>
<tr>
<td valign="top">
<ul id="gallerylist_demo_1">
  <?php echo prepare_plot_photo($plot_photos, 'in_html', 4, 1);?>
</ul>
</td>

<td valign="top">
<ul id="gallerylist_comp_1">
<?php echo prepare_plot_photo($plot_photos, 'in_html', 4, 2);?>  
</ul>
</td>
</tr>
</table>  
</div>
</div>   
</div>
<?php
}
?>


</div>
<?php
}
?>

</div>
</fieldset> 






</div>
</div>  
</div>
</div>

<button value="Print" class="btn btn-primary" onclick="javascript:window.print();">Print</button>

<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.js"></script>
<script type="text/javascript">
$('#gallerylist_demo_1').sortable();
$('#gallerylist_comp_1').sortable();
</script>

</body></html>