<section class="content-header">
<h1>
Upload Retailer POS Sale
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">
<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/inventory_retailer_pos';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/upload_retailer_pos';" data-toggle="tab" aria-expanded="false">Upload New</a></li>
</ul>


<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post" enctype="multipart/formdata">

<div class="row">
  <div class="form-group">        
    <div class="col-sm-12">
      <label>Last Uploaded File Date: <b><?php if(isset($details-> upload_date)) { echo date(DATE, strtotime($details-> upload_date));}else { echo "Not Found";}?></b></label>

    </div>
  </div>
</div>

<div class="row">
<div class="form-group">
<div class="col-sm-6">
<ol>
  <li>Steps:</li>
  <li>Download xls file <a href="<?php echo base_url();?>assets/sample/upload_retailer_pos.xls">click here</a>.</li>
  <li>Fill the data according to given format.</li>
  <li>Now click on left side browse button and select file.</li>
  <li>Click on Save button.</li>
</ol>
</div> 
</div>
</div>



<div class="row">
  <div class="form-group">          
    <div class="col-sm-6">
      <label for="upload_file"><?php echo MANDATORY;?>Select File</label>
      <input class="form-control" name="upload_file" id="upload_file" type="file" accept=".xls,.csv">
      <span class="help_txt">.xls, .csv file types are allowed.</span>
    </div>  
  </div>
</div> 



<div class="row">
  <div class="form-group">        
    <div class="col-sm-6">
      <label for="upload_date"><?php echo MANDATORY;?>Uploading File Of Date</label>
      <input class="form-control" name="upload_date" id="upload_date" type="text" maxlength="50"readonly>  
    </div>
  </div>
</div>  






<br/>
<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/inventory_retailer_pos';" class="btn btn-default btn_process">Cancel</button>
      <input name="upload_type" value="upload_retailer_pos" type="hidden">
    </div>
  </div> 
</div>

</form>
</div>
</div>  
</div>
</div>
</section>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.js"></script>
<script type="text/javascript">

$(document).ready(function()
{
    $("#upload_date").datepicker({changeMonth:true, 
      changeYear:true, 
      maxDate:0, 
      dateFormat:'dd-mm-yy'
    });
    

    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/upload_data",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    //window.location.href = base_url+'admin/inventory_acknowledgement1'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>