<style type="text/css">
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
  margin: 5px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}  
</style>
<section class="content-header">
<h1>
Edit Staff
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/staff';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/staff_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>
</ul>

<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<input name="hdn_id" id="hdn_id" value="<?php echo $details-> user_id;?>" type="hidden">


<?php
$js_chk1 = ""; $js_chk2 = "";
if(isset($details->job_status) && !empty($details->job_status) && $details->job_status == "active")
{
    $js_chk1 = "checked";
}
?>
<div class="row">
  <div class="form-group">        
    <div class="col-sm-6"></div>
    <div class="col-sm-6 text-right" style="float: right;">      
      <label for="first_name">Job Status:</label>
      <b>OFF</b><label class="switch">
         <input type="checkbox" name="job_status" value="active" <?php echo $js_chk1;?> >
        <span class="slider round"></span> 
      </label><b>ON</b>
      <!-- <label class="">
          <input type="radio" name="job_status" value="active" <?php //echo $js_chk1;?>>Active&nbsp;&nbsp;&nbsp;<input type="radio" name="job_status" value="inactive" <?php //echo $js_chk2;?>>&nbsp;Inactive           
      </label> -->

      
    </div>
  </div>
</div>    

<div class="row">
  <div class="form-group">        
    <div class="col-sm-3">      
      <label for="first_name"><?php echo MANDATORY;?>First Name</label>
      <input class="form-control" name="first_name" value="<?php echo $details-> first_name;?>" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="last_name"><?php echo MANDATORY;?>Last Name</label>
      <input class="form-control" name="last_name" value="<?php echo $details-> last_name;?>" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="email"><?php echo MANDATORY;?>Email</label>
      <input class="form-control" name="email" value="<?php echo $details-> email;?>" type="email" maxlength="150">
    </div>

    <div class="col-sm-3">
      <label for="mobile"><?php echo MANDATORY;?>Mobile</label>
      <input class="form-control" name="mobile" value="<?php echo $details-> mobile;?>" type="text" maxlength="10">
    </div>
  </div>
</div> 




<div class="row">
  <div class="form-group">        
    <div class="col-sm-3">
      <label for="role_name"><?php echo MANDATORY;?>Select Role</label>
      <select class="form-control select2" name="role_name">
          <?php
          foreach($roles as $obj)
          {
              if($details-> role_id == $obj-> role_id)
              {
          ?>
                <option value="<?php echo $obj-> role_id;?>"><?php echo $obj-> role_name;?></option>
          <?php  
              }
          }
          ?>
      </select>  
    </div>


    <div class="col-sm-3">
      <label for="admin_access"><?php echo MANDATORY;?>Allow Admin Access</label>
      <select class="form-control select2" name="admin_access">          
          <option value="yes" <?php if($details-> admin_access == 'yes'){echo 'selected';}?>>Yes</option>
          <option value="no" <?php if($details-> admin_access == 'no'){echo 'selected';}?>>No</option>
      </select>
    </div>

    <div class="col-sm-3">
      <label for="app_access"><?php echo MANDATORY;?>Allow App Access</label>
      <select class="form-control select2" name="app_access">          
          <option value="yes" <?php if($details-> app_access == 'yes'){echo 'selected';}?>>Yes</option>
          <option value="no" <?php if($details-> app_access == 'no'){echo 'selected';}?>>No</option>
      </select>
    </div>        
  </div>
</div> 



<hr/>

<div class="row">
<div class="">
<div class="nav-tabs-custom">
<ul class="nav nav-tabs">
<li class="active"><a href="#tab_4" data-toggle="tab"><?php echo MANDATORY;?>Area Of Operation</a></li>        
<li><a href="#tab_1" data-toggle="tab">Address Details</a></li>
<li><a href="#tab_2" data-toggle="tab">Login Details</a></li>
<li><a href="#tab_3" data-toggle="tab">Other Details</a></li>        
</ul>

<div class="tab-content">
<div class="tab-pane" id="tab_1">
<p>
<div class="row"><h5>Current Address</h5>
  <div class="form-group">    
    <div class="col-sm-6">
      <label for="cstates_name">Select State Name</label>
      <select name="cstates_name" id="cstates_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'ccity_name');" style="width: 100%;">
        <option value="">Select</option>
        <?php
          foreach($states_all as $obj)
          {
              $sel = "";
              if($obj-> state_id == $details-> cstate)
              {
                  $sel = "selected";                  
              }
          ?>    
            <option value="<?php echo $obj->state_id;?>" <?php echo $sel;?>><?php echo $obj->state_name;?></option>

            <?php
            if($sel != "")
            {
            ?>
              <script type="text/javascript">                
                get_dd_list(<?php echo $details->cstate;?>, 'district_by_state_aop_dd', 'ccity_name');
              </script>
          <?php
            }
          }
        ?>
       </select>
    </div>


    <div class="col-sm-6">
      <label for="ccity_name">Select City Name</label>
      <select name="ccity_name" id="ccity_name" class="form-control select2" style="width: 100%;"></select>
    </div>
  </div>  

  <div class="form-group">
    <div class="col-sm-6">
      <label for="caddress">Address 1</label>
      <input class="form-control" name="caddress" id="caddress" value="<?php echo $details-> caddress;?>" type="text" maxlength="255">
    </div> 

    <div class="col-sm-6">
      <label for="caddress2">Address 2</label>
      <input class="form-control" name="caddress2" id="caddress2" value="<?php echo $details-> caddress2;?>" type="text" maxlength="255">
    </div>  
    
  </div>
</div>


<div class="row"><h5><input type="checkbox" value="1" onclick="set_address();">&nbsp;Permanent Address</h5>
  <div class="form-group">    
    <div class="col-sm-6">
      <label for="pstates_name">Select State Name</label>
      <select name="pstates_name" id="pstates_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'pcity_name');" style="width: 100%;">
        <option value="">Select</option>
        <?php
          foreach($states_all as $obj)
          {
              $sel = "";
              if($obj-> state_id == $details-> pstate)
              {
                  $sel = "selected";                  
              }
          ?>    
            <option value="<?php echo $obj->state_id;?>" <?php echo $sel;?>><?php echo $obj->state_name;?></option>

            <?php
            if($sel != "")
            {
            ?>
              <script type="text/javascript">                
                get_dd_list(<?php echo $details->pstate;?>, 'district_by_state_aop_dd', 'pcity_name');
              </script>
          <?php
            }
          }
        ?>
       </select>
    </div>


    <div class="col-sm-6">
      <label for="pcity_name">Select City Name</label>
      <select name="pcity_name" id="pcity_name" class="form-control select2" style="width: 100%;"></select>
    </div>
  </div>
  

  <div class="form-group">
    <div class="col-sm-6">
      <label for="paddress">Address 1</label>
      <input class="form-control" name="paddress" id="paddress" value="<?php echo $details-> paddress;?>" type="text" maxlength="255">
    </div>  

    <div class="col-sm-6">
      <label for="paddress2">Address 2</label>
      <input class="form-control" name="paddress2" id="paddress2" value="<?php echo $details-> paddress2;?>" type="text" maxlength="255">
    </div>  
    
  </div>
</div>
</p> 
</div><!--tab_1-->



<div class="tab-pane" id="tab_2">
<p>
<div class="row"><h4>Admin Panel Login Details</h4>
  <div class="form-group">  
    

    <div class="col-sm-3">
      <label for="username">Username</label>
      <input class="form-control" name="" value="<?php echo $details-> username;?>" type="text" maxlength="50" readonly="">
    </div>

    <div class="col-sm-3">
      <label for="password">Password</label>
      <input class="form-control" name="password" value="" type="password" maxlength="255">
    </div>

    <div class="col-sm-3">
      <label for="cpassword">Confirm Password</label>
      <input class="form-control" name="cpassword" value="" type="password" maxlength="255">
    </div>

  </div>
</div>



<div class="row"><h4>Mobile App Login Details</h4>
  <div class="form-group">  
    

    <div class="col-sm-3">
      <label for="keycode1">Key Code</label>
      <input class="form-control" name="" value="<?php echo $details-> keycode;?>" type="text" maxlength="10" readonly>
    </div>
  </div>
</div>  
</p> 
</div><!--tab_2-->



<div class="tab-pane" id="tab_3">
<p>
<div class="row">
  <div class="form-group">  
    <div class="col-sm-3">
      <label for="gender">Gender</label>
      <select class="form-control select2" name="gender" style="width: 100%;">
          <option value="">Select Gender</option>
          <option value="male" <?php if($details-> gender == 'male'){echo 'selected';}?>>Male</option>
          <option value="female" <?php if($details-> gender == 'female'){echo 'selected';}?>>Female</option>
          <option value="other" <?php if($details-> gender == 'other'){echo 'selected';}?>>Other</option>
      </select>
    </div>

    <div class="col-sm-3">
      <label for="dob">Date Of Birth</label>
      <input class="form-control" name="dob" id="dob" value="<?php echo date(DATE_INPUT, strtotime($details-> dob));?>" type="text" maxlength="50">
    </div>
</div>
</div>
    

<div class="row">
  <div class="form-group"> 
    <div class="col-sm-3">
      <label for="qualification">Highest Qualification</label>
      <input class="form-control" name="qualification" value="<?php echo $details-> qualification;?>" type="text" maxlength="255">
    </div>

    <div class="col-sm-3">
      <label for="experience">Total Experience</label>
      <input class="form-control" name="experience" value="<?php echo $details-> experience;?>" type="text" maxlength="100">
    </div>

    <div class="col-sm-3">
      <label for="dob">Date Of Joining</label>
      <input class="form-control" name="doj" id="doj" value="<?php echo date(DATE_INPUT, strtotime($details-> doj));?>" type="text" maxlength="50">
    </div>
  </div>
</div>  
</p> 
</div><!--tab_3-->



<div class="tab-pane active" id="tab_4">
<p>
<div class="row"><h5>Area Of Operation</h5>
  <div class="form-group">        
    <div class="col-sm-12">
      <label for="states_name"><?php echo MANDATORY;?>Select State Name</label>
      <select name="states_name" id="states_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_user', 'dd_list');" style="width: 100%;">
        <option value="">Select State Name</option>
        <?php
          foreach($states_all as $obj)
          {
              $sel = "";
              if(isset($obj-> user_id) && !empty($obj-> user_id) && $details-> user_id == $obj-> user_id)
              {
                  $sel = "selected";                  
              }
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>

            <?php
            if($sel != "")
            {
            ?>
              <script type="text/javascript">
                get_dd_list(<?php echo $obj->state_id;?>, 'district_by_state_aop_user', 'dd_list');
              </script>
          <?php
            }
          }
        ?>
       </select>
    </div>    
  </div>
</div>

<h5>Selected States and Districts:</h5>
<div id="dd_list"></div>   
</p> 
</div><!--tab_4-->


</div>
</div>
</div>
</div>





<div class="row btn_row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/staff';" class="btn btn-default btn_process">Cancel</button>
      
    </div>
  </div> 
</div> 



</form>
</div>
</div>  
</div>
</div>
</section>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.js"></script>
<script type="text/javascript">

function set_address()
{        
    $("#paddress").val($("#caddress").val());
    $("#paddress2").val($("#caddress2").val());

    $("#pstates_name").val($("#cstates_name").val());
    $("#pstates_name").trigger("change");

    setTimeout(function()
    {      
      $("#pcity_name").val($("#ccity_name").val());
      $("#pcity_name").select2().trigger("chosen:updated");
    }, "1000");  
}

$(document).ready(function()
{
    setTimeout(function()
    {
      $("#ccity_name").val("<?php echo $details->ccity;?>");
      $("#pcity_name").val("<?php echo $details->pcity;?>");

      $("#ccity_name").select2().trigger("chosen:updated");
      $("#pcity_name").select2().trigger("chosen:updated");
    }, "1500");

    $("#states_name").trigger("change");

    $("#dob, #doj").datepicker({changeMonth:true, 
      changeYear:true, 
      maxDate:0, 
      dateFormat:'dd-mm-yy'
    });


    $(".select2").select2();


    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/staff_save_edit",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/staff'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>