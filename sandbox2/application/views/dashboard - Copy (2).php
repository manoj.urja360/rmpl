<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dashboard2.js"></script>
<script type="text/javascript">
var yearly_arr = [];
var monthly_arr = [];
var monthly_lbl = [];	
</script>
<?php
$sarr = $darr = $tarr = $snm = $dnm = $yearly = $monthly = $sstate = $sttarget = array();
$total_sales = 0;


if(isset($sales_target) && !empty($sales_target))
{
    foreach($sales_target as $obj)
    {
        $sttarget[$obj-> state_id] = $obj-> total;
    }
}  




if(isset($sales) && !empty($sales))
{
	foreach($sales as $obj)
	{
		  $m = date("m", strtotime($obj-> sales_date));      
		
  		if(isset($yearly[$m]) && !empty($yearly[$m]))
  			$yearly[$m] = $yearly[$m] + $obj-> quantity;
  		else
  			$yearly[$m] = $obj-> quantity;

      
      if(isset($sales_month) && !empty($sales_month))
      {
          $d = date("d", strtotime($obj-> sales_date));
          
          if(isset($monthly[$d]) && !empty($monthly[$d]))
            $monthly[$d] = $monthly[$d] + $obj-> quantity;
          else
            $monthly[$d] = $obj-> quantity;
      }  


  		if(isset($sstate[$obj-> state_id]) && !empty($sstate[$obj-> state_id]))
  			$sstate[$obj-> state_id] = $sstate[$obj-> state_id] + $obj-> quantity;
  		else
  			$sstate[$obj-> state_id] = $obj-> quantity;

  		$total_sales = $total_sales + $obj-> quantity;
	}
}

for($ii=1;$ii<=12;$ii++)
{
	$n = 0;
	if($ii<=9) $ii = "0".$ii;
	if(isset($yearly[$ii]) && !empty($yearly[$ii])) $n = $yearly[$ii];
?>
	<script type="text/javascript">
		yearly_arr.push(<?php echo $n;?>);
	</script>
<?php	
}


if(isset($sales_month) && !empty($sales_month))
{
  $dlen = date("t", strtotime("01-$sales_month-$sales_year"));
  for($ii=1;$ii<=$dlen;$ii++)
  {
    $n = 0;
    if($ii<=9) $ii = "0".$ii;
    if(isset($monthly[$ii]) && !empty($monthly[$ii])) $n = $monthly[$ii];
  ?>
    <script type="text/javascript">
      monthly_lbl.push(<?php echo $ii;?>);
      monthly_arr.push(<?php echo $n;?>);      
    </script>
  <?php 
  }
}



if(isset($sdaop) && !empty($sdaop))
{
	foreach($sdaop as $obj)
	{
		$snm[$obj-> state_id] = $obj-> state_name;
		$dnm[$obj-> district_id] = $obj-> district_name;
	}
}		

if(isset($counts) && !empty($counts))
{
	foreach($counts as $obj)
	{
		$obj-> quantity = number_format($obj-> quantity, '2', '.', '');

		if(isset($darr[$obj-> district_id][$obj-> type]) && !empty($darr[$obj-> district_id][$obj-> type]))
			$darr[$obj-> district_id][$obj-> type] = $darr[$obj-> district_id][$obj-> type] + $obj-> quantity;
		else
			$darr[$obj-> district_id][$obj-> type] = $obj-> quantity;


		if(isset($sarr[$obj-> state_id][$obj-> type]) && !empty($sarr[$obj-> state_id][$obj-> type]))
			$sarr[$obj-> state_id][$obj-> type] = $sarr[$obj-> state_id][$obj-> type] + $obj-> quantity;
		else
			$sarr[$obj-> state_id][$obj-> type] = $obj-> quantity;

		
		if(isset($tarr[$obj-> type]) && !empty($tarr[$obj-> type]))
			$tarr[$obj-> type] = $tarr[$obj-> type] + $obj-> quantity;
		else
			$tarr[$obj-> type] = $obj-> quantity;
	}
}

//echo "<pre>"; print_r($darr); die;

$ack1 = $ack2 = $wstk = $rstk = 0;
if(isset($tarr['ack1']) && !empty($tarr['ack1']))	$ack1 = $tarr['ack1'];
if(isset($tarr['ack2']) && !empty($tarr['ack2']))	$ack2 = $tarr['ack2'];
if(isset($tarr['wstk']) && !empty($tarr['wstk']))	$wstk = $tarr['wstk'];
if(isset($tarr['rstk']) && !empty($tarr['rstk']))	$rstk = $tarr['rstk'];
?>


<section class="content-header">
<h1>
	Dashboard		
</h1>
</section>


<section class="content">

<div class="row">
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">1<small><sup>st</sup></small> Point Ack (MT)</span>
      <span class="info-box-number"><?php echo $ack1;?></span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-red"><i class="fa fa-flag-o"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">2<small><sup>nd</sup></small> Point Ack (MT)</span>
      <span class="info-box-number"><?php echo $ack2;?></span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>

<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Wholesaler Stock (MT)</span>
      <span class="info-box-number"><?php echo $wstk;?></span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Retailer Stock (MT)</span>
      <span class="info-box-number"><?php echo $rstk;?></span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
</div>




<div class="row">
<?php
$bgcolor = array("green", "blue", "red", "yellow", "pink", "aqua");
$sno = 0;
foreach($snm as $id => $nm)
{
	if(isset($sarr[$id]) && !empty($sarr[$id]))
	{
		$bgc = $bgcolor[$sno]; $sno++;
		$ack1 = $ack2 = $wstk = $rstk = 0;
		if(isset($sarr[$id]['ack1']) && !empty($sarr[$id]['ack1']))	$ack1 = $sarr[$id]['ack1'];
		if(isset($sarr[$id]['ack2']) && !empty($sarr[$id]['ack2']))	$ack2 = $sarr[$id]['ack2'];
		if(isset($sarr[$id]['wstk']) && !empty($sarr[$id]['wstk']))	$wstk = $sarr[$id]['wstk'];
		if(isset($sarr[$id]['rstk']) && !empty($sarr[$id]['rstk']))	$rstk = $sarr[$id]['rstk'];
?>	
<div class="col-md-4">
<div class="box box-widget widget-user-2">
<div class="widget-user-header bg-<?php echo $bgc;?>">
  <h4 class=""><?php echo $nm;?></h4>
</div>
<div class="box-footer no-padding">
  <ul class="nav nav-stacked">
    <li><a href="#">1st Point Ack <span class="pull-right badge bg-blue"><?php echo $ack1;?></span></a></li>
    <li><a href="#">2nd Point Ack <span class="pull-right badge bg-aqua"><?php echo $ack2;?></span></a></li>
    <li><a href="#">Wholesaler Stock <span class="pull-right badge bg-green"><?php echo $wstk;?></span></a></li>
    <li><a href="#">Retailer Stock <span class="pull-right badge bg-red"><?php echo $rstk;?></span></a></li>
  </ul>
</div>
</div>
</div>
<?php
	}
}
?>
</div>




<div class="row">
<div class="col-lg-12">
<div class="box">
<div class="box-body">
<?php
foreach($dnm as $id => $nm)
{
	if(isset($darr[$id]) && !empty($darr[$id]))
	{		
		$ack1 = $ack2 = $wstk = $rstk = 0;
		if(isset($darr[$id]['ack1']) && !empty($darr[$id]['ack1']))	$ack1 = $darr[$id]['ack1'];
		if(isset($darr[$id]['ack2']) && !empty($darr[$id]['ack2']))	$ack2 = $darr[$id]['ack2'];
		if(isset($darr[$id]['wstk']) && !empty($darr[$id]['wstk']))	$wstk = $darr[$id]['wstk'];
		if(isset($darr[$id]['rstk']) && !empty($darr[$id]['rstk']))	$rstk = $darr[$id]['rstk'];
?>	
		<div class="col-md-4">
			<h6><?php echo $nm;?></h6>
			<span class="text-muted"><?php echo $ack1;?>&nbsp;|&nbsp;</span>
		    <span class="text-muted"><?php echo $ack2;?>&nbsp;|&nbsp;</span>
		    <span class="text-muted"><?php echo $wstk;?>&nbsp;|&nbsp;</span>
		    <span class="text-muted"><?php echo $rstk;?></span>
			<!-- <span class="badge bg-aqua"><?php echo $ack1;?></span>
		    <span class="badge bg-red"><?php echo $ack2;?></span>
		    <span class="badge bg-green"><?php echo $wstk;?></span>
		    <span class="badge bg-yellow"><?php echo $rstk;?></span> -->
		</div>
<?php
	}
}
?>
</div>
</div>
</div>
</div>




<div class="row">
<div class="col-md-12">
<div class="box">
<div class="box-header with-border">
  <h3 class="box-title">
  	Sales Report
  </h3> 

  <div class="pull-right">
  <div class="col-md-6">
  <select name="sale_year" id="sale_year" class="form-control" onchange="get_chart();">
  <?php
  for($y=date("Y"); $y>=2019;$y--)
  {
  		$s = "";
  		if($sales_year == $y) $s = "selected";
  		echo "<option value='$y' $s>$y</option>";
  }?>
  </select>	
  </div>

  <div class="col-md-6">
  <select name="sale_month" id="sale_month" class="form-control" onchange="get_chart();">
  <option value=''>Select Month</option>
  <?php
  $smonth = array('January', 'February', 'March', 'April', 'May', 'June', 'July', "August", "September", "October", "November", "December");  
  foreach($smonth as $mid => $mnm)
  {
      $mid = $mid + 1;

      $s = "";
      if($sales_month == $mid) $s = "selected";
      echo "<option value='$mid' $s>$mnm</option>";
  }?>
  </select> 
  </div>   
  </div>	
</div>

<div class="box-body">
  <div class="row">
    <div class="col-md-12">
      <p class="text-center">
        <strong>Yearly Sales: 01 Jan <?php echo $sales_year;?> To 31 Dec <?php echo $sales_year;?></strong>
      </p>

      <div class="chart">
      	<canvas id="barChart" style="height:150px"></canvas>
      </div>      
    </div>
  </div>  

<?php
if(isset($sales_month) && !empty($sales_month))
{
?>
  <div class="row">
    <div class="col-md-12">
      <p class="text-center">
        <strong>Monthly Sales: 01 <?php echo date("M Y", strtotime("01-$sales_month-$sales_year"));?>  To <?php echo date("d M Y", strtotime("$dlen-$sales_month-$sales_year"));?></strong>
      </p>

      <div class="chart">
        <canvas id="barChart1" style="height:150px"></canvas>
      </div>      
    </div>
  </div>
<?php
}
?> 


  <div class="row">  
    <div class="col-md-12">
      <p class="text-center">
        <strong>Target Achievement</strong>
      </p>

      <div class="progress-group">
        <span class="progress-text">Overall</span>
        <span class="progress-number"><b><?php echo $total_sales;?></b></span>

        <div class="progress sm">
          <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
        </div>
      </div>
      
      <?php
		$bgcolor = array("green", "blue", "red", "yellow", "pink", "aqua");
		$sno = 0;
		foreach($snm as $id => $nm)
		{
			$bgc = $bgcolor[$sno]; $sno++;
			$num = $per = 0;
			if(isset($sstate[$id]) && !empty($sstate[$id]))
			{							
				  $num = $sstate[$id];
			}

      $percent = 0;
      if($num > 0 && $sales_target > 0) 
      {
          $percent = round( ($num * 100) / $sales_target );
      }

      $sales_target = "NA";
      if(isset($sttarget[$id]) && !empty($sttarget[$id]))
        $sales_target = $sttarget[$id]."%";
		?>
      
              <div class="progress-group">
                <span class="progress-text"><?php echo $nm;?></span>
                <span class="progress-number"><b><?php echo $num;?></b>/<?php echo $sales_target;?></span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-<?php echo $bgc;?>" style="width: <?php echo $percent;?>%"></div>
                </div>
              </div>
      	<?php
      	}
      	?>                  
      
    </div>
    
  </div>
  
</div>


</div>

</div>

</div>



</section>

<script type="text/javascript">
function get_chart()
{
	  var year = $("#sale_year").val(); 
    var month = $("#sale_month").val(); 

    window.location.href = "<?php echo base_url();?>admin/index/"+year+"/"+month;
}	
</script>