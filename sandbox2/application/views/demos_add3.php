<section class="content-header">
<h1>
Add New Demo Stage 3
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add';" data-toggle="tab" aria-expanded="false">Add Demo Stage 1</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add2';" data-toggle="tab" aria-expanded="false">Add Demo Stage 2</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add3';" data-toggle="tab" aria-expanded="false">Add Demo Stage 3</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add4';" data-toggle="tab" aria-expanded="false">Add Demo Stage 4</a></li>

</ul>


<div id="message_box"></div>


<fieldset >
<form class="form-horizontal" name="process_form3" id="process_form3" method="post" style="margin:0px !important;">
<table class="table table-stripped" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;">
    <td colspan="3" align="center"><h4>Stage 3 - Flowering</h4></td>
  </tr>
</thead>

<tr>
    <td colspan="3"><?php echo MANDATORY;?>Select Demo: 
      <select name="farmer_id" id="farmer_id" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php
          foreach($farmers as $obj)
          {
          ?>    
            <option value="<?php echo $obj-> farmer_id;?>"><?php echo $obj-> farmer_name." - D".regno($obj-> farmer_reg_num, 5);?></option>
          <?php
          }
        ?>       
      </select>  
    </td>    
</tr> 

<tr>
  <td colspan="3"><?php echo MANDATORY;?>Visit Date: <input type="text" name="visit_date_stage_3" class="date_sel"></td>
</tr>


<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>


<tr>
  <td>Fertiliser Application</td>
  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>

      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="company_fertilizer_demo_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 1)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="company_fertilizer_demo_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>


  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="company_fertilizer_comparative_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 1)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="company_fertilizer_comparative_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>



<tr>
  <td>Other Fertiliser Application</td>  
  
  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="other_company_fertilizer_demo_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 2)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="other_company_fertilizer_demo_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>

  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="other_company_fertilizer_comparative_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 2)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="other_company_fertilizer_comparative_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>



<tr>
  <td>Micro Nutrients</td>  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Micro Nutrient Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="micro_nutrients_demo_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['micro_nutrients'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="micro_nutrients_demo_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Micro Nutrient Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="micro_nutrients_comparative_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['micro_nutrients'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="micro_nutrients_comparative_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>



<tr>
  <td>Weedicides</td>

  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Weedicide Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="weedicides_demo_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['weedicides'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="weedicides_demo_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>

  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Weedicide Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="weedicides_comparative_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['weedicides'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="weedicides_comparative_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>


<tr>
  <td>Crop Condition</td>
  
  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="crop_condition_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="crop_condition_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="crop_condition_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="crop_condition_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="crop_condition_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="crop_condition_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>



<tr>
  <td>Vegetative Growth</td>

  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="vegitative_growth_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="vegitative_growth_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="vegitative_growth_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="vegitative_growth_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="vegitative_growth_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="vegitative_growth_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>


<tr>
  <td>Flowering Growth</td>
  
  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="flowering_growth_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="flowering_growth_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="flowering_growth_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="flowering_growth_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="flowering_growth_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="flowering_growth_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>


<tr>
  <td>Fruitning Growth</td>
  
  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="fruitning_growth_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="fruitning_growth_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="fruitning_growth_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="fruitning_growth_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="fruitning_growth_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="fruitning_growth_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>


<tr>
  <td>Quality</td>
  
  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="quality_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="quality_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="quality_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="quality_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="quality_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="quality_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>


<tr>
  <td>Photos</td>

  <td>
    <table width="100%">
      <tr>        
        <td><input type="file" name="plot_photos[]" class="form-control" multiple=""></td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <tr>
        <td><input type="file" name="plot_photos_comparative[]" class="form-control" multiple=""></td>
      </tr>
    </table>
  </td>
</tr>

<tr>
  <td colspan="3"><?php echo MANDATORY;?>Stage 4 Visit Date: <input type="text" name="next_stage_date" class="date_sel"></td>
</tr>

<tr>
  <td colspan="3">    
    <button type="button" name="btn_save" id="btn_save" class="btn btn-primary btn_process" onclick="form_submit(3);">Save Stage 3</button>&nbsp;
    
    <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" class="btn btn-default btn_process">Cancel</button>
    
    <input name="hdn_id" value="0" type="hidden">        
</td>
</tr> 
</table>
</form>
</fieldset> 




</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(function()
{
    $(".select2").select2();

    $(".date_sel").datepicker({maxDate:0, dateFormat:"dd-mm-yy"});
});



function form_submit(stage_num)
{
    processing_bar();

    var formData = new FormData($("#process_form"+stage_num)[0]);

    $.ajax({url : base_url+"demos/demo_stage_"+stage_num,
      method: "POST",
      data: formData,
      async: false,
      dataType: 'json',
      success: function(res)
      {   
          if(res.status == 1)
          {
              msg = msg_ok + res.message + '</div>';

              setTimeout(function()
              {                    
                window.location.href = base_url+'admin/demos'; 
                
              }, time_out);
          }
          else
          {
              msg = msg_error + res.message + '</div>';

              hide_msg_box();
          }
          
          show_msg_box(msg);
      },
      cache: false,
      contentType: false,
      processData: false
    });

    return false;
    
}

</script>