<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
	public function __construct()
    {
        parent::__construct();        
        
        $this->load->model("login_model");

    }



	public function index()
	{
		if($this->is_logged_in())
		{
			redirect('/');
		}
		else
		{
			$arr = array("typ" => "users", "id" => 1);
            $data['details'] = $this->common_model->get_detail($arr);
            if(isset($data['details']->company_details) && !empty($data['details']->company_details))
            {
                $data['details']->company_details = json_decode($data['details']->company_details);

                $data['details']->company_details = (array) $data['details']->company_details;
            }

			$this->load->view('login', $data);
		}	
	}


	public function forgot()
	{
		if($this->is_logged_in())
		{
			redirect('/');
		}
		else
		{
			$arr = array("typ" => "users", "id" => 1);
			$data['details'] = $this->common_model->get_detail($arr);
            if(isset($data['details']->company_details) && !empty($data['details']->company_details))
            {
                $data['details']->company_details = json_decode($data['details']->company_details);

                $data['details']->company_details = (array) $data['details']->company_details;
            }

			$this->load->view('forgot', $data);
		}	
	}


	public function is_logged_in()
    {
    	$data = $this->session->all_userdata();

    	if(isset($data['user_id']) && !empty($data['user_id']))
    	{
    		redirect('/admin');
    	}
    	else
    	{ 		
    		return false;
    	}
    }


	public function check_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');	

		if($username == "")
		{
			echo json_encode(array("status"=>0,"message"=>"Please enter Username."));
			
			die;	
		}
		elseif($password == "")
		{
			echo json_encode(array("status"=>0,"message"=>"Please enter Password."));
			
			die;	
		}

		$data = $this->login_model->check_login($username,md5($password));

		if(isset($data) && !empty($data))
		{
			if($this->input->post('rememberme') == 1  && !empty($this->input->post('rememberme')))
			{
				$this->input->set_cookie('username',$username,'3600');
				$this->input->set_cookie('password',$password,'3600');
			}


			$this->dbaccess_model->update('users_login',array("last_login_date"=>date(DATETIME_DB)),array("user_id ="=>$data->user_id, "password_for =" => 1));
			
			$session_data = (array)$data;	
			$this->session->set_userdata($session_data);

			echo json_encode(array("status"=>1,"message"=>"Successfully Sign In.")); die;
		}
		else
		{
			echo json_encode(array("status"=>0,"message"=>"Sign In failed."));
		}

		die;
	}


	public function do_login()
	{
		$keyid = $this->input->post('keyid');

		if($keyid == "")
		{
			echo json_encode(array("status"=>0,"message"=>"Please enter key."));			
			die;	
		}

		$this->load->model("webservice_model");
		$data = $this->webservice_model->do_login($keyid);

		if(isset($data) && !empty($data))	
		{			
			if(strtolower($data-> app_access) == "no")
			{
				echo json_encode(array("status"=>0,"message"=>"You do not have permission to access mobile app. Please contact to management."));			
				die;
			}

			$session_key = time().rand(10000, 99999).time().rand(10000, 99999);

			$this->dbaccess_model->update('users_login',array("last_login_date"=>date(DATETIME_DB), "session_key" => $session_key),array("user_id ="=>$data-> user_id, "password_for =" => 2));
						
			$data-> session_key = $session_key;

			$data-> aoo = $this->login_model->get_aoo($data-> user_id);


			echo json_encode(array("status"=>1, "message"=>"Successfully Sign In.", "data" => $data)); die;
		}
		else
		{
			echo json_encode(array("status"=>0, "message"=>"Sign In failed."));
		}

		die;
	}



	public function forgot_send()
	{
		$username = $this->input->post('username');	
		$email = $this->input->post('email');	

		if($username == "")
		{
			echo json_encode(array("status"=>0,"message"=>"Please enter Username."));
			
			die;	
		}
		if($email == "")
		{
			echo json_encode(array("status"=>0,"message"=>"Please enter Email."));
			
			die;	
		}

		$data = $this->login_model->check_email($username, $email);

		if(isset($data) && !empty($data))
		{
			if($data->admin_access == "no")
			{
				echo json_encode(array("status"=>0,"message"=>"Your account has been disabled by administrator. Please contact to your owner.")); die;
			}

			$newpassword = generate_random_string(10);					
			
			$Content = "Hello ".$data->first_name." ".$data->last_name."<br/><br/>Your password has been reset.<br/>Use new password for accessing your account on RMPCL.<br/><br/>New Password:".$newpassword;

			$SendMail = sendMail(array(
			'emailFromName'	=> SMTP_FROM_NAME,			
			'emailFrom' 	=> SMTP_FROM_EMAIL,			
			'emailTo' 		=> $email,			
			'emailSubject'	=> "Password has been reset - RMPhosphates",
			'emailMessage'	=>  $this->load->view('emailer/email_template',array("Content" =>  $Content),TRUE)
			));

			if($SendMail == true)
			{
				$this->dbaccess_model->update('users_login',array("password"=>md5($newpassword), "updated_date"=>date(DATETIME_DB)),array("user_id ="=>$data->user_id, "password_for =" => 1));	

				echo json_encode(array("status"=>1,"message"=>"Password has been sent on your registered email. Please use new password for login into account.")); die;
			}
			else
			{
				echo json_encode(array("status"=>0,"message"=>"Oops!!! Error occur while sending email. Please check email and try again."));
			}	
		}
		else
		{
			echo json_encode(array("status"=>0,"message"=>"Entered details not found in the system. Please check and try again."));
		}

		die;
	}



	public function logout()
	{
		session_destroy();

		$data = $this->session->all_userdata();

		$this->session->unset_userdata($data);

		echo "<script> window.location.href = '".base_url()."'; </script>";

		die;
	}



	function send()
	{
		error_reporting(E_ALL);

		$Input = array(
			'emailFromName'	=> SMTP_FROM_NAME,			
			'emailFrom' 	=> SMTP_FROM_EMAIL,			
			'emailTo' 		=> "ayaz.dewas@gmail.com",			
			'emailSubject'	=> "Password has been reset - RMPhosphates",
			'emailMessage'	=>  "Test email from RMPCL"
			);

		sendMail($Input);
		die;

		$CI = & get_instance();
        $CI->load->library('email');        
        $config = array(
            'protocol' => mail, // 'mail', 'sendmail', or 'smtp'
            /*'smtp_host' => SMTP_HOST, 
            'smtp_port' => 587,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASS,*/
            'smtp_crypto' => 'ssl', //can be 'ssl' or 'tls' for example
            'mailtype' => 'html', //plaintext 'text' mails or 'html'
            'smtp_timeout' => '4', //in seconds
            'charset' => 'utf-8',
            'wordwrap' => TRUE
        );
        
        $CI->email->initialize($config);
        $CI->email->set_newline("\r\n");
        $CI->email->clear();
        
        $CI->email->from($Input['emailFrom'], $Input['emailFromName']);        
        $CI->email->reply_to($Input['emailFrom'], $Input['emailFromName']);

        $CI->email->to($Input['emailTo']);
        //$CI->email->bcc("vineet.jain@rmphosphates.com");
        //$CI->email->bcc("write2shalabh@gmail.com");        

        $CI->email->subject($Input['emailSubject']);
        $CI->email->message($Input['emailMessage']);
        if (@$CI->email->send()) {
            echo "sent";
            return true;

        } else {
        echo $CI->email->print_debugger();
            return false;
        }
	}

}
