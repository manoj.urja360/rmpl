<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code




/********************************************************************
Basics
********************************************************************/
define('THEME_COLOR', "skin-blue-light");

define('PROJECT_NAME', "RMPCL Fertilizer Management");

define('BASE_PATH', "https://rmphosphates.in");

define('MANDATORY', "<span style='color:red;'>*</span>");


define('TABLE_LISTING_CLASS','table-responsive no-padding table-bordered table-striped'); 
define('TABLE_LIST_CLASS','display nowrap '.TABLE_LISTING_CLASS); 


//Upload Path---------------------------------------------------
define('UPLOAD_PATH', "uploads");
define('UPLOAD_IMAGE_PATH', UPLOAD_PATH."/images");
define('PATH_ALBUM', "assets/uploads/album/");

//JS Variables---------------------------------------------
define("REDIRECT_TIMEOUT","3000");
define("RESPONSE_SUCCESS","success");
define("PAGING_TYPE","full_numbers");
define("PAGE_LENGTH","10");
define("SCROLL_X","true");
define("PAGING_POS_SEARCH",'<"top"<"actions">fpi<"clear">><"clear">rt<"bottom">');
define("PAGING_POS",'<"top">flrt<"bottom"ip><"clear">');


//Date Time Format---------------------------------------------
define('DATETIME','d-m-Y H:i:s');
define('DATE','d-M-Y');
define('TIME','H:i:s');
define('DATETIME_DB','Y-m-d H:i:s');
define('DATE_DB','Y-m-d');
define('DATE_INPUT','d-m-Y');

//Table Columns Width----------------------------------------------
define('COL_30','');
define('COL_50','');
define('COL_70','');
define('COL_100','');
define('COL_150','');
define('COL_200','');
define('COL_300','');
define('COL_400','');
define('COL_500','');
define('COL_600','');




//New Lastest--------------------------------
define("SMTP_HOST","smtp.hostinger.in");
define("SMTP_USER","webadmin@rmphosphates.in");
define("SMTP_PASS","Webmail@2021");
define("SMTP_PORT","587");
define("SMTP_PROTOCOL","smtp");
define("SMTP_FROM_NAME","Webadmin");
define("SMTP_FROM_EMAIL","webadmin@rmphosphates.in");


//SMTP Details Old---------------------------------------------
/*define("SMTP_HOST","mail.smtp2go.com");
define("SMTP_USER","rmpclapp");
define("SMTP_PASS","cG5reXpybHJtMGww");
define("SMTP_PORT","2525");
define("SMTP_PROTOCOL","smtp");
define("SMTP_FROM_NAME","Marketing");
define("SMTP_FROM_EMAIL","marketing.rmpcl@rmphosphates.com");*/






/*define("SMTP_HOST","smtp.hostinger.in");
define("SMTP_USER","sboxmail@sbox.rmphosphates.in");
define("SMTP_PASS","Testacc@1805");
define("SMTP_PORT","587");
define("SMTP_PROTOCOL","smtp");

define("SMTP_FROM_NAME","Webadmin");
define("SMTP_FROM_EMAIL","webadmin@rmphosphates.in");*/

/*define("SMTP_HOST","smtp.hostinger.in");
define("SMTP_USER","webadmin@rmphosphates.in");
define("SMTP_PASS","Webmail@2021");
define("SMTP_PORT","587");
define("SMTP_PROTOCOL","smtp");*/