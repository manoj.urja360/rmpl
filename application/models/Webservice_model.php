<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webservice_model extends CI_Model 
{
    
    public function __construct()
    {
        parent::__construct();
    }


    public function do_login($keyid)
    {
        $password = md5($keyid);

        $sql = "SELECT u.*, r.role_name
        FROM users u
        INNER JOIN users_login ul ON u.user_id = ul.user_id
        INNER JOIN roles r ON r.role_id = u.role_id
        WHERE u.is_deleted = 0 AND u.keycode = '$keyid' AND ul.password = '$password' AND ul.password_for = 2
        LIMIT 1";

        $query = $this->db->query($sql);

        return $query->row();
    }



    public function check_session_key($session_key)
    {
        $sql = "SELECT u.*
        FROM users u
        INNER JOIN users_login ul ON u.user_id = ul.user_id
        WHERE u.is_deleted = 0 AND ul.session_key = '$session_key' AND ul.password_for = 2
        LIMIT 1";

        $query = $this->db->query($sql);

        return (array)$query->row();
    }


    public function get_masters($arr)
    {        
        foreach($arr as $k => $tnm)
        {
            $data[$tnm] = $this->get_rows($tnm);
        }

        return $data;
    }


    public function get_rows($tnm)
    {
        $sql = "";
        if($tnm == "crops")
        {
            $sql = "SELECT crop_id as id, crop_name as name
            FROM crops
            WHERE is_deleted = 0
            ORDER BY crop_name";
        }
        elseif($tnm == "crop_variety")
        {
            $sql = "SELECT cv.crop_variety_id as id, cv.crop_variety_name as name, c.crop_id, c.crop_name
            FROM crop_variety cv
            INNER JOIN crops c ON c.crop_id = cv.crop_id             
            WHERE cv.is_deleted = 0
            ORDER BY c.crop_name, cv.crop_variety_name
            ";
        }
        elseif($tnm == "crops_all")
        {
            $sql = "SELECT crop_id as id, crop_name as name
            FROM crops
            WHERE is_deleted = 0
            ORDER BY crop_name";
        }
        elseif($tnm == "product_category")
        {
            $sql = "SELECT product_category_id as id, product_category_name as name
            FROM product_category
            WHERE is_deleted = 0
            ORDER BY product_category_name";
        }
        elseif($tnm == "products")
        {
            $sql = "SELECT p.product_id as id, p.product_name as name, p.subunit_id, p.cost, pc.product_category_id, pc.product_category_name, s.subunit_name, p.product_code
            FROM products p 
            INNER JOIN product_category pc ON p.product_category_id = pc.product_category_id
            INNER JOIN subunits s ON p.subunit_id = s.subunit_id
            WHERE p.is_deleted = 0
            ORDER BY pc.product_category_id, p.product_name
            ";
        }
        elseif($tnm == "products_only")
        {
            $sql = "SELECT p.product_id as id, p.product_code as name, p.product_category_id
            FROM products p
            WHERE p.is_deleted = 0 AND p.product_category_id = 1
            ORDER BY p.product_code
            ";
        }
        elseif($tnm == "states")
        {
            $sql = "SELECT state_id as id, state_name as name
            FROM states
            WHERE is_deleted = 0 AND is_state_active = 1
            ORDER BY state_name
            ";
        }
        elseif($tnm == "districts")
        {
            $sql = "SELECT d.district_id as id, d.district_name as name, s.state_id, s.state_name
            FROM districts d
            INNER JOIN states s ON s.state_id = d.state_id AND s.is_state_active = 1            
            WHERE d.is_deleted = 0 AND d.is_district_active = 1
            ORDER BY s.state_name, d.district_name
            ";
        }
        elseif($tnm == "soil_type")
        {
            $sql = "SELECT soil_type_id as id, soil_type_name as name
            FROM soil_type
            WHERE is_deleted = 0
            ORDER BY soil_type_name
            ";
        }
        elseif($tnm == "stock_verification_reason")
        {
            $sql = "SELECT stock_verification_reason_id as id, stock_verification_reason_name as name
            FROM stock_verification_reason
            WHERE is_deleted = 0
            ORDER BY stock_verification_reason_name
            ";
        }
        elseif($tnm == "units")
        {
            $sql = "SELECT unit_id as id, unit_name as name
            FROM units
            WHERE is_deleted = 0
            ORDER BY unit_name
            ";
        }
        elseif($tnm == "subunits")
        {
            $sql = "SELECT su.subunit_id as id, su.subunit_name as name, u.unit_id, u.unit_name
            FROM subunits su            
            INNER JOIN units u ON su.unit_id = u.unit_id AND u.is_deleted = 0
            WHERE su.is_deleted = 0
            ORDER BY u.unit_name, su.subunit_name
            ";
        }
        elseif($tnm == "irrigation_method")
        {
            $sql = "SELECT irrigation_id as id, irrigation_name as name
            FROM irrigation_method
            WHERE is_deleted = 0
            ORDER BY irrigation_name
            ";
        }
        elseif($tnm == "fertilizer_used")
        {
            $sql = "SELECT fertilizer_used_id as id, fertilizer_used_name as name
            FROM fertilizer_used
            WHERE is_deleted = 0
            ORDER BY fertilizer_used_name
            ";
        }
        elseif($tnm == "micro_nutrients")
        {
            $sql = "SELECT micro_nutrient_id as id, micro_nutrient_name as name
            FROM micro_nutrients
            WHERE is_deleted = 0
            ORDER BY micro_nutrient_name
            ";
        }
        elseif($tnm == "weedicides")
        {
            $sql = "SELECT weedicide_id as id, weedicide_name as name
            FROM weedicides
            WHERE is_deleted = 0
            ORDER BY weedicide_name
            ";
        }
        elseif($tnm == "taluka")
        {
            $sql = "SELECT d.district_id, d.district_name, t.taluka_id as id, t.taluka_name as name
            FROM taluka t
            INNER JOIN districts d ON t.district_id = d.district_id AND d.is_district_active = 1
            WHERE t.is_deleted = 0 AND t.is_taluka_active = 1
            ORDER BY t.taluka_name, d.district_name
            ";
        }
        elseif($tnm == "farmers")
        {
            $sql = "SELECT f.farmer_id, f.farmer_reg_num, f.farmer_name
            FROM farmers f
            WHERE f.is_deleted = 0
            ORDER BY f.farmer_name";
        }
        

        if(isset($sql) && !empty($sql))
        {
            $query = $this->db->query($sql);

            $result = $query->result();

            if(isset($result) && !empty($result))
            {
                return $this->prepare_response($tnm, $result);
            }
            else
            {
                return array();
            }
        }        
        else
        {
            return array();
        }    
    }


    public function get_taluka($district_id)
    {
        $sql = "SELECT t.taluka_id as id, t.taluka_name as name, d.district_id, d.district_name
        FROM taluka t
        INNER JOIN districts d ON t.district_id = d.district_id AND d.is_district_active = 1
        WHERE t.is_deleted = 0 AND t.is_taluka_active = 1 AND t.district_id IN ($district_id)
        ORDER BY t.taluka_name, d.district_name
        ";        

        $query = $this->db->query($sql);

        $result = $query->result();

        if(isset($result) && !empty($result))
        {
            return $this->prepare_response("", $result);
        }
        else
        {
            return array();
        }   
    }

    public function prepare_response($tnm, $result)
    {
        $response = array();
        
        $response = (array)$result;

        return $response;   
    }


    public function get_farmers($arr)
    {
        $role_id = $this->user_role_id;
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];

        if($role_id == 2)
        {
            $sql = "SELECT f.*, s.state_name, d.district_name, t.taluka_name
            FROM farmers f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            WHERE f.is_deleted = 0 AND f.farmer_state = $state_id
            ORDER BY f.created_date DESC, f.farmer_id DESC";
        }
        elseif($role_id == 3)
        {
            $sql = "SELECT f.*, s.state_name, d.district_name, t.taluka_name
            FROM farmers f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            WHERE f.is_deleted = 0 AND f.farmer_state = $state_id AND f.farmer_district IN ($district_id)
            ORDER BY f.created_date DESC, f.farmer_id DESC";
        }
        else
        {
            $sql = "SELECT f.*, s.state_name, d.district_name, t.taluka_name
            FROM farmers f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            WHERE f.is_deleted = 0
            ORDER BY f.created_date DESC, f.farmer_id DESC";
        }
        

        $query = $this->db->query($sql);

        $result = $query->result();

        $res = array();
        if(isset($result) && !empty($result))
        {
            foreach($result as $obj)
            {   
                if($obj-> farmer_reg_num)
                {
                    $obj-> farmer_reg_num = "F".regno($obj-> farmer_reg_num, 5);
                }
                
                $obj-> crop_last_year = $this->crop_last_year(array("farmer_id" => $obj-> farmer_id));                

                $res[] = $obj;
            }
        }

        return $res;
    }

    
    public function get_farmer_detail($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT f.*, s.state_name, d.district_name, st.soil_type_name, u.first_name, u.last_name, t.taluka_name
        FROM farmers f
        INNER JOIN states s ON s.state_id = f.farmer_state
        INNER JOIN districts d ON d.district_id = f.farmer_district
        LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
        LEFT JOIN soil_type st ON st.soil_type_id = f.farmer_soil_type
        LEFT JOIN users u ON u.user_id = f.created_by
        WHERE f.is_deleted = 0 AND f.farmer_id = $farmer_id
        GROUP BY f.farmer_id
        LIMIT 1";        

        $query = $this->db->query($sql);

        $data['basic'] = $query->row();
        if(isset($data['basic']) && !empty($data['basic']))
        {
            $data['basic']->farmer_equipments = explode(",",$data['basic']->farmer_equipments);
        }

        $data['farmers_fertilizer_used'] = $this->farmers_fertilizer_used($arr);

        $data['crop_last_year'] = $this->crop_last_year($arr);

        $data['crop_current_year'] = $this->crop_current_year($arr);

        $data['irrigation_method'] = $this->farmer_irrigation_method($arr);

        $data['plot_photos'] = $this->farmer_plot_photo($arr);

        return $data;
    }


    public function crop_last_year($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT l.*, c.crop_name
        FROM farmers_crop_last_year l
        INNER JOIN crops c ON c.crop_id = l.last_year_crop_id        
        WHERE l.farmer_id = $farmer_id
        ORDER BY l.last_year_id ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function crop_current_year($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT l.*, c.crop_name
        FROM farmers_crop_current_year l
        INNER JOIN crops c ON c.crop_id = l.current_year_crop_id        
        WHERE l.farmer_id = $farmer_id
        ORDER BY l.current_year_id ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function farmers_fertilizer_used($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT f.fertilizer_used_id as id, f.fertilizer_used_name as name
        FROM fertilizer_used f
        INNER JOIN farmers_fertilizer_used m ON f.fertilizer_used_id = m.fertilizer_used_id
        WHERE m.farmer_id = $farmer_id
        ORDER BY f.fertilizer_used_name ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }

    public function farmer_irrigation_method($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT m.irrigation_id as id, m.irrigation_name as name
        FROM farmers_irrigation f
        INNER JOIN irrigation_method m ON f.irrigation_id = m.irrigation_id        
        WHERE f.farmer_id = $farmer_id
        ORDER BY m.irrigation_name ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function farmer_plot_photo($arr)
    {        
        $farmer_id = $arr['farmer_id'];        
        if(!isset($arr['farmer_type']) || empty($arr['farmer_type'])) 
            $farmer_type = 1;
        else
            $farmer_type = $arr['farmer_type'];

        
        $sql = "SELECT f.*
        FROM farmers_plot_photo f        
        WHERE f.farmer_id = $farmer_id AND f.farmer_type = $farmer_type
        ORDER BY f.plot_photo_id ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function remove_farmer($farmer_id, $farmer_type=1)
    {

        $sql = "DELETE FROM farmers 
        WHERE farmer_id = $farmer_id 
        LIMIT 1";

        $this->db->query($sql);
    }


    public function reset_stage($farmer_id, $stage)
    {
        //demos_product demos_micro_nutrients demos_weedicides farmers_plot_photo
        /*$sql = "DELETE FROM demos_product WHERE farmer_id = $farmer_id AND for_stage = $stage ";
        $this->db->query($sql);

        $sql = "DELETE FROM demos_micro_nutrients WHERE farmer_id = $farmer_id AND for_stage = $stage ";
        $this->db->query($sql);

        $sql = "DELETE FROM demos_weedicides WHERE farmer_id = $farmer_id AND for_stage = $stage ";
        $this->db->query($sql);*/        
        
    }


    public function get_acknowledgement1($arr)
    {
        $role_id = $this->user_role_id;
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];

        $details = $this->admin_model->last_upload_date('upload_acknowledgement1');
        if(!isset($details) || empty($details))
        {
            $file_id = 0;
        }
        else
        {
            $file_id = $details-> file_id;
        }

        if($role_id == 2)
        {
            $sql = "SELECT ss.saler_sys_id,ss.first_name,ss.last_name,ss.mobile, ss.address, ss.district_name,ss.state_name,s.quantity_mt as quantity, s.unit,p.product_name, '4' as rating, s.invoice_date, s.dd_no 
            FROM upload_acknowledgement1 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1 AND ss.state_id = $state_id
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE ss.state_id = $state_id AND s.file_id IN ($file_id)
            ORDER BY ss.saler_sys_id, ss.first_name, ss.last_name";
        }
        elseif($role_id == 3)
        {
            $sql = "SELECT ss.saler_sys_id,ss.first_name,ss.last_name,ss.mobile, ss.address, ss.district_name,ss.state_name,s.quantity_mt as quantity, s.unit,p.product_name, '4' as rating, s.invoice_date, s.dd_no 
            FROM upload_acknowledgement1 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1 AND ss.state_id = $state_id AND ss.district_id IN($district_id)
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE ss.state_id = $state_id AND ss.district_id IN($district_id) AND s.file_id IN ($file_id)
            ORDER BY ss.saler_sys_id, ss.first_name, ss.last_name";
        }
        else
        {
            $sql = "SELECT ss.saler_sys_id,ss.first_name,ss.last_name,ss.mobile, ss.address, ss.district_name,ss.state_name,s.quantity_mt as quantity, s.unit,p.product_name, '4' as rating, s.invoice_date, s.dd_no
            FROM upload_acknowledgement1 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id IN ($file_id)
            ORDER BY ss.saler_sys_id, ss.first_name, ss.last_name";
        }
        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function get_acknowledgement2($arr)
    {
        $role_id = $this->user_role_id;
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];

        $details = $this->admin_model->last_upload_date('upload_acknowledgement2');
        if(!isset($details) || empty($details))
        {
            $file_id = 0;
        }
        else
        {
            $file_id = $details-> file_id;
        }

        /*if($role_id == 2)
        {
            $sql = "SELECT ss.saler_sys_id as dealer_id, ss.first_name as dealer_first_name,ss.last_name as dealer_last_name,ss.mobile as dealer_mobile, ss.address as dealer_address,ss.district_name as dealer_district_name,ss.state_name as dealer_state_name, w.saler_sys_id as wholesaler_id, w.first_name as wholesaler_first_name,w.last_name as wholesaler_last_name,w.mobile as wholesaler_mobile, w.address as wholesaler_address, w.district_name as wholesaler_district_name,w.state_name as wholesaler_state_name,s.quantity, s.unit,p.product_name
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1 AND w.state_id = $state_id
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id IN ($file_id) AND w.state_id = $state_id
            ORDER BY w.saler_sys_id, w.first_name, w.last_name";
        }
        elseif($role_id == 3)
        {
            $sql = "SELECT ss.saler_sys_id as dealer_id, ss.first_name as dealer_first_name,ss.last_name as dealer_last_name,ss.mobile as dealer_mobile, ss.address as dealer_address,ss.district_name as dealer_district_name,ss.state_name as dealer_state_name, w.saler_sys_id as wholesaler_id, w.first_name as wholesaler_first_name,w.last_name as wholesaler_last_name,w.mobile as wholesaler_mobile, w.address as wholesaler_address, w.district_name as wholesaler_district_name,w.state_name as wholesaler_state_name,s.quantity, s.unit,p.product_name
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1 AND w.state_id = $state_id AND w.district_id IN ($district_id)
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id IN ($file_id) AND w.state_id = $state_id AND w.district_id IN ($district_id)
            ORDER BY w.saler_sys_id, w.first_name, w.last_name";
        }
        else
        {
            $sql = "SELECT ss.saler_sys_id as dealer_id, ss.first_name as dealer_first_name,ss.last_name as dealer_last_name,ss.mobile as dealer_mobile, ss.address as dealer_address,ss.district_name as dealer_district_name,ss.state_name as dealer_state_name, w.saler_sys_id as wholesaler_id, w.first_name as wholesaler_first_name,w.last_name as wholesaler_last_name,w.mobile as wholesaler_mobile, w.address as wholesaler_address, w.district_name as wholesaler_district_name,w.state_name as wholesaler_state_name,s.quantity, s.unit,p.product_name
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id IN ($file_id)
            ORDER BY w.saler_sys_id, w.first_name, w.last_name";
        }*/        

        if($role_id == 2)
        {
            $sql = "SELECT ss.saler_sys_id as dealer_id, ss.first_name as dealer_first_name,ss.last_name as dealer_last_name,ss.mobile as dealer_mobile, ss.address as dealer_address,ss.district_name as dealer_district_name,ss.state_name as dealer_state_name, w.saler_sys_id as wholesaler_id, w.first_name as wholesaler_first_name,w.last_name as wholesaler_last_name,w.mobile as wholesaler_mobile, w.address as wholesaler_address, w.district_name as wholesaler_district_name,w.state_name as wholesaler_state_name,s.quantity_mt as quantity, s.unit,p.product_name, '4' as rating, s.invoice_date, s.dispatch_no
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2 AND ss.state_id = $state_id
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1 
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id IN ($file_id) AND ss.state_id = $state_id
            ORDER BY w.saler_sys_id, w.first_name, w.last_name";
        }
        elseif($role_id == 3)
        {
            $sql = "SELECT ss.saler_sys_id as dealer_id, ss.first_name as dealer_first_name,ss.last_name as dealer_last_name,ss.mobile as dealer_mobile, ss.address as dealer_address,ss.district_name as dealer_district_name,ss.state_name as dealer_state_name, w.saler_sys_id as wholesaler_id, w.first_name as wholesaler_first_name,w.last_name as wholesaler_last_name,w.mobile as wholesaler_mobile, w.address as wholesaler_address, w.district_name as wholesaler_district_name,w.state_name as wholesaler_state_name,s.quantity_mt as quantity, s.unit,p.product_name, '4' as rating, s.invoice_date, s.dispatch_no
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2 AND ss.state_id = $state_id AND ss.district_id IN ($district_id)
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1 
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id IN ($file_id) AND ss.state_id = $state_id AND ss.district_id IN ($district_id)
            ORDER BY w.saler_sys_id, w.first_name, w.last_name";
        }
        else
        {
            $sql = "SELECT ss.saler_sys_id as dealer_id, ss.first_name as dealer_first_name,ss.last_name as dealer_last_name,ss.mobile as dealer_mobile, ss.address as dealer_address,ss.district_name as dealer_district_name,ss.state_name as dealer_state_name, w.saler_sys_id as wholesaler_id, w.first_name as wholesaler_first_name,w.last_name as wholesaler_last_name,w.mobile as wholesaler_mobile, w.address as wholesaler_address, w.district_name as wholesaler_district_name,w.state_name as wholesaler_state_name,s.quantity_mt as quantity, s.unit,p.product_name, '4' as rating, s.invoice_date, s.dispatch_no
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id IN ($file_id)
            ORDER BY w.saler_sys_id, w.first_name, w.last_name";
        }

        $query = $this->db->query($sql);

        return $query->result();
    }



    public function get_upcoming_demos($arr)
    {
        $role_id = $this->user_role_id;
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];
        
        $from_date = $arr['from_date'];
        $to_date = $arr['to_date'];

        if($role_id == 2)
        {
            $sql = "SELECT f.farmer_id, f.farmer_reg_num, f.farmer_name, f.farmer_mobile, f.farmer_village, f.farmer_taluka, d.district_name, s.state_name, CONCAT('Stage',' ',f.current_demo_stage) as current_demo_stage, t.taluka_name, DATE_FORMAT(f.demo_start_date, '%d-%b-%Y') as demo_start_date
            FROM demos f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            WHERE f.is_deleted = 0 AND f.farmer_state = $state_id
            ORDER BY f.created_date DESC, f.farmer_id DESC";
        }
        elseif($role_id == 3)
        {
            $sql = "SELECT f.farmer_id, f.farmer_reg_num, f.farmer_name, f.farmer_mobile, f.farmer_village, f.farmer_taluka, d.district_name, s.state_name, CONCAT('Stage',' ',f.current_demo_stage) as current_demo_stage, t.taluka_name, DATE_FORMAT(f.demo_start_date, '%d-%b-%Y') as demo_start_date
            FROM demos f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            WHERE f.is_deleted = 0 AND f.farmer_state = $state_id AND f.farmer_district IN ($district_id)
            ORDER BY f.created_date DESC, f.farmer_id DESC";
        }
        else
        {
            $sql = "SELECT f.farmer_id, f.farmer_reg_num, f.farmer_name, f.farmer_mobile, f.farmer_village, f.farmer_taluka, d.district_name, s.state_name, CONCAT('Stage',' ',f.current_demo_stage) as current_demo_stage, t.taluka_name, DATE_FORMAT(f.demo_start_date, '%d-%b-%Y') as demo_start_date
            FROM demos f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            WHERE f.is_deleted = 0
            ORDER BY f.created_date DESC, f.farmer_id DESC";
        }
        

        $query = $this->db->query($sql);

        return $query->result();
    }


    

    public function get_next_demo_of_farmer($arr)
    {
        $farmer_id = $arr['farmer_id'];        
        
        $from_date = $arr['from_date'];
        $to_date = $arr['to_date'];

        $sql = "
            SELECT next_stage_date, for_stage 
            FROM demos_stage 
            WHERE for_stage IN
            (
                SELECT MAX(for_stage) as for_stage
                FROM demos_stage        
                WHERE farmer_id = $farmer_id AND date(next_stage_date) >= '$from_date' AND date(next_stage_date) <= '$to_date'                
            ) AND farmer_id = $farmer_id AND date(next_stage_date) >= '$from_date' AND date(next_stage_date) <= '$to_date'
            LIMIT 1        
        "; 

        $query = $this->db->query($sql);

        return $query->row();
    }



    public function get_sales($arr)
    {
        $role_id = $this->user_role_id;
        
        $from_date = $arr['from_date'];
        $to_date = $arr['to_date'];
        
        $state_id = $arr['state_id'];

        $district_id = $arr['district_id'];        
        $product_id = $arr['product_id'];

        $app = "";

        if(isset($state_id) && !empty($state_id))
        {
            $app .= " AND s.state_id IN ($state_id) ";
        }

        if(isset($district_id) && !empty($district_id))
        {
            $app .= " AND s.district_id IN ($district_id) ";
        }

        if(isset($product_id) && !empty($product_id))
        {
            $app .= " AND s.product_id IN ($product_id) ";
        }

        
        $sql = "SELECT s.id, IF(s.company_type=1,'RMPCL','OTHERS') as company, s.quantity, p.product_name, p.product_color 
            FROM upload_sales s             
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.sales_date >= '$from_date' AND s.sales_date <= '$to_date' AND s.state_id = $state_id $app
            ORDER BY p.product_name";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function get_cost($arr)
    {
        $farmer_id = $arr['farmer_id'];        
            
        $sql = "SELECT SUM((s.product_qty * p.cost)) as cost
            FROM demos_product s             
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.for_stage IN (2,3) AND s.demo_type = 1 AND s.farmer_id = '$farmer_id'
            ";        

        $query = $this->db->query($sql);

        return $query->result();
    }



    public function get_demo_farmer_detail($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT f.*, s.state_name, d.district_name, u.first_name, u.last_name, c.crop_name, v.crop_variety_name, t.taluka_name
        FROM demos f
        INNER JOIN states s ON s.state_id = f.farmer_state
        INNER JOIN districts d ON d.district_id = f.farmer_district 
        LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka       
        LEFT JOIN users u ON u.user_id = f.created_by
        LEFT JOIN crops c ON c.crop_id = f.demo_crop_id
        LEFT JOIN crop_variety v ON v.crop_variety_id = f.demo_crop_variety_id
        WHERE f.is_deleted = 0 AND f.farmer_id = $farmer_id
        GROUP BY f.farmer_id
        LIMIT 1";        

        $query = $this->db->query($sql);

        $data['basic'] = $query->row();
        if(isset($data['basic']) && !empty($data['basic']))
        {
            //$data['basic']->farmer_equipments = explode(",",$data['basic']->farmer_equipments);
        }

        

        $data['crop_last_year'] = $this->crop_last_year_demo($arr);

        $data['crop_current_year'] = $this->crop_current_year_demo($arr);

        $data['demos_product'] = $this->demos_product($arr);

        $data['demos_micro_nutrients'] = $this->demos_micro_nutrients($arr);

        $data['demos_crop_quality'] = $this->demos_crop_quality($arr);

        $data['demos_weedicides'] = $this->demos_weedicides($arr);

        $data['demos_stage'] = $this->demos_stage($arr);

        $data['plot_photos'] = $this->farmer_plot_photo($arr);

        return $data;
    }


    public function crop_last_year_demo($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT l.*, p.product_name as crop_name
        FROM demos_crop_last_year l              
        INNER JOIN products p ON p.product_id = l.last_year_crop_id
        WHERE l.farmer_id = $farmer_id
        ORDER BY l.last_year_id ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function crop_current_year_demo($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT l.*, p.product_name as crop_name
        FROM demos_crop_current_year l
        INNER JOIN products p ON p.product_id = l.current_year_crop_id
        WHERE l.farmer_id = $farmer_id
        ORDER BY l.current_year_id ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }



    public function demos_product($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT l.*, p.product_code as product_name, p.product_category_id
        FROM demos_product l
        INNER JOIN products p ON p.product_id = l.product_id                
        WHERE l.farmer_id = $farmer_id
        ORDER BY l.demos_product_id ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function demos_micro_nutrients($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT l.*, p.micro_nutrient_name
        FROM demos_micro_nutrients l
        INNER JOIN micro_nutrients p ON p.micro_nutrient_id = l.micro_nutrient_id        
        WHERE l.farmer_id = $farmer_id
        ORDER BY l.demos_micro_nutrients_id ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function demos_crop_quality($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT l.*
        FROM demos_crop_quality l      
        WHERE l.farmer_id = $farmer_id
        ORDER BY l.demos_crop_quality_id ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function demos_weedicides($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT l.*, p.weedicide_name
        FROM demos_weedicides l
        INNER JOIN weedicides p ON p.weedicide_id = l.weedicide_id        
        WHERE l.farmer_id = $farmer_id
        ORDER BY l.for_stage, l.demo_type";        

        $query = $this->db->query($sql);

        return $query->result();
    }



    public function demos_stage($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        
        $sql = "SELECT l.*
        FROM demos_stage l                       
        WHERE l.farmer_id = $farmer_id
        ORDER BY l.next_stage_date ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function get_wholesalers($arr)
    {        
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];        
        $file_id = $arr['file_id'];        

        $sql = "SELECT (ss.saler_id) as saler_id, (ss.first_name) as first_name, (ss.last_name) as last_name, (ss.email) as email, (ss.mobile) as mobile, (ss.address) as address, (ss.district_name) as district_name, (ss.state_name) as state_name, SUM(s.balance_with_ws) as current_quantity, s.product_id, (p.product_name) as product_name, ss.saler_sys_id  
            FROM upload_wholesaler s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE ss.saler_type = 1 AND ss.state_id = $state_id AND ss.district_id IN($district_id) AND s.file_id = '$file_id'
            GROUP BY ss.saler_sys_id, s.product_id 
            ";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function get_retailers($arr)
    {
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];
        $file_id = $arr['file_id'];

        $sql = "SELECT (ss.saler_id) as saler_id, (ss.first_name) as first_name, (ss.last_name) as last_name, (ss.email) as email, (ss.mobile) as mobile, (ss.address) as address, (ss.district_name) as district_name, (ss.state_name) as state_name, SUM(s.availabilty) as current_quantity, s.product_id, (p.product_name) as product_name, ss.saler_sys_id  
            FROM upload_retailer s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE ss.saler_type = 2 AND ss.state_id = $state_id AND ss.district_id IN($district_id) AND s.file_id = '$file_id'
            GROUP BY ss.saler_sys_id, s.product_id
            ";

        $query = $this->db->query($sql);

        return $query->result();    
    }


    public function quick_get_farmers($arr)
    {
        $role_id = $this->user_role_id;
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];
        //9979693015
        $app = " AND f.farmer_mobile NOT IN (SELECT farmer_mobile FROM farmers WHERE is_deleted = 0)";
        if($role_id == 2)
        {
            $sql = "SELECT f.*, s.state_name, d.district_name, t.taluka_name
            FROM farmers_temp f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district            
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            WHERE f.is_deleted = 0 AND f.farmer_state = $state_id $app
            ORDER BY f.created_date DESC, f.farmer_id DESC";
        }
        elseif($role_id == 3)
        {
            $sql = "SELECT f.*, s.state_name, d.district_name, t.taluka_name
            FROM farmers_temp f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            WHERE f.is_deleted = 0 AND f.farmer_state = $state_id AND f.farmer_district IN ($district_id) $app
            ORDER BY f.created_date DESC, f.farmer_id DESC";
        }
        else
        {
            $sql = "SELECT f.*, s.state_name, d.district_name, t.taluka_name
            FROM farmers_temp f
            INNER JOIN states s ON s.state_id = f.farmer_state
            INNER JOIN districts d ON d.district_id = f.farmer_district
            LEFT JOIN taluka t ON t.taluka_id = f.farmer_taluka
            WHERE f.is_deleted = 0 $app
            ORDER BY f.created_date DESC, f.farmer_id DESC";
        }
        

        $query = $this->db->query($sql);

        $result = $query->result();

        $res = array();
        if(isset($result) && !empty($result))
        {
            foreach($result as $obj)
            {
                $res[] = $obj;
            }
        }

        return $res;
    }


    public function get_target_for_state($arr)
    {
        $state_id = $arr['state_id'];
        $year = $arr['year'];
        $month = $arr['month'];
            
        $sql = "SELECT target_value, target_percent
            FROM target                         
            WHERE state_id = '$state_id' AND target_year = '$year' AND target_month = '$month'
            LIMIT 1
            ";        

        $query = $this->db->query($sql);

        return $query->row();
    }



    public function get_wholesalers_profile($arr)
    {        
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];                 

        $sql = "SELECT ss.saler_id, ss.saler_sys_id, ss.first_name, ss.last_name, ss.mobile, ss.address, ss.district_name, ss.state_name, ss.rating 
            FROM salers ss
            WHERE ss.saler_type = 1 AND ss.state_id = $state_id AND ss.district_id IN($district_id)
            ORDER BY ss.saler_id 
            ";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function get_retailers_profile($arr)
    {        
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];                 

        $sql = "SELECT ss.saler_id, ss.saler_sys_id, ss.first_name, ss.last_name, ss.mobile, ss.address, ss.district_name, ss.state_name, ss.rating 
            FROM salers ss
            WHERE ss.saler_type = 2 AND ss.state_id = $state_id AND ss.district_id IN($district_id)
            ORDER BY ss.saler_id 
            ";        

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function get_ack_value($type, $saler_id, $state_id, $district_id)
    {
        $details = $this->admin_model->last_upload_date($type);
        if(!isset($details) || empty($details))
        {
            $file_id = 0;
        }
        else
        {
            $file_id = $details-> file_id;
        }


        if($type == "upload_acknowledgement1")
        {
            $sql = "SELECT SUM(s.quantity_mt) as quantity
            FROM upload_acknowledgement1 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.state_id = $state_id AND ss.district_id IN($district_id) AND ss.saler_id = $saler_id           
            WHERE ss.saler_id = $saler_id AND ss.state_id = $state_id AND ss.district_id IN($district_id) AND s.file_id IN ($file_id)
            ";
        }
        elseif($type == "upload_acknowledgement2")
        {
            $sql = "SELECT SUM(s.quantity_mt) as quantity
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2 AND ss.state_id = $state_id AND ss.district_id IN ($district_id) AND ss.saler_id = $saler_id           
            WHERE ss.saler_id = $saler_id AND s.file_id IN ($file_id) AND ss.state_id = $state_id AND ss.district_id IN ($district_id)
            ";
        }
        else
        {
            return 0; die;
        }    
        
        $query = $this->db->query($sql);

        $data = $query->row();

        if(isset($data) && !empty($data))
        {
            return $data-> quantity; die;
        }
        else
        {
            return 0; die;
        }
    }



    public function get_inventory_value($type, $saler_id, $state_id, $district_id)
    {        
        $details = $this->admin_model->last_upload_date($type);
        if(!isset($details) || empty($details))
        {
            $file_id = 0;
        }
        else
        {
            $file_id = $details-> file_id;
        }        


        if($type == "upload_wholesaler")
        {
            $sql = "SELECT SUM(s.balance_with_ws) as quantity  
            FROM upload_wholesaler s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1 AND ss.saler_id = $saler_id            
            WHERE ss.saler_type = 1 AND ss.saler_id = $saler_id AND ss.state_id = $state_id AND ss.district_id IN($district_id) AND s.file_id = '$file_id'            
            "; 
        }
        elseif($type == "upload_retailer")  
        {
            $sql = "SELECT SUM(s.availabilty) as quantity  
            FROM upload_retailer s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2 AND ss.saler_id = $saler_id           
            WHERE ss.saler_type = 2 AND ss.saler_id = $saler_id AND ss.state_id = $state_id AND ss.district_id IN($district_id) AND s.file_id = '$file_id' 
            ";       
        }        
        else
        {
            return 0; die;
        }
        

        $query = $this->db->query($sql);

        $data = $query->row();

        if(isset($data) && !empty($data))
        {
            return $data-> quantity; die;
        }
        else
        {
            return 0; die;
        }
    }


    public function get_crops_variety($crop_id)
    {        
        $sql = "SELECT cv.crop_variety_id as id, cv.crop_variety_name as name
        FROM crop_variety cv
        INNER JOIN crops c ON c.crop_id = cv.crop_id             
        WHERE cv.is_deleted = 0 AND c.crop_id = $crop_id
        ORDER BY cv.crop_variety_name
        ";        

        $query = $this->db->query($sql);

        return $query->result();
    }




    public function get_sum($arr)
    {
        $role_id = $this->user_role_id;
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];

        $details = $this->admin_model->last_upload_date('upload_acknowledgement1');
        if(!isset($details) || empty($details))
        {
            $file_id1 = 0;
        }
        else
        {
            $file_id1 = $details-> file_id;
        }


        $details = $this->admin_model->last_upload_date('upload_acknowledgement2');
        if(!isset($details) || empty($details))
        {
            $file_id2 = 0;
        }
        else
        {
            $file_id2 = $details-> file_id;
        }

        $details = $this->admin_model->last_upload_date('upload_wholesaler');
        if(!isset($details) || empty($details))
        {
            $file_id3 = 0;
        }
        else
        {
            $file_id3 = $details-> file_id;
        }

        
        $details = $this->admin_model->last_upload_date('upload_retailer');
        if(!isset($details) || empty($details))
        {
            $file_id4 = 0;
        }
        else
        {
            $file_id4 = $details-> file_id;
        }

        $sql = "SELECT SUM(s.quantity_mt) as quantity, 'inv_ack1' as type 
            FROM upload_acknowledgement1 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1 AND ss.state_id = $state_id AND ss.district_id IN($district_id)
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE ss.state_id = $state_id AND ss.district_id IN($district_id) AND s.file_id IN ($file_id1)
           
            UNION ALL

            SELECT SUM(s.quantity_mt) as quantity, 'inv_ack2' as type 
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2 AND ss.state_id = $state_id AND ss.district_id IN ($district_id)
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1 
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.file_id IN ($file_id2) AND ss.state_id = $state_id AND ss.district_id IN ($district_id)
        
            UNION ALL

            SELECT SUM(s.balance_with_ws) as quantity, 'inv_ws' as type
            FROM upload_wholesaler s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE ss.saler_type = 1 AND ss.state_id = $state_id AND ss.district_id IN($district_id) AND s.file_id = '$file_id3'
            
            UNION ALL


            SELECT SUM(s.availabilty) as quantity, 'inv_rt' as type
            FROM upload_retailer s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            INNER JOIN products p ON p.product_id = s.product_id 
            WHERE ss.saler_type = 2 AND ss.state_id = $state_id AND ss.district_id IN($district_id) AND s.file_id = '$file_id4'
            
            ";    
           
        $query = $this->db->query($sql);

        return $query->result();
    }





    public function get_stock_aging($type = "", $saler_id, $state_id, $district_id)
    {
        if($type == "rt")    
        {
            $sql = "SELECT p.product_code, s.r_ack_days as ageing, s.invoice_date as dispatch_date, s.dispatch_no as dd_no
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2 AND ss.saler_id = $saler_id          
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.r_ack_date != '' AND s.r_ack_days != ''  AND ss.saler_id = $saler_id
            ORDER BY s.ack_days DESC           
            ";
        }
        else
        {
            $sql = "SELECT p.product_code, s.w_ack_days as ageing, s.dispatch_date, ack.dd_no
            FROM upload_dispatch s
            INNER JOIN upload_acknowledgement1 ack ON ack.transaction_id = s.transaction_id 
            INNER JOIN salers slr ON slr.saler_sys_id = s.sold_to_id AND slr.saler_id = $saler_id             
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE s.w_ack_date != '' AND s.w_ack_days != '' AND slr.saler_id = $saler_id            
            ORDER BY s.ack_days DESC";

            //echo "<br/><br/>".$sql;
        }
                

        $query = $this->db->query($sql);

        return $query->result();    
        
    }



    public function check_farmer_exist($arr)
    {        
        $farmer_id = $arr['farmer_id'];
        $farmer_parent_id = $arr['farmer_parent_id'];
        
        $sql = "SELECT farmer_id
        FROM demos
        WHERE farmer_id = $farmer_id AND farmer_parent_id = $farmer_parent_id
        LIMIT 1";        

        $query = $this->db->query($sql);

        return $query->row();
    }



    public function approved_demos($arr)
    {        
        $state_id = $arr['state_id'];
        $district_id = $arr['district_id'];        
        
        $sql = "SELECT f.*, d.district_name, s.state_name, CONCAT('Stage',' ',f.current_demo_stage) as current_demo_stage
        FROM demos f
        INNER JOIN states s ON s.state_id = f.farmer_state
        INNER JOIN districts d ON d.district_id = f.farmer_district        
        WHERE f.is_deleted = 0 AND f.is_approved = 1 AND f.farmer_state = $state_id AND f.farmer_district IN ($district_id)
        ORDER BY f.farmer_name ASC, f.farmer_id ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }
      
}//End of Class
?>