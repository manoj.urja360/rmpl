<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webservice extends CI_Controller 
{
    public $user_first_name = "";
    public $user_last_name = "";
    public $user_fullname = "";
    public $user_email = "";
    public $user_mobile = "";
    public $user_role_id = "";
    public $user_id = "";
    public $all_modules = array();

    public function __construct()
    {
        parent::__construct();
                
        $this->load->model("admin_model");
        $this->load->model("webservice_model");
    }


    public function is_logged_in($session_key)
    {
        $data = $this->webservice_model->check_session_key($session_key);
        
        if(isset($data['user_id']) && !empty($data['user_id']))
        {
            $this->user_first_name = ucfirst($data['first_name']);

            $this->user_last_name = ucfirst($data['last_name']);

            $this->user_fullname = ucwords($data['first_name']." ".$data['last_name']);

            $this->user_email = $data['email'];

            $this->user_mobile = $data['mobile'];

            $this->user_role_id = $data['role_id'];

            $this->user_id = $data['user_id'];

            $this->user_created_date = date("M, Y", strtotime($data['created_date']));

            return true;
        }
        else
        {
            echo json_encode(array("status"=>0,"message"=>"Please login to access content.")); 
            die;        
        }

    }


    public function get_masters()
    {
        $session_key = $this->input->post('session_key');
        $master_str = $this->input->post('master_list');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);

        if(!isset($master_str) || empty($master_str))
        {
            $master_list = array("crops", "crop_variety", "product_category", "products", "districts", "states", "soil_type", "stock_verification_reason", "units", "subunits", "irrigation_method", "fertilizer_used", "micro_nutrients", "weedicides", "crops_all");
        }
        else
        {
            $master_list = explode(",", $master_str);
        }
        
        $data = $this->webservice_model->get_masters($master_list);

        if($master_str == "crops_all")
        {
            $temp = $temp2 = array();
            $temp1 = $data['crops_all'];
             
            foreach($temp1 as $arr)
            {
                $temp2 = $this->webservice_model->get_crops_variety($arr-> id);
                $temp[] = array("id"=>$arr-> id, "name"=>$arr-> name, "variety"=> $temp2);
            }

            $data = $temp;
        }

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }



    public function get_taluka()
    {
        $session_key = $this->input->post('session_key');
        $district_id = $this->input->post('district_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }
        elseif(!isset($district_id) || empty($district_id))
        {
            echo json_encode(array("status"=>0,"message"=>"District ids required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        
        
        $data = $this->webservice_model->get_taluka($district_id);

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }


    public function get_dd_list()
    {
        $typ = $this->input->post('typ');
        $id = $this->input->post('parent_id');
        $uid = $this->input->post('uid');

        $arr = array("typ"=>$typ, "id"=>$id, "uid"=>$uid);

        $data = $this->common_model->get_dd_list($arr);

        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>1,"message"=>"","records"=>$data)); 
            die;
        }
        else
        {
            echo json_encode(array("status"=>0,"message"=>"")); 
            die;
        }    
    }



    public function get_farmers()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($this->user_role_id == 2 && ($aoo_state_id == "" || $aoo_state_id <= 0))
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($this->user_role_id == 3)
        {
            if($aoo_state_id == "" || $aoo_state_id <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
                die;
            }
            elseif($aoo_district_id == "")    
            {
                echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
                die;
            }
        }
        
        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id);
        $data = $this->webservice_model->get_farmers($arr);

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }


    public function get_farmer_detail()
    {
        $session_key = $this->input->post('session_key');        
        $farmer_id = $this->input->post('farmer_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if(($farmer_id == "" || $farmer_id <= 0))
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer ID required.")); 
            die;
        }
        
        $arr = array("farmer_id" => $farmer_id);
        $data = $this->webservice_model->get_farmer_detail($arr);

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found."));
        
        die;
    }



    public function add_farmer()
    {
        $session_key = $this->input->post('session_key');
        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        $farmer_name = $this->input->post('farmer_name');
        $farmer_mobile = $this->input->post('farmer_mobile');
        $farmer_email = "";//$this->input->post('farmer_email');
        $farmer_taluka = $this->input->post('farmer_taluka');
        $farmer_district = $this->input->post('farmer_district');
        $farmer_state = $this->input->post('farmer_state');

        if($farmer_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Name required.")); die;
        }       
        elseif($farmer_mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number required."));
            die;    
        }
        elseif(strlen($farmer_mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        elseif($farmer_taluka == "" || $farmer_taluka <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Taluka required."));
            die;    
        }
        elseif($farmer_district == "" || $farmer_district <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer District required."));
            die;    
        }
        elseif($farmer_state == "" || $farmer_state <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer State required."));
            die;    
        }
        
                
        $arr = array("typ"=>"farmers", "id"=>0, "name"=>$farmer_mobile);
        $data = $this->common_model->is_already_exist($arr);
        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number already exist. Please enter different Mobile Number.")); die;
        }

                
        /*$farmer_equipments = json_decode($this->input->post('farmer_equipments'));
        if(isset($farmer_equipments) && !empty($farmer_equipments))
        {
            $farmer_equipments = implode(",", $farmer_equipments);
        }

        $farmer_last_soil_test_date = $this->input->post('farmer_last_soil_test_date');        
        if(isset($farmer_last_soil_test_date) && !empty($farmer_last_soil_test_date))
        {
            $farmer_last_soil_test_date = format_date($farmer_last_soil_test_date, DATE_DB);
        }

        $farmer_last_water_test_date = $this->input->post('farmer_last_water_test_date');
        if(isset($farmer_last_water_test_date) && !empty($farmer_last_water_test_date))
        {
            $farmer_last_water_test_date = format_date($farmer_last_water_test_date, DATE_DB);
        }*/


        $data = array("farmer_name" => $farmer_name, 
        "farmer_mobile" => $farmer_mobile, 
        //"farmer_email" => $farmer_email, 
        "farmer_plot_no" => $this->input->post('farmer_plot_no'), 
        "farmer_village" => $this->input->post('farmer_village'), 
        "farmer_taluka" => $this->input->post('farmer_taluka'), 
        "farmer_district" => $this->input->post('farmer_district'), 
        "farmer_state" => $this->input->post('farmer_state'),
        "on_or_off_site" => $this->input->post('on_or_off_site'),
        "geo_lat" => $this->input->post('geo_lat'),
        "geo_long" => $this->input->post('geo_long'),
        //"farmer_landmark" => $this->input->post('farmer_landmark'),
        //"farmer_soil_type" => $this->input->post('farmer_soil_type'),
        "farmer_land_area" => $this->input->post('farmer_land_area'),        
        //"farmer_equipments" => $farmer_equipments,
        //"farmer_last_soil_test_date" => $farmer_last_soil_test_date, 
        //"farmer_last_water_test_date" => $farmer_last_water_test_date, 
        "farmer_willing_demo" => $this->input->post('farmer_willing_demo'), 
        "farmer_plot_area_consultancy" => $this->input->post('farmer_plot_area_consultancy'),
        "farmer_reg_date" => date(DATE_DB),                
        

        "kharif_crop" => $this->input->post('kharif_crop'), 
        "is_ziron_used" => $this->input->post('is_ziron_used'), 
        "ziron_bought_from" => $this->input->post('ziron_bought_from'), 
        "ziron_feedback" => $this->input->post('ziron_feedback'), 
        "farmer_refrence" => $this->input->post('farmer_refrence'), 

        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id,
        "updated_date" => date(DATETIME_DB),                
        "updated_by" => $this->user_id
        );

        $farmerid = $this->dbaccess_model->insert("farmers", $data);            
        if($farmerid > 0)
        {
            $farmer_reg_num = $this->common_model->get_farmer_reg_no();
            $farmer_reg_num = $farmer_reg_num + 1;
            $this->dbaccess_model->update("farmers", array("farmer_reg_num"=>$farmer_reg_num), array("farmer_id" => $farmerid));



            $sdarr = array("typ"=>'district', "id" => $this->input->post('farmer_district'));
            $sd = $this->common_model->get_detail($sdarr);
            $fstate_name = strtolower(trim($sd-> state_name));
            $fdistrict_name = strtolower(trim($sd-> district_name));
            $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/farmer";

            if(isset($_FILES['farmer_photo']['name']) && !empty($_FILES['farmer_photo']['name']))
            {
                $farmer_photo = $_FILES['farmer_photo'];            
                if(isset($farmer_photo) && !empty($farmer_photo))
                {                
                    $ext = pathinfo($farmer_photo['name'], PATHINFO_EXTENSION);

                    $chk = $this->upload_image($path, 'farmer_photo', $farmerid.'_'.$this->user_id."_fp_".$farmer_photo['name']);
                    if(isset($chk) && !empty($chk))
                    {
                       $this->webservice_model->remove_farmer($farmerid);                   
                       echo json_encode(array("status"=>0,"message"=>$chk)); 
                       die; 
                    }
                    else
                    {
                        $spath = BASE_PATH.'/'.$path."/".$farmerid.'_'.$this->user_id."_fp_".$farmer_photo['name'];
                        $this->dbaccess_model->update("farmers", array("farmer_photo" => $farmerid.'_'.$this->user_id."_fp_".$farmer_photo['name'],"farmer_photo_url"=>$spath), array("farmer_id" => $farmerid));
                    }
                }
            }    
            
            //$farmer_fertilizer_used = json_decode($this->input->post('farmer_fertilizer_used'));
            $crop_last_year = json_decode($this->input->post('crop_last_year'));
            $crop_current_year = json_decode($this->input->post('crop_current_year'));
            //$irrigation_method = json_decode($this->input->post('irrigation_method'));
            

            /*if(isset($farmer_fertilizer_used) && !empty($farmer_fertilizer_used))
            {
                foreach($farmer_fertilizer_used as $k => $obj)
                {
                    $datain = array("farmer_id" => $farmerid,
                    "fertilizer_used_id" => $obj-> fertilizer_used_id,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_fertilizer_used", $datain);
                }    
            }*/

            
            if(isset($crop_last_year) && !empty($crop_last_year))
            {
                foreach($crop_last_year as $k => $obj)
                {
                    $datain = array("farmer_id" => $farmerid,
                    "last_year_crop_id" => $obj-> last_year_crop_id,
                    /*"last_year_area_sowed" => $obj-> last_year_area_sowed,
                    "last_year_fertilizer_expenses" => $obj-> last_year_fertilizer_expenses,
                    "last_year_yields" => $obj-> last_year_yields,
                    "last_year_remark" => $obj-> last_year_remark,*/
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_crop_last_year", $datain);
                }    
            }


            if(isset($crop_current_year) && !empty($crop_current_year))
            {
                foreach($crop_current_year as $k => $obj)
                {
                    $datain = array("farmer_id" => $farmerid,
                    "current_year_crop_id" => $obj-> current_year_crop_id,
                    /*"current_year_area_sowed" => $obj-> current_year_area_sowed,
                    "current_year_acrage_planned" => $obj-> current_year_acrage_planned,
                    "current_year_expect_date_sowing" => date(DATE_DB, strtotime($obj-> current_year_expect_date_sowing)),*/                    
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_crop_current_year", $datain);
                }    
            }


            /*if(isset($irrigation_method) && !empty($irrigation_method))
            {
                foreach($irrigation_method as $k => $obj)
                {
                    $datain = array("farmer_id" => $farmerid,
                    "irrigation_id" => $obj-> irrigation_id,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_irrigation", $datain);
                }    
            }*/
            
            /*$other_irrigation_method = $this->input->post('other_irrigation_method');
            if(isset($other_irrigation_method) && !empty($other_irrigation_method))
            {
                $datain = array("irrigation_name" => $other_irrigation_method,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                );
                
                $imid = $this->dbaccess_model->insert("irrigation_method", $datain);

                
                $datain = array("farmer_id" => $farmerid,
                "irrigation_id" => $imid,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("farmers_irrigation", $datain);
            }*/

                       
                          
            if(isset($_FILES['plot_photos']['name']) && !empty($_FILES['plot_photos']['name']))
            {
                $files = $_FILES['plot_photos'];

                $config = array(
                    'upload_path'   => $path,
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'overwrite'     => 1                       
                );
                $this->load->library('upload', $config);

                foreach ($files['name'] as $key => $image) 
                {
                    $_FILES['plot_photos[]']['name']= $files['name'][$key];
                    $_FILES['plot_photos[]']['type']= $files['type'][$key];
                    $_FILES['plot_photos[]']['tmp_name']= $files['tmp_name'][$key];
                    $_FILES['plot_photos[]']['error']= $files['error'][$key];
                    $_FILES['plot_photos[]']['size']= $files['size'][$key];
                    
                    $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_pp_".$image;

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('plot_photos[]')) 
                    {
                        $dataup = $this->upload->data();
                        $ext = $dataup['file_ext'];
                        $unm = $dataup['file_name'];
                        
                        $datain = array("farmer_id" => $farmerid,
                        "plot_photo_name" => $unm,
                        "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                        "created_date" => date(DATETIME_DB),                
                        "created_by" => $this->user_id
                        );
                    
                        $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                    } 
                    else 
                    {
                        $this->webservice_model->remove_farmer($farmerid);
                        echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                        die;
                    }
                }
            }    
        }

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;
    }


    function upload_image($path, $filename, $newfilename)
    { 
        $config['file_name'] = $newfilename;
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        
        $this->load->library('upload');
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) 
        {
            $error = $this->upload->display_errors();        
        } 
        else 
        {            
            $error = "";
        }

        return $error;
    }



    public function get_acknowledgement1()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($this->user_role_id == 2 && ($aoo_state_id == "" || $aoo_state_id <= 0))
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($this->user_role_id == 3)
        {
            if($aoo_state_id == "" || $aoo_state_id <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
                die;
            }
            elseif($aoo_district_id == "")    
            {
                echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
                die;
            }
        }
        
        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id);
        $data = $this->webservice_model->get_acknowledgement1($arr);

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }



    public function get_acknowledgement2()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($this->user_role_id == 2 && ($aoo_state_id == "" || $aoo_state_id <= 0))
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($this->user_role_id == 3)
        {
            if($aoo_state_id == "" || $aoo_state_id <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
                die;
            }
            elseif($aoo_district_id == "")    
            {
                echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
                die;
            }
        }
        
        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id);
        $data = $this->webservice_model->get_acknowledgement2($arr);

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found."));
        
        die;
    }



    public function add_demo()
    {
        $session_key = $this->input->post('session_key');
        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        $farmer_id_master = $this->input->post('farmer_id');
        $farmer_name = $this->input->post('farmer_name');
        $farmer_mobile = $this->input->post('farmer_mobile');        
        $farmer_district = $this->input->post('farmer_district');
        $farmer_state = $this->input->post('farmer_state');
        $farmer_taluka = $this->input->post('farmer_taluka');

        if($farmer_id_master == "" || $farmer_id_master <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select any Farmer.")); die;
        }
        elseif($farmer_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Name required.")); die;
        }       
        elseif($farmer_mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number required."));
            die;    
        }
        elseif(strlen($farmer_mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        elseif($farmer_district == "" || $farmer_district <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer District required."));
            die;    
        }
        elseif($farmer_state == "" || $farmer_state <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer State required."));
            die;    
        }
        elseif($farmer_taluka == "" || $farmer_taluka <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Taluka required."));
            die;    
        }
        
                
        /*$arr = array("typ"=>"demos", "id"=>0, "name"=>$farmer_mobile);
        $data = $this->common_model->is_already_exist($arr);
        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number already exist. Please enter different Mobile Number.")); die;
        }*/

        $farmer_email = $this->input->post('farmer_email');


        $farmer_last_soil_test_date = $this->input->post('farmer_last_soil_test_date');        
        if(isset($farmer_last_soil_test_date) && !empty($farmer_last_soil_test_date))
        {
            $farmer_last_soil_test_date = format_date($farmer_last_soil_test_date, DATE_DB);
        }

        $farmer_last_water_test_date = $this->input->post('farmer_last_water_test_date');
        if(isset($farmer_last_water_test_date) && !empty($farmer_last_water_test_date))
        {
            $farmer_last_water_test_date = format_date($farmer_last_water_test_date, DATE_DB);
        }



        $data = array("farmer_parent_id" => $farmer_id_master, 
        "farmer_name" => $farmer_name, 
        "farmer_mobile" => $farmer_mobile, 
        "farmer_email" => $farmer_email,         
        "farmer_village" => $this->input->post('farmer_village'), 
        "farmer_taluka" => $farmer_taluka, 
        "farmer_district" => $this->input->post('farmer_district'), 
        "farmer_state" => $this->input->post('farmer_state'),
        "total_acrage" => $this->input->post('total_acrage'),        
        /*"crop_1" => $this->input->post('crop_1'),
        "crop_2" => $this->input->post('crop_2'),*/
        "gps_location" => $this->input->post('gps_location'),
        "geo_lat" => $this->input->post('geo_lat'),
        "geo_long" => $this->input->post('geo_long'),
        "soil_test" => $this->input->post('soil_test'),
        "water_test" => $this->input->post('water_test'),
        "irrigated" => $this->input->post('irrigated'),         
        "demo_crop_id" => $this->input->post('demo_crop_id'), 
        "demo_crop_variety_id" => $this->input->post('demo_crop_variety_id'),
        "demo_crop_reason" => $this->input->post('demo_crop_reason'),
        "acrage_for_demo" => $this->input->post('acrage_for_demo'),        
        "visit_date" => date(DATE_DB, strtotime($this->input->post('visit_date'))),
        "demo_start_date" => date(DATE_DB, strtotime($this->input->post('demo_start_date'))),
        
        "farmer_last_soil_test_date" => $farmer_last_soil_test_date,
        "farmer_last_water_test_date" => $farmer_last_water_test_date,     

        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id,
        "updated_date" => date(DATETIME_DB),                
        "updated_by" => $this->user_id
        );

        $farmerid = $this->dbaccess_model->insert("demos", $data);            
        if($farmerid > 0)
        {
            $farmer_reg_num = $this->common_model->get_farmer_reg_no("demos");
            $farmer_reg_num = $farmer_reg_num + 1;
            $this->dbaccess_model->update("demos", array("farmer_reg_num"=>$farmer_reg_num), array("farmer_id" => $farmerid));



            $sdarr = array("typ"=>'district', "id" => $this->input->post('farmer_district'));
            $sd = $this->common_model->get_detail($sdarr);
            $fstate_name = strtolower(trim($sd-> state_name));
            $fdistrict_name = strtolower(trim($sd-> district_name));
            $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/demo";





            $next_stage_date = $this->input->post('next_stage_date');
            if(isset($next_stage_date) && !empty($next_stage_date))
            {
                $datain = array("farmer_id" => $farmerid,
                    "next_stage_date" => date(DATE_DB, strtotime($next_stage_date)),
                    "for_stage" => "2",
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                );

                $this->dbaccess_model->insert("demos_stage", $datain);
            }          


            $crop_last_year = json_decode($this->input->post('crop_last_year'));
            $crop_current_year = json_decode($this->input->post('crop_current_year'));
            $demos_product = json_decode($this->input->post('demos_product'));
            $demos_stage = json_decode($this->input->post('demos_stage'));

            if(isset($crop_last_year) && !empty($crop_last_year))
            {
                foreach($crop_last_year as $k => $obj)
                {
                    $datain = array("farmer_id" => $farmerid,
                    "last_year_crop_id" => $obj-> last_year_crop_id,
                    "last_year_crop_qty" => $obj-> last_year_crop_qty,                    
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("demos_crop_last_year", $datain);
                }    
            }


            if(isset($crop_current_year) && !empty($crop_current_year))
            {
                foreach($crop_current_year as $k => $obj)
                {
                    $datain = array("farmer_id" => $farmerid,
                    "current_year_crop_id" => $obj-> current_year_crop_id,
                    "current_year_crop_qty" => $obj-> current_year_crop_qty,                    
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("demos_crop_current_year", $datain);
                }    
            }


            if(isset($demos_product) && !empty($demos_product))
            {
                foreach($demos_product as $k => $obj)
                {
                    $datain = array("farmer_id" => $farmerid,
                    "product_id" => $obj-> product_id,
                    "product_qty" => $obj-> product_qty,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("demos_product", $datain);
                }    
            }           
            
            
        }

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;
    }



    public function get_upcoming_demos()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($this->user_role_id == 2 && ($aoo_state_id == "" || $aoo_state_id <= 0))
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($this->user_role_id == 3)
        {
            if($aoo_state_id == "" || $aoo_state_id <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
                die;
            }
            elseif($aoo_district_id == "")    
            {
                echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
                die;
            }
        }

        if(!isset($from_date) || empty($from_date)) $from_date = date("Y-m-01");
        else $from_date = format_date($from_date, DATE_DB);

        if(!isset($to_date) || empty($to_date)) $to_date = date("Y-m-d");
        else $to_date = format_date($to_date, DATE_DB);

        
        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id, "from_date" => $from_date, "to_date" => $to_date);

        $data = $this->webservice_model->get_upcoming_demos($arr);        

        $response = array();
        $dates = array();

        if(isset($data) && !empty($data))
        {
            foreach($data as $key => $obj)
            {
                $getarr = array("farmer_id" => $obj-> farmer_id, "from_date" => $from_date, "to_date" => $to_date);

                $arr = $this->webservice_model->get_next_demo_of_farmer($getarr);

                if(isset($arr) && !empty($arr))
                {
                    $response[$key] = $obj;

                    $response[$key]->next_stage_date = format_date($arr-> next_stage_date, DATE);
                    $response[$key]->for_next_stage = "Stage ".$arr-> for_stage;

                    $dates[] = $response[$key]->next_stage_date;
                }
            }
        }

        $dates = array_unique($dates);
        
        $res_data = array();
        $res_data['dates'] = $dates;
        $res_data['listing'] = $response;

        if(isset($response) && !empty($response))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$res_data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }



    public function demo_stage_2()
    {
        $session_key = $this->input->post('session_key');        
        $farmerid = $this->input->post('farmer_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($farmerid == "" || $farmerid <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer ID required.")); 
            die;
        }

        $next_stage_date = $this->input->post('next_stage_date');
        if(!isset($next_stage_date) || empty($next_stage_date))
        {
            echo json_encode(array("status"=>0,"message"=>"Next visit date required.")); 
            die;
        }


        $visit_date = $this->input->post('visit_date');
        if(!isset($visit_date) || empty($visit_date))
        {
            $visit_date = date(DATE_DB);
        }
        else
        {
            $visit_date = format_date($visit_date, DATE_DB);
        }

        $arr = array("typ"=>"demos", "id"=>$farmerid);
        $data_chk = $this->common_model->is_exist($arr);
        if(!isset($data_chk) || empty($data_chk))
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer details not found in the system.")); die;
        } 


        if(isset($next_stage_date) && !empty($next_stage_date))
        {
            $datain = array("farmer_id" => $farmerid,
                "next_stage_date" => date(DATE_DB, strtotime($next_stage_date)),
                "for_stage" => 3,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
            );

            $this->dbaccess_model->insert("demos_stage", $datain);
        }       


        $acrage_demo = $this->input->post('acrage_demo');
        $company_fertilizer_demo = json_decode($this->input->post('company_fertilizer_demo'));
        $other_company_fertilizer_demo = json_decode($this->input->post('other_company_fertilizer_demo'));
        $micro_nutrients_demo = json_decode($this->input->post('micro_nutrients_demo'));
        $weedicides_demo = json_decode($this->input->post('weedicides_demo'));        
        
        if(isset($company_fertilizer_demo) && !empty($company_fertilizer_demo))
        {
            foreach($company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $obj-> product_id,
                "product_qty" => $obj-> product_qty,
                "for_stage" => 2,
                "demo_type" => 1,                
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        }

        if(isset($other_company_fertilizer_demo) && !empty($other_company_fertilizer_demo))
        {
            foreach($other_company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $obj-> product_id,
                "product_qty" => $obj-> product_qty,
                "for_stage" => 2,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        } 


        if(isset($micro_nutrients_demo) && !empty($micro_nutrients_demo))
        {
            foreach($micro_nutrients_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "micro_nutrient_id" => $obj-> micro_nutrient_id,
                "micro_nutrient_qty" => $obj-> micro_nutrient_qty,
                "for_stage" => 2,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_micro_nutrients", $datain);
            }    
        }


        if(isset($weedicides_demo) && !empty($weedicides_demo))
        {
            foreach($weedicides_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "weedicide_id" => $obj-> weedicide_id,
                "weedicide_qty" => $obj-> weedicide_qty,
                "for_stage" => 2,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_weedicides", $datain);
            }    
        }


        $farmer_district = $data_chk->farmer_district;
        $sdarr = array("typ"=>'district', "id" => $farmer_district);
        $sd = $this->common_model->get_detail($sdarr);
        $fstate_name = strtolower(trim($sd-> state_name));
        $fdistrict_name = strtolower(trim($sd-> district_name));
        $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/demo";

        $farmer_reg_num = $data_chk->farmer_reg_num;
                     
        if(isset($_FILES['plot_photos']) && !empty($_FILES['plot_photos']))
        {   
            $files = $_FILES['plot_photos']; 

            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos[]']['name']= $files['name'][$key];
                $_FILES['plot_photos[]']['type']= $files['type'][$key];
                $_FILES['plot_photos[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos[]']['error']= $files['error'][$key];
                $_FILES['plot_photos[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s2_demo_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 2,
                    "demo_type" => 1,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }



        $acrage_comparative = $this->input->post('acrage_comparative');
        $company_fertilizer_demo = json_decode($this->input->post('company_fertilizer_comparative'));
        $other_company_fertilizer_demo = json_decode($this->input->post('other_company_fertilizer_comparative'));
        $micro_nutrients_demo = json_decode($this->input->post('micro_nutrients_comparative'));
        $weedicides_demo = json_decode($this->input->post('weedicides_comparative'));        
        if(isset($company_fertilizer_demo) && !empty($company_fertilizer_demo))
        {
            foreach($company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $obj-> product_id,
                "product_qty" => $obj-> product_qty,
                "for_stage" => 2,
                "demo_type" => 2,                
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        }

        if(isset($other_company_fertilizer_demo) && !empty($other_company_fertilizer_demo))
        {
            foreach($other_company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $obj-> product_id,
                "product_qty" => $obj-> product_qty,
                "for_stage" => 2,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        } 


        if(isset($micro_nutrients_demo) && !empty($micro_nutrients_demo))
        {
            foreach($micro_nutrients_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "micro_nutrient_id" => $obj-> micro_nutrient_id,
                "micro_nutrient_qty" => $obj-> micro_nutrient_qty,
                "for_stage" => 2,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_micro_nutrients", $datain);
            }    
        }


        if(isset($weedicides_demo) && !empty($weedicides_demo))
        {
            foreach($weedicides_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "weedicide_id" => $obj-> weedicide_id,
                "weedicide_qty" => $obj-> weedicide_qty,
                "for_stage" => 2,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_weedicides", $datain);
            }    
        }


                      
        if(isset($_FILES['plot_photos_comparative']) && !empty($_FILES['plot_photos_comparative']))
        {   
            $files = $_FILES['plot_photos_comparative'];
            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos_comparative[]']['name']= $files['name'][$key];
                $_FILES['plot_photos_comparative[]']['type']= $files['type'][$key];
                $_FILES['plot_photos_comparative[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos_comparative[]']['error']= $files['error'][$key];
                $_FILES['plot_photos_comparative[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s2_comp_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos_comparative[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 2,
                    "demo_type" => 2,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }

        $d = array("current_demo_stage"=>2,
                "acrage_demo"=>$acrage_demo,                
                "acrage_comparative"=>$acrage_comparative,
                "visit_date_stage_2"=>$visit_date
                );
        $this->dbaccess_model->update("demos", $d , array("farmer_id" => $farmerid));

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;

        
    }




    public function demo_stage_3()
    {
        $session_key = $this->input->post('session_key');        
        $farmerid = $this->input->post('farmer_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($farmerid == "" || $farmerid <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer ID required.")); 
            die;
        }

        $next_stage_date = $this->input->post('next_stage_date');
        if(!isset($next_stage_date) || empty($next_stage_date))
        {
            echo json_encode(array("status"=>0,"message"=>"Next visit date required.")); 
            die;
        }


        $visit_date = $this->input->post('visit_date');
        if(!isset($visit_date) || empty($visit_date))
        {
            $visit_date = date(DATE_DB);
        }
        else
        {
            $visit_date = format_date($visit_date, DATE_DB);
        }


        $arr = array("typ"=>"demos", "id"=>$farmerid);
        $data_chk = $this->common_model->is_exist($arr);
        if(!isset($data_chk) || empty($data_chk))
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer details not found in the system.")); die;
        } 


        if(isset($next_stage_date) && !empty($next_stage_date))
        {
            $datain = array("farmer_id" => $farmerid,
                "next_stage_date" => date(DATE_DB, strtotime($next_stage_date)),
                "for_stage" => 4,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
            );

            $this->dbaccess_model->insert("demos_stage", $datain);
        }       

        
        $company_fertilizer_demo = json_decode($this->input->post('company_fertilizer_demo'));
        $other_company_fertilizer_demo = json_decode($this->input->post('other_company_fertilizer_demo'));
        $micro_nutrients_demo = json_decode($this->input->post('micro_nutrients_demo'));
        $weedicides_demo = json_decode($this->input->post('weedicides_demo'));        
        
        if(isset($company_fertilizer_demo) && !empty($company_fertilizer_demo))
        {
            foreach($company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $obj-> product_id,
                "product_qty" => $obj-> product_qty,
                "for_stage" => 3,
                "demo_type" => 1,                
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        }

        if(isset($other_company_fertilizer_demo) && !empty($other_company_fertilizer_demo))
        {
            foreach($other_company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $obj-> product_id,
                "product_qty" => $obj-> product_qty,
                "for_stage" => 3,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        } 


        if(isset($micro_nutrients_demo) && !empty($micro_nutrients_demo))
        {
            foreach($micro_nutrients_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "micro_nutrient_id" => $obj-> micro_nutrient_id,
                "micro_nutrient_qty" => $obj-> micro_nutrient_qty,
                "for_stage" => 3,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_micro_nutrients", $datain);
            }    
        }


        if(isset($weedicides_demo) && !empty($weedicides_demo))
        {
            foreach($weedicides_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "weedicide_id" => $obj-> weedicide_id,
                "weedicide_qty" => $obj-> weedicide_qty,
                "for_stage" => 3,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_weedicides", $datain);
            }    
        }


        $farmer_district = $data_chk->farmer_district;
        $sdarr = array("typ"=>'district', "id" => $farmer_district);
        $sd = $this->common_model->get_detail($sdarr);
        $fstate_name = strtolower(trim($sd-> state_name));
        $fdistrict_name = strtolower(trim($sd-> district_name));
        $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/demo";

        $farmer_reg_num = $data_chk->farmer_reg_num;
        
        if(isset($_FILES['plot_photos']) && !empty($_FILES['plot_photos']))
        {   
            $files = $_FILES['plot_photos'];              

            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos[]']['name']= $files['name'][$key];
                $_FILES['plot_photos[]']['type']= $files['type'][$key];
                $_FILES['plot_photos[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos[]']['error']= $files['error'][$key];
                $_FILES['plot_photos[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s3_demo_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 3,
                    "demo_type" => 1,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }


        
        $company_fertilizer_demo = json_decode($this->input->post('company_fertilizer_comparative'));
        $other_company_fertilizer_demo = json_decode($this->input->post('other_company_fertilizer_comparative'));
        $micro_nutrients_demo = json_decode($this->input->post('micro_nutrients_comparative'));
        $weedicides_demo = json_decode($this->input->post('weedicides_comparative'));        
        if(isset($company_fertilizer_demo) && !empty($company_fertilizer_demo))
        {
            foreach($company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $obj-> product_id,
                "product_qty" => $obj-> product_qty,
                "for_stage" => 3,
                "demo_type" => 2,                
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        }

        if(isset($other_company_fertilizer_demo) && !empty($other_company_fertilizer_demo))
        {
            foreach($other_company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $obj-> product_id,
                "product_qty" => $obj-> product_qty,
                "for_stage" => 3,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        } 


        if(isset($micro_nutrients_demo) && !empty($micro_nutrients_demo))
        {
            foreach($micro_nutrients_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "micro_nutrient_id" => $obj-> micro_nutrient_id,
                "micro_nutrient_qty" => $obj-> micro_nutrient_qty,
                "for_stage" => 3,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_micro_nutrients", $datain);
            }    
        }


        if(isset($weedicides_demo) && !empty($weedicides_demo))
        {
            foreach($weedicides_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "weedicide_id" => $obj-> weedicide_id,
                "weedicide_qty" => $obj-> weedicide_qty,
                "for_stage" => 3,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_weedicides", $datain);
            }    
        }


        
        if(isset($_FILES['plot_photos_comparative']) && !empty($_FILES['plot_photos_comparative']))
        {
            $files = $_FILES['plot_photos_comparative'];              

            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos_comparative[]']['name']= $files['name'][$key];
                $_FILES['plot_photos_comparative[]']['type']= $files['type'][$key];
                $_FILES['plot_photos_comparative[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos_comparative[]']['error']= $files['error'][$key];
                $_FILES['plot_photos_comparative[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s3_comp_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos_comparative[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 3,
                    "demo_type" => 2,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }


        $crop_condition_demo = $this->input->post('crop_condition_demo');
        $vegitative_growth_demo = $this->input->post('vegitative_growth_demo');
        $flowering_growth_demo = $this->input->post('flowering_growth_demo');
        $fruitning_growth_demo = $this->input->post('fruitning_growth_demo');
        $quality_demo = $this->input->post('quality_demo');
        
        $datain = array("farmer_id" => $farmerid,        
        "for_stage" => 3,
        "demo_type" => 1,
        "crop_condition" => $crop_condition_demo,
        "vegitative_growth" => $vegitative_growth_demo,
        "flowering_growth" => $flowering_growth_demo,
        "fruitning_growth" => $fruitning_growth_demo,
        "quality" => $quality_demo,
        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id
        );    
        $this->dbaccess_model->insert("demos_crop_quality", $datain);
        
        
        $crop_condition_demo = $this->input->post('crop_condition_comparative');
        $vegitative_growth_demo = $this->input->post('vegitative_growth_comparative');
        $flowering_growth_demo = $this->input->post('flowering_growth_comparative');
        $fruitning_growth_demo = $this->input->post('fruitning_growth_comparative');
        $quality_demo = $this->input->post('quality_comparative');
        $datain = array("farmer_id" => $farmerid,        
        "for_stage" => 3,
        "demo_type" => 2,
        "crop_condition" => $crop_condition_demo,
        "vegitative_growth" => $vegitative_growth_demo,
        "flowering_growth" => $flowering_growth_demo,
        "fruitning_growth" => $fruitning_growth_demo,
        "quality" => $quality_demo,
        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id
        );    
        $this->dbaccess_model->insert("demos_crop_quality", $datain);


        $d = array("current_demo_stage"=>3, "visit_date_stage_3"=>$visit_date);
        $this->dbaccess_model->update("demos", $d , array("farmer_id" => $farmerid));

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;

        
    }



    public function demo_stage_4()
    {
        $session_key = $this->input->post('session_key');        
        $farmerid = $this->input->post('farmer_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($farmerid == "" || $farmerid <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer ID required.")); 
            die;
        }

        
        $arr = array("typ"=>"demos", "id"=>$farmerid);
        $data_chk = $this->common_model->is_exist($arr);
        if(!isset($data_chk) || empty($data_chk))
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer details not found in the system.")); die;
        } 


        $visit_date = $this->input->post('visit_date');
        if(!isset($visit_date) || empty($visit_date))
        {
            $visit_date = date(DATE_DB);
        }
        else
        {
            $visit_date = format_date($visit_date, DATE_DB);
        }


        $yields_demo = $this->input->post('yields_demo');
        $yields_comparative = $this->input->post('yields_comparative');

        
        $quality_demo = $this->input->post('quality_demo');
        $quality_comparative = $this->input->post('quality_comparative');

        $market_price = $this->input->post('market_price');
        $yield_difference = $this->input->post('yield_difference');
        $produce_value = $this->input->post('produce_value');
        $cost_of_production = $this->input->post('cost_of_production');
        $yield_difference_justification = $this->input->post('yield_difference_justification');
        
        $farmers_feedback = $this->input->post('farmers_feedback');
        $executive_remark = $this->input->post('executive_remark');

        $analysis_report_1 = $this->input->post('analysis_report_1');
        $analysis_report_2 = $this->input->post('analysis_report_2');
        $analysis_report_3 = $this->input->post('analysis_report_3');
        $analysis_report_4 = $this->input->post('analysis_report_4');



        $farmer_district = $data_chk->farmer_district;
        $sdarr = array("typ"=>'district', "id" => $farmer_district);
        $sd = $this->common_model->get_detail($sdarr);
        $fstate_name = strtolower(trim($sd-> state_name));
        $fdistrict_name = strtolower(trim($sd-> district_name));
        $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/demo";

        $farmer_reg_num = $data_chk->$farmer_reg_num;

        $guest_name = $this->input->post('guest_name');
        if(isset($_FILES['guest_photo']) && !empty($_FILES['guest_photo']))
        {
            $guest_photo = $_FILES['guest_photo'];            
            if(isset($guest_photo) && !empty($guest_photo))
            {                
                $ext = pathinfo($guest_photo['name'], PATHINFO_EXTENSION);

                $chk = $this->upload_image($path, 'guest_photo', $farmerid.'_'.$this->user_id."_gp_".$guest_photo['name']);
                if(isset($chk) && !empty($chk))
                {
                    
                }
                else
                {
                    $spath = BASE_PATH.'/'.$path."/".$farmerid.'_'.$this->user_id."_gp_".$guest_photo['name'];
                    $this->dbaccess_model->update("demos", array("guest_photo_url"=>$spath), array("farmer_id" => $farmerid));
                }
            }
        }



        $farmer_district = $data_chk->farmer_district;
        $sdarr = array("typ"=>'district', "id" => $farmer_district);
        $sd = $this->common_model->get_detail($sdarr);
        $fstate_name = strtolower(trim($sd-> state_name));
        $fdistrict_name = strtolower(trim($sd-> district_name));
        $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/demo";


        if(isset($_FILES['plot_photos']) && !empty($_FILES['plot_photos']))
        {   
            $files = $_FILES['plot_photos'];              

            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos[]']['name']= $files['name'][$key];
                $_FILES['plot_photos[]']['type']= $files['type'][$key];
                $_FILES['plot_photos[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos[]']['error']= $files['error'][$key];
                $_FILES['plot_photos[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s4_demo_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 4,
                    "demo_type" => 1,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }



        if(isset($_FILES['plot_photos_comparative']) && !empty($_FILES['plot_photos_comparative']))
        {
            $files = $_FILES['plot_photos_comparative'];              

            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos_comparative[]']['name']= $files['name'][$key];
                $_FILES['plot_photos_comparative[]']['type']= $files['type'][$key];
                $_FILES['plot_photos_comparative[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos_comparative[]']['error']= $files['error'][$key];
                $_FILES['plot_photos_comparative[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s4_comp_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos_comparative[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 4,
                    "demo_type" => 2,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }




        $datain = array("farmer_id" => $farmerid,        
        "for_stage" => 4,
        "demo_type" => 1,        
        "quality" => $quality_demo,
        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id
        );    
        $this->dbaccess_model->insert("demos_crop_quality", $datain);
        
        
        $datain = array("farmer_id" => $farmerid,        
        "for_stage" => 4,
        "demo_type" => 2,        
        "quality" => $quality_comparative,
        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id
        );    
        $this->dbaccess_model->insert("demos_crop_quality", $datain);
        

        $d = array("current_demo_stage"=>4,
            "yields_demo"=>$yields_demo,
            "yields_comparative"=>$yields_comparative,

            "market_price"=>$market_price,
            "yield_difference"=>$yield_difference,
            "produce_value"=>$produce_value,
            "cost_of_production"=>$cost_of_production,
            "yield_difference_justification"=>$yield_difference_justification,
            
            "analysis_report_1"=>$analysis_report_1,
            "analysis_report_2"=>$analysis_report_2,
            "analysis_report_3"=>$analysis_report_3,
            "analysis_report_4"=>$analysis_report_4,

            "farmers_feedback"=>$farmers_feedback,
            "executive_remark"=>$executive_remark,

            "guest_name"=>$guest_name,

            "visit_date_stage_4"=>$visit_date
        );

        $this->dbaccess_model->update("demos",$d , array("farmer_id" => $farmerid));

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;

        
    }



    public function get_sales()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');
        
        $product_id = $this->input->post('product_id');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($this->user_role_id == 2 && ($aoo_state_id == "" || $aoo_state_id <= 0))
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($this->user_role_id == 3)
        {
            if($aoo_state_id == "" || $aoo_state_id <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
                die;
            }
            elseif($aoo_district_id == "")    
            {
                echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
                die;
            }
        }

        if(!isset($from_date) || empty($from_date)) $from_date = date("Y-m-01");
        else $from_date = format_date($from_date, DATE_DB);

        if(!isset($to_date) || empty($to_date)) $to_date = date("Y-m-d");
        else $to_date = format_date($to_date, DATE_DB);

        
        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id, "from_date" => $from_date, "to_date" => $to_date, "product_id" => $product_id);

        $data = $this->webservice_model->get_sales($arr);        
        
        $response = array();
        
        $temp = $colors = $tabular = array();

        if(isset($data) && !empty($data))
        {
            foreach($data as $key => $obj)
            {
                if(isset($tabular[$obj-> product_name][$obj-> company]) && !empty($tabular[$obj-> product_name][$obj-> company]))
                {
                    $tabular[$obj-> product_name][$obj-> company] = $tabular[$obj-> product_name][$obj-> company] + $obj-> quantity;
                }
                else
                {
                    $tabular[$obj-> product_name][$obj-> company] = $obj-> quantity;
                } 

                $colors[$obj-> product_name] = $obj-> product_color;                
            }
            
            foreach($tabular as $pnm => $arr)
            {
                if(isset($arr['OTHERS']) && !empty($arr['OTHERS']))
                {
                    $other = $arr['OTHERS'];
                }
                else
                {
                    $other = 0;
                }


                if(isset($arr['RMPCL']) && !empty($arr['RMPCL']))
                {
                    $rmpcl = $arr['RMPCL'];
                }
                else
                {
                    $rmpcl = 0;
                }

                $total = $other + $rmpcl;
                $percent = 0;
                if($total > 0)
                {
                    $percent = number_format((($rmpcl * 100) / $total), '2', '.', '');
                }

                $temp[] = array("product_name" => $pnm, "rmpcl" => $rmpcl, "others" => $other, "total" => $total, "product_color" => $colors[$pnm], "percent" => $percent);                
            }
        }
        
        if(isset($temp) && !empty($temp))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$temp)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }



    public function get_cost()
    {
        $session_key = $this->input->post('session_key');        
        $farmerid = $this->input->post('farmer_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($farmerid == "" || $farmerid <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer ID required.")); 
            die;
        }


        $arr = array("typ"=>"demos", "id"=>$farmerid);
        $data_chk = $this->common_model->is_exist($arr);
        if(!isset($data_chk) || empty($data_chk))
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer details not found in the system.")); die;
        }

        $arr = array("farmer_id"=>$farmerid);
        $data = $this->webservice_model->get_cost($arr); 

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;

    } 




    public function get_wholesalers()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($aoo_state_id == "" || $aoo_state_id <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($aoo_district_id == "")    
        {
            echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
            die;       
        }
        
        $details = $this->admin_model->last_upload_date('upload_wholesaler');
        if(!isset($details) || empty($details))
        {
            $file_id = 0;
        }
        else
        {
            $file_id = $details-> file_id;
        }
        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id, "file_id" => $file_id);
        $data = $this->webservice_model->get_wholesalers($arr);

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }



    public function get_retailers()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($aoo_state_id == "" || $aoo_state_id <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($aoo_district_id == "")    
        {
            echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
            die;       
        }
        $details = $this->admin_model->last_upload_date('upload_retailer');
        if(!isset($details) || empty($details))
        {
            $file_id = 0;
        }
        else
        {
            $file_id = $details-> file_id;
        }
        
        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id, "file_id" => $file_id);
        
        $data = $this->webservice_model->get_retailers($arr);

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }


    public function save_stock()
    {
        $session_key = $this->input->post('session_key');
        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        $saler_id = $this->input->post('saler_id');
        $saler_type = $this->input->post('saler_type');
        $current_quantity = $this->input->post('current_quantity');
        $verify_quantity = $this->input->post('verify_quantity');
        $photo = $this->input->post('photo'); 
        $product_id = $this->input->post('product_id');
        $reason_id = $this->input->post('reason_id');       
        $visit_date = $this->input->post('visit_date');       

        $txt = "Saler";
        if($saler_type == 1) $txt = "Wholesaler";
        elseif($saler_type == 2) $txt = "Retailer";
         
        if($saler_type == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Saler type required.")); die;
        }
        elseif($saler_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"$txt ID required.")); die;
        }      
        elseif($current_quantity == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Current quantity required."));
            die;    
        }
        elseif($verify_quantity == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Verify quantity required."));
            die;    
        } 
        elseif($product_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Product name required."));
            die;    
        }
        elseif($reason_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Stock Verification Reason required."));
            die;    
        }/*
        elseif($visit_date == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Visit Date required."));
            die;    
        }*/    

               
                
        
        if($saler_type == 1)
            $arr = array("typ"=>"wholesalers", "id"=>$saler_id);
        elseif($saler_type == 2)
            $arr = array("typ"=>"dealers", "id"=>$saler_id);

        $data_chk = $this->common_model->is_exist($arr);
        if(!isset($data_chk) || empty($data_chk))
        {
            echo json_encode(array("status"=>0,"message"=>"$txt details not found in the system.")); die;
        }              
        

        if(isset($visit_date) && !empty($visit_date))
        {
            $visit_date = date(DATE_DB, strtotime($visit_date));
        }


        $data = array("stock_saler_id" => $saler_id,
        "product_id" => $product_id, 
        "stock_verification_reason_id" => $reason_id, 
        "stock_current_quantity" => $current_quantity, 
        "stock_verify_quantity" => $verify_quantity, 
        "stock_photo_url" => "",
        "visit_date" => $visit_date,         
        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id,
        "updated_date" => date(DATETIME_DB),                
        "updated_by" => $this->user_id
        );
        
        $farmerid = $this->dbaccess_model->insert("stock_verification", $data);            
        
        if($farmerid > 0)
        {            
            $path = "assets/".UPLOAD_IMAGE_PATH;

            
            if(isset($_FILES['stock_photo']) && !empty($_FILES['stock_photo']))
            {
                $files = $_FILES['stock_photo'];

                $config = array(
                    'upload_path'   => $path,
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'overwrite'     => 1                       
                );
                $this->load->library('upload', $config);

                $stock_photo_arr = array();
                foreach ($files['name'] as $key => $image) 
                {
                    $_FILES['stock_photo[]']['name']= $files['name'][$key];
                    $_FILES['stock_photo[]']['type']= $files['type'][$key];
                    $_FILES['stock_photo[]']['tmp_name']= $files['tmp_name'][$key];
                    $_FILES['stock_photo[]']['error']= $files['error'][$key];
                    $_FILES['stock_photo[]']['size']= $files['size'][$key];
                    
                                        
                    $config['file_name'] = $saler_id.'_'.$this->user_id."_stk_".$farmerid."_".$image;

                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('stock_photo[]')) 
                    {
                        $dataup = $this->upload->data();                        
                        $unm = $dataup['file_name'];
                        
                        $stock_photo_arr[] = array("file_name" => $unm, "stock_photo_url" => BASE_PATH.'/'.$path."/".$unm);
                    } 
                    else 
                    {
                        $sql = "DELETE FROM stock_verification WHERE id = '$farmerid' LIMIT 1";
                        $this->dbaccess_model->executeSql($sql);
                        
                        echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                        die;
                    }
                }

                if(isset($stock_photo_arr) && !empty($stock_photo_arr))
                {
                    $str = json_encode($stock_photo_arr);

                    $this->dbaccess_model->update("stock_verification", 
                        array("stock_photo_url"=>$str), 
                        array("id" => $farmerid)
                    );
                }    
            } 
        }

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;
    }




    public function quick_add_farmer()
    {
        $session_key = $this->input->post('session_key');
        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        


        $farmer_state = $this->input->post('farmer_state');
        $farmer_district = $this->input->post('farmer_district');
        $farmer_taluka = $this->input->post('farmer_taluka');
        $farmer_village = $this->input->post('farmer_village');


        $farmer_name = $this->input->post('farmer_name');
        $farmer_mobile = $this->input->post('farmer_mobile');        
        $farmer_land_area = $this->input->post('farmer_land_area');
        

        if($farmer_state == "" || $farmer_state <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer State required."));
            die;    
        }
        elseif($farmer_district == "" || $farmer_district <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer District required."));
            die;    
        }
        elseif($farmer_taluka == "" || $farmer_taluka <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Taluka required."));
            die;    
        }
        /*elseif($farmer_village == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Village required."));
            die;    
        }*/
        elseif($farmer_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Name required.")); die;
        }       
        elseif($farmer_mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number required."));
            die;    
        }
        elseif(strlen($farmer_mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        elseif($farmer_land_area == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Land Area required."));
            die;    
        }        
        
                
        $arr = array("typ"=>"farmers_temp", "id"=>0, "name"=>$farmer_mobile);
        $data = $this->common_model->is_already_exist($arr);
        if(isset($data) && !empty($data))
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number already exist. Please enter different Mobile Number.")); die;
        }        


        $data = array("farmer_name" => $farmer_name, 
        "farmer_mobile" => $farmer_mobile,         
        
        "farmer_village" => $farmer_village, 
        "farmer_taluka" => $farmer_taluka, 
        "farmer_district" => $farmer_district, 
        "farmer_state" => $farmer_state,        
        "farmer_land_area" => $farmer_land_area,        

        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id,
        "updated_date" => date(DATETIME_DB),                
        "updated_by" => $this->user_id
        );

        $farmerid = $this->dbaccess_model->insert("farmers_temp", $data);            
        
        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;
    }


    public function quick_get_farmers()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($this->user_role_id == 2 && ($aoo_state_id == "" || $aoo_state_id <= 0))
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($this->user_role_id == 3)
        {
            if($aoo_state_id == "" || $aoo_state_id <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
                die;
            }
            elseif($aoo_district_id == "")    
            {
                echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
                die;
            }
        }
        
        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id);
        $data = $this->webservice_model->quick_get_farmers($arr);

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }



    public function get_target_for_state()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        
        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if(($aoo_state_id == "" || $aoo_state_id <= 0))
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        
        
        $arr = array("state_id" => $aoo_state_id, "year" => date("Y"), "month" => date("m"));
        $data = $this->webservice_model->get_target_for_state($arr);

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }



    public function get_wholesalers_profile()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($aoo_state_id == "" || $aoo_state_id <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($aoo_district_id == "")    
        {
            echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
            die;       
        }

        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id);
        $data = $this->webservice_model->get_wholesalers_profile($arr);
        
        if(isset($data) && !empty($data))
        {
            $temp = array();
            foreach($data as $obj)
            {
                $obj-> rating = $obj-> rating;

                $obj-> ack_value = $this->webservice_model->get_ack_value("upload_acknowledgement1", $obj-> saler_id, $aoo_state_id, $aoo_district_id);

                $obj-> inventory = $this->webservice_model->get_inventory_value("upload_wholesaler", $obj-> saler_id, $aoo_state_id, $aoo_district_id);

                $stock_aging_arr = $this->webservice_model->get_stock_aging("ws", $obj-> saler_id, $aoo_state_id, $aoo_district_id);
                $obj-> stock_aging = $this->format_array($stock_aging_arr);


                if($obj-> inventory == null) $obj-> inventory = 0;

                $temp[] = $obj;
            }

            echo json_encode(array("status"=>1,"message"=>"","data"=>$temp)); 
        }
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }


    public function format_array($arr)
    {
        $res = $temp = $temp1 = array();

        if(isset($arr) && !empty($arr))
        {
            foreach($arr as $obj)
            {
                $product_code = $obj-> product_code;
                
                unset($obj-> product_code);
                
                $temp1[$product_code][] = $obj;                
            }

            foreach($temp1 as $product_code => $a)
            {                
                $temp[] = array("product_code" => $product_code, "details" => $a);                
            }

        }

        return $temp;

    }


    public function get_retailers_profile()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($aoo_state_id == "" || $aoo_state_id <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($aoo_district_id == "")    
        {
            echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
            die;       
        }
        
        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id);
        $data = $this->webservice_model->get_retailers_profile($arr);

        if(isset($data) && !empty($data))
        {
            $temp = array();
            foreach($data as $obj)
            {
                $obj-> rating = $obj-> rating;

                $obj-> ack_value = $this->webservice_model->get_ack_value("upload_acknowledgement2", $obj-> saler_id, $aoo_state_id, $aoo_district_id);

                $obj-> inventory = $this->webservice_model->get_inventory_value("upload_retailer", $obj-> saler_id, $aoo_state_id, $aoo_district_id);

                $stock_aging_arr = $this->webservice_model->get_stock_aging("rt", $obj-> saler_id, $aoo_state_id, $aoo_district_id);
                $obj-> stock_aging = $this->format_array($stock_aging_arr);

                if($obj-> inventory == null) $obj-> inventory = 0;

                

                $temp[] = $obj;
            }

            echo json_encode(array("status"=>1,"message"=>"","data"=>$temp)); 
        }
        else
            echo json_encode(array("status"=>0,"message"=>"No record found.")); 
        
        die;
    }


    public function get_sum()
    {
        $session_key = $this->input->post('session_key');        
        $aoo_state_id = $this->input->post('aoo_state_id');
        $aoo_district_id = $this->input->post('aoo_district_id');

        if(!isset($session_key) || empty($session_key))
        {
            echo json_encode(array("status"=>0,"message"=>"Session key required.")); 
            die;    
        }

        $this->is_logged_in($session_key);        

        if($this->user_role_id == 2 && ($aoo_state_id == "" || $aoo_state_id <= 0))
        {
            echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
            die;
        }
        elseif($this->user_role_id == 3)
        {
            if($aoo_state_id == "" || $aoo_state_id <= 0)
            {
                echo json_encode(array("status"=>0,"message"=>"State ID required.")); 
                die;
            }
            elseif($aoo_district_id == "")    
            {
                echo json_encode(array("status"=>0,"message"=>"District ID required.")); 
                die;
            }
        }
        
        $arr = array("state_id" => $aoo_state_id, "district_id" => $aoo_district_id);
        $data = $this->webservice_model->get_sum($arr);

        if(isset($data) && !empty($data))
            echo json_encode(array("status"=>1,"message"=>"","data"=>$data)); 
        else
            echo json_encode(array("status"=>0,"message"=>"No record found."));
        
        die;
    }

}
?>