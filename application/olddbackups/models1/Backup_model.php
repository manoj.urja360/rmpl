<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* Description of backup_model
*
* @author https://roytuts.com
*/
class Backup_model extends CI_Model 
{
    private $backup = 'backup';
   
    
    public function restore_backup($backup_date, $backup_location)
    {        
        set_time_limit(0);

        $backup_date = date(DATE_DB, strtotime($backup_date));

        // Database configuration------------------------------------------------------
        $host = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $database_name = $this->db->database;
        
        $exclude_tables = array("db_backup", "cronjobs", "cronjobs_queue","modules","permissions");

        // Get connection object and set the charset
        $conn = mysqli_connect($host, $username, $password, $database_name);
        $conn->set_charset("utf8");


        // Get All Table Names From the Database
        $tables = array();
        $sql = "SHOW TABLES";
        $result = mysqli_query($conn, $sql);

        while ($row = mysqli_fetch_row($result)) 
        {
            $tables[] = $row[0];
        }

        $tables = array_diff($tables, $exclude_tables);        
        
        //Code for removing newly inserted records-----------------------------------------------------------
        $sqlScript = "";
        foreach ($tables as $table) 
        {            
            $query = "TRUNCATE TABLE $table";
            $result = mysqli_query($conn, $query);            
        }


        $query = '';
        $sqlScript = file($backup_location); 

        foreach ($sqlScript as $line) 
        {          
          $startWith = substr(trim($line), 0 ,2);
          $endWith = substr(trim($line), -1 ,1);
          
          if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') 
          {
            continue;
          }
          
          $query = $query . $line;
          
          if ($endWith == ';') 
          {
                mysqli_query($conn,$query);
                
                $query= '';   
          }
        }
    }     

}
?>