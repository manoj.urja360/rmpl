<section class="content-header">
<h1>
Edit Demo
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>
</ul>


<div id="message_box"></div>

<fieldset >
<form class="form-horizontal" name="process_form" id="process_form" method="post" style="margin:0px !important;">

<table class="table table-stripped" width="100%">    
<tr>
    <td><?php echo MANDATORY;?>Registration Number: <input type="text" class="form-control" value="<?php echo "D".regno($details['basic']-> farmer_reg_num, 5);?>" readonly></td>    

    <td><?php echo MANDATORY;?>Registration Date: <input type="text" class="form-control" value="<?php echo format_date($details['basic']-> visit_date, "d-m-Y");?>" readonly></td>
</tr> 

<tr>
    <td colspan="2"><?php echo MANDATORY;?>Select Farmer: 
      <select class="form-control select2" style="width:100%;">        
        <?php
          foreach($farmers as $obj)
          {
            $sel = "";
            if($obj->farmer_id == $details['basic']-> farmer_parent_id) 
            {
              $sel = "selected";
          ?>    
            <option value="<?php echo $obj->farmer_id;?>" <?php echo $sel;?>><?php echo $obj->farmer_name;?></option>
          <?php
            }
          }
        ?>
      </select>  
    </td>    
</tr>       

<tr><td colspan="3"><b>Farmer Information</b></td></tr>

<tr>
    <td><?php echo MANDATORY;?>Farmer Name: <input type="text" name="farmer_name" class="form-control" value="<?php echo $details['basic']-> farmer_name;?>"></td>    
    
    <td><?php echo MANDATORY;?>Mobile: <input type="text" class="form-control" name="farmer_mobile" class="form-control" value="<?php echo $details['basic']-> farmer_mobile;?>"></td>

    <td>Email: <input type="email" name="farmer_email" value="<?php echo $details['basic']-> farmer_email;?>" class="form-control"></td>

    
</tr>

<tr>
    <td><?php echo MANDATORY;?>State: <select name="farmer_state" id="farmer_state" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'farmer_district');" style="width: 100%;">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
              $sel = "";
              if($obj->state_id == $details['basic']-> farmer_state) $sel = "selected";
          ?>    
            <option value="<?php echo $obj->state_id;?>" <?php echo $sel;?>><?php echo $obj->state_name;?></option>
          
          <?php
            if($sel != "")
            {
          ?>
            <script type="text/javascript">                
                get_dd_list(<?php echo $obj->state_id;?>, 'district_by_state_aop_dd', 'farmer_district');
              </script>
          <?php
            }
          }
        ?>
       </select></td>   

    <td><?php echo MANDATORY;?>District: <select name="farmer_district" id="farmer_district" class="form-control select2" onchange="get_dd_list(this.value, 'taluka_by_district', 'farmer_taluka');" style="width: 100%;"></select></td>
    
    <td><?php echo MANDATORY;?>Taluka: <select name="farmer_taluka" id="farmer_taluka" class="form-control select2" style="width: 100%;"></select></td>
</tr>
  

<tr>
    <td><?php echo MANDATORY;?>Village: <input type="text" name="farmer_village" class="form-control" value="<?php echo $details['basic']-> farmer_village;?>"></td>

    <td><?php echo MANDATORY;?>Total Acrage: <input type="text" name="total_acrage" class="form-control" value="<?php echo $details['basic']-> total_acrage;?>"></td>
    <!-- <td>By Executive: <select name="fby_executive" id="fby_executive" class="form-control select2"></select></td> -->
</tr>


<tr>
    <td>Crop 1: 
      <select name="crop_1" id="crop_1" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], $details['basic']-> crop_1);?>
      </select>
    </td>
    
    <td>Crop 2:
      <select name="crop_2" id="crop_2" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], $details['basic']-> crop_2);?>
      </select>
    </td>
</tr>


<tr>
    <td>Soil Test: <select name="soil_test" class="form-control select2" style="width: 100%;">
      <option value="yes" <?php if($details['basic']-> soil_test == 'yes') {echo 'selected';}?>>Yes</option>
      <option value="no" <?php if($details['basic']-> soil_test == 'no') {echo 'selected';}?>>No</option>
    </select></td>
    
    <td>Irrigated: <select name="irrigated" class="form-control select2" style="width: 100%;">
      <option value="yes" <?php if($details['basic']-> irrigated == 'yes') {echo 'selected';}?>>Yes</option>
      <option value="no" <?php if($details['basic']-> irrigated == 'no') {echo 'selected';}?>>No</option>
    </select></td>
</tr>


<tr>
  <td colspan="2">    
    <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
    
    <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" class="btn btn-default btn_process">Cancel</button>
    
    <input name="hdn_id" value="<?php echo $details['basic']-> farmer_id;?>" type="hidden">        
</td>
</tr>
</table>

</form>
</fieldset> 




</div>
</div>  
</div>
</div>
</section>



<script type="text/javascript">
$(function()
{
    $(".select2").select2();
    $(".select2").select2().trigger("chosen:updated");

    $(".date_sel").datepicker({maxDate:0, dateFormat:"dd-mm-yy"});



    setTimeout(function()
    {
      $("#farmer_district").val("<?php echo $details['basic']-> farmer_district;?>");
      $("#farmer_district").select2().trigger("chosen:updated");

      get_dd_list(<?php echo $details['basic']-> farmer_district;?>, 'taluka_by_district', 'farmer_taluka');
     
        
      setTimeout(function()
      {
        $("#farmer_taluka").val("<?php echo $details['basic']-> farmer_taluka;?>");
        $("#farmer_taluka").select2().trigger("chosen:updated");
        
      }, "1000");

    }, "1000");


    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"demos/demo_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/demos'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
        
    });


});



</script>