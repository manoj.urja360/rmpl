<section class="content-header">
<h1>
Add New Dealer
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/dealers';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/dealers_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/dealers_import';" data-toggle="tab" aria-expanded="false">Import</a></li>
</ul>

<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">

<div class="row">
  <div class="form-group">        
    <div class="col-sm-3">      
      <label for="saler_sys_id"><?php echo MANDATORY;?>Dealer ID</label>
      <input class="form-control" name="saler_sys_id" value="" type="text" maxlength="50">
    </div>
  </div>
</div>

<div class="row">
  <div class="form-group">        
    <div class="col-sm-3">      
      <label for="first_name"><?php echo MANDATORY;?>First Name</label>
      <input class="form-control" name="first_name" value="" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="last_name"><?php echo MANDATORY;?>Last Name</label>
      <input class="form-control" name="last_name" value="" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="email"><?php echo MANDATORY;?>Email</label>
      <input class="form-control" name="email" value="" type="email" maxlength="150">
    </div>

    <div class="col-sm-3">
      <label for="mobile"><?php echo MANDATORY;?>Mobile</label>
      <input class="form-control" name="mobile" value="" type="text" maxlength="10">
    </div>
  </div>
</div> 



<div class="row"><h4>Address Details</h4>
  <div class="form-group">    
    <div class="col-sm-4"> 
      <label for="cstates_name"><?php echo MANDATORY;?>State Name</label>
      <select name="cstates_name" id="cstates_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'ccity_name'); get_dd_list(this.value, 'state_head', 'state_head');">
        <option value="">Select</option>
        <?php
          foreach($states_all as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-4">
      <label for="ccity_name"><?php echo MANDATORY;?>District Name</label>
      <select name="ccity_name" id="ccity_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_executive', 'district_executive');"></select>
    </div>

    <div class="col-sm-4">
      <label for="caddress"><?php echo MANDATORY;?>Address</label>
      <input class="form-control" name="caddress" id="caddress" type="text" maxlength="255">
    </div>  
    
  </div>
</div>




<div class="row">
  <div class="form-group">  
    <div class="col-sm-4">
      <label for="contact_person">Contact Person</label>
      <input class="form-control" name="contact_person" type="text" maxlength="100">
    </div>    
  </div>
</div>



<!-- <div class="row">
  <div class="form-group">        
    <div class="col-sm-6">
      <label for="state_head">State Head</label>
      <select name="state_head" id="state_head" class="form-control select2">        
       </select>
    </div>    

    <div class="col-sm-6">
      <label for="district_executive">District Executive</label>
      <select name="district_executive" id="district_executive" class="form-control select2">        
       </select>
    </div>    


  </div>
</div> -->


<div class="row btn_row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/dealers';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="0" type="hidden">
      <input name="saler_type" value="2" type="hidden">
    </div>
  </div> 
</div> 



</form>
</div>
</div>  
</div>
</div>
</section>

<script type="text/javascript">

$(document).ready(function()
{
    $(".select2").select2();

    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/salers_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/dealers'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>