<section class="content-header">
<h1>
Add New Demo Stage 4
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add';" data-toggle="tab" aria-expanded="false">Add Demo Stage 1</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add2';" data-toggle="tab" aria-expanded="false">Add Demo Stage 2</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add3';" data-toggle="tab" aria-expanded="false">Add Demo Stage 3</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add4';" data-toggle="tab" aria-expanded="false">Add Demo Stage 4</a></li>

</ul>


<div id="message_box"></div>


<fieldset >
<form class="form-horizontal" name="process_form4" id="process_form4" method="post" style="margin:0px !important;">
<table class="table table-stripped" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;">
    <td colspan="3" align="center"><h4>Stage 4 - Harvesting & Analysis Report</h4></td>
  </tr>
</thead>


<tr>
    <td colspan="3"><?php echo MANDATORY;?>Select Demo: 
      <select name="farmer_id" id="farmer_id" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php
          foreach($farmers as $obj)
          {
          ?>    
            <option value="<?php echo $obj-> farmer_id;?>"><?php echo $obj-> farmer_name." - D".regno($obj-> farmer_reg_num, 5);?></option>
          <?php
          }
        ?>       
      </select>  
    </td>    
</tr> 

<tr>
  <td colspan="3"><?php echo MANDATORY;?>Visit Date: <input type="text" name="visit_date" class="date_sel"></td>
</tr>

<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>

<tr>
  <td>Yield (KGs/Acre)</td>  
  <td><input type="text" name="yields_demo" class="form-control"></td>
  <td><input type="text" name="yields_comparative" class="form-control"></td>
</tr>


<tr>
  <td>Quality</td>
  
  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="quality_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="quality_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="quality_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="quality_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="quality_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="quality_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>

<tr>
  <td>Photos</td>

  <td>
    <table width="100%">
      <tr>        
        <td><input type="file" name="plot_photos[]" class="form-control" multiple=""></td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <tr>
        <td><input type="file" name="plot_photos_comparative[]" class="form-control" multiple=""></td>
      </tr>
    </table>
  </td>
</tr>


<tr>
  <td>Market Price INR</td>  
  <td colspan="2"><input type="text" name="market_price" class="form-control"></td>
</tr>

<tr>
  <td>Yield Difference</td>  
  <td colspan="2"><input type="text" name="yield_difference" class="form-control"></td>
</tr>

<tr>
  <td>Produce Value / Acre</td>  
  <td colspan="2"><input type="text" name="produce_value" class="form-control"></td>
</tr>

<tr>
  <td>Cost of Production/Acre (fertilisers)</td>  
  <td colspan="2"><input type="text" name="cost_of_production" class="form-control"></td>
</tr>

<tr>
  <td>Yield Difference Justification</td>  
  <td colspan="2"><input type="text" name="yield_difference_justification" class="form-control"></td>
</tr>


<tr>
  <td>Analysis Report</td>  
  <td colspan="2">
    Comparative analysis of yield: <input type="text" name="analysis_report_1" class="form-control"><br/>
    Comparative time for life cycle: <input type="text" name="analysis_report_2" class="form-control"><br/>
    Requirement of irrigation analysis: <input type="text" name="analysis_report_3" class="form-control"><br/>
    Effect on growth, colour, yield and cost benefitation: <input type="text" name="analysis_report_4" class="form-control">
  </td>
</tr>


<tr>
  <td>Farmers Feedback</td>  
  <td colspan="2"><input type="text" name="farmers_feedback" class="form-control"></td>
</tr>

<tr>
  <td>Executive Remark</td>  
  <td colspan="2"><input type="text" name="executive_remark" class="form-control"></td>
</tr>

<tr>
  <td>Guest Name & Photo</td>  
  <td colspan="2"><input type="text" name="guest_name" class="form-control"><br/><input type="file" name="guest_photo"></td>
</tr>

<tr>
  <td colspan="3">    
    <button type="button" name="btn_save" id="btn_save" class="btn btn-primary btn_process" onclick="form_submit(4);">Complete Demo</button>&nbsp;
    
    <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" class="btn btn-default btn_process">Cancel</button>
    
    <input name="hdn_id" value="0" type="hidden">        
</td>
</tr>
</table>
</form>
</fieldset> 




</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(function()
{
    $(".select2").select2();

    $(".date_sel").datepicker({maxDate:0, dateFormat:"dd-mm-yy"});
});



function form_submit(stage_num)
{
    processing_bar();

    var formData = new FormData($("#process_form"+stage_num)[0]);

    $.ajax({url : base_url+"demos/demo_stage_"+stage_num,
      method: "POST",
      data: formData,
      async: false,
      dataType: 'json',
      success: function(res)
      {   
          if(res.status == 1)
          {
              msg = msg_ok + res.message + '</div>';

              setTimeout(function()
              {                    
                window.location.href = base_url+'admin/demos'; 
                
              }, time_out);
          }
          else
          {
              msg = msg_error + res.message + '</div>';

              hide_msg_box();
          }
          
          show_msg_box(msg);
      },
      cache: false,
      contentType: false,
      processData: false
    });

    return false;
    
}


</script>