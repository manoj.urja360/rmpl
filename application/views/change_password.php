<section class="content-header">
<h1>
Change Password
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">


<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
  <div class="form-group">        
    <div class="col-sm-4">      
      <label for="password"><?php echo MANDATORY;?>Choose New Password</label>
      <input class="form-control" name="password" value="" type="password" maxlength="70">
    </div>
  </div>
  
  <div class="form-group">  
    <div class="col-sm-4">
      <label for="cpassword"><?php echo MANDATORY;?>Confirm New Password</label>
      <input class="form-control" name="cpassword" value="" type="password" maxlength="70">
    </div>
  </div>
</div> 



<div class="row btn_row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin';" class="btn btn-default btn_process">Cancel</button>
      
    </div>
  </div> 
</div> 



</form>
</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">

$(document).ready(function()
{
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/change_password_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>