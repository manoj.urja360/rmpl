<?php
$modules = $this->common_model->get_all_modules();
$permitted_modules = $this->common_model->permitted_modules($this->user_role_id);

$pm_arr = array();
if(isset($permitted_modules) && !empty($permitted_modules))
{
    foreach($permitted_modules as $obj)
    {
        $pm_arr[$obj-> module_id][] = $obj-> permission_id;
    }
}

?>

<section class="sidebar" style="height: auto;">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url().$this->user_photo_url;?>?t=<?php echo time();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->user_fullname;?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree" style="height: 550px; overflow: auto;">
        <?php
        if(isset($modules) && !empty($modules))
        {
            foreach($modules as $obj)
            { 
                if($obj-> url == "#")
                {
                ?>
                    <li class="treeview">
                      <a href="#">
                        <i class="<?php echo $obj-> icon;?>"></i>
                        <span><?php echo $obj-> module_name;?></span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>

                          <ul class="treeview-menu" >
                      
                      <?php                        
                      
                      $submodules = $this->common_model->get_sub_modules($obj-> module_id); 
                      
                      foreach($submodules as $sobj)
                      { 
                          if(isset($pm_arr[$sobj-> module_id]))
                          {
                      ?>
                            <li><a href="<?php echo base_url(); ?><?php echo $sobj-> url;?>"><i class="<?php echo $sobj-> icon;?>"></i> <?php echo $sobj-> module_name;?></a></li>                        
                      <?php
                          }
                          elseif(isset($pm_arr[$sobj-> parent_id]))
                          {
                      ?>
                            <li><a href="<?php echo base_url(); ?><?php echo $sobj-> url;?>"><i class="<?php echo $sobj-> icon;?>"></i> <?php echo $sobj-> module_name;?></a></li>                        
                      <?php
                          }
                      }
                      ?>
                      </ul>
                    </li>
                <?php  
                }
                else
                {
                    if(isset($pm_arr[$obj-> module_id]))
                    {
            ?>
                    <li>
                      <a href="<?php echo base_url(); ?><?php echo $obj-> url;?>">
                        <i class="<?php echo $obj-> icon;?>"></i> <span><?php echo $obj-> module_name;?></span>
                      </a>
                    </li>
            <?php
                    }
                }
            ?>    
                <!--  -->

            <?php  
            }
        }
        ?>        
      </ul>      
</section>