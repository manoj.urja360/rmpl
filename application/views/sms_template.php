<section class="content-header">
<h1>
Manage SMS Templates
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sms_template';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sms_template_add_w';" data-toggle="tab" aria-expanded="false">Acknowledgement 1</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sms_template_add_r1';" data-toggle="tab" aria-expanded="false">Acknowledgement 2</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sms_template_add_r';" data-toggle="tab" aria-expanded="false">Stock Movement</a></li>
</ul>


<div class="row" style="margin-top:2%; margin-bottom:2%;">
  <div class="form-group">
    <div class="col-sm-3"></div>

    <div class="col-sm-6">
      <label for="template_for">Search Template Type</label>
      <select name="template_for" id="template_for" class="form-control select2 fby">
        <option value="">All</option>

        <option value="1">Acknowledgement 1</option>
        <option value="3">Acknowledgement 2</option>
        <option value="2">Stock Movement</option>
        
       </select>
    </div>
</div>
</div>

<fieldset style="overflow: auto;">

<div id="message_box"></div>

<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_200;?>">Content</th>
<th style="<?php echo COL_70;?>">Days</th>
<th style="<?php echo COL_100;?>">Copy To</th>
<th style="<?php echo COL_100;?>">Template For</th>
<th style="<?php echo COL_50;?>">Actions</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;
 
$(document).ready(function() 
{
 
    $(".select2").select2();
 
    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "asc" ]],       
    "aoColumns": [        
    { "bSortable": true },    
    { "bSortable": true },    
    { "bSortable": true },    
    { "bSortable": true },        
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/sms_template_list",
            "type": "POST",
            "data": function ( data ) {
                data.template_for = $('#template_for').val();                
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ], 
    });


    $(".fby").change(function()
    {
        table.ajax.reload(null, false);
    });
 
});



function set_as_default(template_type, template_for, template_id)
{
    if(template_id <= 0) return;    

    var formData = {"template_id":template_id, "template_type":template_type, "template_for":template_for};

    $.ajax({url : base_url+"admin/set_as_default",
      method: "POST",
      data: formData,
      async: false,
      dataType: 'json',
      success: function(res)
      {   
          table.ajax.reload(null,false);          
      }
  });        
    
}
</script>