<style type="text/css">
.fa.fa-frown-o
{
    color: #df2424 !important;
    font-size: 12px !important;
}

.fa.fa-star
{
    color: #6cc644 !important;
    font-size: 12px !important;
}
</style>
<section class="content-header">
<h1>
Wholesalers
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/wholesalers';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/wholesalers_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/wholesalers_import';" data-toggle="tab" aria-expanded="false">Import</a></li>
</ul>



<div class="row" style="margin-top:2%; margin-bottom:2%;">
  <div class="form-group">    
    <div class="col-sm-5">
      <label for="fby_state">State Name</label>
      <select name="fby_state" id="fby_state" class="form-control select2 fby" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'fby_city');">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-5">
      <label for="fby_city">District Name</label>
      <select name="fby_city" id="fby_city" class="form-control select2 fby"></select>
    </div>     
  </div>
</div>

<div class="row btn_row">
<div class="form-group">
    <div class="col-sm-4">
      <button type="submit" name="btn_fby" id="btn_fby" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;Search</button>&nbsp;
      <button type="button" name="btn_reset" id="btn_reset" class="btn btn-danger"><i class="fa fa-empty"></i>&nbsp;Clear Search</button>
    </div>

    <div class="col-sm-8">        
      <div class="btn-group" style="float: right; margin-right: 2%;">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="fa fa-cloud-download"></span>&nbsp;Export</button>
        
        <ul class="dropdown-menu" role="menu">
          <li><a href="#" onclick="export_data('csv');"><i class="fa fa-file-text-o"></i>&nbsp;CSV</a></li>
          

          <li><a href="#" onclick="export_data('xls');"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</a></li>
          

          <li><a href="#" onclick="export_data('pdf');"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a></li>
        </ul>
      </div>      
    </div>
</div>
</div>



<fieldset style="overflow: auto;">


<div id="message_box"></div>

<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_70;?>">WS ID</th>
<th style="<?php echo COL_200;?>">Full Name</th>
<th style="<?php echo COL_150;?>">Email</th>
<th style="<?php echo COL_100;?>">Mobile</th>
<th style="<?php echo COL_150;?>">Address</th>
<th style="<?php echo COL_150;?>">District</th>
<th style="<?php echo COL_150;?>">State</th>
<th style="<?php echo COL_50;?>">Actions</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;

<?php
$osearch_filter = "";
if(isset($fsid) && !empty($fsid))
{
    $osearch_filter = "'search': {
        'search': '".$fsid."'
      },";
}
?>

 
$(document).ready(function() {
    
    $(".select2").select2();

    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    <?php echo $osearch_filter;?>
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "asc" ]],       
    "aoColumns": [        
    { "bSortable": true },        
    { "bSortable": true },        
    { "bSortable": true },        
    { "bSortable": true },        
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true },        
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/salers_list",
            "type": "POST",
            "data": function ( data ) {
                data.saler_type = 1;
                data.fby_state = $("#fby_state").val();
                data.fby_city = $("#fby_city").val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ],
 
    });


    $("#btn_fby").click(function()
    {
        table.ajax.reload(null, true);
    });


    $("#btn_reset").click(function()
    {
        $(".fby").val("");
        $(".select2").select2().trigger("chosen:updated");

        $("#btn_fby").trigger("click");
    });

});



function export_data(typ)
{
    var file_id = "0";
    var report_type = "wholesalers_all";

    var fby_state = $("#fby_state").val();
    var fby_city = $("#fby_city").val();
    var fby_product = "0";
    var fby_from_date = "0";
    var fby_to_date = "0";
    var fby_keyword = "0";
    var fby_executive = "0";

    if(fby_state == "" || fby_state == null) fby_state = 0;
    if(fby_city == "" || fby_city == null) fby_city = 0;    
    if(fby_from_date == "" || fby_from_date == null) fby_from_date = 0;
    if(fby_to_date == "" || fby_to_date == null) fby_to_date = 0;
    if(fby_keyword == "" || fby_keyword == null) fby_keyword = 0;
    if(fby_executive == "" || fby_executive == null) fby_executive = 0;
    
    var url = base_url + "admin/export_data/"+ typ + "/" + report_type + "/" + file_id + "/" + fby_state + "/" + fby_city + "/" + fby_product + "/" + fby_from_date + "/" + fby_to_date + "/" + fby_keyword + "/" + fby_executive;

    window.open(url,"");
}
</script>