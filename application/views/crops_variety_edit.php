<section class="content-header">
<h1>
Edit Crop Variety
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/crops_variety';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/crops_variety_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>
</ul>

<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
  <div class="form-group">        
    <div class="col-sm-6">
      <input type="hidden" name="crop_name" value="<?php echo $crop_variety_id;?>">
      <label for="crop_name">Crop Name: </label>
             
        <?php
          foreach($crops as $obj)
          {
              if($details->crop_id == $obj->crop_id)
              {          
                  echo "".$obj->crop_name."";
              }            
          }
        ?>
        
    </div>
  </div>
</div>


<div class="row">
  <div class="form-group">        
    <div class="col-sm-6">
      <label for="sub_name"><?php echo MANDATORY;?>Crop Variety Name</label>
      <input class="form-control" name="sub_name" id="sub_name" value="<?php echo $details->crop_variety_name;?>" type="text" maxlength="100">
    </div>
  </div>
</div>  

<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/crops_variety';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="<?php echo $details->crop_variety_id;?>" type="hidden">
    </div>
  </div> 
</div> 

</form>
</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(document).ready(function()
{
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/crops_variety_edit_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/crops_variety'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>