<style type="text/css">
.view_gallery_model
{
    position: fixed;
    left: 0%;
    top:4%;
    background-color: #fff;
    border: 5px solid #ddd;
    border-radius: 3px;
    display: none;
    width: 94%;
    height: auto;
    overflow-y: auto; 
    overflow-x: hidden;
    z-index: 999;
    margin: 3% 3%;
}


.modal.and.carousel, .carousel-control 
{
  position: absolute; 
} 


/*.view_gallery_model .modal-body
{ 
  overflow-y: auto;
  overflow-x: hidden;
  max-height: 400px;
} */


.modal-content
{
  /*min-height: 500px;*/
}

.plot_photos
{
  height: 100px;
  width: 100px;
  border: 3px solid #ccc;
  text-align: center;
  border-radius: 3px;
  margin-right:15px;
  margin-bottom: 15px; 
}


.full_image
{
  /*width: 100%;
  height: 450px;*/
}
</style>


<section class="content-header">
<h1>
Photo Gallery
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/album_farmer';" data-toggle="tab" aria-expanded="false">Farmer Album</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/album_demo';" data-toggle="tab" aria-expanded="false">Demo Album</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/album_stock';" data-toggle="tab" aria-expanded="false">Stock Album</a></li>
</ul>


<div class="row" style="margin-top:2%; margin-bottom:2%;">
  <div class="form-group">    
    <div class="col-sm-3">
      <label for="fby_state">State Name</label>
      <select name="fby_state" id="fby_state" class="form-control select2 fby" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'fby_city');">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-3">
      <label for="fby_city">District Name</label>
      <select name="fby_city" id="fby_city" class="form-control select2 fby" onchange="get_dd_list(this.value, 'district_executive', 'fby_executive');"></select>
    </div>


    <div class="col-sm-3">
      <label for="fby_executive">Executive Name</label>
      <select name="fby_executive" id="fby_executive" class="form-control select2 fby"></select>
    </div>


    <div class="col-sm-3">
      <label for="fby_stage">Stage</label>
      <select name="fby_stage" id="fby_stage" class="form-control select2 fby">
        <option value="">Select</option>
        <option value="1">Stage 1</option>
        <option value="2">Stage 2</option>
        <option value="3">Stage 3</option>
        <option value="4">Stage 4</option>
      </select>
    </div>  
    
  </div>
</div>

<div class="row btn_row">
<div class="form-group">
    <div class="col-sm-4">
      <button type="submit" name="btn_fby" id="btn_fby" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;Search</button>&nbsp;
      <button type="button" name="btn_reset" id="btn_reset" class="btn btn-danger"><i class="fa fa-empty"></i>&nbsp;Clear Search</button>
    </div>
</div>
</div>



<fieldset style="overflow: auto;">
<div id="message_box"></div>

<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_150;?>">Farmer Name</th>
<th style="<?php echo COL_150;?>">Executive Name</th>
<th style="<?php echo COL_100;?>">Stage</th>
<th style="<?php echo COL_100;?>">Demo Plot Images</th>
<th style="<?php echo COL_100;?>">Comparative Plot Images</th>
<th style="<?php echo COL_50;?>">Action</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>



<div class="view_gallery_model" id="view_gallery_model">
<div class="modal-header">
<h4 class="modal-title font-weight-semibold txt-blue ">Photo Gallery</h4>      
</div>
<div class="modal-body">
<div class="form-area"> 
<div class="row">
  <div class="col-md-6"><h4>Demo Plot</h4>
    <ul id="gallerylist_demo"></ul>
  </div>  

  <div class="col-md-6"><h4>Comparative Plot</h4>
    <ul id="gallerylist_comp"></ul>
  </div>  
</div>
</div>   
</div>
<div class="modal-footer">
<!-- <button type="button" class="btn btn-success" onclick="export_gallery();">Select & Export</button>
 --><button type="button" class="btn btn-danger" onclick="hide_gallery_popup();">Close</button>
</div>
</div>



<div class="container">  
<div class="modal fade and carousel slide" id="lightbox">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-body">
<ol class="carousel-indicators" id="gallerylistzoom_li"></ol>
<div class="carousel-inner" id="gallerylistzoom"></div>
<a class="left carousel-control" href="#lightbox" role="button" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" href="#lightbox" role="button" data-slide="next">
<span class="glyphicon glyphicon-chevron-right"></span>
</a>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>


<style type="text/css">
.plot_photos_sort
{
    width: 100%;
    height:200px;
}  
</style>
<div class="container">  
<div class="modal fade and carousel slide" id="lightbox2">
<div class="modal-dialog modal-xl">
<div class="modal-content">
<div class="modal-body">
<div class="form-area"> 
<div class="row">
  <div class="col-md-6"><h4>Demo Plot</h4>
    <ul id="gallerylist_demo_1"></ul>
  </div>  

  <div class="col-md-6"><h4>Comparative Plot</h4>
    <ul id="gallerylist_comp_1"></ul>
  </div>  
</div>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>



<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var def_search = "";
<?php
$url_id = $this->uri->segment(3);    
if(isset($url_id) && !empty($url_id))
{
?>
    def_search = "<?php echo $url_id;?>"; 
<?php  
}
?>

var table;
 
$(document).ready(function() 
{
    $("#fby_state").change(function()
    {
        $("#fby_executive").empty();
    });

    $(".select2").select2();
 
    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "asc" ]],
    "oSearch": {"sSearch": def_search },       
    "aoColumns": [        
    { "bSortable": true },
    { "bSortable": true },        
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },        
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/album_demo_list",
            "type": "POST",
            "data": function ( data ) 
            {
                data.fby_state = $("#fby_state").val();
                data.fby_city = $("#fby_city").val();
                data.fby_executive = $("#fby_executive").val();
                data.fby_stage = $("#fby_stage").val();

            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ], 
    });


    $("#btn_fby").click(function()
    {
        table.ajax.reload(null, false);
    });


    $("#btn_reset").click(function()
    {
        $(".fby").val("");
        $(".select2").select2().trigger("chosen:updated");

        $("#btn_fby").trigger("click");
    });


    <?php
    $url_id = $this->uri->segment(3);    
    if(isset($url_id) && !empty($url_id))
    {
    ?>
        $("#example1_wrapper").find("#example1_filter").find("input[type='search']").val("<?php echo $url_id;?>");
        $("#example1_wrapper").find("#example1_filter").find("input").val("<?php echo $url_id;?>");
        table.ajax.reload(null, false);
    <?php  
    }
    ?>
 
});



function load_photo_gallery(farmer_id, farmer_type, for_stage)
{
    processing_bar();

    $.ajax({url : base_url+"admin/load_photo_gallery",
      method: "POST",
      data: {"farmer_id":farmer_id,"farmer_type":farmer_type,"for_stage":for_stage},
      async: false,
      dataType: 'json',
      success: function(res)
      {   
          if(res.status == 1)
          {     
               $("#view_gallery_model").fadeIn("slow");

               var images_demo="";
               var images_comp="";
               var images_demo_1="";
               var images_comp_1="";

               var images_zoom="";
               var images_zoom_li="";
               var cls="";
               
               
                $.each(res.data, function(i,v)
                {
                  if(v.demo_type == 1)
                  {
                    images_demo = images_demo + "<li style='float: left; display:inline;'><a href='#lightbox' data-toggle='modal' data-slide-to='"+i+"'><img class='plot_photos' src='"+v.plot_photo_url+"' alt='"+v.plot_photo_url+"'></a></li>";

                    images_demo_1 = images_demo_1 + "<li class='ui-state-default'><img class='plot_photos_sort' src='"+v.plot_photo_url+"' alt='"+v.plot_photo_url+"'></li>";
                  }
                  else if(v.demo_type == 2)
                  {  
                    images_comp = images_comp + "<li style='float: left; display:inline;'><a href='#lightbox' data-toggle='modal' data-slide-to='"+i+"'><img class='plot_photos' src='"+v.plot_photo_url+"' alt='"+v.plot_photo_url+"'></a></li>";

                    images_comp_1 = images_comp_1 + "<li class='ui-state-default'><img class='plot_photos_sort' src='"+v.plot_photo_url+"' alt='"+v.plot_photo_url+"'></li>";
                  }


                   cls = "";
                   if(i == 0) cls = "active";

                   images_zoom_li = images_zoom_li + '<li data-target="#lightbox" data-slide-to="'+i+'" class="'+cls+'"></li>';
                    

                   images_zoom = images_zoom + '<div class="item '+cls+'"><img src="'+v.plot_photo_url+'" alt="'+v.plot_photo_url+'" class="full_image"></div>';
                });

                
                 
                $("#gallerylist_demo").html(images_demo);
                $("#gallerylist_comp").html(images_comp);

                $("#gallerylistzoom").html(images_zoom);
                $("#gallerylistzoom_li").html(images_zoom_li);


                $("#gallerylist_demo_1").html(images_demo_1);
                $("#gallerylist_comp_1").html(images_comp_1);


                $("#gallerylist_demo_1").sortable();
                $("#gallerylist_comp_1").sortable();
                
          }
          else
          {
              msg = msg_error + res.message + '</div>';
              
              hide_gallery_popup();
          }

          hide_msg_box();
          
          show_msg_box(msg);
      }
    });
}


function export_gallery()
{
    $("#lightbox2").modal({"show":true});


}


function hide_gallery_popup()
{
    $("#gallerylist").html('');
    $("#gallerylistzoom").html('');
    $("#gallerylistzoom_li").html('');

    $("#view_gallery_model").fadeOut("slow");
}
</script>