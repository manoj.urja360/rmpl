<section class="content-header">
<h1>
Set Ratings
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">


<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
<div class="form-group">            
<div class="col-sm-12">
      
<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th colspan="2" style="<?php echo COL_150;?>;text-align: center;">No. Of Days</th>
<th rowspan="2" style="<?php echo COL_150;?>;text-align: center;">Rating</th>
</tr>

<tr class="table_head">
<th style="<?php echo COL_150;?>;text-align: center;">From Days</th>
<th style="<?php echo COL_150;?>;text-align: center;">To Days</th>
</tr> 
</thead>
<tbody>
<?php
foreach($rating as $k => $obj)
{  
    $readonly1 = $readonly2 = "";
    $numeric1  = $numeric2 = "numericonly";
    if($k == 0) { $readonly1 = "readonly"; $numeric1 = "";}
    if($k == (count($rating)-1)) { $readonly2 = "readonly"; $numeric2 = "";}
?> 
<tr>
    <td>
      <input type="text" name="rating_from[<?php echo $obj->rating_id;?>]" class="form-control <?php echo $numeric1;?>" value="<?php echo $obj-> rating_from;?>" <?php echo $readonly1;?>>
    </td>

    <td>
      <input type="text" name="rating_to[<?php echo $obj->rating_id;?>]" class="form-control <?php echo $numeric2;?>" value="<?php echo $obj-> rating_to;?>" <?php echo $readonly2;?>>
    </td>

    <td>
      <select name="rating_star[<?php echo $obj->rating_id;?>]" class="form-control">        
        <?php
        for($i=5; $i>=1;$i--) 
        {
            $sel = "";
            if($obj-> rating_star == $i)
            {
              $sel = "selected";
            }
        ?>
          <option value="<?php echo $i;?>" <?php echo $sel;?>><?php echo $i;?> Star</option>
        <?php  
        }        

        $sel = "";
        if($obj-> rating_star == 0)
        {
          $sel = "selected";
        }
        ?> 
        <option value="0" <?php echo $sel;?>>Sad</option>
      </select>
    </td>
</tr>  
<?php
}
?>
</tbody>
</table>
      
</div>
</div>
</div>  



<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/units';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="0" type="hidden">
    </div>
  </div> 
</div> 

</form>
</div>
</div>  
</div>
</div>
</section>

<script type="text/javascript">
$(document).ready(function()
{
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/rating_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/rating'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>