<section class="content-header">
<h1>
Target Settings
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">


<div class="box-body">

<!-- <ul class="nav nav-tabs">
<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/area_of_operation';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/area_of_operation_set';" data-toggle="tab" aria-expanded="false">Set Area Of Operation</a></li>
</ul> -->
<?php
$month = array("01"=>'January', 
"02"=>'February', 
"03"=>'March', 
"04"=>'April', 
"05"=>'May', 
"06"=>'June', 
"07"=>'July', 
"08"=>"August", 
"09"=>"September", 
"10"=>"October", 
"11"=>"November", 
"12"=>"December");
?>
<fieldset>
<div id="message_box"></div>
<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row"><h5>Set Target for <b><?php echo $year;?></b></h5>
<div class="form-group">        
<?php
if(isset($details) && !empty($details))
{
  foreach($details as $obj)
  { 
      $mnm = date("F", strtotime($obj -> target_year."-".$obj -> target_month."-01"));
      $mid = $obj -> target_month;
      $val = $obj -> target_value;
  ?>
      <div class="col-sm-2">
        <label><?php echo $mnm;?></label>
        <input class="form-control" name="year[<?php echo $mid;?>]" value="<?php echo $val;?>" type="text" maxlength="10" onkeyup="chk_numeric(this);">
      </div>
  <?php
  }
}
else
{
  foreach($month as $mid => $mnm)
  {
  ?>
      <div class="col-sm-2">
        <label><?php echo $mnm;?></label>
        <input class="form-control" name="year[<?php echo $mid;?>]" value="" type="text" maxlength="10" onkeyup="chk_numeric(this);">
      </div>
  <?php
  }
}
?>
<input name="cyear" value="<?php echo $year;?>" type="hidden" >    
</div>
</div>



<br/>
<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/target';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="0" type="hidden">
    </div>
  </div> 
</div>
</form>


<br/>
<hr/>
<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_100;?>">Target Year</th>
<th style="<?php echo COL_100;?>">Month</th>
<th style="<?php echo COL_100;?>">Value</th>              
<th style="<?php echo COL_50;?>">Actions</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>


</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;
 
$(document).ready(function() 
{
 
    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "DESC" ], [ 1, "asc" ]],       
    "aoColumns": [        
    { "bSortable": true },    
    { "bSortable": true },
    { "bSortable": true },    
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/target_list",
            "type": "POST",
            "data": function ( data ) {
                //data.qs_name = $('#qs_name').val();                
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ],
 
    });
 
    $('#search').click(function()
    { 
        table.ajax.reload(null,false);  //just reload table
    });

    
});



$(document).ready(function()
{
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/target_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/target'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>