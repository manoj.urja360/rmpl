<section class="content-header">
<h1>
Listing
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-header with-border">
<i class="fa fa-warning"></i>
<h3 class="box-title">Listing</h3>
</div>


<div class="box-body">
<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>billing/ipd_patients';" data-toggle="tab" aria-expanded="false"><img src="<?php echo base_url();?>theme/images/list_tab_16.png">&nbsp;Admitted Patients</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>billing/discharged';" data-toggle="tab" aria-expanded="false"><img src="<?php echo base_url();?>theme/images/list_tab_16.png">&nbsp;Discharged Patients</a></li>
</ul>

<fieldset>
<legend>Patient List</legend>
<input type="hidden" name="filterby" id="filterby" value="all">
<div class="form-group">
<div class="col-lg-3"></div>

<div class="col-lg-2">
  <label>From Date</label>
  <input type="text" name="from_date" id="from_date" class="form-control">
</div>

<div class="col-lg-2">
  <label>To Date</label>
  <input type="text" name="to_date" id="to_date" class="form-control">
</div>

<div class="col-lg-2" style="margin-left: 0px !important;padding-left: 0px !important;">
  <label><br/><br/></label>
  <input type="button" name="search" id="search" value="Search" class="btn btn-primary" style="margin-top:11px;">&nbsp;
  <input type="button" name="resetbtn" id="resetbtn" value="Reset" class="btn btn-danger" style="margin-top:11px;">
</div>

</div>
<br/><br/><br/><br/>

<table id="example1" class="<?php echo TABLE_LISTING_CLASS;?>" width="100%">
<thead>
<tr>
<th style="<?php echo COL_70;?>"><?php echo PATIENT_ID_VISIT;?><input type="text" name="qs_visit" id="qs_visit" class="form-control"></th> 
<th style="<?php echo COL_70;?>">Reg. No.<input type="text" name="qs_regno" id="qs_regno" class="form-control"></th>
<th style="<?php echo COL_150;?>">Patient Name<input type="text" name="qs_name" id="qs_name" class="form-control"></th>
<th style="<?php echo COL_300;?>">Address<input type="text" name="qs_address" id="qs_address" class="form-control"></th>
<th style="<?php echo COL_150;?>">Department<input type="text" name="qs_department" id="qs_department" class="form-control"></th>
<th style="<?php echo COL_150;?>">Admit Date<input type="text" name="qs_admdate" id="qs_admdate" class="form-control"></th>
<th style="<?php echo COL_150;?>">Discharged Date<input type="text" name="qs_disdate" id="qs_disdate" class="form-control"></th>
<th style="<?php echo COL_200;?>">Discharged Status<input type="text" name="qs_disstatus" id="qs_disstatus" class="form-control"></th>
<th style="<?php echo COL_100;?>">Action</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;
 
$(document).ready(function() {
 
    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 1, "desc" ], [ 0, "desc" ]],   
    "aoColumns": [    
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true },    
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"billing/discharged_listing",
            "type": "POST",
            "data": function ( data ) {
                data.filterby = $('#filterby').val();
                data.qs_from_date = $('#from_date').val();
                data.qs_to_date = $('#to_date').val();

                
                data.qs_visit = $('#qs_visit').val();
                data.qs_regno = $('#qs_regno').val();
                data.qs_name = $('#qs_name').val();                
                data.qs_address = $('#qs_address').val();                
                data.qs_department = $('#qs_department').val();
                data.qs_admdate = $('#qs_admdate').val();                
                
                data.qs_disdate = $('#qs_disdate').val();
                data.qs_disstatus = $('#qs_disstatus').val();                               
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
    $('#search').click(function()
    { 
        table.ajax.reload(null,false);  //just reload table
    });

    $('#qs_visit, #qs_regno, #qs_name, #qs_address, #qs_department, #qs_admdate, #qs_pcat, #qs_roomno').keyup(function()
    { 
        table.ajax.reload(null,false);  //just reload table
    });


    $('#resetbtn').click(function()
    { 
        $('#qs_visit, #qs_regno, #qs_name, #qs_address, #qs_department, #qs_admdate, #qs_pcat, #qs_roomno').val('');

        $('#from_date, #to_date').val('');

        table.ajax.reload(null,false);  //just reload table
    });


    $('#from_date').datetimepicker({
      format:'d-m-Y',
      formatDate:'d-m-Y',
      onShow:function( ct )
      {
          this.setOptions({
            maxDate:$('#to_date').val()?$('#to_date').val():false
          })
      },
      timepicker:false
    });

    $('#to_date').datetimepicker({
      format:'d-m-Y',
      formatDate:'d-m-Y',
      onShow:function( ct )
      {
          this.setOptions({
            minDate:$('#from_date').val()?$('#from_date').val():false
          })
      },
      timepicker:false
    });
    
 
});
</script>