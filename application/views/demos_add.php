<section class="content-header">
<h1>
Add New Demo
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add';" data-toggle="tab" aria-expanded="false">Add Demo Stage 1</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add2';" data-toggle="tab" aria-expanded="false">Add Demo Stage 2</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add3';" data-toggle="tab" aria-expanded="false">Add Demo Stage 3</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add4';" data-toggle="tab" aria-expanded="false">Add Demo Stage 4</a></li>

</ul>


<div id="message_box"></div>


<fieldset >
<form class="form-horizontal" name="process_form1" id="process_form1" method="post" style="margin:0px !important;">
<table class="table table-stripped" width="100%">    

<tr>
    <td><?php echo MANDATORY;?>Registration Number: <input type="text" name="farmer_reg_num" class="form-control" value="<?php echo $farmer_reg_num;?>" readonly></td>    

    <td><?php echo MANDATORY;?>Registration Date: <input type="text" name="visit_date" id="visit_date" class="form-control date_sel" value="<?php echo date("d-m-Y");?>"></td>
</tr> 

<tr>
    <td colspan="2"><?php echo MANDATORY;?>Select Farmer: 
      <select name="farmer_id" id="farmer_id" onchange="load_farmer_details(this.value);" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php
          foreach($farmers as $obj)
          {
          ?>    
            <option value="<?php echo $obj->farmer_id;?>"><?php echo $obj->farmer_name;?></option>
          <?php
          }
        ?>       
      </select>  
    </td>    
</tr>       

<tr><td colspan="3"><b>Farmer Information</b></td></tr>

<tr>
    <td><?php echo MANDATORY;?>Farmer Name: <input type="text" name="farmer_name" id="farmer_name" class="form-control"></td>    
    
    <td><?php echo MANDATORY;?>Mobile: <input type="text" class="form-control" name="farmer_mobile" id="farmer_mobile" class="form-control"></td>

    <td>Email: <input type="email" name="farmer_email" id="farmer_email" class="form-control"></td>

    
</tr>

<tr>
    <td><?php echo MANDATORY;?>State: <select name="farmer_state" id="farmer_state" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'farmer_district');" style="width: 100%;">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select></td>    

    <td><?php echo MANDATORY;?>District: <select name="farmer_district" id="farmer_district" class="form-control select2" onchange="get_dd_list(this.value, 'taluka_by_district', 'farmer_taluka');" style="width: 100%;"></select></td>
    
    <td><?php echo MANDATORY;?>Taluka: <select name="farmer_taluka" id="farmer_taluka" class="form-control select2" style="width: 100%;"></select></td>
</tr>
  

<tr>
    <td>Village: <input type="text" name="farmer_village" id="farmer_village" class="form-control"></td>

    <td><?php echo MANDATORY;?>Total Acrage: <input type="text" name="total_acrage" id="total_acrage" class="form-control"></td>
    <!-- <td>By Executive: <select name="fby_executive" id="fby_executive" class="form-control select2"></select></td> -->
</tr>


<tr>
    <td>Crop 1: 
      <select name="crop_1" id="crop_1" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], 0);?>
      </select>
    </td>
    
    <td>Crop 2:
      <select name="crop_2" id="crop_2" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], 0);?>
      </select>
    </td>
</tr>


<tr>
    <td>Soil Test: <select name="soil_test" class="form-control select2" style="width: 100%;"><option value="yes">Yes</option><option value="no">No</option></select></td>
    
    <td>Irrigated: <select name="irrigated" class="form-control select2" style="width: 100%;"><option value="yes">Yes</option><option value="no">No</option></select></td>
</tr>
</table>

<table class="table table-stripped table-hover" width="100%">    
<tr>
    <td>Crop Selected: 
      <select name="demo_crop_id" class="form-control select2" style="width: 100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], 0);?>
      </select>

    </td>

    <td>Variety: 
      <select name="demo_crop_variety_id" class="form-control select2" style="width: 100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crop_variety'] , 0);?>
      </select>
    </td>
</tr>            

<tr>
    <td>Acrage for Demo: <input type="text" class="form-control" name="acrage_for_demo"></td>

    <td>Reason: <input type="text" class="form-control" name="demo_crop_reason"></td>
</tr>


<tr>
  <td colspan="2"><b>Last Year Fertiliser Application for Same Crop:</b><br/>
    <table width="100%" class="table table-bordered">
    <tr>      
      <td>Product</td>
      <td>Qty.</td>
    </tr>
    <?php
    for($i=1; $i<=5;$i++)
    {
    ?>
      <tr>        
        <td>
          <select type="text" name="crop_last_year_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
            <option value="">Select</option>
            <?php 
            foreach($masters['products'] as $obj)
            {
                $pt = "OTHER";
                if($obj-> product_category_id == 1) $pt = "RMPCL";
                echo "<option value='".$obj-> id."'>".$obj-> name." (".$pt.")</option>";
            }
            ?>
          </select>  
        </td>
        

        <td>
          <input type="text" name="crop_last_year_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
        </td>
      </tr>
    <?php
    }
    ?>

    </table>  
  </td>  
</tr>


<tr>
  <td colspan="2"><b>Plan for This Year Fertiliser Application:</b><br/>
    <table width="100%" class="table table-bordered">
    <tr>      
      <td>Product</td>
      <td>Qty.</td>
    </tr>
    <?php
    for($i=1; $i<=5;$i++)
    {
    ?>
      <tr>        
        <td>
          <select type="text" name="crop_current_year_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
            <option value="">Select</option>
            <?php 
            foreach($masters['products'] as $obj)
            {
                $pt = "OTHER";
                if($obj-> product_category_id == 1) $pt = "RMPCL";
                echo "<option value='".$obj-> id."'>".$obj-> name." (".$pt.")</option>";
            }
            ?>
          </select>  
        </td>
        

        <td>
          <input type="text" name="crop_current_year_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
        </td>
      </tr>
    <?php
    }
    ?>

    </table>
  </td>
</tr>


<tr>
  <td colspan="2"><b>Fertiliser Applied by RMPCL in Demo:</b><br/>
    <table width="100%" class="table table-bordered">
    <tr>      
      <td>Product</td>
      <td>Qty.</td>
    </tr>
    <?php
    for($i=1; $i<=5;$i++)
    {
    ?>
      <tr>        
        <td>
          <select type="text" name="demos_product_<?php echo $i;?>" class="form-control select2" maxlength="100" style="width: 100%;">
            <option value="">Select</option>
            <?php 
            foreach($masters['products'] as $obj)
            {                
                if($obj-> product_category_id == 1)
                {
                  echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                }  
            }
            ?>
          </select>  
        </td>
        

        <td>
          <input type="text" name="demos_product_qty_<?php echo $i;?>" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
        </td>
      </tr>
    <?php
    }
    ?>
    </table>
  </td>
</tr>


<tr>
  <td>Last Soil Test Date: <input type="text" name="farmer_last_soil_test_date" class="form-control date_sel"></td>

  <td>Last Water Test Date: <input type="text" name="farmer_last_water_test_date" class="form-control date_sel"></td>
</tr>


<tr>
  <td><?php echo MANDATORY;?>Demo Start Date: <input type="text" name="demo_start_date" class="form-control date_sel"></td>

  <td><?php echo MANDATORY;?>Stage 2 Visit Date: <input type="text" name="next_stage_date" class="form-control date_sel"></td>
</tr>

<tr>
  <td colspan="2">    
    <button type="button" name="btn_save" id="btn_save" class="btn btn-primary btn_process" onclick="form_submit(1);">Save Stage 1</button>&nbsp;
    
    <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" class="btn btn-default btn_process">Cancel</button>
    
    <input name="hdn_id" value="0" type="hidden">        
</td>
</tr>
</table>
</form>

</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(function()
{
    $(".select2").select2();

    $(".date_sel").datepicker({maxDate:0, dateFormat:"dd-mm-yy"});
});



function form_submit(stage_num)
{
    processing_bar();

    var formData = new FormData($("#process_form"+stage_num)[0]);

    $.ajax({url : base_url+"demos/demo_stage_"+stage_num,
      method: "POST",
      data: formData,
      async: false,
      dataType: 'json',
      success: function(res)
      {   
          if(res.status == 1)
          {
              msg = msg_ok + res.message + '</div>';

              setTimeout(function()
              {                    
                window.location.href = base_url+'admin/demos'; 
                
              }, time_out);
          }
          else
          {
              msg = msg_error + res.message + '</div>';

              hide_msg_box();
          }
          
          show_msg_box(msg);
      },
      cache: false,
      contentType: false,
      processData: false
    });

    return false;
    
}



function load_farmer_details(farmer_id)
{
    $.ajax({url : base_url+"demos/load_farmer_details",
      method: "POST",
      data: {"farmer_id":farmer_id},
      async: false,
      dataType: 'json',
      success: function(res)
      {   
          if(res.status == 1)
          {
              $("#farmer_name").val(res.data.basic.farmer_name);
              $("#farmer_mobile").val(res.data.basic.farmer_mobile);
              $("#farmer_email").val(res.data.basic.farmer_email);

              $("#farmer_state").val(res.data.basic.farmer_state);              
              $("#farmer_village").val(res.data.basic.farmer_village);
              $("#total_acrage").val(res.data.basic.farmer_land_area);
              
              $("#crop_1").val(res.data.crop_last_year[0].last_year_crop_id);
              $("#crop_2").val(res.data.crop_last_year[1].last_year_crop_id);   

              get_dd_list(res.data.basic.farmer_state, 'district_by_state_aop_dd', 'farmer_district');
              $("#farmer_district").val(res.data.basic.farmer_district);

              

              setTimeout(function()
              {
                $("#farmer_district").val(res.data.basic.farmer_district);

                get_dd_list(res.data.basic.farmer_district, 'taluka_by_district', 'farmer_taluka');
                $("#farmer_taluka").val(res.data.basic.farmer_taluka);

                $("#farmer_state").select2().trigger("chosen:updated");
                $("#farmer_district").select2().trigger("chosen:updated");
                $("#farmer_taluka").select2().trigger("chosen:updated");

              }, "2000");            
          }
          else
          {
              alert("Farmer details not found in the system.");
          }
      }
    });
}

</script>





