<section class="content-header">
<h1>
Restore Backup
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<br/>
<div class="alert alert-info">
  Restore the backup file. Once the backup is restored, all current data will be erased and system will set to restored date. 
</div>
<br/>

<div id="message_box"></div>

<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_200;?>">Backup File Name</th>
<th style="<?php echo COL_150;?>">File Size</th>
<th style="<?php echo COL_150;?>">Actions</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>
 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;
 
$(document).ready(function() {
 
    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "asc" ]],       
    "aoColumns": [        
    { "bSortable": false },            
    { "bSortable": false },            
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"backups/backups_list",
            "type": "POST",
            "data": function ( data ) {
                               
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ],
 
    });
 
});


function restore_backup(backup_id)
{
    if(backup_id <= 0 || backup_id == "") return;

    if(confirm("Are you sure want to restore backup? This process will erase current data and restore selected data. Do you want to proceed ?"))
    {
        processing_bar();        

        $.ajax({url : base_url+"backups/restore_backup",
          method: "POST",
          data: {"backup_id": backup_id},
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'backups';
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);              
          }
        });

        return false;
    }
}
</script>

