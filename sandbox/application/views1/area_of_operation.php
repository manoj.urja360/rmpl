<section class="content-header">
<h1>
Area Of Operation
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<!-- <div class="box-header with-border">
<i class="fa fa-warning"></i>
<h3 class="box-title">Listing</h3>
</div>
 -->

<div class="box-body">

<ul class="nav nav-tabs">
<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/area_of_operation';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/area_of_operation_set';" data-toggle="tab" aria-expanded="false">Set Area Of Operation</a></li>
</ul>

<fieldset style="overflow: auto;">

<div id="message_box"></div>

<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_150;?>">State Name</th>
<th style="<?php echo COL_300;?>">District Name</th>              
<th style="<?php echo COL_50;?>">Actions</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;
 
$(document).ready(function() {
 
    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "asc" ]],       
    "aoColumns": [        
    { "bSortable": true },    
    { "bSortable": true },
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/area_of_operation_list",
            "type": "POST",
            "data": function ( data ) {
                //data.qs_name = $('#qs_name').val();                
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ],
 
    });
 
    $('#search').click(function()
    { 
        table.ajax.reload(null,false);  //just reload table
    });

    $('#qs_visit, #qs_regno, #qs_name, #qs_address, #qs_department, #qs_admdate, #qs_pcat, #qs_roomno').keyup(function()
    { 
        table.ajax.reload(null,false);  //just reload table
    });


    $('#resetbtn').click(function()
    { 
        $('#qs_visit, #qs_regno, #qs_name, #qs_address, #qs_department, #qs_admdate, #qs_pcat, #qs_roomno').val('');

        $('#from_date, #to_date').val('');

        table.ajax.reload(null,false);  //just reload table
    });


    /*$('#from_date').datetimepicker({
      format:'d-m-Y',
      formatDate:'d-m-Y',
      onShow:function( ct )
      {
          this.setOptions({
            maxDate:$('#to_date').val()?$('#to_date').val():false
          })
      },
      timepicker:false
    });

    $('#to_date').datetimepicker({
      format:'d-m-Y',
      formatDate:'d-m-Y',
      onShow:function( ct )
      {
          this.setOptions({
            minDate:$('#from_date').val()?$('#from_date').val():false
          })
      },
      timepicker:false
    });*/
    
 
});
</script>