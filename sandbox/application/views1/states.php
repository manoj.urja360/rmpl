<section class="content-header">
<h1>
States List
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<!-- <div class="box-header with-border">
<i class="fa fa-warning"></i>
<h3 class="box-title">Listing</h3>
</div>
 -->

<div class="box-body">

<!-- <ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>billing/ipd_patients';" data-toggle="tab" aria-expanded="false"><img src="<?php echo base_url();?>theme/images/list_tab_16.png">&nbsp;Admitted Patients</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>billing/discharged';" data-toggle="tab" aria-expanded="false"><img src="<?php echo base_url();?>theme/images/list_tab_16.png">&nbsp;Discharged Patients</a></li>
</ul> -->

<fieldset style="overflow: auto;">
<!-- <legend>Patient List</legend> -->
<!-- <input type="hidden" name="filterby" id="filterby" value="all">
<div class="form-group">
<div class="col-lg-3"></div>

<div class="col-lg-2">
  <label>From Date</label>
  <input type="text" name="from_date" id="from_date" class="form-control">
</div>

<div class="col-lg-2">
  <label>To Date</label>
  <input type="text" name="to_date" id="to_date" class="form-control">
</div>

<div class="col-lg-2" style="margin-left: 0px !important;padding-left: 0px !important;">
  <label><br/><br/></label>
  <input type="button" name="search" id="search" value="Search" class="btn btn-primary" style="margin-top:11px;">&nbsp;
  <input type="button" name="resetbtn" id="resetbtn" value="Reset" class="btn btn-danger" style="margin-top:11px;">
</div>

</div>
<br/><br/><br/><br/> -->

<table id="example1" class="<?php echo TABLE_LISTING_CLASS;?> listing_table" width="98%">
<thead>
<tr>
<th style="<?php echo COL_50;?>">S.No.</th>
<th style="<?php echo COL_50;?>">State Name</th>
<th style="<?php echo COL_50;?>">Action</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;
 
$(document).ready(function() {
 
    //datatables
    table = $('.listing_table').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "asc" ], [ 1, "asc" ]],   
    "aoColumns": [    
    { "bSortable": true },
    { "bSortable": true },    
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/states_list",
            "type": "POST",
            "data": function ( data ) {
                //data.qs_name = $('#qs_name').val();                
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
    $('#search').click(function()
    { 
        table.ajax.reload(null,false);  //just reload table
    });

    $('#qs_visit, #qs_regno, #qs_name, #qs_address, #qs_department, #qs_admdate, #qs_pcat, #qs_roomno').keyup(function()
    { 
        table.ajax.reload(null,false);  //just reload table
    });


    $('#resetbtn').click(function()
    { 
        $('#qs_visit, #qs_regno, #qs_name, #qs_address, #qs_department, #qs_admdate, #qs_pcat, #qs_roomno').val('');

        $('#from_date, #to_date').val('');

        table.ajax.reload(null,false);  //just reload table
    });


    $('#from_date').datetimepicker({
      format:'d-m-Y',
      formatDate:'d-m-Y',
      onShow:function( ct )
      {
          this.setOptions({
            maxDate:$('#to_date').val()?$('#to_date').val():false
          })
      },
      timepicker:false
    });

    $('#to_date').datetimepicker({
      format:'d-m-Y',
      formatDate:'d-m-Y',
      onShow:function( ct )
      {
          this.setOptions({
            minDate:$('#from_date').val()?$('#from_date').val():false
          })
      },
      timepicker:false
    });
    
 
});
</script>