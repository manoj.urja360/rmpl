<section class="content-header">
<h1>
Manage Email Templates
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">


<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template_add_w';" data-toggle="tab" aria-expanded="false">Acknowledgement 1</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template_add_r1';" data-toggle="tab" aria-expanded="false">Acknowledgement 2</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template_add_r';" data-toggle="tab" aria-expanded="false">Stock Movement</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template_signature';" data-toggle="tab" aria-expanded="false">Set Signature</a></li>
</ul>


<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
<div style="color: #555;">Note: 
<ol>
<li>You can use system variables {WHOLESALER_NAME}, {NUMBER_X} in your message.</li>

<li>A common generic email can be send without use of the variables mentioned above.</li>
</ol>
</div>
</div>



<div class="row">
<div class="col-lg-12">
<div class="row" style="background-color: #f5f5f5;padding: 10px; margin-bottom: 2%;">
 
  <div class="form-group">
    
    <div class="col-sm-12">
      
      <label for="content"><?php echo MANDATORY;?>Email Content</label>
      
      <textarea class="form-control textarea" name="content" id="content" rows="7">Dear WHOLESALER_NAME, Acknowledgement due over NUMBER_X days.</textarea>
      
      <span id="content_lbl"></span>

    </div>

  </div>
  


  <div class="form-group">  
    <div class="col-sm-12">
      <?php echo MANDATORY;?>Send Email when Acknowledgement is due for more than <input type="text" name="days" value="5" onkeyup="chk_numeric(this);" maxlength="3"> days.
    </div>  
  </div>

  <div class="form-group">  
    <label class="col-sm-12">Send copy of email to:</label>
    <div class="col-sm-3">
       <input type="checkbox" name="email_to[]" value="Executive">&nbsp;Executive Assigned
    </div>

    <div class="col-sm-2">
       <input type="checkbox" name="email_to[]" value="State Head">&nbsp;State Head
    </div>

    <div class="col-sm-2">
       <input type="checkbox" name="email_to[]" value="Management">&nbsp;Management
    </div>
  </div>


  <div class="form-group">  
    <div class="col-sm-12"><br/>
      <label>Select Signature</label>
      <select name="email_sign" id="email_sign" class="form-control select2" style="width: 100%;">
        <option value="">Select</option>
        <?php
          foreach($email_signatures as $obj)
          {
          ?>    
            <option value="<?php echo $obj->email_signature_id;?>"><?php echo $obj->signame;?></option>
          <?php
          }
        ?>
       </select>
    </div>  
  </div>

</div>
</div>
</div>


<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template';" class="btn btn-default btn_process">Cancel</button>
      <input name="email_for" value="1" type="hidden">
      <input name="template_id" value="0" type="hidden">
    </div>
  </div> 
</div>
</form>

</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(document).ready(function()
{
    $(".select2").select2();

    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/email_template_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/email_template'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>

<style type="text/css">
.wysihtml5-toolbar .btn
{
  min-width: 20px !important;
}  

</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
$(function () 
{
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5();
});
</script>