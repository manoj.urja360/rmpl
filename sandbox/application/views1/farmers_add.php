<section class="content-header">
<h1>
Add New Farmer
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers_quick';" data-toggle="tab" aria-expanded="false">Quick Registered Farmers</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers_import';" data-toggle="tab" aria-expanded="false">Import</a></li>
</ul>


<div id="message_box"></div>

<fieldset >
<form class="form-horizontal" name="process_form" id="process_form" method="post" style="margin:0px !important;">

<table class="table table-stripped" width="100%" border="0">    
<tr>
    <td><?php echo MANDATORY;?>Registration Number: <input type="text" name="farmer_reg_num" class="form-control" value="<?php echo $farmer_reg_num;?>" readonly></td>    

    <td><?php echo MANDATORY;?>Registration Date: <input type="text" name="visit_date" id="visit_date" class="form-control" value="<?php echo date("d-m-Y");?>"></td>
</tr>        


<tr><td colspan="3"><b>Farmer Information</b></td></tr>

<tr>
   <td colspan="3">Farmer Photo: <input type="file" name="farmer_photo" class="form-control"></td> 
</tr>
<tr>
    <td><?php echo MANDATORY;?>Farmer Name: <input type="text" name="farmer_name" id="farmer_name" class="form-control"></td>    
    
    <td><?php echo MANDATORY;?>Mobile: <input type="text" class="form-control" name="farmer_mobile" id="farmer_mobile" class="form-control" maxlength="10" onkeyup="chk_numeric(this);"></td>

    <td>Email: <input type="email" name="farmer_email" class="form-control"></td>

</tr>

<tr>
    <td><?php echo MANDATORY;?>State: <select name="farmer_state" id="farmer_state" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'farmer_district');" style="width:100%;">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select></td>

    <td><?php echo MANDATORY;?>District: <select name="farmer_district" id="farmer_district" class="form-control select2" onchange="get_dd_list(this.value, 'taluka_by_district', 'farmer_taluka'); get_dd_list(this.value, 'retailer_by_district', 'ziron_bought_from');" style="width:100%;"></select></td>
    
    <td><?php echo MANDATORY;?>Taluka: <select name="farmer_taluka" id="farmer_taluka" class="form-control select2" style="width:100%;"></select></td>
</tr>
  


<tr>
    <td>Village: <input type="text" name="farmer_village" id="farmer_village" class="form-control"></td>

    <td colspan="2">Land Plot Details: <input type="text" name="farmer_plot_no" class="form-control"></td>    
</tr>

<tr>
    <td><?php echo MANDATORY;?>Total Acrage: <input type="text" id="farmer_land_area" name="farmer_land_area" class="form-control" value=""></td>

    <td>Plot Area for consultancy: <input type="text" name="farmer_plot_area_consultancy" class="form-control" value=""></td>

</tr>


<tr>
    <td colspan="3">Willing for Demo: 
      <input type="radio" name="farmer_willing_demo" value="yes">&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="farmer_willing_demo" value="no" checked="">&nbsp;No
    </td>
</tr>

<tr>
    <td><b>Crop details last season:</b><br/>   

    Crop 1: 
      <select name="crop_last_year[]" id="crop_last_year_1" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], 0);?>
      </select>
    </td>
    
    <td><br/>Crop 2:
      <select name="crop_last_year[]" id="crop_last_year_2" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], 0);?>
      </select>
    </td>
</tr>


<tr>
    <td colspan="3">If some other kharif crop: <input type="text" name="kharif_crop" class="form-control"></td>
</tr>


<tr>
    <td><b>Ziron used ?</b><br/>
      <select name="is_ziron_used" class="form-control select2">
        <option value="yes">Yes</option>        
        <option value="no">No</option>        
      </select>
    </td>    

    <td>Name of retailer bought from: 
      <select name="ziron_bought_from" id="ziron_bought_from" class="form-control select2">
      </select>  
    </td>
    
    <td>Feedback of ziron used: <input type="text" name="ziron_feedback" class="form-control">
    </td>
</tr>


<tr>
    <td><b>Next season crop plan:</b><br/>    

    Crop 1: 
      <select name="crop_next_year[]" id="crop_next_year_1" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], 0);?>
      </select>
    </td>
    
    <td><br/>Crop 2:
      <select name="crop_next_year[]" id="crop_next_year_2" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], 0);?>
      </select>
    </td>
</tr>


<tr>
   <td colspan="3">Plot Photos: <input type="file" name="plot_photos[]" class="form-control" multiple=""></td> 
</tr>


<tr>
<td colspan="3"><b>Reference name & numbers of farmers (max. 5)</b><br/>    
  <table width="100%" class="table table-bordered">
    <tr>
      <td>S.No.</td>
      <td>Name</td>
      <td>Mobile</td>
    </tr>

    <?php
    for($i=1; $i<=5;$i++)
    {
    ?>
      <tr>
        <td><?php echo $i;?></td>
        <td><input type="text" name="farmer_refrence_name[]" class="form-control" maxlength="100"></td>
        <td><input type="text" name="farmer_refrence_mobile[]" class="form-control" maxlength="10"></td>
      </tr>
    <?php
    }
    ?>  
  </table>
</td>
</tr>


<tr>
  <td colspan="3">
    
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="0" type="hidden">      
      <input name="hdn_quick_id" id="hdn_quick_id" value="0" type="hidden">
    
</td>
</tr>  
</table>


 

</form>
</fieldset> 




</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(function()
{
    $(".select2").select2();

    $("#visit_date").datepicker({maxDate:0, dateFormat:"dd-mm-yy"});
});


$(document).ready(function()
{
    <?php
    if(isset($details) && !empty($details))
    {
    ?>
        $("#hdn_quick_id").val("<?php echo $details-> farmer_id;?>");

        $("#farmer_name").val("<?php echo $details-> farmer_name;?>");
        $("#farmer_mobile").val("<?php echo $details-> farmer_mobile;?>");
        $("#farmer_state").val("<?php echo $details-> farmer_state;?>");
        
        get_dd_list("<?php echo $details-> farmer_state;?>", 'district_by_state_aop_dd', 'farmer_district');

        get_dd_list("<?php echo $details-> farmer_district;?>", 'taluka_by_district', 'farmer_taluka');

        setTimeout(function()
        {
          $("#farmer_district").val("<?php echo $details-> farmer_district;?>");
          $("#farmer_taluka").val("<?php echo $details-> farmer_taluka;?>");
          $("#farmer_village").val("<?php echo $details-> farmer_village;?>");
          $("#farmer_land_area").val("<?php echo $details-> farmer_land_area;?>");
          
          $("#farmer_state").select2().trigger("chosen:updated");
          $("#farmer_district").select2().trigger("chosen:updated");
          $("#farmer_taluka").select2().trigger("chosen:updated");

          $("#farmer_mobile").attr("readonly", "true");        
      }, "500");
    <?php
    }
    ?>  

    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/farmers_add_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/farmers'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>