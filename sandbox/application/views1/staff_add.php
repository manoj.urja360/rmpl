<section class="content-header">
<h1>
Add New Staff
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/staff';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/staff_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>
</ul>

<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">

<div class="row">
  <div class="form-group">        
    <div class="col-sm-3">
      <input name="keycode" value="<?php echo $keycode;?>" type="hidden">
      <label for="first_name"><?php echo MANDATORY;?>First Name</label>
      <input class="form-control" name="first_name" value="" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="last_name"><?php echo MANDATORY;?>Last Name</label>
      <input class="form-control" name="last_name" value="" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="email"><?php echo MANDATORY;?>Email</label>
      <input class="form-control" name="email" value="" type="email" maxlength="150">
    </div>

    <div class="col-sm-3">
      <label for="mobile"><?php echo MANDATORY;?>Mobile</label>
      <input class="form-control" name="mobile" value="" type="text" maxlength="10">
    </div>
  </div>
</div>



<div class="row">
  <div class="form-group">        
    <div class="col-sm-3">
      <label for="role_name"><?php echo MANDATORY;?>Select Role</label>
      <select class="form-control select2" name="role_name">
          <option value="">Select Role</option>
          <?php
          foreach($roles as $obj)
          {
          ?>
            <option value="<?php echo $obj->role_id;?>"><?php echo $obj->role_name;?></option>
          <?php  
          }
          ?>
      </select>  
    </div>


    <div class="col-sm-3">
      <label for="admin_access"><?php echo MANDATORY;?>Allow Admin Access</label>
      <select class="form-control select2" name="admin_access">          
          <option value="yes">Yes</option>
          <option value="no">No</option>
      </select>
    </div>


    <div class="col-sm-3">
      <label for="app_access"><?php echo MANDATORY;?>Allow App Access</label>
      <select class="form-control select2" name="app_access">          
          <option value="yes">Yes</option>
          <option value="no">No</option>
      </select>
    </div>

        
  </div>
</div> 

<hr/>

<div class="row">
<div class="">
<div class="nav-tabs-custom">
<ul class="nav nav-tabs">
<li class="active"><a href="#tab_4" data-toggle="tab"><?php echo MANDATORY;?>Area Of Operation</a></li>        
<li><a href="#tab_1" data-toggle="tab">Address Details</a></li>
<li><a href="#tab_2" data-toggle="tab">Login Details</a></li>
<li><a href="#tab_3" data-toggle="tab">Other Details</a></li>        
</ul>


<div class="tab-content">
<div class="tab-pane" id="tab_1">
<p>
<div class="row"><h5>Current Address</h5>
  <div class="form-group">    
    <div class="col-sm-6">
      <label for="cstates_name">Select State Name</label>
      <select name="cstates_name" id="cstates_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'ccity_name');" style="width: 100%;">
        <option value="">Select</option>
        <?php
          foreach($states_all as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-6">
      <label for="ccity_name">Select City Name</label>
      <select name="ccity_name" id="ccity_name" class="form-control select2" style="width: 100%;"></select>
    </div>
  </div>
  
  <div class="form-group">   
    <div class="col-sm-6">
      <label for="caddress">Address 1</label>
      <input class="form-control" name="caddress" id="caddress" type="text" maxlength="255">
    </div> 

    <div class="col-sm-6">
      <label for="caddress2">Address 2</label>
      <input class="form-control" name="caddress2" id="caddress2" type="text" maxlength="255">
    </div> 
    
  </div>
</div>


<div class="row"><h5><input type="checkbox" value="1" onclick="set_address();">&nbsp;Permanent Address</h5>
  <div class="form-group">    
    <div class="col-sm-6">
      <label for="pstates_name">Select State Name</label>
      <select name="pstates_name" id="pstates_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'pcity_name');" style="width: 100%;">
        <option value="">Select</option>
        <?php
          foreach($states_all as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-6">
      <label for="pcity_name">Select City Name</label>
      <select name="pcity_name" id="pcity_name" class="form-control select2" style="width: 100%;"></select>
    </div>
  </div>
  
  <div class="form-group">  
    <div class="col-sm-6">
      <label for="caddress">Address 1</label>
      <input class="form-control" name="paddress" id="paddress" type="text" maxlength="255">
    </div>

    <div class="col-sm-6">
      <label for="paddress2">Address 2</label>
      <input class="form-control" name="paddress2" id="paddress2" type="text" maxlength="255">
    </div>  
    
  </div>
</div>
</p>      
</div><!--tab_1--> 



<div class="tab-pane" id="tab_2">
<p>
<div class="row"><h4>Admin Panel Login Details</h4>
  <div class="form-group">  
    

    <div class="col-sm-3">
      <label for="username">Username</label>
      <input class="form-control" name="username" value="" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="password">Password</label>
      <input class="form-control" name="password" value="" type="password" maxlength="255">
    </div>

    <div class="col-sm-3">
      <label for="cpassword">Confirm Password</label>
      <input class="form-control" name="cpassword" value="" type="password" maxlength="255">
    </div>

  </div>
</div>



<div class="row"><h4>Mobile App Login Details</h4>
  <div class="form-group">  
    

    <div class="col-sm-3">
      <label for="keycode1">Key Code</label>
      <input class="form-control" name="keycode1" value="<?php echo $keycode;?>" type="text" maxlength="10" readonly>
    </div>
  </div>
</div>  
</p>
</div><!--tab_2--> 



<div class="tab-pane" id="tab_3">
<p>
<div class="row">
<div class="form-group">
<div class="col-sm-3">
  <label for="gender">Gender</label>
  <select class="form-control select2" name="gender" style="width: 100%;">
      <option value="">Select Gender</option>
      <option value="male">Male</option>
      <option value="female">Female</option>
      <option value="other">Other</option>
  </select>
</div>

<div class="col-sm-3">
  <label for="dob">Date Of Birth</label>
  <input class="form-control" name="dob" id="dob" type="text" maxlength="50">
</div>
</div>  
</div>

<div class="row">
  <div class="form-group">
    <div class="col-sm-3">
      <label for="qualification">Highest Qualification</label>
      <input class="form-control" name="qualification" type="text" maxlength="255">
    </div>

    <div class="col-sm-3">
      <label for="experience">Total Experience</label>
      <input class="form-control" name="experience" type="text" maxlength="100">
    </div>

    <div class="col-sm-3">
      <label for="dob">Date Of Joining</label>
      <input class="form-control" name="doj" id="doj" type="text" maxlength="50">
    </div>
  </div>
</div>
</p>
</div><!--tab_3--> 


<div class="tab-pane active" id="tab_4">
<p>
<div class="row">
  <div class="form-group">        
    <div class="col-sm-12">
      <label for="states_name"><?php echo MANDATORY;?>Select State Name</label><br/>
      <select name="states_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop', 'dd_list');">
        <option value="">Select State Name</option>
        <?php
          foreach($states_all as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>    
  </div>
</div>

<h4>Selected States and Districts:</h4>
<div id="dd_list"></div> 

   
</p>
</div><!--tab_4--> 

</div>  
</div>
</div>
</div>


<div class="row btn_row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/staff';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="0" type="hidden">
    </div>
  </div> 
</div>

</form>
</div>
</div>  
</div>
</div>
</section>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.js"></script>
<script type="text/javascript">

function set_address()
{        
    $("#paddress").val($("#caddress").val());
    $("#paddress2").val($("#caddress2").val());

    $("#pstates_name").val($("#cstates_name").val());
    $("#pstates_name").trigger("change");

    setTimeout(function()
    {      
      $("#pcity_name").val($("#ccity_name").val());
      $("#pcity_name").select2().trigger("chosen:updated");
    }, "1000");
}

$(document).ready(function()
{
    $("#dob, #doj").datepicker({changeMonth:true, 
      changeYear:true, 
      maxDate:0, 
      dateFormat:'dd-mm-yy'
    });

    $(".select2").select2();

    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/staff_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/staff'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>