<section class="content-header">
<h1>
Edit Demo
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">

<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>
</ul>


<fieldset >
<form class="form-horizontal" name="process_form" id="process_form" method="post" style="margin:0px !important;">


<table class="table table-stripped" width="100%">    

<tr>
    <td><?php echo MANDATORY;?>Registration Number: <input type="text" name="farmer_reg_num" class="form-control" value="<?php echo "D".regno($details['basic']-> farmer_reg_num, 5);?>" readonly></td>    

    <td><?php echo MANDATORY;?>Registration Date: <input type="text" name="visit_date" id="visit_date" class="form-control date_sel" value="<?php echo format_date($details['basic']-> visit_date, "d-m-Y");?>" readonly></td>
</tr> 

<tr>
    <td colspan="2"><?php echo MANDATORY;?>Select Farmer: 
      <select name="farmer_id" id="farmer_id" class="form-control select2" style="width:100%;">

      </select>  
    </td>    
</tr>       

<tr><td colspan="3"><b>Farmer Information</b></td></tr>

<tr>
    <td><?php echo MANDATORY;?>Farmer Name: <input type="text" name="farmer_name" class="form-control" value="<?php echo $details['basic']-> farmer_name;?>"></td>    
    
    <td><?php echo MANDATORY;?>Mobile: <input type="text" class="form-control" name="farmer_mobile" class="form-control" value="<?php echo $details['basic']-> farmer_mobile;?>"></td>

    <td>Email: <input type="email" name="farmer_email" value="<?php echo $details['basic']-> farmer_email;?>" class="form-control"></td>

    
</tr>

<tr>
    <td><?php echo MANDATORY;?>State: <select name="farmer_state" id="farmer_state" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'farmer_district');" style="width: 100%;">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select></td>    

    <td><?php echo MANDATORY;?>District: <select name="farmer_district" id="farmer_district" class="form-control select2" onchange="get_dd_list(this.value, 'taluka_by_district', 'farmer_taluka');" style="width: 100%;"></select></td>
    
    <td><?php echo MANDATORY;?>Taluka: <select name="farmer_taluka" id="farmer_taluka" class="form-control select2" style="width: 100%;"></select></td>
</tr>
  

<tr>
    <td><?php echo MANDATORY;?>Village: <input type="text" name="farmer_village" class="form-control"></td>

    <td><?php echo MANDATORY;?>Total Acrage: <input type="text" name="total_acrage" class="form-control"></td>
    <!-- <td>By Executive: <select name="fby_executive" id="fby_executive" class="form-control select2"></select></td> -->
</tr>


<tr>
    <td>Crop 1: 
      <select name="crop_last_year[]" id="crop_last_year_1" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], 0);?>
      </select>
    </td>
    
    <td>Crop 2:
      <select name="crop_last_year[]" id="crop_last_year_2" class="form-control select2" style="width:100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], 0);?>
      </select>
    </td>
</tr>


<tr>
    <td>Soil Test: <select name="soil_test" class="form-control select2" style="width: 100%;"><option value="yes" <?php if($details['basic']-> soil_test == 'yes') echo 'checked';?>>Yes</option><option value="no" <?php if($details['basic']-> soil_test == 'no') echo 'checked';?>>No</option></select></td>
    
    <td>Irrigated: <select name="irrigated" class="form-control select2" style="width: 100%;"><option value="yes" <?php if($details['basic']-> irrigated == 'yes') echo 'checked';?>>Yes</option><option value="no" <?php if($details['basic']-> irrigated == 'no') echo 'checked';?>>No</option></select></td>
</tr>
</table>



<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item active">
    <a class="nav-link active" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="true">Stage 1</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false">Stage 2</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="false">Stage 3</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="tab4-tab" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false">Stage 4</a>
  </li>

</ul>


<div class="tab-content" id="myTabContent">

<div class="tab-pane active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
<table class="table table-stripped table-hover" width="100%">    
<tr>
    <td>Crop Selected: 
      <select name="demo_crop_id" class="form-control select2" style="width: 100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crops'], 0);?>
      </select>

    </td>

    <td>Variety: 
      <select name="demo_crop_variety_id" class="form-control select2" style="width: 100%;">
        <option value="">Select</option>
        <?php echo prepare_drop_down($masters['crop_variety'] , 0);?>
      </select>
    </td>
</tr>            

<tr>
    <td>Acrage for Demo: <input type="text" class="form-control" value="<?php echo $details['basic']-> acrage_for_demo;?>" name="acrage_for_demo"></td>

    <td>Reason: <input type="text" class="form-control" value="<?php echo $details['basic']-> demo_crop_reason;?>" name="demo_crop_reason"></td>
</tr>


<tr>
  <td colspan="2"><b>Last Year Fertiliser Application for Same Crop:</b><br/>
    <table width="100%" class="table table-bordered">
    <tr>      
      <td>Product</td>
      <td>Qty.</td>
    </tr>
    <?php
    for($i=1; $i<=5;$i++)
    {
    ?>
      <tr>        
        <td>
          <select type="text" name="crop_last_year['last_year_crop_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
            <option value="">Select</option>
            <?php 
            foreach($masters['products'] as $obj)
            {
                $pt = "OTHER";
                if($obj-> product_category_id == 1) $pt = "RMPCL";
                echo "<option value='".$obj-> id."'>".$obj-> name." (".$pt.")</option>";
            }
            ?>
          </select>  
        </td>
        

        <td>
          <input type="text" name="crop_last_year['last_year_crop_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
        </td>
      </tr>
    <?php
    }
    ?>

    </table>  
  </td>  
</tr>


<tr>
  <td colspan="2"><b>Plan for This Year Fertiliser Application:</b><br/>
    <table width="100%" class="table table-bordered">
    <tr>      
      <td>Product</td>
      <td>Qty.</td>
    </tr>
    <?php
    for($i=1; $i<=5;$i++)
    {
    ?>
      <tr>        
        <td>
          <select type="text" name="crop_current_year['current_year_crop_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
            <option value="">Select</option>
            <?php 
            foreach($masters['products'] as $obj)
            {
                $pt = "OTHER";
                if($obj-> product_category_id == 1) $pt = "RMPCL";
                echo "<option value='".$obj-> id."'>".$obj-> name." (".$pt.")</option>";
            }
            ?>
          </select>  
        </td>
        

        <td>
          <input type="text" name="crop_current_year['current_year_crop_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
        </td>
      </tr>
    <?php
    }
    ?>

    </table>
  </td>
</tr>


<tr>
  <td colspan="2"><b>Fertiliser Applied by RMPCL in Demo:</b><br/>
    <table width="100%" class="table table-bordered">
    <tr>      
      <td>Product</td>
      <td>Qty.</td>
    </tr>
    <?php
    for($i=1; $i<=5;$i++)
    {
    ?>
      <tr>        
        <td>
          <select type="text" name="demos_product['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
            <option value="">Select</option>
            <?php 
            foreach($masters['products'] as $obj)
            {                
                if($obj-> product_category_id == 1)
                {
                  echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                }  
            }
            ?>
          </select>  
        </td>
        

        <td>
          <input type="text" name="demos_product['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
        </td>
      </tr>
    <?php
    }
    ?>
    </table>
  </td>
</tr>


<tr>
  <td><?php echo MANDATORY;?>Demo Start Date: <input type="text" name="demo_start_date" class="form-control date_sel" value="<?php echo $details['basic']-> demo_start_date;?>"></td>

  <td><?php echo MANDATORY;?>Stage 2 Visit Date: <input type="text" name="next_stage_date" class="form-control date_sel" value="<?php echo $details['demos_stage'][0]-> next_stage_date;?>"></td>
</tr>

<tr>
  <td colspan="2">    
    <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save Stage 1</button>&nbsp;
    
    <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" class="btn btn-default btn_process">Cancel</button>
    
    <input name="hdn_id" value="0" type="hidden">        
</td>
</tr>
</table>
</div>




<div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
<table class="table table-stripped" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;">
    <td colspan="3" align="center"><h4>Stage 2 - Demo Application</h4></td>
  </tr>
</thead>


<tr>
  <td colspan="3">Visit Date: <input type="text" name="visit_date_stage_2" class="date_sel"></td>
</tr>


<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>


<tr>
  <td>Acrage</td>
  <td><input type="text" name="acrage_demo" class="form-control"></td>
  <td><input type="text" name="acrage_comparative" class="form-control"></td>
</tr>  


<tr>
  <td>Fertiliser Application</td>
  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>

      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="company_fertilizer_demo['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 1)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="company_fertilizer_demo['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>


  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="company_fertilizer_comparative['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 1)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="company_fertilizer_comparative['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>



<tr>
  <td>Other Fertiliser Application</td>  
  
  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="other_company_fertilizer_demo['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 2)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="other_company_fertilizer_demo['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>

  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="other_company_fertilizer_comparative['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 2)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="other_company_fertilizer_comparative['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>



<tr>
  <td>Micro Nutrients</td>  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Micro Nutrient Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="micro_nutrients_demo['micro_nutrient_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['micro_nutrients'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="micro_nutrients_demo['micro_nutrient_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Micro Nutrient Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="micro_nutrients_comparative['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['micro_nutrients'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="micro_nutrients_comparative['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>



<tr>
  <td>Weedicides</td>

  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Weedicide Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="weedicides_demo['weedicide_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['weedicides'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="weedicides_demo['weedicide_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>

  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Weedicide Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="weedicides_comparative['weedicide_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['weedicides'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="weedicides_comparative['weedicide_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>


<tr>
  <td>Photos</td>

  <td>
    <table width="100%">
      <tr>        
        <input type="file" name="plot_photos[]" class="form-control" multiple="">
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <tr>
        <input type="file" name="plot_photos_comparative[]" class="form-control" multiple="">
      </tr>
    </table>
  </td>
</tr>

<tr>
  <td colspan="3">Stage 3 Visit Date: <input type="text" name="next_stage_date" class="date_sel"></td>
</tr>

<tr>
  <td colspan="3">    
    <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save Stage 2</button>&nbsp;
    
    <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" class="btn btn-default btn_process">Cancel</button>
    
    <input name="hdn_id" value="0" type="hidden">        
</td>
</tr> 
</table>
</div>




<div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
<table class="table table-stripped" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;">
    <td colspan="3" align="center"><h4>Stage 3 - Flowering</h4></td>
  </tr>
</thead>

<tr>
  <td colspan="3">Visit Date: <input type="text" name="visit_date_stage_3" class="date_sel"></td>
</tr>


<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>


<tr>
  <td>Fertiliser Application</td>
  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>

      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="company_fertilizer_demo['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 1)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="company_fertilizer_demo['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>


  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="company_fertilizer_comparative['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 1)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="company_fertilizer_comparative['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>



<tr>
  <td>Other Fertiliser Application</td>  
  
  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="other_company_fertilizer_demo['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 2)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="other_company_fertilizer_demo['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>

  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Product Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="other_company_fertilizer_comparative['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php 
              foreach($masters['products'] as $obj)
              {                
                  if($obj-> product_category_id == 2)
                  {
                    echo "<option value='".$obj-> id."'>".$obj-> name."</option>";
                  }  
              }
              ?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="other_company_fertilizer_comparative['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>



<tr>
  <td>Micro Nutrients</td>  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Micro Nutrient Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="micro_nutrients_demo['micro_nutrient_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['micro_nutrients'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="micro_nutrients_demo['micro_nutrient_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Micro Nutrient Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="micro_nutrients_comparative['product_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['micro_nutrients'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="micro_nutrients_comparative['product_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>



<tr>
  <td>Weedicides</td>

  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Weedicide Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="weedicides_demo['weedicide_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['weedicides'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="weedicides_demo['weedicide_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>

  
  <td>
    <table width="100%">
      <tr>      
        <td width="70%">Weedicide Name</td>
        <td width="30%">Qty.</td>
      </tr>
      
      <?php
      for($i=1; $i<=5;$i++)
      {
      ?>
        <tr>        
          <td>
            <select type="text" name="weedicides_comparative['weedicide_id'][]" class="form-control select2" maxlength="100" style="width: 100%;">
              <option value="">Select</option>
              <?php echo prepare_drop_down($masters['weedicides'], 0);?>
            </select>  
          </td>
          

          <td>
            <input type="text" name="weedicides_comparative['weedicide_qty'][]" class="form-control" maxlength="10" onkeyup="chk_numeric(this);">
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </td>
</tr>


<tr>
  <td>Crop Condition</td>
  
  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="crop_condition_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="crop_condition_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="crop_condition_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="crop_condition_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="crop_condition_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="crop_condition_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>



<tr>
  <td>Vegetative Growth</td>

  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="vegitative_growth_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="vegitative_growth_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="vegitative_growth_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="vegitative_growth_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="vegitative_growth_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="vegitative_growth_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>


<tr>
  <td>Flowering Growth</td>
  
  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="flowering_growth_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="flowering_growth_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="flowering_growth_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="flowering_growth_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="flowering_growth_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="flowering_growth_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>


<tr>
  <td>Fruitning Growth</td>
  
  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="fruitning_growth_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="fruitning_growth_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="fruitning_growth_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="fruitning_growth_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="fruitning_growth_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="fruitning_growth_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>


<tr>
  <td>Quality</td>
  
  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="quality_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="quality_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="quality_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="quality_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="quality_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="quality_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>


<tr>
  <td>Photos</td>

  <td>
    <table width="100%">
      <tr>        
        <input type="file" name="plot_photos[]" class="form-control" multiple="">
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <tr>
        <input type="file" name="plot_photos_comparative[]" class="form-control" multiple="">
      </tr>
    </table>
  </td>
</tr>

<tr>
  <td colspan="3">Stage 4 Visit Date: <input type="text" name="next_stage_date" class="date_sel"></td>
</tr>

<tr>
  <td colspan="3">    
    <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save Stage 3</button>&nbsp;
    
    <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" class="btn btn-default btn_process">Cancel</button>
    
    <input name="hdn_id" value="0" type="hidden">        
</td>
</tr> 
</table>
</div>

  


<div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
<table class="table table-stripped" width="100%">
<thead>
  <tr style="background-color: #3c8dbc;">
    <td colspan="3" align="center"><h4>Stage 4 - Harvesting & Analysis Report</h4></td>
  </tr>
</thead>


<tr>
  <td colspan="3">Visit Date: <input type="text" name="reg_no" class="date_sel"></td>
</tr>

<tr>
  <th width="30%">Parameters and Application</th>
  <th width="35%">Demo Plot</th>
  <th width="35%">Comparative Plot</th>
</tr>

<tr>
  <td>Yield (KGs/Acre)</td>  
  <td><input type="text" name="yields_demo" class="form-control"></td>
  <td><input type="text" name="yields_comparative" class="form-control"></td>
</tr>


<tr>
  <td>Quality</td>
  
  <td>
    <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="quality_demo" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="quality_demo" value="good">&nbsp;Good</td>

        <td><input type="radio" name="quality_demo" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
  
  <td>
    <table width="100%">
      <table width="100%" class="">
      <tr>        
        <td><input type="radio" name="quality_comparative" value="excellent">&nbsp;Excellent</td>

        <td><input type="radio" name="quality_comparative" value="good">&nbsp;Good</td>

        <td><input type="radio" name="quality_comparative" value="poor">&nbsp;Poor</td>
      </tr>
    </table>
  </td>
</tr>


<tr>
  <td>Market Price INR</td>  
  <td colspan="2"><input type="text" name="market_price" class="form-control"></td>
</tr>

<tr>
  <td>Yield Difference</td>  
  <td colspan="2"><input type="text" name="yield_difference" class="form-control"></td>
</tr>

<tr>
  <td>Produce Value / Acre</td>  
  <td colspan="2"><input type="text" name="produce_value" class="form-control"></td>
</tr>

<tr>
  <td>Cost of Production/Acre (fertilisers)</td>  
  <td colspan="2"><input type="text" name="cost_of_production" class="form-control"></td>
</tr>

<tr>
  <td>Yield Difference Justification</td>  
  <td colspan="2"><input type="text" name="yield_difference_justification" class="form-control"></td>
</tr>


<tr>
  <td>Analysis Report</td>  
  <td colspan="2">
    Comparative analysis of yield: <input type="text" name="analysis_report_1" class="form-control"><br/>
    Comparative time for life cycle: <input type="text" name="analysis_report_2" class="form-control"><br/>
    Requirement of irrigation analysis: <input type="text" name="analysis_report_3" class="form-control"><br/>
    Effect on growth, colour, yield and cost benefitation: <input type="text" name="analysis_report_4" class="form-control">
  </td>
</tr>


<tr>
  <td>Farmers Feedback</td>  
  <td colspan="2"><input type="text" name="farmers_feedback" class="form-control"></td>
</tr>

<tr>
  <td>Executive Remark</td>  
  <td colspan="2"><input type="text" name="executive_remark" class="form-control"></td>
</tr>

<tr>
  <td>Guest Name & Photo</td>  
  <td colspan="2"><input type="text" name="guest_name" class="form-control"><br/><input type="file" name="guest_photo"></td>
</tr>

<tr>
  <td colspan="3">    
    <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Complete Demo</button>&nbsp;
    
    <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" class="btn btn-default btn_process">Cancel</button>
    
    <input name="hdn_id" value="0" type="hidden">        
</td>
</tr>
</table>
</div>
</div>


 

</form>
</fieldset> 




</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(function()
{
    $(".select2").select2();

    $(".date_sel").datepicker({maxDate:0, dateFormat:"dd-mm-yy"});
});
</script>