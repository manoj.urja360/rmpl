<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-knob/js/jquery.knob.js"></script>

<script type="text/javascript">
var yearly_arr = [];
var monthly_arr = [];
var monthly_lbl = []; 
</script>
<?php
$sarr = $darr = $tarr = $snm = $dnm = $yearly = $monthly = $sstate = $sttarget = array();


if(isset($sdaop) && !empty($sdaop))
{
  foreach($sdaop as $obj)
  {
    $snm[$obj-> state_id] = $obj-> state_name;
    $dnm[$obj-> district_id] = $obj-> district_name;
  }
}   


if(isset($counts) && !empty($counts))
{
  foreach($counts as $obj)
  {
    $obj-> quantity = number_format($obj-> quantity, '2', '.', '');

    /*if(isset($darr[$obj-> district_id][$obj-> type]) && !empty($darr[$obj-> district_id][$obj-> type]))
      $darr[$obj-> district_id][$obj-> type] = $darr[$obj-> district_id][$obj-> type] + $obj-> quantity;
    else
      $darr[$obj-> district_id][$obj-> type] = $obj-> quantity;*/

    if(isset($darr[$obj-> state_id][$obj-> district_id][$obj-> type]) && !empty($darr[$obj-> state_id][$obj-> district_id][$obj-> type]))
      $darr[$obj-> state_id][$obj-> district_id][$obj-> type] = $darr[$obj-> state_id][$obj-> district_id][$obj-> type] + $obj-> quantity;
    else
      $darr[$obj-> state_id][$obj-> district_id][$obj-> type] = $obj-> quantity;


    if(isset($sarr[$obj-> state_id][$obj-> type]) && !empty($sarr[$obj-> state_id][$obj-> type]))
      $sarr[$obj-> state_id][$obj-> type] = $sarr[$obj-> state_id][$obj-> type] + $obj-> quantity;
    else
      $sarr[$obj-> state_id][$obj-> type] = $obj-> quantity;

    
    if(isset($tarr[$obj-> type]) && !empty($tarr[$obj-> type]))
      $tarr[$obj-> type] = $tarr[$obj-> type] + $obj-> quantity;
    else
      $tarr[$obj-> type] = $obj-> quantity;
  }
}

//echo "<pre>"; print_r($darr); die;

$ack1 = $ack2 = $wstk = $rstk = $rpos = 0;
if(isset($tarr['ack1']) && !empty($tarr['ack1'])) $ack1 = $tarr['ack1'];
if(isset($tarr['ack2']) && !empty($tarr['ack2'])) $ack2 = $tarr['ack2'];
if(isset($tarr['wstk']) && !empty($tarr['wstk'])) $wstk = $tarr['wstk'];
if(isset($tarr['rstk']) && !empty($tarr['rstk'])) $rstk = $tarr['rstk'];
if(isset($tarr['rpos']) && !empty($tarr['rpos'])) $rpos = $tarr['rpos'];
?>

<style type="text/css">
.info-box-icon img
{
  width: 50px;
  height: 50px;
}  

.info-box-content .info-box-text
{
  white-space: normal !important;
  font-size: 12px !important;
}
</style>
<section class="content-header">
<h1>
  Dashboard   
</h1>
</section>


<section class="content">

<div class="row">
<div class="col-md-3 col-sm-6 col-xs-12" style="width: 20% !important;">
  <div class="info-box">
    <a href="<?php echo base_url();?>admin/inventory_acknowledgement1">
    <span class="info-box-icon bg-aqua"><img src="<?php echo base_url();?>assets/images/dashboard/ack1.jpeg"></span>

    <div class="info-box-content">
      <span class="info-box-text">1<small><sup>st</sup></small> Point Ack (MT)</span>
      <span class="info-box-number"><?php echo $ack1;?></span>
    </div>
    </a>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12" style="width: 20% !important;">
  <div class="info-box">
    <a href="<?php echo base_url();?>admin/inventory_acknowledgement2">
    <span class="info-box-icon bg-red"><img src="<?php echo base_url();?>assets/images/dashboard/ack2.jpeg"></span>

    <div class="info-box-content">
      <span class="info-box-text">2<small><sup>nd</sup></small> Point Ack (MT)</span>
      <span class="info-box-number"><?php echo $ack2;?></span>
    </div>
    </a>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>

<div class="col-md-3 col-sm-6 col-xs-12" style="width: 20% !important;">
  <div class="info-box">
    <a href="<?php echo base_url();?>admin/inventory_wholesaler">
    <span class="info-box-icon bg-green"><img src="<?php echo base_url();?>assets/images/dashboard/wstock.jpeg"></span>

    <div class="info-box-content">
      <span class="info-box-text">Wholesaler Stock (MT)</span>
      <span class="info-box-number"><?php echo $wstk;?></span>
    </div>
  </a>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12" style="width: 20% !important;">
  <div class="info-box">
    <a href="<?php echo base_url();?>admin/inventory_retailer">
    <span class="info-box-icon bg-yellow"><img src="<?php echo base_url();?>assets/images/dashboard/rstock.jpeg"></span>

    <div class="info-box-content">
      <span class="info-box-text">Retailer Stock (MT)</span>
      <span class="info-box-number"><?php echo $rstk;?></span>
    </div>
  </a>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>


<div class="col-md-3 col-sm-6 col-xs-12" style="width: 20% !important;">
  <div class="info-box">
    <a href="<?php echo base_url();?>admin/inventory_retailer_pos">
    <span class="info-box-icon bg-purple"><img src="<?php echo base_url();?>assets/images/dashboard/ack1.jpeg"></span>

    <div class="info-box-content">
      <span class="info-box-text">Retailer POS Sale (MT)</span>
      <span class="info-box-number"><?php echo $rpos;?></span>
    </div>
  </a>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
</div>




<div class="row">
<?php
$bgcolor = array("green", "blue", "red", "yellow", "pink", "aqua");
$sno = 0; 
foreach($snm as $id => $nm)
{
  if(isset($sarr[$id]) && !empty($sarr[$id]))
  {
    $bgc = $bgcolor[$sno]; $sno++;
    $ack1 = $ack2 = $wstk = $rstk = $rpos = 0;
    if(isset($sarr[$id]['ack1']) && !empty($sarr[$id]['ack1'])) $ack1 = $sarr[$id]['ack1'];
    if(isset($sarr[$id]['ack2']) && !empty($sarr[$id]['ack2'])) $ack2 = $sarr[$id]['ack2'];
    if(isset($sarr[$id]['wstk']) && !empty($sarr[$id]['wstk'])) $wstk = $sarr[$id]['wstk'];
    if(isset($sarr[$id]['rstk']) && !empty($sarr[$id]['rstk'])) $rstk = $sarr[$id]['rstk'];
    if(isset($sarr[$id]['rpos']) && !empty($sarr[$id]['rpos'])) $rpos = $sarr[$id]['rpos'];
?>  
<div class="col-md-4">
<div class="box box-widget widget-user-2">
<div class="widget-user-header bg-<?php echo $bgc;?>">
  <h4 class=""><?php echo $nm;?></h4>
</div>
<div class="box-footer no-padding">
  <ul class="nav nav-stacked">
    <li><a href="<?php echo base_url();?>admin/inventory_acknowledgement1/<?php echo $id;?>/0">1st Point Ack <span class="pull-right badge bg-blue"><?php echo $ack1;?></span></a></li>
    
    <li><a href="<?php echo base_url();?>admin/inventory_acknowledgement2/<?php echo $id;?>/0">2nd Point Ack <span class="pull-right badge bg-aqua"><?php echo $ack2;?></span></a></li>

    <li><a href="<?php echo base_url();?>admin/inventory_wholesaler/<?php echo $id;?>/0">Wholesaler Stock <span class="pull-right badge bg-green"><?php echo $wstk;?></span></a></li>
    
    <li><a href="<?php echo base_url();?>admin/inventory_retailer/<?php echo $id;?>/0">Retailer Stock <span class="pull-right badge bg-red"><?php echo $rstk;?></span></a></li>

    <li><a href="<?php echo base_url();?>admin/inventory_retailer_pos/<?php echo $id;?>/0">Retailer POS Sale <span class="pull-right badge bg-purple"><?php echo $rpos;?></span></a></li>
  </ul>
</div>
</div>
</div>
<?php
  }
}
?>
</div>




<div class="row">
<div class="col-lg-12">
<div class="box">
<div class="box-body">
<?php
foreach($snm as $stid => $stnm)
{
?>    
    <fieldset>
      <legend><?php echo $stnm;?></legend>
<?php
    foreach($dnm as $id => $nm)
    {
      if(isset($darr[$stid][$id]) && !empty($darr[$stid][$id]))
      {       
          $ack1 = $ack2 = $wstk = $rstk = $rpos = 0;
          
          if(isset($darr[$stid][$id]['ack1']) && !empty($darr[$stid][$id]['ack1'])) $ack1 = $darr[$stid][$id]['ack1'];
          
          if(isset($darr[$stid][$id]['ack2']) && !empty($darr[$stid][$id]['ack2'])) $ack2 = $darr[$stid][$id]['ack2'];
          
          if(isset($darr[$stid][$id]['wstk']) && !empty($darr[$stid][$id]['wstk'])) $wstk = $darr[$stid][$id]['wstk'];
          
          if(isset($darr[$stid][$id]['rstk']) && !empty($darr[$stid][$id]['rstk'])) $rstk = $darr[$stid][$id]['rstk'];

          if(isset($darr[$stid][$id]['rpos']) && !empty($darr[$stid][$id]['rpos'])) $rpos = $darr[$stid][$id]['rpos'];
    ?>  
        <div class="col-md-3">
            <h6><?php echo $nm;?></h6>
            
            <span class="text-muted"><a href="<?php echo base_url();?>admin/inventory_acknowledgement1/<?php echo $stid;?>/<?php echo $id;?>"><?php echo $ack1;?></a>&nbsp;|&nbsp;</span>
            
            <span class="text-muted"><a href="<?php echo base_url();?>admin/inventory_acknowledgement2/<?php echo $stid;?>/<?php echo $id;?>"><?php echo $ack2;?></a>&nbsp;|&nbsp;</span>
            
            <span class="text-muted"><a href="<?php echo base_url();?>admin/inventory_wholesaler/<?php echo $stid;?>/<?php echo $id;?>"><?php echo $wstk;?></a>&nbsp;|&nbsp;</span>
            
            <span class="text-muted"><a href="<?php echo base_url();?>admin/inventory_retailer/<?php echo $stid;?>/<?php echo $id;?>"><?php echo $rstk;?></a>&nbsp;|&nbsp;</span>

            <span class="text-muted"><a href="<?php echo base_url();?>admin/inventory_retailer_pos/<?php echo $stid;?>/<?php echo $id;?>"><?php echo $rpos;?></a></span>      
        </div>
    <?php
      }
    }
?>
    </fieldset>
<?php    
}    
?>
</div>
</div>
</div>
</div>




<div class="row">
<div class="col-md-12">
<div class="box">
<div class="box-header with-border">
  <h3 class="box-title">
    Sales Report
  </h3> 

  <div class="pull-right">
  <div class="col-md-12">
  <select name="sale_year" id="sale_year" class="form-control" onchange="get_chart();">
  <?php
  for($y=date("Y"); $y>=2019;$y--)
  {
      $s = "";
      if($sales_year == $y) $s = "selected";
      echo "<option value='$y' $s>$y</option>";
  }?>
  </select> 
  </div>
     
  </div>  
</div>

<div class="box-body">
  <div class="row">
    <div class="col-md-12">
      <p class="text-center">
        <strong id="sale_year_text"></strong>
      </p>

      <div class="col-12" align="center">
          <div id="graph_loader" style="display: none;"></div>
      </div>

      <div id="barChartContent">
        <canvas id="barChart" style="height:250px;"></canvas>
      </div>  
          
    </div>
  </div>  
  
</div>


<div class="box-footer">
<div class="row"><div class="col-lg-12"><h5>Target Achievement For Current Month - <?php echo date('F');?></h5></div></div>
<div class="row" id="target_details" align="center" style="padding: 15px 0px;">
 
</div>
</div>

</div>
</div>
</div>

</section>



<script type="text/javascript">
get_chart();

function get_target_achievement()
{
    var year = $("#sale_year").val();

    if(year <= 0) return;    

    $("#target_details").html("<img src='"+base_url+"assets/images/loader_add.gif' style='width: auto !important;margin: 2%;'><br/>Loading Target Achievement...");

    var formData = {"year":year};

    $.ajax({url : base_url+"admin/get_target_achievement",
      method: "POST",
      data: formData,
      async: false,
      dataType: 'json',
      success: function(res)
      {
          /*var html = '<div class="col-md-12">
      <p class="text-center">
        <strong>Target Achievement</strong>
      </p>

      <div class="progress-group">
        <span class="progress-text">Overall</span>
        <span class="progress-number"><b></b></span>

        <div class="progress sm">
          <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
        </div>
      </div>      
    </div>';*/
          if(res.status == 1)
          {
              var html = '';
              html = html + '<div class="col-sm-3 col-xs-6"><div class="description-block border-right"><span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span><h5 class="description-header">$35,210.43</h5><span class="description-text">TOTAL REVENUE</span></div></div>';



          }
          else
          {                
              $("#target_details").html("No record found.");              
          }
      }
    }); 
}        




function get_chart()
{
    var year = $("#sale_year").val();

    if(year <= 0) return;    

    $("#sale_year_text").html("1st Jan "+year+" - 31st Dec "+year);

    $("#barChartContent").hide();
    $("#graph_loader").show();
    $("#graph_loader").html("<img src='"+base_url+"assets/images/loader_add.gif' style='width: auto !important;margin: 2%;'><br/>Preparing Graph...");


    var formData = {"year":year};

    $.ajax({url : base_url+"admin/get_sales_yearly",
      method: "POST",
      data: formData,
      async: false,
      dataType: 'json',
      success: function(res)
      {
            if(res.status == 1 && res.data.target != "")
            {
                var html = '';               
                $("#target_details").html(html);
                var tsoverall_s = 0;
                var tsoverall_t = 0;
                $.each(res.data.target, function(it, vt)
                {
                    html = '<div class="col-sm-3 col-xs-6"><input type="text" class="knob" value="'+vt.percent+'" data-width="120" data-height="120" data-thickness="0.2" data-fgColor="#00a65a" data-readonly="true"><div class="knob-label"><br/>'+vt.total_sales+'/'+vt.total_target+'<br/>'+vt.state_name+'</div>';

                    $("#target_details").prepend(html);

                    $(".knob").knob();
                    //display_chart('g'+it, vt.state_name, vt.percent, '#3c8dbc');

                    tsoverall_s = parseFloat(tsoverall_s) + parseFloat(vt.total_sales);
                    tsoverall_t = parseFloat(tsoverall_t) + parseFloat(vt.total_target);
                });  
                
                
                var p = 0;
                if(tsoverall_s > 0 && tsoverall_t > 0)
                {
                    p = (parseFloat(tsoverall_s) * 100 ) / parseFloat(tsoverall_t);
                    p = p.toFixed(2);
                }


                html = '<div class="col-sm-3 col-xs-6"><input type="text" class="knob" value="'+p+'" data-width="120" data-height="120" data-thickness="0.2" data-fgColor="#00a65a" data-readonly="true"><div class="knob-label"><br/>'+tsoverall_s+'/'+tsoverall_t+'<br/>OVERALL</div>';

                $("#target_details").prepend(html);

                $(".knob").knob();

                //display_chart('goverall', "OVERALL", p , '#3c8dbc');
            }
            else
            {
                $("#target_details").html("No record found.");
            }


            if(res.status == 1 && res.data.sales != "")
            {                
                $("#graph_loader").hide();

                $("#barChartContent").show();

                var gdata = [];

                var fcolor = 0;
                $.each(res.data.sales, function(i, v)
                {
                    var temp = [];

                    fcolor = fcolor + 1;

                    $.each(v, function(ii, vv)
                    {
                        temp.push(vv);
                    });  

                    gdata.push({label       : i,
                        fillColor           : 'rgba(210, 214, 222, 1)',
                        strokeColor         : 'rgba(210, 214, 222, 1)',
                        pointColor          : 'rgba(210, 214, 222, 1)',
                        pointStrokeColor    : '#c1c7d1',
                        pointHighlightFill  : '#fff',
                        pointHighlightStroke: 'rgba(220,220,220,1)',
                        data                : temp
                    });
                });

                //console.log(gdata);

                var areaChartData = {
                  
                  labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July', "August", "September", "October", "November", "December"],
                  
                  datasets: gdata
                }



                var areaChartOptions = {
                  //Boolean - If we should show the scale at all
                  showScale               : true,
                  //Boolean - Whether grid lines are shown across the chart
                  scaleShowGridLines      : false,
                  //String - Colour of the grid lines
                  scaleGridLineColor      : 'rgba(0,0,0,.05)',
                  //Number - Width of the grid lines
                  scaleGridLineWidth      : 1,
                  //Boolean - Whether to show horizontal lines (except X axis)
                  scaleShowHorizontalLines: true,
                  //Boolean - Whether to show vertical lines (except Y axis)
                  scaleShowVerticalLines  : true,
                  //Boolean - Whether the line is curved between points
                  bezierCurve             : true,
                  //Number - Tension of the bezier curve between points
                  bezierCurveTension      : 0.3,
                  //Boolean - Whether to show a dot for each point
                  pointDot                : false,
                  //Number - Radius of each point dot in pixels
                  pointDotRadius          : 4,
                  //Number - Pixel width of point dot stroke
                  pointDotStrokeWidth     : 1,
                  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                  pointHitDetectionRadius : 20,
                  //Boolean - Whether to show a stroke for datasets
                  datasetStroke           : true,
                  //Number - Pixel width of dataset stroke
                  datasetStrokeWidth      : 2,
                  //Boolean - Whether to fill the dataset with a color
                  datasetFill             : true,
                  //String - A legend template
                  legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                  //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                  maintainAspectRatio     : true,
                  //Boolean - whether to make the chart responsive to window resizing
                  responsive              : true
                }

                               

                //-------------
                //- BAR CHART -
                //-------------
                var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
                var barChart                         = new Chart(barChartCanvas)
                var barChartData                     = areaChartData
                
                var cc = 55;                
                var dd = 10;
                for(var j = 1; j<fcolor; j++)
                {
                    barChartData.datasets[j].fillColor   = '#'+cc+'a'+dd+'a';
                    barChartData.datasets[j].strokeColor = '#'+cc+'a'+dd+'a';
                    barChartData.datasets[j].pointColor  = '#'+cc+'a'+dd+'a';

                    cc = cc + 40;
                    dd = dd + 5;
                }
                    
                var barChartOptions                  = {
                  //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                  scaleBeginAtZero        : true,
                  //Boolean - Whether grid lines are shown across the chart
                  scaleShowGridLines      : true,
                  //String - Colour of the grid lines
                  scaleGridLineColor      : 'rgba(0,0,0,.05)',
                  //Number - Width of the grid lines
                  scaleGridLineWidth      : 1,
                  //Boolean - Whether to show horizontal lines (except X axis)
                  scaleShowHorizontalLines: true,
                  //Boolean - Whether to show vertical lines (except Y axis)
                  scaleShowVerticalLines  : true,
                  //Boolean - If there is a stroke on each bar
                  barShowStroke           : true,
                  //Number - Pixel width of the bar stroke
                  barStrokeWidth          : 2,
                  //Number - Spacing between each of the X value sets
                  barValueSpacing         : 5,
                  //Number - Spacing between data sets within X values
                  barDatasetSpacing       : 1,
                  //String - A legend template
                  legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                  //Boolean - whether to make the chart responsive
                  responsive              : true,
                  maintainAspectRatio     : true
                }

                barChartOptions.datasetFill = false
                barChart.Bar(barChartData, barChartOptions)            
                      
            }
            else
            {                
                $("#graph_loader").html("No record found.");

                $("#barChartContent").hide();                
            }
      }
    }); 
}



function display_chart(chart_id, lbl, num, color)
{
    var pieChartCanvas = $("#target_details").find('.'+chart_id).get(0).getContext('2d')
    
    var pieChart       = new Chart(pieChartCanvas)
    
    var PieData        = [{                     
        value    : num,
        color    : color,

        highlight: color,
        label    : lbl }];


    var pieOptions     = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      
      //String - The colour of each segment stroke
      segmentStrokeColor   : 'green',
      //Number - The width of each segment stroke
      
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,

      rotation: 1 * Math.PI,
      circumference: 1 * Math.PI,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions)
}
</script>


<script type="text/javascript">
$(function()
{
    
});  
</script>