<style type="text/css">
.fa.fa-frown-o
{
    color: #df2424 !important;
    font-size: 12px !important;
}

.fa.fa-star
{
    color: #6cc644 !important;
    font-size: 12px !important;
}
</style>
<section class="content-header">
<h1>
Dealers
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<ul class="nav nav-tabs">
<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/dealers';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/dealers_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/dealers_import';" data-toggle="tab" aria-expanded="false">Import</a></li>
</ul>



<div class="row" style="margin-top:2%; margin-bottom:2%;">
  <div class="form-group">

    <div class="col-sm-5">
      <label for="fby_state">State Name</label>
      <select name="fby_state" id="fby_state" class="form-control select2 fby" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'fby_city');">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-5">
      <label for="fby_city">District Name</label>
      <select name="fby_city" id="fby_city" class="form-control select2 fby"></select>
    </div>     
  </div>
</div>

<div class="row btn_row">
<div class="form-group">
    <div class="col-sm-4">
      <button type="submit" name="btn_fby" id="btn_fby" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;Search</button>&nbsp;
      <button type="button" name="btn_reset" id="btn_reset" class="btn btn-danger"><i class="fa fa-empty"></i>&nbsp;Clear Search</button>
    </div>
</div>
</div>



<fieldset style="overflow: auto;">


<div id="message_box"></div>

<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_70;?>">Dealer ID</th>
<th style="<?php echo COL_200;?>">Full Name</th>
<th style="<?php echo COL_150;?>">Email</th>
<th style="<?php echo COL_100;?>">Mobile</th>
<th style="<?php echo COL_150;?>">Address</th>
<th style="<?php echo COL_150;?>">District</th>
<th style="<?php echo COL_150;?>">State</th>
<th style="<?php echo COL_50;?>">Actions</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;

<?php
$osearch_filter = "";
if(isset($fsid) && !empty($fsid))
{
    $osearch_filter = "'search': {
        'search': '".$fsid."'
      },";
}
?>

$(document).ready(function() {
    
    $(".select2").select2();

    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    <?php echo $osearch_filter;?>
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "asc" ]],       
    "aoColumns": [        
    { "bSortable": true },
    { "bSortable": true },        
    { "bSortable": true },        
    { "bSortable": true },        
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true },        
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/salers_list",
            "type": "POST",
            "data": function ( data ) {
                data.saler_type = 2;
                data.fby_state = $("#fby_state").val();
                data.fby_city = $("#fby_city").val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ],
 
    });


    $("#btn_fby").click(function()
    {
        table.ajax.reload(null, true);
    });


    $("#btn_reset").click(function()
    {
        $(".fby").val("");
        $(".select2").select2().trigger("chosen:updated");

        $("#btn_fby").trigger("click");
    });
 
});
</script>