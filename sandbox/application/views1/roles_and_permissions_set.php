<section class="content-header">
<h1>
Set Permissions For Modules
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<!-- <div class="box-header with-border">
<i class="fa fa-warning"></i>
<h3 class="box-title">Listing</h3>
</div>
 -->

<div class="box-body">

<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
  <div class="form-group">            
    <div class="col-sm-12">
      <label for="unit_name">Role Name: </label>
      <input type="hidden" name="role_name" value="<?php echo $sel_role_id;?>">
             
        <?php
          foreach($roles as $obj)
          {
              if($sel_role_id == $obj-> role_id)
              {          
                echo "".$obj-> role_name."";          
              }  
          }
        ?>
       
    </div>
  </div>
</div>  



<div class="row" id="pgrp">
  <div class="">
    <div class="box box-primary" style="padding: 3px 9px !important; width: 96%;">
      <!-- <div class="">      
      <h5>Set Permissions</h5>
      </div> -->

      <div class="box-body">
        <div class="col-sm-2">Permissions:</div>
        <?php
        foreach($permissions as $obj)
        {
        ?>
            <div class="col-sm-2">
              <input type="checkbox" class="p<?php echo $obj-> permission_id;?>" onclick="check_uncheck(this,<?php echo $obj-> permission_id;?>);" value="<?php echo $obj-> permission_id;?>">&nbsp;<?php echo $obj-> permission_name;?>
            </div>  
        <?php  
        }
      ?>
    </div>
  </div>
  </div>
</div>    


<div class="row" id="mpgrp">
<?php
$temparr = array();
if(isset($permitted_modules) && !empty($permitted_modules))
{
    foreach($permitted_modules as $obj)
    {
        $temparr[$obj-> module_id][$obj-> permission_id] = 1;
    }
}

foreach($modules as $mobj)
{
?>
    <div class="col-sm-6" style="padding: 0px !important;margin-right: 7px;
width: 47%;">
      <div class="box box-primary" style="border: 1px solid #ddd !important; padding: 3px 9px !important;">
      <div class="">      
      <h5><?php echo $mobj-> module_name;?></h5>
      </div>

      <div class="box-body">
      <?php
        foreach($permissions as $obj)
        {
            $checked = "";
            if(isset($temparr[$mobj-> module_id][$obj-> permission_id]) && !empty($temparr[$mobj-> module_id][$obj-> permission_id]))
            {
                $checked = "checked='true'";
            }

        ?>            
            <input type="checkbox" name="chkbox[<?php echo $mobj-> module_id;?>][<?php echo $obj-> permission_id;?>]" class="mp<?php echo $obj-> permission_id;?>" value="<?php echo $obj-> permission_id;?>" <?php echo $checked;?>>&nbsp;<?php echo $obj-> permission_name;?>&nbsp;&nbsp;&nbsp;
              
        <?php  
        }
      ?>
      </div>

      </div>
      </div>           
<?php  
}
?>
</div>  



<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/roles_and_permissions';" class="btn btn-default btn_process">Cancel</button>
      <input name="hdn_id" value="0" type="hidden">
    </div>
  </div> 
</div> 

</form>
</div>
</div>  
</div>
</div>
</section>

<script type="text/javascript">

function check_uncheck(obj, pid)
{
  if($(obj).is(":checked") == true)
  {
      $("#mpgrp").find(".mp"+pid).attr("checked", "true");      

      if(pid == 1)
      {
          $("#mpgrp").find(".mp2").attr("checked", "true");
          $("#mpgrp").find(".mp3").attr("checked", "true");
          $("#mpgrp").find(".mp4").attr("checked", "true");
          $("#mpgrp").find(".mp5").attr("checked", "true");

          $("#pgrp").find(".p2").attr("checked", "true");
          $("#pgrp").find(".p3").attr("checked", "true");
          $("#pgrp").find(".p4").attr("checked", "true");
          $("#pgrp").find(".p5").attr("checked", "true");
      }
  }
  else
  {      
      $("#mpgrp").find(".mp"+pid).removeAttr("checked");
      
      if(pid == 1)
      {
          $("#mpgrp").find(".mp2").removeAttr("checked");
          $("#mpgrp").find(".mp3").removeAttr("checked");
          $("#mpgrp").find(".mp4").removeAttr("checked");
          $("#mpgrp").find(".mp5").removeAttr("checked");

          $("#pgrp").find(".p2").removeAttr("checked");
          $("#pgrp").find(".p3").removeAttr("checked");
          $("#pgrp").find(".p4").removeAttr("checked");
          $("#pgrp").find(".p5").removeAttr("checked");          
      }      
  }
}

$(document).ready(function()
{
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/roles_and_permissions_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/roles_and_permissions'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>