<section class="content-header">
<h1>
Manage SMS Templates
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">


<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sms_template';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sms_template_add_w';" data-toggle="tab" aria-expanded="false">Acknowledgement 1</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sms_template_add_r1';" data-toggle="tab" aria-expanded="false">Acknowledgement 2</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sms_template_add_r';" data-toggle="tab" aria-expanded="false">Stock Movement</a></li>
</ul>


<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
<div style="color: #555;">Note: 
<ol>
<li>SMS content limit is 100 characters including spaces.</li>

<li>You can use system variables {RETAILER_NAME}, {NUMBER_X} in your message.</li>

<li>A common generic message can be send without use of the variables mentioned above.</li>
</ol>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<div class="row" style="background-color: #f5f5f5;padding: 10px; margin-bottom: 2%;">
 
  <div class="form-group">
    
    <div class="col-sm-12">
      
      <label for=""><?php echo MANDATORY;?>SMS Content</label>
      
      <textarea class="form-control rounded-0" name="content" id="content" rows="2" onkeyup="character_limit_sms(this.id, 100);" onblur="character_limit_sms(this.id, 100);">Dear RETAILER_NAME, No Stock movement over NUMBER_X days.</textarea>
      
      <span id="content_lbl"></span>

    </div>

  </div>
  


  <div class="form-group">  
    <div class="col-sm-12">
      <?php echo MANDATORY;?>Send SMS when no stock movement over <input type="text" name="days" value="5" onkeyup="chk_numeric(this);" maxlength="3"> days.
    </div>  
  </div>

  <div class="form-group">  
    <label class="col-sm-12">Send copy of SMS to:</label>
    <div class="col-sm-3">
       <input type="checkbox" name="sms_to[]" value="Executive">&nbsp;Executive Assigned
    </div>

    <div class="col-sm-2">
       <input type="checkbox" name="sms_to[]" value="State Head">&nbsp;State Head
    </div>

    <div class="col-sm-2">
       <input type="checkbox" name="sms_to[]" value="Management">&nbsp;Management
    </div>
  </div>

</div>
</div>
</div>


<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sms_template';" class="btn btn-default btn_process">Cancel</button>
      <input name="sms_for" value="2" type="hidden">
      <input name="template_id" value="0" type="hidden">
    </div>
  </div> 
</div>
</form>

</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(document).ready(function()
{
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/sms_template_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/sms_template'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>