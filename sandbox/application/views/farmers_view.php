<section class="content-header">
<h1>
Farmer Details
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">

<ul class="nav nav-tabs">
<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers_quick';" data-toggle="tab" aria-expanded="false">Quick Registered Farmers</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers_import';" data-toggle="tab" aria-expanded="false">Import</a></li>
</ul>

  
<fieldset style="overflow: auto;">


<table class="table no-border" border="0" width="100%">    
<tr>
    <td valign="top" width="10%"><img src="<?php echo $basic-> farmer_photo_url;?>" width="70px" height="70px"></td>

    <td width="90%" valign="top">
        <small><?php echo "F".regno($basic-> farmer_reg_num, 5);?></small>
        <h4><?php echo $basic->farmer_name;?></h4>
        <?php echo $basic->district_name.", ".$basic->state_name;?>
    </td>
</tr>
</table>

<table class="table table-bordered" width="100%">    
<tr>
    <td>Registration Number: <?php echo "F".regno($basic-> farmer_reg_num, 5);?></td>

    <td>Date: <?php echo format_date($basic->created_date,DATETIME);?></td>
    
    <td>By: <?php echo ucwords($basic->first_name." ".$basic->last_name);?></td>
</tr>        

<tr><td colspan="3" align="center"><b>Farmer Information</b></td></tr>

<tr>
    <td>Farmer Name: <?php echo $basic->farmer_name;?></td>

    <td>Mobile: <?php echo $basic->farmer_mobile;?></td>

    <td>Email: <?php echo $basic->farmer_email;?></td>

    
</tr>

<tr>
    <td>Plot: <?php echo $basic->farmer_plot_no;?></td>
    
    <td>Village: <?php echo $basic->farmer_village;?></td>

    <td>Taluka: <?php echo $basic->taluka_name;?></td>

    
</tr>
  

<tr>
    <td>District: <?php echo $basic->district_name;?></td>
    
    <td>State: <?php echo $basic->state_name;?></td>

    <td>&nbsp;</td>
</tr>


<tr>
    <td>Total Acrage: <?php echo $basic->farmer_land_area;?></td>
    
    <td>Plot Area for consultancy: <?php echo $basic->farmer_plot_area_consultancy;?></td>

    <td>&nbsp;</td>
</tr>

<tr>
    <td>Willing For Demo: <?php echo $basic->farmer_willing_demo;?></td>

    <td colspan="2">GPS Location: Lat:<?php echo $basic->geo_lat;?> and lon:<?php echo $basic->geo_long;?></td>
</tr>

<tr>
    <td colspan="3">If some other kharif crop: <?php echo $basic->kharif_crop;?></td>
</tr>
</table>



<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item active">
    <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false">Last Yr. Crop</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="false">Current Yr. Crop</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="tab4-tab" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false">Ziron Used</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="tab5-tab" data-toggle="tab" href="#tab5" role="tab" aria-controls="tab5" aria-selected="false">Farmer Reference</a>
  </li>     
</ul>

<div class="tab-content" id="myTabContent">
  
  <div class="tab-pane active" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
    <table class="table table-stripped table-hover" width="100%">
    <thead>
       <tr>
        <th>Crop</th>
        <!-- <th>Area Sowed</th>
        <th>Fertilizer Expenses</th>
        <th>Yields</th>
        <th>Remark</th> -->
       </tr> 
    </thead>
    <?php 
    if(isset($crop_last_year) && !empty($crop_last_year))
    {
        foreach($crop_last_year as $obj)
        {
    ?>
            <tr>
                <td><?php echo $obj->crop_name;?></td>
                <!-- <td><?php echo $obj->last_year_area_sowed;?></td>
                <td><?php echo $obj->last_year_fertilizer_expenses;?></td>
                <td><?php echo $obj->last_year_yields;?></td>
                <td><?php echo $obj->last_year_remark;?></td> -->
            </tr> 

    <?php        
        }
    }
    else
    {
        echo "<tr><td colspan=5>No record found.</td></tr>";
    }
    ?> 
    </table>
  </div>


  <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
      <table class="table table-stripped table-hover" width="100%">
    <thead>
       <tr>
        <th>Crop</th>
        <!-- <th>Area Sowed</th>
        <th>Acrage Planned</th>
        <th>Expected Date Of Sowing</th> -->
       </tr> 
    </thead>
    <?php 
    if(isset($crop_current_year) && !empty($crop_current_year))
    {
        foreach($crop_current_year as $obj)
        {
    ?>
            <tr>
                <td><?php echo $obj->crop_name;?></td>
                <!-- <td><?php echo $obj->current_year_area_sowed;?></td>
                <td><?php echo $obj->current_year_acrage_planned;?></td>
                <td><?php echo format_date($obj->current_year_expect_date_sowing,DATE);?></td> -->
            </tr> 

    <?php        
        }    
    }
    else
    {
        echo "<tr><td colspan=4>No record found.</td></tr>";
    }
   ?> 
    </table>
  </div>

  
  <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
      <table class="table table-stripped table-hover" width="100%">
       
        <tr>
            <td>Ziron used: <?php echo ucfirst($basic->is_ziron_used);?></td>

            <td>Name of retailer bought from: <?php echo $basic->ziron_bought_from;?></td>            
        </tr> 

        <tr>
            <td colspan="2">Feedback of ziron used: <?php echo $basic->ziron_feedback;?></td>
        </tr>    
     
    </table>
  </div>


  <div class="tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="tab5-tab">
      <table class="table table-stripped table-hover" width="100%">
    <thead>
       <tr>
        <th>S.No.</th>
        <th>Name</th>
        <th>Mobile</th>
       </tr> 
    </thead>
    <?php 
    if(isset($basic-> farmer_refrence) && !empty($basic-> farmer_refrence))
    {     
        $arr = json_decode($basic-> farmer_refrence);
        foreach($arr as $sno => $obj)
        {
    ?>
            <tr>
                <td><?php echo ++$sno;?></td>
                <td><?php echo $obj-> name;?></td>
                <td><?php echo $obj-> mobile;?></td>
            </tr> 
    <?php        
        }    
    }
    else
    {
        echo "<tr><td>No record found.</td></tr>";
    }
   ?> 
    </table>
  </div>


</div>
</fieldset> 




<style type="text/css">
.modal.and.carousel, .carousel-control 
{
  position: fixed; 
} 


.plot_photos
{
  height: 100px;
  width: 100px;
  border: 3px solid #ccc;
  text-align: center;
  border-radius: 3px;
}
</style>
<?php
if(isset($plot_photos) && !empty($plot_photos))
{    
?>
<div class="row">
<div class="col-md-12">
    <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Plot Photos</h3>
    </div>
    
    <div class="">

    <ul class="nav nav-pills nav-stacked">    
     <?php
        foreach($plot_photos as $k => $obj)
        {            
        ?>
          <li style="float: left; display:inline;">
            <a href="#lightbox" data-toggle="modal" data-slide-to="<?php echo $k;?>">
              <img src="<?php echo $obj->plot_photo_url;?>" alt="<?php echo $obj->plot_photo_name;?>" class="plot_photos">
            </a>
          </li>                
                
        <?php
        }
        ?>          
    </ul>
  
  <div class="modal fade and carousel slide" id="lightbox">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">          
          <div class="carousel-inner">
          <?php
        foreach($plot_photos as $k => $obj)
        {
            $class="";
            if($k == 0) $class="active";
        ?>  
              <div class="item <?php echo $class;?>">
                <img src="<?php echo $obj->plot_photo_url;?>" alt="<?php echo $obj->plot_photo_name;?>" style="text-align: center;">
                <!-- <div class="carousel-caption">
                  <p><?php //echo $obj->plot_photo_name;?></p>
                </div> -->
              </div>
        <?php
        }
        ?> 
            


          </div>
          <a class="left carousel-control" href="#lightbox" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="right carousel-control" href="#lightbox" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div><!-- /.modal-body -->
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div><!-- /.container -->

    
    
  </div>

</div>    
</div>


<?php
}
?>





</div>
</div>  
</div>
</div>
</section>
<script type="text/javascript">

</script>