<?php
$sarr = $darr = $tarr = $snm = $dnm = $yearly = $monthly = $sstate = $sttarget = array();


if(isset($sdaop) && !empty($sdaop))
{
  foreach($sdaop as $obj)
  {
    $snm[$obj-> state_id] = trim($obj-> state_name);
    $dnm[$obj-> district_id] = trim($obj-> district_name);
  }
}   


if(isset($counts) && !empty($counts))
{
  foreach($counts as $obj)
  {
    $obj-> quantity = number_format($obj-> quantity, '2', '.', '');

    /*if(isset($darr[$obj-> district_id][$obj-> type]) && !empty($darr[$obj-> district_id][$obj-> type]))
      $darr[$obj-> district_id][$obj-> type] = $darr[$obj-> district_id][$obj-> type] + $obj-> quantity;
    else
      $darr[$obj-> district_id][$obj-> type] = $obj-> quantity;*/

    if(isset($darr[$obj-> state_id][$obj-> district_id][$obj-> type]) && !empty($darr[$obj-> state_id][$obj-> district_id][$obj-> type]))
      $darr[$obj-> state_id][$obj-> district_id][$obj-> type] = $darr[$obj-> state_id][$obj-> district_id][$obj-> type] + $obj-> quantity;
    else
      $darr[$obj-> state_id][$obj-> district_id][$obj-> type] = $obj-> quantity;


    if(isset($sarr[$obj-> state_id][$obj-> type]) && !empty($sarr[$obj-> state_id][$obj-> type]))
      $sarr[$obj-> state_id][$obj-> type] = $sarr[$obj-> state_id][$obj-> type] + $obj-> quantity;
    else
      $sarr[$obj-> state_id][$obj-> type] = $obj-> quantity;

    
    if(isset($tarr[$obj-> type]) && !empty($tarr[$obj-> type]))
      $tarr[$obj-> type] = $tarr[$obj-> type] + $obj-> quantity;
    else
      $tarr[$obj-> type] = $obj-> quantity;
  }
}

//echo "<pre>"; print_r($darr); die;

$ack1 = $ack2 = $wstk = $rstk = $rpos = 0;
if(isset($tarr['ack1']) && !empty($tarr['ack1'])) $ack1 = $tarr['ack1'];
if(isset($tarr['ack2']) && !empty($tarr['ack2'])) $ack2 = $tarr['ack2'];
if(isset($tarr['wstk']) && !empty($tarr['wstk'])) $wstk = $tarr['wstk'];
if(isset($tarr['rstk']) && !empty($tarr['rstk'])) $rstk = $tarr['rstk'];
if(isset($tarr['rpos']) && !empty($tarr['rpos'])) $rpos = $tarr['rpos'];
?>


<div class="row">
<div class="col-md-3 col-sm-6 col-xs-12" style="width: 20% !important;">
  <div class="info-box">
    <a href="<?php echo base_url();?>admin/inventory_acknowledgement1">
    <span class="info-box-icon bg-aqua"><img src="<?php echo base_url();?>assets/images/dashboard/ack1.jpeg"></span>

    <div class="info-box-content">
      <span class="info-box-text">1<small><sup>st</sup></small> Point Ack (MT)</span>
      <span class="info-box-number"><?php echo $ack1;?></span>
    </div>
    </a>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12" style="width: 20% !important;">
  <div class="info-box">
    <a href="<?php echo base_url();?>admin/inventory_acknowledgement2">
    <span class="info-box-icon bg-red"><img src="<?php echo base_url();?>assets/images/dashboard/ack2.jpeg"></span>

    <div class="info-box-content">
      <span class="info-box-text">2<small><sup>nd</sup></small> Point Ack (MT)</span>
      <span class="info-box-number"><?php echo $ack2;?></span>
    </div>
    </a>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>

<div class="col-md-3 col-sm-6 col-xs-12" style="width: 20% !important;">
  <div class="info-box">
    <a href="<?php echo base_url();?>admin/inventory_wholesaler">
    <span class="info-box-icon bg-green"><img src="<?php echo base_url();?>assets/images/dashboard/wstock.jpeg"></span>

    <div class="info-box-content">
      <span class="info-box-text">Wholesaler Stock (MT)</span>
      <span class="info-box-number"><?php echo $wstk;?></span>
    </div>
  </a>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12" style="width: 20% !important;">
  <div class="info-box">
    <a href="<?php echo base_url();?>admin/inventory_retailer">
    <span class="info-box-icon bg-yellow"><img src="<?php echo base_url();?>assets/images/dashboard/rstock.jpeg"></span>

    <div class="info-box-content">
      <span class="info-box-text">Retailer Stock (MT)</span>
      <span class="info-box-number"><?php echo $rstk;?></span>
    </div>
  </a>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>


<div class="col-md-3 col-sm-6 col-xs-12" style="width: 20% !important;">
  <div class="info-box">
    <a href="<?php echo base_url();?>admin/inventory_retailer_pos">
    <span class="info-box-icon bg-purple"><img src="<?php echo base_url();?>assets/images/dashboard/ack1.jpeg"></span>

    <div class="info-box-content">
      <span class="info-box-text">Retailer POS Sale (MT)</span>
      <span class="info-box-number"><?php echo $rpos;?></span>
    </div>
  </a>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
</div>




<div class="row">
<?php
$bgcolor = array("green", "blue", "red", "yellow", "pink", "aqua");
$sno = 0; 
foreach($snm as $id => $nm)
{
  if(isset($sarr[$id]) && !empty($sarr[$id]))
  {
    $bgc = $bgcolor[$sno]; $sno++;
    $ack1 = $ack2 = $wstk = $rstk = $rpos = 0;
    if(isset($sarr[$id]['ack1']) && !empty($sarr[$id]['ack1'])) $ack1 = $sarr[$id]['ack1'];
    if(isset($sarr[$id]['ack2']) && !empty($sarr[$id]['ack2'])) $ack2 = $sarr[$id]['ack2'];
    if(isset($sarr[$id]['wstk']) && !empty($sarr[$id]['wstk'])) $wstk = $sarr[$id]['wstk'];
    if(isset($sarr[$id]['rstk']) && !empty($sarr[$id]['rstk'])) $rstk = $sarr[$id]['rstk'];
    if(isset($sarr[$id]['rpos']) && !empty($sarr[$id]['rpos'])) $rpos = $sarr[$id]['rpos'];
?>  
<div class="col-md-4">
<div class="box box-widget widget-user-2">
<div class="widget-user-header bg-<?php echo $bgc;?>">
  <h4 class=""><?php echo $nm;?></h4>
</div>
<div class="box-footer no-padding">
  <ul class="nav nav-stacked">
    <li><a href="<?php echo base_url();?>admin/inventory_acknowledgement1/<?php echo $id;?>/0">1st Point Ack <span class="pull-right badge bg-blue"><?php echo $ack1;?></span></a></li>
    
    <li><a href="<?php echo base_url();?>admin/inventory_acknowledgement2/<?php echo $id;?>/0">2nd Point Ack <span class="pull-right badge bg-aqua"><?php echo $ack2;?></span></a></li>

    <li><a href="<?php echo base_url();?>admin/inventory_wholesaler/<?php echo $id;?>/0">Wholesaler Stock <span class="pull-right badge bg-green"><?php echo $wstk;?></span></a></li>
    
    <li><a href="<?php echo base_url();?>admin/inventory_retailer/<?php echo $id;?>/0">Retailer Stock <span class="pull-right badge bg-red"><?php echo $rstk;?></span></a></li>

    <li><a href="<?php echo base_url();?>admin/inventory_retailer_pos/<?php echo $id;?>/0">Retailer POS Sale <span class="pull-right badge bg-purple"><?php echo $rpos;?></span></a></li>
  </ul>
</div>
</div>
</div>
<?php
  }
}
?>
</div>




<div class="row">
<div class="col-lg-12">
<div class="box">
<div class="box-body">
<?php
foreach($snm as $stid => $stnm)
{
?>    
    <fieldset>
      <legend><?php echo $stnm;?></legend>
<?php
    foreach($dnm as $id => $nm)
    {
      if(isset($darr[$stid][$id]) && !empty($darr[$stid][$id]))
      {       
          $ack1 = $ack2 = $wstk = $rstk = $rpos = 0;
          
          if(isset($darr[$stid][$id]['ack1']) && !empty($darr[$stid][$id]['ack1'])) $ack1 = $darr[$stid][$id]['ack1'];
          
          if(isset($darr[$stid][$id]['ack2']) && !empty($darr[$stid][$id]['ack2'])) $ack2 = $darr[$stid][$id]['ack2'];
          
          if(isset($darr[$stid][$id]['wstk']) && !empty($darr[$stid][$id]['wstk'])) $wstk = $darr[$stid][$id]['wstk'];
          
          if(isset($darr[$stid][$id]['rstk']) && !empty($darr[$stid][$id]['rstk'])) $rstk = $darr[$stid][$id]['rstk'];

          if(isset($darr[$stid][$id]['rpos']) && !empty($darr[$stid][$id]['rpos'])) $rpos = $darr[$stid][$id]['rpos'];
    ?>  
        <div class="col-md-3">
            <h6><?php echo $nm;?></h6>
            
            <span class="text-muted"><a href="<?php echo base_url();?>admin/inventory_acknowledgement1/<?php echo $stid;?>/<?php echo $id;?>"><?php echo $ack1;?></a>&nbsp;|&nbsp;</span>
            
            <span class="text-muted"><a href="<?php echo base_url();?>admin/inventory_acknowledgement2/<?php echo $stid;?>/<?php echo $id;?>"><?php echo $ack2;?></a>&nbsp;|&nbsp;</span>
            
            <span class="text-muted"><a href="<?php echo base_url();?>admin/inventory_wholesaler/<?php echo $stid;?>/<?php echo $id;?>"><?php echo $wstk;?></a>&nbsp;|&nbsp;</span>
            
            <span class="text-muted"><a href="<?php echo base_url();?>admin/inventory_retailer/<?php echo $stid;?>/<?php echo $id;?>"><?php echo $rstk;?></a>&nbsp;|&nbsp;</span>

            <span class="text-muted"><a href="<?php echo base_url();?>admin/inventory_retailer_pos/<?php echo $stid;?>/<?php echo $id;?>"><?php echo $rpos;?></a></span>      
        </div>
    <?php
      }
    }
?>
    </fieldset>
<?php    
}    
?>
</div>
</div>
</div>
</div>