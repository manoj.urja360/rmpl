<section class="content-header">
<h1>
Import Farmers
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">
<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers_add';" data-toggle="tab" aria-expanded="false">Add New</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers_quick';" data-toggle="tab" aria-expanded="false">Quick Registered Farmers</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers_import';" data-toggle="tab" aria-expanded="false">Import</a></li>
</ul>



<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post" enctype="multipart/formdata">

<div class="row">
  <div class="form-group">          
    <div class="col-sm-4">
        <label><?php echo MANDATORY;?>Import for State</label>
        <select name="farmer_state" id="farmer_state" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'farmer_district');" style="width: 100%;">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-4">
      <label><?php echo MANDATORY;?>District Name</label>
        <select name="farmer_district" id="farmer_district" class="form-control select2" onchange="get_dd_list(this.value, 'taluka_by_district', 'farmer_taluka');" style="width: 100%;"></select>
    </div>


    <div class="col-sm-4">
      <label><?php echo MANDATORY;?>Taluka Name</label>
      <select name="farmer_taluka" id="farmer_taluka" class="form-control select2" style="width: 100%;"></select>
    </div>
   
  </div>
</div>      



<div class="row">
  <div class="form-group">          
    <div class="col-sm-6">
      <label for="upload_file"><?php echo MANDATORY;?>Select Excel File</label>
      <input class="form-control" name="upload_file" id="upload_file" type="file" accept=".xls">
      <span class="help_txt">.xls file type is allowed.</span>
    </div>    

      <div class="col-sm-6">
        <small>
        <ol>
          <li>Steps:</li>
          <li>Download xls file <a href="<?php echo base_url();?>assets/sample/farmers.xls">click here</a>.</li>
          <li>Fill the data according to given format.</li>
          <li>Now click on left side browse button and select file.</li>
          <li>Click on Save button.</li>
        </ol>
        </small>
      </div>
  </div>
</div> 


<br/>
<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/farmers';" class="btn btn-default btn_process">Cancel</button>
      <input name="upload_type" value="import_farmers" type="hidden">
    </div>
  </div> 
</div>

</form>
</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">

$(document).ready(function()
{
    $(".select2").select2();

   
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/upload_data",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>