<section class="content-header">
<h1>
Upload Sales
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">
<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sales_report';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/upload_sales';" data-toggle="tab" aria-expanded="false">Upload New</a></li>
</ul>


<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post" enctype="multipart/formdata">

<div class="row">
  <div class="form-group">        
    <div class="col-sm-3"><label>Last Uploaded File:</label></div>
    <div class="col-sm-3"><label>
        <?php 
        if(isset($details-> upload_from_date) && !empty($details-> upload_from_date) && isset($details-> upload_to_date) && !empty($details-> upload_to_date)) 
        { 
            echo "From Date: <b>".date(DATE, strtotime($details-> upload_from_date))."</b>";
        }
        ?></label></div>
     <div class="col-sm-4"><label>
        <?php 
        if(isset($details-> upload_from_date) && !empty($details-> upload_from_date) && isset($details-> upload_to_date) && !empty($details-> upload_to_date)) 
        { 
            echo "To Date: <b>".date(DATE, strtotime($details-> upload_to_date))."</b>";
        }
        ?></label>
    </div>
  </div>
</div>

<div class="row">
<div class="form-group">
<div class="col-sm-6">
<ol>
  <li>Steps:</li>
  <li>Download xls file <a href="<?php echo base_url();?>assets/sample/upload_sales.xls">click here</a>.</li>
  <li>Fill the data according to given format.</li>
  <li>Now click on left side browse button and select file.</li>
  <li>Click on Save button.</li>
</ol>
</div> 
</div>
</div>


<div class="row">
  <div class="form-group">        
    <div class="col-sm-6">
      <label for="cstates_name"><?php echo MANDATORY;?>Select State Name</label>
      <select name="cstates_name" id="cstates_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'ccity_name');">
        <option value="">Select</option>
        <?php
          foreach($states_all as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-6">
      <label for="ccity_name"><?php echo MANDATORY;?>Select District Name</label>
      <select name="ccity_name" id="ccity_name" class="form-control select2"></select>
    </div>
  </div>
</div>


<div class="row">
  <div class="form-group">
    <div class="col-sm-6">
      <label for="upload_from_date"><?php echo MANDATORY;?>Uploading From Date</label>
      <input class="form-control" name="upload_from_date" id="upload_from_date" type="text" maxlength="50" value="<?php echo date('01-m-Y');?>" readonly>  
    </div>

    <div class="col-sm-6">
      <label for="upload_to_date"><?php echo MANDATORY;?>Uploading To Date</label>
      <input class="form-control" name="upload_to_date" id="upload_to_date" type="text" maxlength="50" value="<?php echo date('d-m-Y');?>" readonly>  
    </div>
  </div>
</div>

  

<div class="row">
  <div class="form-group">          
    <div class="col-sm-6">
      <label for="upload_file"><?php echo MANDATORY;?>Select File</label>
      <input class="form-control" name="upload_file" id="upload_file" type="file" accept=".xls,.csv">
      <span class="help_txt">.xls, .csv file types are allowed.</span>
    </div>  
  </div>
</div>


<br/>
<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/sales_report';" class="btn btn-default btn_process">Cancel</button>
      <input name="upload_type" value="upload_sales" type="hidden">
    </div>
  </div> 
</div>

</form>
</div>
</div>  
</div>
</div>
</section>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.js"></script>
<script type="text/javascript">

$(document).ready(function()
{
    var dateFormat = "dd-mm-yy",
      from = $( "#upload_from_date" )
        .datepicker({
          defaultDate: "+1m",
          changeMonth: true,
          changeYear: true,
          numberOfMonths: 1,
          dateFormat: "dd-mm-yy",
          maxDate:0
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#upload_to_date" ).datepicker({
        defaultDate: "+1m",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: "dd-mm-yy",
        maxDate:0
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
      
    
    $(".select2").select2();


    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/upload_data",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    //window.location.href = base_url+'admin/sales_report'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>