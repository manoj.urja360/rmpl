<section class="content-header">
<h1>
Edit Your Profile
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">

<?php
if($this->user_role_id == 1 && $this->user_id == 1)
{
?>
  <ul class="nav nav-tabs">
  <li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/profile';" data-toggle="tab" aria-expanded="false">Company Profile</a></li>

  <li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/profile_user';" data-toggle="tab" aria-expanded="false">Your Profile</a></li>
  </ul>
<?php
}
?>

<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<input name="hdn_id" id="hdn_id" value="<?php echo $details-> user_id;?>" type="hidden">

<div class="row">
  <div class="form-group">        
    <div class="col-sm-2">
      <img class="img-circle" src="<?php echo base_url().$details->user_photo_url;?>?t=<?php echo time();?>" width="45px" height="45px">
    </div>

    <div class="col-sm-10">      
      <label for="user_photo">Upload New Photo</label>
      <input class="form-control" name="user_photo" value="" type="file">
      <input name="user_photo_url" value="<?php echo $details->user_photo_url;?>" type="hidden">
    </div>
</div>
</div>

<br/>


<div class="row"><h4>General Details</h4>
  <div class="form-group">        
    <div class="col-sm-3">      
      <label for="first_name"><?php echo MANDATORY;?>First Name</label>
      <input class="form-control" name="first_name" value="<?php echo $details-> first_name;?>" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="last_name"><?php echo MANDATORY;?>Last Name</label>
      <input class="form-control" name="last_name" value="<?php echo $details-> last_name;?>" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="email"><?php echo MANDATORY;?>Email</label>
      <input class="form-control" name="email" value="<?php echo $details-> email;?>" type="email" maxlength="150">
    </div>

    <div class="col-sm-3">
      <label for="mobile"><?php echo MANDATORY;?>Mobile</label>
      <input class="form-control" name="mobile" value="<?php echo $details-> mobile;?>" type="text" maxlength="10">
    </div>
  </div>
</div> 



<div class="row"><h4>Current Address</h4>
  <div class="form-group">    
    <div class="col-sm-6">
      <label for="cstates_name"><?php echo MANDATORY;?>Select State Name</label>
      <select name="cstates_name" id="cstates_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'ccity_name');">
        <option value="">Select</option>
        <?php
          foreach($states_all as $obj)
          {
              $sel = "";
              if($obj-> state_id == $details-> cstate)
              {
                  $sel = "selected";                  
              }
          ?>    
            <option value="<?php echo $obj->state_id;?>" <?php echo $sel;?>><?php echo $obj->state_name;?></option>

            <?php
            if($sel != "")
            {
            ?>
              <script type="text/javascript">                
                get_dd_list(<?php echo $details->cstate;?>, 'district_by_state_aop_dd', 'ccity_name');
              </script>
          <?php
            }
          }
        ?>
       </select>
    </div>


    <div class="col-sm-6">
      <label for="ccity_name"><?php echo MANDATORY;?>Select City Name</label>
      <select name="ccity_name" id="ccity_name" class="form-control select2"></select>
    </div>
  </div>  

  <div class="form-group"> 
    <div class="col-sm-6">
      <label for="caddress"><?php echo MANDATORY;?>Address 1</label>
      <input class="form-control" name="caddress" id="caddress" value="<?php echo $details-> caddress;?>" type="text" maxlength="255">
    </div> 


    <div class="col-sm-6">
      <label for="caddress2">Address 2</label>
      <input class="form-control" name="caddress2" id="caddress2" value="<?php echo $details-> caddress2;?>" type="text" maxlength="255">
    </div> 
    
  </div>
</div>


<div class="row"><h4><input type="checkbox" value="1" onclick="set_address();">&nbsp;Permanent Address</h4>
  <div class="form-group">    
    <div class="col-sm-6">
      <label for="pstates_name"><?php echo MANDATORY;?>Select State Name</label>
      <select name="pstates_name" id="pstates_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'pcity_name');">
        <option value="">Select</option>
        <?php
          foreach($states_all as $obj)
          {
              $sel = "";
              if($obj-> state_id == $details-> pstate)
              {
                  $sel = "selected";                  
              }
          ?>    
            <option value="<?php echo $obj->state_id;?>" <?php echo $sel;?>><?php echo $obj->state_name;?></option>

            <?php
            if($sel != "")
            {
            ?>
              <script type="text/javascript">                
                get_dd_list(<?php echo $details->pstate;?>, 'district_by_state_aop_dd', 'pcity_name');
              </script>
          <?php
            }
          }
        ?>
       </select>
    </div>


    <div class="col-sm-6">
      <label for="pcity_name"><?php echo MANDATORY;?>Select City Name</label>
      <select name="pcity_name" id="pcity_name" class="form-control select2"></select>
    </div>
  </div>  


  <div class="form-group">
    <div class="col-sm-6">
      <label for="caddress"><?php echo MANDATORY;?>Address 1</label>
      <input class="form-control" name="paddress" id="paddress" value="<?php echo $details-> paddress;?>" type="text" maxlength="255">
    </div>

    <div class="col-sm-6">
      <label for="paddress2">Address 2</label>
      <input class="form-control" name="paddress2" id="paddress2" value="<?php echo $details-> paddress2;?>" type="text" maxlength="255">
    </div>   
    
  </div>
</div>


<div class="row">
  <div class="form-group">  
    <div class="col-sm-3">
      <label for="dob">Date Of Birth</label>
      <input class="form-control" name="dob" id="dob" value="<?php echo date(DATE_INPUT, strtotime($details-> dob));?>" type="text" maxlength="50">
    </div>

    <div class="col-sm-3">
      <label for="qualification">Highest Qualification</label>
      <input class="form-control" name="qualification" value="<?php echo $details-> qualification;?>" type="text" maxlength="255">
    </div>

    <div class="col-sm-3">
      <label for="experience">Total Experience</label>
      <input class="form-control" name="experience" value="<?php echo $details-> experience;?>" type="text" maxlength="100">
    </div>
  </div>
</div>




<div class="row btn_row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin';" class="btn btn-default btn_process">Cancel</button>
      
    </div>
  </div> 
</div> 



</form>
</div>
</div>  
</div>
</div>
</section>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.js"></script>
<script type="text/javascript">

function set_address()
{        
    $("#paddress").val($("#caddress").val());
    $("#paddress2").val($("#caddress2").val());

    $("#pstates_name").val($("#cstates_name").val());
    $("#pstates_name").trigger("change");

    setTimeout(function()
    {      
      $("#pcity_name").val($("#ccity_name").val());
      $("#pcity_name").select2().trigger("chosen:updated");
    }, "1000");  
}

$(document).ready(function()
{
    setTimeout(function()
    {
      $("#ccity_name").val("<?php echo $details->ccity;?>");
      $("#pcity_name").val("<?php echo $details->pcity;?>");

      $("#ccity_name").select2().trigger("chosen:updated");
      $("#pcity_name").select2().trigger("chosen:updated");
    }, "1500");

    $("#states_name").trigger("change");

    $("#dob, #doj").datepicker({changeMonth:true, 
      changeYear:true, 
      maxDate:0, 
      dateFormat:'dd-mm-yy'
    });


    $(".select2").select2();


    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/profile_user_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/profile_user'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>