<?php
$udata = $this->common_model->get_detail(array("typ" => "users", "id" => 1));
$hd_address = $hd_contact = "";
$compname = PROJECT_NAME;
if(isset($udata) && !empty($udata) && isset($udata-> company_details) && !empty($udata-> company_details))
{
    $carr = json_decode($udata-> company_details);
    $carr = (array)$carr;
    $hd_address = $carr['caddress'].", ".$carr['ccity_name'].", ".$carr['cstates_name']."<br/>";
    $hd_contact = $carr['mobile'].", ".$carr['email'];

    $compname = $carr['company_name'];
}


?>

<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title><?php echo $compname;?></title><style type="text/css">table,tr,td{font-size:10px !important; border-collapse:collapse; } td { font-family: freeserif; } .table, .table tr, .table td, .table th {border:1px solid #bbb; padding:5px; color:#222;}</style></head>


<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; font-family: 'Noto Sans', sans-serif;  font-weight: 400;
font-size: 15px;  line-height: 1.8;  color: #6a6a6a; margin: 0 auto !important; padding: 0 !important; height: 100% !important; width: 100% !important; background: #f1f1f1;">


<center style="width: 100%; background-color: #fff;">
<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
</div>
<div style="max-width: 600px; margin: 0 auto;" class="email-container">
<!-- BEGIN BODY -->
<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto; border: 1px solid rgba(144,144,144,0.6); background: #f7f7f7; border-spacing: 0 !important; border-collapse: collapse !important; table-layout: fixed !important; margin: 0 auto !important;">
<tr>
<td valign="top" style="padding: 0.5em 2.5em 0 2.5em; border-bottom: 1px solid rgba(0,0,0,.10); color: rgba(0,0,0,.5);mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">


<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">

<tr>
	<td>
		<img src="<?php echo base_url();?>assets/images/logo.png" width="70px" height="70px" alt="<?php echo $compname;?>">
	</td>
	<td><h2><?php echo $compname;?></h2></td>
</tr>
</table>

</td>
</tr><!-- header end tr -->




<td class="email-section" style="padding: 0; width: 100%; background: #fff; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
<tr>
<td valign="middle" width="100%" style="mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style=" border-spacing: 0 !important; border-collapse: collapse !important; table-layout: fixed !important; margin: 0 auto !important;">
<tr>
<td class="text-services" style="text-align: left; padding: 20px 30px; color:#6a6a6a; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">

<?php echo $Content;?>

</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr><!-- middle end: tr -->



<tr>
<td valign="middle" width="100%" align="center" style="border-top: 1px solid rgba(0,0,0,.10);  color: rgba(0,0,0,.5); background: #f7f7f7; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
<table width="100%" align="center" style=" border-spacing: 0 !important; border-collapse: collapse !important; table-layout: fixed !important; margin: 0 auto !important; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">

<tr style="text-align: center;">
<td valign="middle" width="100%" style="padding-top: 20px; text-align: center; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
</td>
</tr>


<tr>
	<td align="center" valign="middle" width="100%" style="text-align: center; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
		<?php echo $compname;?><br/><?php echo $hd_address.$hd_contact;?><br/><?php echo $rname;?>
	</td>
</tr>
</table>
</td>
</tr><!-- footer end: tr -->
</table>

</div>
</center>
</body>
</html>