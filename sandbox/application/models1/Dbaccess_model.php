<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dbaccess_model extends CI_Model 
{
    
    public function __construct()
    {
        parent::__construct();
    }


    public function executeOnlySql($sql)
    {
        $query = $this->db->query($sql);        
    }


    public function executeSql($sql)
    {
        $query = $this->db->query($sql);

        return $query->result();
    }

    public function insertSql($sql)
    {
        $this->db->query($sql);

        return $this->db->insert_id();
    }

    
    public function insert($table, $data)
    {
        $this->db->insert($table, $data);

        return $this->db->insert_id();
    }
    

    public function update($table,$data,$condition)
    {
        $this->db->update($table, $data, $condition);

        return $this->db->affected_rows();
    }
    
    
    public function is_exist($sql)
    {
        $query = $this->db->query($sql);

        $data = $query->row();

        if(isset($data) && !empty($data))
        {
            return $data-> id;
        }
        else
        {
            return 0;
        }
    }  

}
?>