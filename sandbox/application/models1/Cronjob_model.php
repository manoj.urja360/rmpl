<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cronjob_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	function triggerSMS()
	{
		$sql = "SELECT QueueID, Content, SendTo
		FROM cronjobs_queue	
		WHERE IsSent = 0 AND SentType = 'sms'				
		ORDER BY QueueID 
		LIMIT 3
		";			

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{						
			$QueueIDArr = array();

			foreach($Query->result_array() as $counter => $Record)
			{
				$QueueID = trim($Record['QueueID']);
				$Content = trim($Record['Content']);
				$SendTo = trim($Record['SendTo']);

				$Content = substr($Content, 0, 160);					

				++$counter;
				$SendTo = "9993593886";
				$Content = "Job Post Message No = ".$counter;

				sendSMS(array("PhoneNumber" => $SendTo, "Message" => $Content));

				$data = array("IsSent" =>	1);
				$this->db->where("QueueID", $QueueID);
				$this->db->update('cronjobs_queue', $data);

				$chk = "";
				$chk = $this->db->affected_rows();				
				if(isset($chk) && !empty($chk))
				{
					$QueueIDArr[] = $QueueID;
				}
			}


			if(isset($QueueIDArr) && !empty($QueueIDArr))
			{
				$ids = implode(",", $QueueIDArr);

				$sql = "DELETE 
				FROM cronjobs_queue	
				WHERE QueueID IN ($ids) AND IsSent = 1 AND SentType = 'sms'	";			

				$this->db->query($sql);
			}				
		}		

		return true;			
	}


	function triggerEmail()
	{
		$sql = "SELECT QueueID, Subject, Content, SendTo
		FROM cronjobs_queue	
		WHERE IsSent = 0 AND SentType = 'email'				
		ORDER BY QueueID ";			

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{						
			$QueueIDArr = array();

			foreach($Query->result_array() as $Record)
			{
				$QueueID = trim($Record['QueueID']);
				$Subject = trim($Record['Subject']);
				$Content = trim($Record['Content']);
				$SendTo = trim($Record['SendTo']);					

				$SendMail = sendMail(array(
				'emailTo' 		=> $SendTo,			
				'emailSubject'	=> $Subject,
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $Content),TRUE))
				));

				$data = array("IsSent" =>	1);
				$this->db->where("QueueID", $QueueID);
				$this->db->update('cronjobs_queue', $data);

				$chk = "";
				$chk = $this->db->affected_rows();				
				if(isset($chk) && !empty($chk))
				{
					$QueueIDArr[] = $QueueID;
				}
			}


			if(isset($QueueIDArr) && !empty($QueueIDArr))
			{
				$ids = implode(",", $QueueIDArr);

				$sql = "DELETE 
				FROM cronjobs_queue	
				WHERE QueueID IN ($ids) AND IsSent = 1 AND SentType = 'email'	";			

				$this->db->query($sql);
			}				
		}		

		return true;			
	}


	function get_template_content($template_type, $template_for)
	{
		if($template_type == 2)
		{			
			$sql = "SELECT t.*, s.signature_name, s.signature_from, s.signature_content  
			FROM templates t
			LEFT JOIN email_signature s ON t.email_signature_id = s.email_signature_id AND s.is_deleted = 0		
			WHERE t.template_type = '$template_type' AND t.template_for = '$template_for' AND t.is_default = 1
			LIMIT 1	";			
		}
		else
		{
			$sql = "SELECT t.*
			FROM templates t
			WHERE t.template_type = '$template_type' AND t.template_for = '$template_for' AND t.is_default = 1
			LIMIT 1	";			
		}	

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			$data = $Query->row();

			return $data; die;
		}

		return array();
	}
	

	//Alert for acknowledgement 1-------------------------------------------------
	function alert_for_ack1_due($file_id)
	{
		$SMS_template = "";
		$EML_template = "";
		
		$sql = "SELECT ss.first_name, ss.last_name, ss.mobile, ss.email, ss.state_id, ss.district_id, date(s.created_date) as created_date
            FROM upload_acknowledgement1 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id            
            WHERE s.file_id = '$file_id'";		

		$MainQuery = $this->db->query($sql);

		if($MainQuery->num_rows()>0)
		{
			$sms_template = $this-> get_template_content(1,1);
			$email_template = $this-> get_template_content(2,1);

			//echo "<pre>"; print_r($sms_template); print_r($email_template);

			$email_template_content_def = "Dear WHOLESALER_NAME, Acknowledgement due over NUMBER_X days.";
			$sms_template_content_def = "Dear WHOLESALER_NAME, Acknowledgement due over NUMBER_X days.";
			$sms_template_days = $email_template_days = 0;
			$sms_template_copy_to_arr = $email_template_copy_to_arr = array();
			if(isset($sms_template) && !empty($sms_template))
			{
				$sms_template_content = trim($sms_template-> content);
				$sms_template_copy_to = trim($sms_template-> copy_to);
				$sms_template_days = trim($sms_template-> days);
				if(isset($sms_template_copy_to) && !empty($sms_template_copy_to))
				{
					$sms_template_copy_to_arr = explode(",", $sms_template_copy_to);
				}
			}
			

			if(!isset($sms_template_content) || empty($sms_template_content))
			{
				$sms_template_content = $sms_template_content_def;
			}


			$signature_name = "RMPCL";
			$signature_from = "RMPCL";
			$signature_content = "Regards<br/>RMPCL";
			if(isset($email_template) && !empty($email_template))
			{
				$email_template_content = trim($email_template-> content);
				$email_template_copy_to = trim($email_template-> copy_to);
				$email_template_days = trim($email_template-> days);

				$signature_name = trim($email_template-> signature_name);
				$signature_from = trim($email_template-> signature_from);
				$signature_content = trim($email_template-> signature_content);

				if(isset($email_template_copy_to) && !empty($email_template_copy_to))
				{
					$email_template_copy_to_arr = explode(",", $email_template_copy_to);
				}
			}
			

			if(!isset($email_template_content) || empty($email_template_content))
			{
				$email_template_content = $email_template_content_def;
			}


			$curdate = date("Y-m-d");

			foreach($MainQuery->result_array() as $MainRecord)
			{
				$full_name = ucwords($MainRecord['first_name']." ".$MainRecord['last_name']);				
				$mobile = $MainRecord['mobile'];
				$email = $MainRecord['email'];
				$state_id = $MainRecord['state_id'];
				$district_id = $MainRecord['district_id'];
				$created_date = $MainRecord['created_date'];				
				
				$EmailContent = $email_template_content;
				$SmsContent = $sms_template_content;

				$diff = "";
				$diff = date_difference($curdate, $created_date);
				
				//SMS------------------------------------------
				echo "<br/>SMS===$diff===$sms_template_days===$mobile";
				if($diff > $sms_template_days)
				{
					$SmsContent = str_replace("WHOLESALER_NAME", $full_name, $SmsContent);
					$SmsContent = str_replace("NUMBER_X", $diff, $SmsContent);						
				
					$data = array(
					"Subject" =>	"Acknowledgement 1 Due - RMPCL",				
					"Content" =>	$SmsContent,
					"SendTo" => 	trim($mobile),
					"SentType" => 	'sms' );

					$this->db->insert('cronjobs_queue', $data);	
					
					if(isset($sms_template_copy_to_arr) && !empty($sms_template_copy_to_arr))
					{
						foreach($sms_template_copy_to_arr as $copyto)
						{
							$this->send_copy('sms', $SmsContent, $copyto, $state_id, $district_id, "Acknowledgement 1 Due - RMPCL");
						}
					}			
				}



				//Email-----------------------------------------
				echo "<br/>Email===$diff===$email_template_days===$email";
				if($diff > $email_template_days && isset($email) && !empty($email))
				{
					$EmailContent = str_replace("WHOLESALER_NAME", $full_name, $EmailContent);
					$EmailContent = str_replace("NUMBER_X", $diff, $EmailContent);						
					$EmailContent = $EmailContent."<br/>".$signature_content;

					$data = array(
					"Subject" =>	"Acknowledgement 1 Due - RMPCL",				
					"Content" =>	$EmailContent,
					"SendTo" => 	trim($email),
					"SentType" => 	'email' );

					$this->db->insert('cronjobs_queue', $data);	

					if(isset($email_template_copy_to_arr) && !empty($email_template_copy_to_arr))
					{
						foreach($email_template_copy_to_arr as $copyto)
						{
							$this->send_copy('email', $EmailContent, $copyto, $state_id, $district_id, "Acknowledgement 1 Due - RMPCL");
						}
					}			
				}
			}					
		}		

		return true;			
	}



	//Alert for acknowledgement 2-------------------------------------------------
	function alert_for_ack2_due($file_id)
	{
		$SMS_template = "";
		$EML_template = "";
		
		$sql = "SELECT ss.first_name, ss.last_name, ss.mobile, ss.email, ss.state_id, ss.district_id, date(s.created_date) as created_date
            FROM upload_acknowledgement2 s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2            
            WHERE s.file_id = '$file_id'";		

		$MainQuery = $this->db->query($sql);

		if($MainQuery->num_rows()>0)
		{
			$sms_template = $this-> get_template_content(1,3);
			$email_template = $this-> get_template_content(2,3);

			//echo "<pre>"; print_r($sms_template); print_r($email_template);

			$email_template_content_def = "Dear RETAILER_NAME, Acknowledgement due over NUMBER_X days.";
			$sms_template_content_def = "Dear RETAILER_NAME, Acknowledgement due over NUMBER_X days.";
			$sms_template_days = $email_template_days = 0;
			$sms_template_copy_to_arr = $email_template_copy_to_arr = array();
			if(isset($sms_template) && !empty($sms_template))
			{
				$sms_template_content = trim($sms_template-> content);
				$sms_template_copy_to = trim($sms_template-> copy_to);
				$sms_template_days = trim($sms_template-> days);
				if(isset($sms_template_copy_to) && !empty($sms_template_copy_to))
				{
					$sms_template_copy_to_arr = explode(",", $sms_template_copy_to);
				}
			}
			

			if(!isset($sms_template_content) || empty($sms_template_content))
			{
				$sms_template_content = $sms_template_content_def;
			}


			$signature_name = "RMPCL";
			$signature_from = "RMPCL";
			$signature_content = "Regards<br/>RMPCL";
			if(isset($email_template) && !empty($email_template))
			{
				$email_template_content = trim($email_template-> content);
				$email_template_copy_to = trim($email_template-> copy_to);
				$email_template_days = trim($email_template-> days);

				$signature_name = trim($email_template-> signature_name);
				$signature_from = trim($email_template-> signature_from);
				$signature_content = trim($email_template-> signature_content);

				if(isset($email_template_copy_to) && !empty($email_template_copy_to))
				{
					$email_template_copy_to_arr = explode(",", $email_template_copy_to);
				}
			}
			

			if(!isset($email_template_content) || empty($email_template_content))
			{
				$email_template_content = $email_template_content_def;
			}


			$curdate = date("Y-m-d");

			foreach($MainQuery->result_array() as $MainRecord)
			{
				$full_name = ucwords($MainRecord['first_name']." ".$MainRecord['last_name']);				
				$mobile = $MainRecord['mobile'];
				$email = $MainRecord['email'];
				$state_id = $MainRecord['state_id'];
				$district_id = $MainRecord['district_id'];
				$created_date = $MainRecord['created_date'];				
				
				$EmailContent = $email_template_content;
				$SmsContent = $sms_template_content;

				$diff = "";
				$diff = date_difference($curdate, $created_date);
				
				//SMS------------------------------------------
				echo "<br/>SMS===$diff===$sms_template_days===$mobile";
				if($diff > $sms_template_days)
				{
					$SmsContent = str_replace("RETAILER_NAME", $full_name, $SmsContent);
					$SmsContent = str_replace("NUMBER_X", $diff, $SmsContent);						
				
					$data = array(
					"Subject" =>	"Acknowledgement 2 Due - RMPCL",				
					"Content" =>	$SmsContent,
					"SendTo" => 	trim($mobile),
					"SentType" => 	'sms' );

					$this->db->insert('cronjobs_queue', $data);	
					
					if(isset($sms_template_copy_to_arr) && !empty($sms_template_copy_to_arr))
					{
						foreach($sms_template_copy_to_arr as $copyto)
						{
							$this->send_copy('sms', $SmsContent, $copyto, $state_id, $district_id, "Acknowledgement 2 Due - RMPCL");
						}
					}			
				}



				//Email-----------------------------------------
				echo "<br/>Email===$diff===$email_template_days===$email";
				if($diff > $email_template_days && isset($email) && !empty($email))
				{
					$EmailContent = str_replace("RETAILER_NAME", $full_name, $EmailContent);
					$EmailContent = str_replace("NUMBER_X", $diff, $EmailContent);						
					$EmailContent = $EmailContent."<br/>".$signature_content;

					$data = array(
					"Subject" =>	"Acknowledgement 2 Due - RMPCL",				
					"Content" =>	$EmailContent,
					"SendTo" => 	trim($email),
					"SentType" => 	'email' );

					$this->db->insert('cronjobs_queue', $data);	

					if(isset($email_template_copy_to_arr) && !empty($email_template_copy_to_arr))
					{
						foreach($email_template_copy_to_arr as $copyto)
						{
							$this->send_copy('email', $EmailContent, $copyto, $state_id, $district_id, "Acknowledgement 2 Due - RMPCL");
						}
					}			
				}
			}					
		}		

		return true;			
	}


	public function send_copy($senttype, $content, $copyto, $state_id, $district_id, $subject)
	{		
		if($copyto == "Management")
		{
			$udata = $this->common_model->get_detail(array("typ" => "users", "id" => 1));
		    if(isset($udata) && !empty($udata) && isset($udata-> company_details) && !empty($udata-> company_details))
		    {
		        $carr = json_decode($udata-> company_details);
		        $carr = (array)$carr;
		        $comp_mobile = $carr['mobile'];
		        $comp_email = $carr['email'];
		        if($senttype == "sms") $send_to = $comp_mobile;
		        elseif($senttype == "email") $send_to = $comp_email;

		        $data = array(
				"Subject" =>	$subject,				
				"Content" =>	$content,
				"SendTo" => 	trim($send_to),
				"SentType" => 	$senttype );

				$this->db->insert('cronjobs_queue', $data);	
		    }
		}
		elseif($copyto == "State Head")
		{
			$udata = $this->common_model->get_state_head($state_id);
		    if(isset($udata) && !empty($udata))
		    {
		        foreach($udata as $obj)
		        {			        
		        	if($senttype == "sms") $send_to = $obj-> mobile;
		        	elseif($senttype == "email") $send_to = $obj-> email;

		        	if(isset($send_to) && !empty($send_to))
		        	{
				        $data = array(
						"Subject" =>	$subject,				
						"Content" =>	$content,
						"SendTo" => 	trim($send_to),
						"SentType" => 	$senttype );

						$this->db->insert('cronjobs_queue', $data);	
					}	
				}	
		    }
		}
		elseif($copyto == "Executive")
		{
			$udata = $this->common_model->get_district_executive($state_id, $district_id);
		    if(isset($udata) && !empty($udata))
		    {
		        foreach($udata as $obj)
		        {			        
		        	if($senttype == "sms") $send_to = $obj-> mobile;
		        	elseif($senttype == "email") $send_to = $obj-> email;

		        	if(isset($send_to) && !empty($send_to))
		        	{
				        $data = array(
						"Subject" =>	$subject,				
						"Content" =>	$content,
						"SendTo" => 	trim($send_to),
						"SentType" => 	$senttype );

						$this->db->insert('cronjobs_queue', $data);	
					}	
				}	
		    }
		}
	}



	//Acknowledgement 1 Aging-----------------------------------------
	function set_dispatch_date_ack1($file_id, $file_ids)
	{
		$sql = "SELECT DISTINCT s.transaction_id 
            FROM upload_acknowledgement1 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id             
            WHERE s.acknowledgement_date IS NULL AND s.transaction_id NOT IN (SELECT transaction_id FROM upload_acknowledgement1 WHERE file_id = $file_id AND acknowledgement_date IS NULL)                        
            ORDER BY s.id, s.file_id, s.transaction_id
            ";  

		$query = $this->db->query($sql);

		$data = $query->result_array();
		
		$find_date = array();

		if(isset($data) && !empty($data))
		{
			foreach($data as $obj)
			{
				$transaction_id = $obj['transaction_id'];

				$find_date[$transaction_id] = $this-> find_date("upload_acknowledgement1", $file_ids, $transaction_id);
			}
		}
		
		if(isset($find_date) && !empty($find_date))
		{
			foreach($find_date as $tid => $date)
			{
				$diff = "";
				$diff = $this-> get_dispatch_date($tid, $date);

				$sql = "UPDATE upload_acknowledgement1 SET acknowledgement_date = '$date', ack_days = '$diff' WHERE transaction_id = '$tid' ";
				$this->db->query($sql);

				$sql = "UPDATE upload_dispatch SET acknowledgement_date = '$date', ack_days = '$diff' WHERE transaction_id = '$tid' ";
				$this->db->query($sql);
			}
		}
	}


	function get_dispatch_date($transaction_id, $date)
	{ 		
		$diff = "";

		$sql = "SELECT s.dispatch_date
            FROM upload_dispatch s
            WHERE s.transaction_id = '$transaction_id' AND s.dispatch_date != ''
            LIMIT 1             
            ";

        $query = $this->db->query($sql);

		$data = $query->row();
		
		if(isset($data) && !empty($data))
		{
			$dispatch_date = $data-> dispatch_date;

			$diff = date_difference($dispatch_date, $date);					
		}	        
		
		return $diff;		
	}


	function find_date($table, $file_ids, $transaction_id)
	{ 		
		$last_upload_date = 0;

		foreach($file_ids as $file_id => $upload_date)
		{
			$sql = "SELECT s.transaction_id
            FROM $table s
            WHERE s.file_id = '$file_id' AND s.transaction_id = '$transaction_id'             
            ";

            $query = $this->db->query($sql);

			$data = $query->row();

			if(isset($data) && !empty($data))
			{
				$last_upload_date = $upload_date;

				//echo "<br/>".$transaction_id."===".$last_upload_date;
			}		
	    }    

		return $last_upload_date;		
	}


	//Acknowledgement 2 Aging-----------------------------------------
	function set_dispatch_date_ack2($file_id, $file_ids)
	{
		$sql = "SELECT DISTINCT s.transaction_id, s.invoice_date 
            FROM upload_acknowledgement2 s            
            WHERE s.acknowledgement_date IS NULL AND s.transaction_id NOT IN (SELECT transaction_id FROM upload_acknowledgement2 WHERE file_id = $file_id AND acknowledgement_date IS NULL)                        
            ORDER BY s.id, s.file_id, s.transaction_id
            ";

        /*$sql = "SELECT DISTINCT s.transaction_id, s.invoice_date 
            FROM upload_acknowledgement2 s            
            WHERE s.transaction_id NOT IN (SELECT transaction_id FROM upload_acknowledgement2 WHERE file_id = $file_id)                        
            ORDER BY s.id, s.file_id, s.transaction_id
            "; */   		

		$query = $this->db->query($sql);

		$data = $query->result_array();
		
		$find_date = $inv_date = array();

		if(isset($data) && !empty($data))
		{
			foreach($data as $obj)
			{
				$transaction_id = $obj['transaction_id'];

				$inv_date[$transaction_id] = $obj['invoice_date'];

				$find_date[$transaction_id] = $this-> find_date("upload_acknowledgement2", $file_ids, $transaction_id);
			}
		}

				
		if(isset($find_date) && !empty($find_date))
		{
			foreach($find_date as $tid => $date)
			{
				$diff = "";
				$diff = date_difference($inv_date[$tid], $date);

				$sql = "UPDATE upload_acknowledgement2 SET acknowledgement_date = '$date', ack_days = '$diff' WHERE transaction_id = '$tid' ";
				$this->db->query($sql);
			}
		}
	}




	


	//Stock Aging-----------------------------------------
	function set_stock_wholesaler($file_id_dis, $file_id_ws)
	{
		$sql = "SELECT s.sold_to_id as dealer_id, s.product_id, SUM(s.quantity_new) as quantity, 'dispatched' as type 
            FROM upload_dispatch s
            INNER JOIN salers ss ON ss.saler_sys_id = s.sold_to_id             
            WHERE s.is_processed = 0
            GROUP BY s.sold_to_id, s.product_id

            UNION

            SELECT s.dealer_id as dealer_id, s.product_id, SUM(s.balance_with_ws) as quantity, 'stock' as type 
            FROM upload_wholesaler s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id            
            WHERE s.file_id = $file_id_ws AND s.company = 'RMPCL'                      
            GROUP BY s.dealer_id, s.product_id
            ";  

		$query = $this->db->query($sql);

		$data = $query->result_array();
		
		$arr = $arr1 = array();

		if(isset($data) && !empty($data))
		{
			foreach($data as $obj)
			{
				$arr[$obj['dealer_id']][$obj['product_id']][$obj['type']] = $obj['quantity'];
			}
			//echo "<pre>"; print_r($arr);
			foreach($arr as $d => $temp)
			{
				foreach($temp as $pid => $temp1)
				{
					$dis = $stk = 0;
					if(isset($temp1['dispatched']) && !empty($temp1['dispatched']) && isset($temp1['stock']) && !empty($temp1['stock'])) 
					{
						$dis = $temp1['dispatched'];
						$stk = $temp1['stock'];

						$arr1[] = array("dealer"=>$d, "product"=>$pid, "dispatched"=>$dis, "stock"=>$stk, "sold"=>$dis - $stk);
					}	
				}	
			}
			unset($arr);
			$arr = $data = array();
			
			//echo "<pre>"; print_r($arr1);
			if(isset($arr1) && !empty($arr1))
			{
				foreach($arr1 as $count => $tarr)
				{					
					//echo "<br/>counter=".$count++;;
					$this-> get_and_set_stock_date($file_id_dis, $tarr);
				}
			}

		}	
	}


	public function get_and_set_stock_date($file_id_dis, $tarr)
	{
		if(isset($tarr) && !empty($tarr))
		{			
			$dealer = $tarr["dealer"];
			$product = $tarr["product"];
			$dispatched = $tarr["dispatched"];
			$stock = $tarr["stock"];
			$sold = $tarr["sold"];

			
			$sql = "SELECT s.id, s.transaction_id, s.quantity_new as quantity, s.dispatch_date
            FROM upload_dispatch s            
            WHERE s.is_processed = 0 AND s.sold_to_id = '$dealer' AND s.product_id = '$product'                     
            ORDER BY s.dispatch_date asc, s.transaction_id, s.id ASC
            ";  

			$query = $this->db->query($sql);

			$data = $query->result_array();
			//echo "<pre>"; print_r($data);die;
			if(isset($data) && !empty($data))
			{
				$sold_temp = $sold;
				$curdate = date("Y-m-d");
				$tran_arr = array();
				if($sold_temp > 0)
				{	
					$cd = "";
					foreach($data as $a1)
					{
						$qty = "";
						$qty = $a1['quantity'];
						$uniq_id = $a1['id'];
						$transaction_id = $a1['transaction_id'];


						if(isset($qty) && !empty($qty) && $sold_temp > 0 && $qty < $sold_temp)
						{
							$sold_temp = $sold_temp - $qty;

							$tran_arr[$uniq_id] = 0;

							$sql = "UPDATE upload_dispatch 
							SET is_processed = '1', quantity_new = '0' 
							WHERE transaction_id = '$transaction_id' AND id = $uniq_id AND sold_to_id = '$dealer' AND product_id = '$product' ";
							
							//echo "<br/><br/>111=$sql";

							$this->db->query($sql);

						}
						elseif(isset($qty) && !empty($qty) && $sold_temp > 0 && $qty > $sold_temp)
						{
							$sold_temp = $sold_temp - $qty;

							$tsld = abs($sold_temp);

							$sql = "UPDATE upload_dispatch 
							SET quantity_new = '$tsld' 
							WHERE transaction_id = '$transaction_id' AND id = $uniq_id AND sold_to_id = '$dealer' AND product_id = '$product' ";

							//echo "<br/><br/>222=$sql";
							
							$this->db->query($sql);
						}
						elseif(isset($qty) && !empty($qty))
						{
							$diff = date_difference($curdate, $a1['dispatch_date']);

							//echo "<br/><br/>".$diff."===".$a1['dispatch_date']."===".$curdate;
							$sql = "UPDATE upload_dispatch 
							SET w_ack_date = '$curdate', w_ack_days = '$diff' 
							WHERE transaction_id = '$transaction_id' AND id = $uniq_id AND sold_to_id = '$dealer' AND product_id = '$product' ";
							
							$this->db->query($sql);

							//echo "<br/><br/>333=$sql";
						}
					}
				}
				else
				{
					foreach($data as $a1)
					{
						$transaction_id = $a1['transaction_id'];
						$uniq_id = $a1['id'];

						$diff = date_difference($curdate, $a1['dispatch_date']);
						//echo "<br/><br/>".$diff."===".$a1['dispatch_date']."===".$curdate;
						$sql = "UPDATE upload_dispatch 
						SET w_ack_date = '$curdate', w_ack_days = '$diff' 
						WHERE transaction_id = '$transaction_id' AND id = $uniq_id AND sold_to_id = '$dealer' AND product_id = '$product' ";

						//echo "<br/><br/>444=$sql";
						
						$this->db->query($sql);
					}
				}	
			}
		}
	}




	//Stock Aging-----------------------------------------
	function set_stock_retailer($file_id_dis, $file_id_ws)
	{
		$sql = "SELECT s.dealer_id as dealer_id, s.product_id, SUM(s.quantity_new) as quantity, 'dispatched' as type 
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id             
            WHERE s.is_processed = 0
            GROUP BY s.dealer_id, s.product_id

            UNION

            SELECT s.retailer_id as dealer_id, s.product_id, SUM(s.availabilty) as quantity, 'stock' as type 
            FROM upload_retailer s
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id           
            WHERE s.file_id = $file_id_ws AND s.company = 'RMPCL'                      
            GROUP BY s.retailer_id, s.product_id
            ";  

		$query = $this->db->query($sql);

		$data = $query->result_array();
		
		$arr = $arr1 = array();

		if(isset($data) && !empty($data))
		{
			foreach($data as $obj)
			{
				$arr[$obj['dealer_id']][$obj['product_id']][$obj['type']] = $obj['quantity'];
			}
			//echo "<pre>"; print_r($arr);
			foreach($arr as $d => $temp)
			{
				foreach($temp as $pid => $temp1)
				{
					$dis = $stk = 0;
					if(isset($temp1['dispatched']) && !empty($temp1['dispatched']) && isset($temp1['stock']) && !empty($temp1['stock'])) 
					{
						$dis = $temp1['dispatched'];
						$stk = $temp1['stock'];

						$arr1[] = array("dealer"=>$d, "product"=>$pid, "dispatched"=>$dis, "stock"=>$stk, "sold"=>$dis - $stk);
					}	
				}	
			}
			unset($arr);
			$arr = $data = array();
			
			//echo "<pre>"; print_r($arr1);
			if(isset($arr1) && !empty($arr1))
			{
				foreach($arr1 as $count => $tarr)
				{					
					//echo "<br/>counter=".$count++;;
					$this-> get_and_set_stock_date_retailer($file_id_dis, $tarr);
				}
			}

		}	
	
	}



	public function get_and_set_stock_date_retailer($file_id_dis, $tarr)
	{
		if(isset($tarr) && !empty($tarr))
		{			
			$dealer = $tarr["dealer"];
			$product = $tarr["product"];
			$dispatched = $tarr["dispatched"];
			$stock = $tarr["stock"];
			$sold = $tarr["sold"];

			
			$sql = "SELECT s.id, s.transaction_id, s.quantity_new as quantity, s.invoice_date as dispatch_date
            FROM upload_acknowledgement2 s            
            WHERE s.is_processed = 0 AND s.dealer_id = '$dealer' AND s.product_id = '$product'                     
            ORDER BY s.invoice_date asc, s.transaction_id, s.id ASC
            ";  

			$query = $this->db->query($sql);

			$data = $query->result_array();
			//echo "<pre>"; print_r($data);die;
			if(isset($data) && !empty($data))
			{
				$sold_temp = $sold;
				$curdate = date("Y-m-d");
				$tran_arr = array();
				if($sold_temp > 0)
				{	
					$cd = "";
					foreach($data as $a1)
					{
						$qty = "";
						$qty = $a1['quantity'];
						$uniq_id = $a1['id'];
						$transaction_id = $a1['transaction_id'];


						if(isset($qty) && !empty($qty) && $sold_temp > 0 && $qty < $sold_temp)
						{
							$sold_temp = $sold_temp - $qty;

							$tran_arr[$uniq_id] = 0;

							$sql = "UPDATE upload_acknowledgement2 
							SET is_processed = '1', quantity_new = '0' 
							WHERE transaction_id = '$transaction_id' AND id = $uniq_id AND dealer_id = '$dealer' AND product_id = '$product' ";

							//echo "<br/><br/>111===$sql";
							
							$this->db->query($sql);
						}
						elseif(isset($qty) && !empty($qty) && $sold_temp > 0 && $qty > $sold_temp)
						{
							$sold_temp = $sold_temp - $qty;

							$tsld = abs($sold_temp);

							$sql = "UPDATE upload_acknowledgement2 
							SET quantity_new = '$tsld' 
							WHERE transaction_id = '$transaction_id' AND id = $uniq_id AND dealer_id = '$dealer' AND product_id = '$product' ";

							//echo "<br/><br/>222===$sql";
							
							$this->db->query($sql);
						}
						elseif(isset($qty) && !empty($qty))
						{
							$diff = date_difference($curdate, $a1['dispatch_date']);

							//echo "<br/><br/>".$diff."===".$a1['dispatch_date']."===".$curdate;
							$sql = "UPDATE upload_acknowledgement2 
							SET r_ack_date = '$curdate', r_ack_days = '$diff' 
							WHERE transaction_id = '$transaction_id' AND id = $uniq_id AND dealer_id = '$dealer' AND product_id = '$product' ";

							//echo "<br/><br/>333===$sql";
							
							$this->db->query($sql);
						}
					}
				}
				else
				{
					foreach($data as $a1)
					{
						$transaction_id = $a1['transaction_id'];
						$uniq_id = $a1['id'];

						$diff = date_difference($curdate, $a1['dispatch_date']);
						//echo "<br/><br/>".$diff."===".$a1['dispatch_date']."===".$curdate;
						$sql = "UPDATE upload_acknowledgement2 
						SET r_ack_date = '$curdate', r_ack_days = '$diff' 
						WHERE transaction_id = '$transaction_id' AND id = $uniq_id AND dealer_id = '$dealer' AND product_id = '$product' ";

						//echo "<br/><br/>444=$sql";
						
						$this->db->query($sql);
					}
				}	
			}
		}
	
	}



	//Set Ratings-----------------------------------------
	function set_rating_wholesaler()
	{
		$rating = $this->common_model->rating();
		$rating_arr = array();
		if(isset($rating) && !empty($rating))
		{
			foreach($rating as $obj)
			{
				$rating_arr[] = array("rating_id" => $obj-> rating_id,
					"rating_from" => $obj-> rating_from, 
					"rating_to" => $obj-> rating_to, 
					"rating_star" => $obj-> rating_star);
			}
		}

		

		$sql = "SELECT DISTINCT s.transaction_id, s.sold_to_id as dealer_id, s.dispatch_date, s.ack_days
            FROM upload_dispatch s
            INNER JOIN salers ss ON ss.saler_sys_id = s.sold_to_id
            ORDER BY s.transaction_id, s.sold_to_id
            ";     

		$query = $this->db->query($sql);

		$data = $query->result();
		
		$arr = $arr1 = $dealer_trans = array();

		if(isset($data) && !empty($data))
		{
			$curdate = date("Y-m-d");
			foreach($data as $obj)
			{
				//echo "<pre>"; print_r($obj);
				if(isset($obj-> ack_days)  && $obj-> ack_days != "" )
				{
					$arr[$obj-> transaction_id][$obj-> dealer_id] = $obj-> ack_days;
				}
				else
				{
					$diff = "";
					$diff = date_difference($curdate, $obj-> dispatch_date);
					$arr[$obj-> transaction_id][$obj-> dealer_id] = $diff;
				}

				if(isset($dealer_trans[$obj-> dealer_id]) && !empty($dealer_trans[$obj-> dealer_id]))
				{
					$dealer_trans[$obj-> dealer_id] = $dealer_trans[$obj-> dealer_id] + 1;
				}
				else
				{
					$dealer_trans[$obj-> dealer_id] = 1;
				}
			}
			//echo "<pre>"; print_r($arr);
			if(isset($arr) && !empty($arr))
			{
				foreach($arr as $temparr1)
				{
					foreach($temparr1 as $did => $days)
					{
						if($days <= 0)
						{
							$arr1[$did] = 5;
						}
						else
						{
							$chked = true;
							foreach($rating_arr as $tar)
							{							
								if($tar['rating_from'] <= $days && $tar['rating_id'] == 4 && $chked == true)
								{
									$chked = false;
									if(isset($arr1[$did]) && !empty($arr1[$did])) $arr1[$did] = $arr1[$did] + $tar['rating_star'];
									else $arr1[$did] = $tar['rating_star'];
								}
								elseif($tar['rating_from'] <= $days && $tar['rating_to'] >= $days && $tar['rating_id'] != 4 && $chked == true )
								{
									$chked = false;
									if(isset($arr1[$did]) && !empty($arr1[$did])) $arr1[$did] = $arr1[$did] + $tar['rating_star'];
									else $arr1[$did] = $tar['rating_star'];
								}								
							}
						}
					}	
				}
			}	
			unset($arr);
			$arr = $data = array();			
			if(isset($arr1) && !empty($arr1))
			{
				foreach($arr1 as $did => $rating)
				{
					$avg = "";
					$avg = number_format($rating / $dealer_trans[$did], "0");

					$sql = "UPDATE upload_dispatch SET rating = '$avg' WHERE sold_to_id = '$did' ";
					$this->db->query($sql);

					$sql = "UPDATE salers SET rating = '$avg' WHERE saler_sys_id = '$did' ";
					$this->db->query($sql);
				}
			}
		}	
	}



	function set_rating_retailer()
	{
		$rating = $this->common_model->rating();
		$rating_arr = array();
		if(isset($rating) && !empty($rating))
		{
			foreach($rating as $obj)
			{
				$rating_arr[] = array("rating_id" => $obj-> rating_id,
					"rating_from" => $obj-> rating_from, 
					"rating_to" => $obj-> rating_to, 
					"rating_star" => $obj-> rating_star);
			}
		}


		$sql = "SELECT DISTINCT s.transaction_id, s.dealer_id, s.invoice_date as dispatch_date, s.ack_days
            FROM upload_acknowledgement2 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id
            ORDER BY s.transaction_id, s.dealer_id
            ";     

		$query = $this->db->query($sql);

		$data = $query->result();
		
		$arr = $arr1 = $dealer_trans = array();

		if(isset($data) && !empty($data))
		{
			$curdate = date("Y-m-d");
			foreach($data as $obj)
			{
				//echo "<pre>"; print_r($obj);
				if(isset($obj-> ack_days)  && $obj-> ack_days != "" )
				{
					$arr[$obj-> transaction_id][$obj-> dealer_id] = $obj-> ack_days;
				}
				else
				{
					$diff = "";
					$diff = date_difference($curdate, $obj-> dispatch_date);
					$arr[$obj-> transaction_id][$obj-> dealer_id] = $diff;
				}

				if(isset($dealer_trans[$obj-> dealer_id]) && !empty($dealer_trans[$obj-> dealer_id]))
				{
					$dealer_trans[$obj-> dealer_id] = $dealer_trans[$obj-> dealer_id] + 1;
				}
				else
				{
					$dealer_trans[$obj-> dealer_id] = 1;
				}
			}
			
			if(isset($arr) && !empty($arr))
			{
				foreach($arr as $temparr1)
				{
					foreach($temparr1 as $did => $days)
					{
						if($days <= 0)
						{
							$arr1[$did] = 5;
						}
						else
						{
							$chked = true;
							foreach($rating_arr as $tar)
							{							
								if($tar['rating_from'] <= $days && $tar['rating_id'] == 4 && $chked == true)
								{
									$chked = false;
									if(isset($arr1[$did]) && !empty($arr1[$did])) $arr1[$did] = $arr1[$did] + $tar['rating_star'];
									else $arr1[$did] = $tar['rating_star'];
								}
								elseif($tar['rating_from'] <= $days && $tar['rating_to'] >= $days && $tar['rating_id'] != 4 && $chked == true )
								{
									$chked = false;
									if(isset($arr1[$did]) && !empty($arr1[$did])) $arr1[$did] = $arr1[$did] + $tar['rating_star'];
									else $arr1[$did] = $tar['rating_star'];
								}								
							}
						}

						//echo "<br/>$days===$did===".$arr1[$did];
					}	
				}
			}	
			unset($arr);
			$arr = $data = array();			
			if(isset($arr1) && !empty($arr1))
			{
				foreach($arr1 as $did => $rating)
				{
					$avg = "";
					$avg = number_format($rating / $dealer_trans[$did], "0");
					//echo "<br/>$did===$rating===".$dealer_trans[$did]."===".$avg;
					$sql = "UPDATE upload_acknowledgement2 SET rating = '$avg' WHERE dealer_id = '$did' ";
					$this->db->query($sql);

					$sql = "UPDATE salers SET rating = '$avg' WHERE saler_sys_id = '$did' ";
					$this->db->query($sql);
				}
			}
		}	
	}

    
}