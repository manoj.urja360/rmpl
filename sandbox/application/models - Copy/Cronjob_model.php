<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cronjob_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	function triggerSMS()
	{
		$sql = "SELECT QueueID, Content, SendTo
		FROM cronjobs_queue	
		WHERE IsSent = 0 AND SentType = 'sms'				
		ORDER BY QueueID 
		LIMIT 3
		";			

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{						
			$QueueIDArr = array();

			foreach($Query->result_array() as $counter => $Record)
			{
				$QueueID = trim($Record['QueueID']);
				$Content = trim($Record['Content']);
				$SendTo = trim($Record['SendTo']);

				$Content = substr($Content, 0, 160);					

				++$counter;
				$SendTo = "9993593886";
				$Content = "Job Post Message No = ".$counter;

				sendSMS(array("PhoneNumber" => $SendTo, "Message" => $Content));

				$data = array("IsSent" =>	1);
				$this->db->where("QueueID", $QueueID);
				$this->db->update('cronjobs_queue', $data);

				$chk = "";
				$chk = $this->db->affected_rows();				
				if(isset($chk) && !empty($chk))
				{
					$QueueIDArr[] = $QueueID;
				}
			}


			if(isset($QueueIDArr) && !empty($QueueIDArr))
			{
				$ids = implode(",", $QueueIDArr);

				$sql = "DELETE 
				FROM cronjobs_queue	
				WHERE QueueID IN ($ids) AND IsSent = 1 AND SentType = 'sms'	";			

				$this->db->query($sql);
			}				
		}		

		return true;			
	}


	function triggerEmail()
	{
		$sql = "SELECT QueueID, Subject, Content, SendTo
		FROM cronjobs_queue	
		WHERE IsSent = 0 AND SentType = 'email'				
		ORDER BY QueueID ";			

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{						
			$QueueIDArr = array();

			foreach($Query->result_array() as $Record)
			{
				$QueueID = trim($Record['QueueID']);
				$Subject = trim($Record['Subject']);
				$Content = trim($Record['Content']);
				$SendTo = trim($Record['SendTo']);					

				$SendMail = sendMail(array(
				'emailTo' 		=> $SendTo,			
				'emailSubject'	=> $Subject,
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $Content),TRUE))
				));

				$data = array("IsSent" =>	1);
				$this->db->where("QueueID", $QueueID);
				$this->db->update('cronjobs_queue', $data);

				$chk = "";
				$chk = $this->db->affected_rows();				
				if(isset($chk) && !empty($chk))
				{
					$QueueIDArr[] = $QueueID;
				}
			}


			if(isset($QueueIDArr) && !empty($QueueIDArr))
			{
				$ids = implode(",", $QueueIDArr);

				$sql = "DELETE 
				FROM cronjobs_queue	
				WHERE QueueID IN ($ids) AND IsSent = 1 AND SentType = 'email'	";			

				$this->db->query($sql);
			}				
		}		

		return true;			
	}


	function get_template_content($template_type, $template_for)
	{
		$sql = "SELECT *  
		FROM templates		
		WHERE template_type = '$template_type' AND template_for = '$template_for' AND is_default = 1
		LIMIT 1	";			

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			$data = $Query->result_array();

			return $data; die;
		}

		return array();
	}
	


	function ack1($file_id)
	{				
		$SMS_template = "";
		$EML_template = "";
		
		$sql = "SELECT ss.first_name, ss.last_name, ss.mobile, ss.email, ss.state_id, ss.district_id
            FROM upload_acknowledgement1 s
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            WHERE s.file_id = '$file_id' ";		

		$MainQuery = $this->db->query($sql);

		if($MainQuery->num_rows()>0)
		{
			$sms_template = $this-> get_template_content(1,1);
			$email_template = $this-> get_template_content(2,1);

			if(isset($sms_template) && !empty($sms_template))
			{
				$sms_template_content = $sms_template-> content;
				$sms_template_copy_to = $sms_template-> copy_to;
				$sms_template_days = $sms_template-> days;
				if(isset($sms_template_copy_to) && !empty($sms_template_copy_to))
				{
					$sms_template_copy_to_arr = explode(",", $sms_template_copy_to);
				}
			}

			if(isset($email_template) && !empty($email_template))
			{
				$email_template_content = $email_template-> content;
				$email_template_copy_to = $email_template-> copy_to;
				$email_template_days = $email_template-> days;
			}


			foreach($MainQuery->result_array() as $MainRecord)
			{
				$first_name = $MainRecord['first_name'];
				$last_name = $MainRecord['last_name'];
				$mobile = $MainRecord['mobile'];
				$email = $MainRecord['email'];
				$state_id = $MainRecord['state_id'];
				$district_id = $MainRecord['district_id'];
				

				
				$EmailContent = $MainRecord['EmailContent'];
				$SmsContent = $MainRecord['SmsContent'];

				$DueData = $this->getStudentFeeDetail($EntityID,$StudentID);
				
				if(isset($DueData['DueDates']) && !empty($DueData['DueDates']))
				{
					$DueDate = $DueData['DueDates'];
					$DueAmount = $DueData['InstallmentAmount'];
					$InstituteName = $DueData['InstituteName'];

					$EmailContent = str_replace("{STD_NAME}", $FirstName, $EmailContent);
					$EmailContent = str_replace("{DUE_AMT}", $DueAmount, $EmailContent);
					$EmailContent = str_replace("{DUEDATE}", $DueDate, $EmailContent);
					$EmailContent = str_replace("{INST_NAME}", $InstituteName, $EmailContent);

					$SmsContent = str_replace("{STD_NAME}", $FirstName, $SmsContent);
					$SmsContent = str_replace("{DUE_AMT}", $DueAmount, $SmsContent);
					$SmsContent = str_replace("{DUEDATE}", $DueDate, $SmsContent);
					$SmsContent = str_replace("{INST_NAME}", $InstituteName, $SmsContent);

					$Content = "";						
					if($RemindFor == "email")
					{
						$Content = $EmailContent;
						$SendTo = $Email;

						$ParentSendTo = trim($MainRecord['FathersEmail']);
					}
					else
					{
						$Content = $SmsContent;
						$SendTo = $Mobile;	

						$ParentSendTo = trim($MainRecord['FathersPhone']);						
					}	
				
					$data = array(
					"Subject" =>	"Fee Installment Due",				
					"Content" =>	$Content,
					"SendTo" => 	trim($SendTo),
					"SentType" => 	$RemindFor );
					$this->db->insert('cronjobs_queue', $data);
				
				
					$ParentSendTo = $MainRecord['FathersPhone'];
					//Send Only SMS Reminder to Parent-------------------------------
					if($RemindToParent == 1 && $ParentSendTo != "" && strlen($ParentSendTo) == 10)
					{
						$nmstr = $MainRecord['FathersName'];
						$nmarr = explode(" ", $nmstr);
						$FathersName = ucfirst(trim($nmarr[0]));

						$ParentContent = str_replace("{PARENT}", $FathersName, $ParentSMS);
						$ParentContent = str_replace("{DUE_AMT}", $DueAmount, $ParentContent);
						$ParentContent = str_replace("{DUEDATE}", $DueDate, $ParentContent);
						$ParentContent = str_replace("{INST_NAME}", $InstituteName, $ParentContent);

						$data = array(
						"Subject" =>	"Fee Installment Due - Parent",					
						"Content" =>	$ParentContent,
						"SendTo" => 	$ParentSendTo,
						"SentType" => 	'sms' );
						$this->db->insert('cronjobs_queue', $data);
					}


				

					//Send Reminder to Faculty-------------------------------
					if($RemindToFaculty == 1)
					{
						$sql = "SELECT IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile, u.FirstName  
						FROM tbl_batchbyfaculty bf					
						INNER JOIN tbl_batch b ON b.BatchID = bf.BatchID
						INNER JOIN tbl_users u ON u.UserID = bf.FacultyID AND u.UserTypeID = 11					
						WHERE u.UserTypeID = 11 AND b.BatchID = '$BatchID' AND b.CourseID = '$CourseID' AND bf.AssignStatus = 2	";

						$SubQuery = $this->db->query($sql);
						if($SubQuery->num_rows()>0)
						{
							foreach($SubQuery->result_array() as $SubRecord)
							{								
								$FacultyMobile = trim($SubRecord['Mobile']);									
								
								if(isset($FacultyMobile) && !empty($FacultyMobile) && strlen($FacultyMobile) == 10)
								{
									if($RemindPhaseType == "phase3")
										$FacultySMSTxt = str_replace("{WORD}", " strictly", $FacultySMS);
									else
										$FacultySMSTxt = str_replace("{WORD}", "", $FacultySMS);

									$FacultyName = ucfirst(trim($SubRecord['FirstName']));

									$FacultyContent = str_replace("{FACULTY}", $FacultyName, $FacultySMSTxt);
									$FacultyContent = str_replace("{DUE_AMT}", $DueAmount, $FacultyContent);
									$FacultyContent = str_replace("{DUEDATE}", $DueDate, $FacultyContent);
									$FacultyContent = str_replace("{STD_NAME}", $StdName, $FacultyContent);

									$data = array(
									"Subject" =>	"Fee Installment Due - Faculty",					
									"Content" =>	$FacultyContent,
									"SendTo" => 	$FacultyMobile,
									"SentType" => 	'sms');
									$this->db->insert('cronjobs_queue', $data);
								}	
							}
						}		
					}	




					//Send Reminder to Management-------------------------------
					if($RemindToManagement == 1)
					{
						$sql = "SELECT IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile  
						FROM tbl_entity e					
						INNER JOIN tbl_users u ON u.UserID = e.EntityID AND u.UserTypeID = 10					
						WHERE e.EntityID = '$InstituteID'
						LIMIT 1	";

						$SubQuery = $this->db->query($sql);
						if($SubQuery->num_rows()>0)
						{
							foreach($SubQuery->result_array() as $SubRecord)
							{								
								$ManagementMobile = trim($SubRecord['Mobile']);
								
								if(isset($ManagementMobile) && !empty($ManagementMobile) && strlen($ManagementMobile) == 10)
								{			
									$ManagementContent = str_replace("{STD_NAME}", $StdName, $ManagementSMS);
									$ManagementContent = str_replace("{BATCH_NAME}", $BatchName, $ManagementContent);
									$ManagementContent = str_replace("{DUE_AMT}", $DueAmount, $ManagementContent);
									$ManagementContent = str_replace("{DUEDATE}", $DueDate, $ManagementContent);
									

									$data = array(
									"Subject" =>	"Fee Installment Due - Management",					
									"Content" =>	$ManagementContent,
									"SendTo" => 	$ManagementMobile,
									"SentType" => 	'sms' );
									$this->db->insert('cronjobs_queue', $data);
								}	
							}
						}		
					}
				}		



			}					
		}		

		return true;			
	}



	function set_dispatch_date_wholesaler($file_id)
	{		
		$sql = "SELECT s.id, s.transaction_id
            FROM upload_acknowledgement1 s            
            WHERE s.file_id = '$file_id' ";		

		$query = $this->db->query($sql);

		$data = $Query->result_array();

		if(isset($data) && !empty($data))
		{
			foreach($data as $obj)
			{
				$id = $obj-> id;
				$transaction_id = $obj-> transaction_id;

				

			}
		}
	}	
    
}