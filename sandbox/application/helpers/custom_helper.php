<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


function p($arr)
{
    echo "<pre>";
        print_r($arr);
    echo "</pre>";

    exit;
}


function ddg($obj, $typ = "subunits")
{
    $arr = array();

    if(isset($obj) && !empty($obj))
    {
        foreach($obj as $out)
        {            
            $arr[$out-> unit_name][$out-> subunit_id] = $out-> subunit_name;
        }
    }

    return $arr;
}


function prepare_drop_down($arr, $sel_id)
{
    $str = "";

    if(isset($arr) && !empty($arr))
    {
        foreach($arr as $obj)
        {            
            $sel = "";
            if($obj-> id == $sel_id) $sel = "selected";

            $str = $str."<option value='".$obj-> id."' ".$sel.">".$obj-> name."</option>";
        }
    }

    return $str;
}


function generate_random_string($length) 
{ 
    $str = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ'; 

    return substr(str_shuffle($str), 0, $length); 
} 


function format_date_upload($date, $to=DATE_DB,$upload_type="")
{
    if(isset($date) && !empty($date) && $date != "-")
    {
        $date = str_replace("/", "-", $date);

        $arr = explode("-", $date);

        if(strlen($arr[1]) <= 1) $arr[1] = "0".$arr[1];
        if(strlen($arr[2]) <= 1) $arr[2] = "0".$arr[2];

        if($upload_type == "upload_acknowledgement1" || $upload_type == "upload_acknowledgement2" || $upload_type == "upload_retailer_pos" || $upload_type == "upload_dispatch")
        {
            $date = $arr[0]."-".$arr[1]."-".$arr[2];
        }    
        
        $date = date($to, strtotime($date));
    }

    return $date;
}


function format_date($date, $to=DATE_DB,$upload_type="")
{
    if(isset($date) && !empty($date) && $date != "-")
    {
        $date = str_replace(".", "-", $date);
        $date = str_replace("/", "-", $date);

        $arr = explode("-", $date);

        if(strlen($arr[0]) <= 1) $arr[0] = "0".$arr[0];
        if(strlen($arr[1]) <= 1) $arr[1] = "0".$arr[1];

        if($upload_type == "upload_acknowledgement1" || $upload_type == "upload_acknowledgement2" || $upload_type == "upload_retailer_pos" || $upload_type == "upload_dispatch")
        {
            $date = $arr[1]."-".$arr[0]."-".$arr[2];
        }
        else
        {    
            $date = $arr[0]."-".$arr[1]."-".$arr[2];
        }    
        
        $date = date($to, strtotime($date));
    }

    return $date;
}


function prepare_date($date, $to=DATE_DB)
{
    if(isset($date) && !empty($date) && $date != "-")
    {
        $date = str_replace("/", "-", $date);

        $arr = explode("-", $date);

        if(strlen($arr[0]) <= 1) $arr[0] = "0".$arr[0];
        if(strlen($arr[1]) <= 1) $arr[1] = "0".$arr[1];

        $date = $arr[0]."-".$arr[1]."-".$arr[2];

        $date = date($to, strtotime($date));
    }

    return $date;
}


function sanitize_text($text)
{
    $text = trim($text);
    $text = str_replace("'", "", $text);
    $text = str_replace('"', "", $text);
    //$text = str_replace("&", "", $text);    
    
    return $text;
}


function regno($reg, $len)
{
    $reg = str_pad($reg, $len, '0', STR_PAD_LEFT);
    
    return $reg;
}


function prepare_plot_photo($plot_photos, $list_type = 'thmb', $stage = 2, $demo_type = "")
{
    $list = "";
    $key = 0;
    foreach($plot_photos as $obj)
    {
        if($obj-> for_stage == $stage && $list_type == "thmb")
        {
            ++$key;                

            $list = $list."<li style='float: left; display:inline;'><a href='#lightbox_".$stage."' data-toggle='modal' data-slide-to='".$key."'><img class='plot_photos' src='".$obj->plot_photo_url."' alt='".$obj->plot_photo_url."'></a></li>";

        }
        elseif($obj-> for_stage == $stage && $list_type == "pop_li")
        {

                ++$key;

                $cls = "";
                if($key == 1) $cls = "active";

                $list = $list.'<li data-target="#lightbox_'.$stage.'" data-slide-to="'.$key.'" class="'.$cls.'"></li>';
        }    
        elseif($obj-> for_stage == $stage && $list_type == "pop_zom")
        {
            ++$key;

            $cls = "";
            if($key == 1) $cls = "active";

            $list = $list.'<div class="item '.$cls.'"><img src="'.$obj->plot_photo_url.'" alt="'.$obj->plot_photo_url.'" class="full_image"></div>';
        }
        elseif($obj-> for_stage == $stage && $list_type == "in_pdf")
        {
            $list = $list.'<tr><td><img src="'.$obj->plot_photo_url.'" alt="'.$obj->plot_photo_url.'" class="full_image"></td></tr>';
        }
        elseif($obj-> for_stage == $stage && $list_type == "in_html")
        {
            if($obj-> demo_type == $demo_type)
            $list = $list.'<li class="ui-state-default"><img src="'.$obj->plot_photo_url.'" alt="'.$obj->plot_photo_url.'" class="full_image"></li>';
        }
    }

    return $list;
}


function get_crop_name($typ, $id)
{
    $ci = get_instance();
    $ci->load->model('common_model');
    
    $udata = $ci->common_model->get_detail(array("typ" => $typ, "id" => $id));
    if(isset($udata) && !empty($udata))
    {
        return $udata-> crop_name;
    }

    return "";
}


/*---------------------------------------------------------------------------
Download Excel Code
---------------------------------------------------------------------------*/
function export_excel($rname, $rtype, $columns, $results)
{
    $ci = get_instance();
    $ci->load->model('common_model');
    $udata = $ci->common_model->get_detail(array("typ" => "users", "id" => 1));
    $hd_address = $hd_contact = "";
    $compname = PROJECT_NAME;

    if(isset($udata) && !empty($udata) && isset($udata-> company_details) && !empty($udata-> company_details))
    {
        $carr = json_decode($udata-> company_details);
        $carr = (array)$carr;
        $hd_address = $carr['caddress'].", ".$carr['ccity_name'].", ".$carr['cstates_name'];
        $hd_contact = $carr['mobile'].", ".$carr['email'];
        $compname = $carr['company_name'];
    }
   


    require_once APPPATH . 'third_party/PHPExcel.php';
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $sheet = $objPHPExcel->getActiveSheet();    

    //Settings------------------------------------------
    $styleArray = array(
      'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
          )
      )
    );
    $objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);

    
    //Set font to bold of row----------------------------
    $objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A2:Z2")->getFont()->setBold(true);


    //Set Auto width of cell----------------------------    
    $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
    foreach(range('A','L') as $columnID) 
    {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
    }

    
    //Set height of first cell----------------------------
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(70);

    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );
    $sheet->getStyle("A1:B1")->applyFromArray($style);
    
    //Data--------------------------------------------
    $rows = 1;
    $cols = 0;
    
    
    $sheet->mergeCells('A1:G1');    
    $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($cols, $rows, $compname."\n$hd_address\n$hd_contact\n$rname");
    $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setWrapText(true);


    $rows = 2;
    $cols = 0;
    foreach($columns as $val)
    {            
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($cols, $rows, $val);
        $cols++;
    }

    $rows++;
    foreach($results as $arr)
    {
        $cols = 0;
        foreach($arr as $val)
        {
            $val = strip_tags($val);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($cols, $rows, $val);
            $cols++;
        }
        $rows++;
    }


    //Download--------------------------------------
    $down_filename = $rname."_".date('d-M-Y');

    $objPHPExcel->getActiveSheet()->setTitle('Report');
    $objPHPExcel->setActiveSheetIndex(0);        
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$down_filename.'.xls"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');       
    exit;
}



function export_csv($rname, $rtype, $columns, $results)
{    
    $file_name = $rname."_".date('d-M-Y').".csv";           
    
    header("Content-type: application/csv");
    header("Content-Disposition: attachment; filename=\"$file_name\"");
    header("Pragma: no-cache");
    header("Expires: 0");

    $handle = fopen('php://output', 'w');
    
    fputcsv($handle, $columns);    
    
    foreach ($results as $arr) 
    {                        
        fputcsv($handle, $arr);
    }       

    fclose($handle);
   
    exit;           
}




function export_pdf($rname, $rtype, $columns, $results)
{
    $ci = get_instance();

    $ci->load->model('common_model');
    $udata = $ci->common_model->get_detail(array("typ" => "users", "id" => 1));
    $hd_address = $hd_contact = "";
    $compname = PROJECT_NAME;
    if(isset($udata) && !empty($udata) && isset($udata-> company_details) && !empty($udata-> company_details))
    {
        $carr = json_decode($udata-> company_details);
        $carr = (array)$carr;
        $hd_address = "<tr><td align='center'>".$carr['caddress'].", ".$carr['ccity_name'].", ".$carr['cstates_name']."</td></tr>";
        $hd_contact = "<tr><td align='center'>".$carr['mobile'].", ".$carr['email']."</td></tr>";

        $compname = $carr['company_name'];
    }



    $html = '<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>'.$compname.'</title><style type="text/css">table,tr,td{font-size:10px !important; border-collapse:collapse; } td { font-family: freeserif; } .table, .table tr, .table td, .table th {border:1px solid #bbb; padding:5px; color:#222;}</style></head><body><table align="center" width="100%" border="0"><tr><td align="center"><img src="'.base_url().'assets/images/logo.png" width="50px" height="50px" alt="'.$compname.'"><h2>'.$compname.'</h2></td></tr>'.$hd_address.$hd_contact.'<tr><td align="center"><h3>'.$rname.'</h3></td></tr></table><br/><table class="table" width="100%" border="1">';    
    
    //Header--------------------------------------------    
    $head = "";
    foreach($columns as $val)
    {            
        $head .= "<th style='color:#222 !important; border-color:#aaa;'>$val</th>";
    }
    $html .= "<thead><tr style='background-color:#bbb;'>$head</tr></thead>";

    //Data--------------------------------------------
    $body = "";
    foreach($results as $arr)
    {        
        $head = "";
        foreach($arr as $val)
        {
            $val = strip_tags($val);
            $head .= "<td>$val</td>";            
        }

        $body .= "<tr>$head</tr>";
    }
    $html .= $body;
    $html = $html."</table></body></html>";

    //Download--------------------------------------
    $down_filename = $rname."_".date('d-M-Y').".pdf";

    $ci->load->library('m_pdf');        

    $ci->m_pdf->pdf->setFooter($rname." (".'{PAGENO}'.")");

    $ci->m_pdf->pdf->WriteHTML($html);

    $ci->m_pdf->pdf->Output($down_filename, "I");
}


function count_plot_images($farmer_id, $farmer_type, $for_stage, $demo_type)
{
    $ci = get_instance();

    $ci->load->model('common_model');
    
    $count = 0;
    $count = $ci->common_model->count_plot_images(array("farmer_id" => $farmer_id, 
        "farmer_type" => $farmer_type, 
        "for_stage" => $for_stage, 
        "demo_type" => $demo_type));    
    

    return $count;
}



function spacer($txt)
{
    $words = "";

    $len = strlen($txt);

    for($i=1; $i <= 30 - $len; $i++)
    {
        $words .= " ";
    }

    return substr($txt.$words, 0, 30);
}



function remove_characters($arr)
{
    $temp = array();
    if(isset($arr) && !empty($arr))
    {
        foreach($arr as $key1 => $arr1)
        {
            foreach($arr1 as $key => $val)
            {
                
                $val = preg_replace("/[,]/", " / ", $val);
                $val = str_replace(",", "  /  ", $val);
                $val = str_replace("<br/>", "          ", $val);
                $temp[$key1][$key] = $val;
                
            }
        }
    }       

    return $temp;
}




/*------------------------------*/
/*------------------------------*/
function sendPushMessage($UserID, $Message, $Data = array()) {
    if (!isset($Data['content_available'])) {
        $Data['content_available'] = 1;
    }
    $Obj = & get_instance();
    $Obj->db->select('U.UserRoleID, US.DeviceTypeID, US.DeviceToken');
    $Obj->db->from('tbl_users_session US');
    $Obj->db->from('tbl_users U');
    $Obj->db->where("US.UserID", $UserID);
    $Obj->db->where("US.UserID", "U.UserID", FALSE);
    $Obj->db->where("US.DeviceToken!=", '');
    $Obj->db->where('US.DeviceToken is NOT NULL', NULL, FALSE);
    if(!MULTISESSION){ 
        $this->db->limit(1);
    }
    $Query = $Obj->db->get();
    //echo $Obj->db->last_query();
    if ($Query->num_rows() > 0) {
        foreach ($Query->result_array() as $Notifications) {
            if ($Notifications['DeviceTypeID'] == 2) { /*I phone */
                pushNotificationIphone($Notifications['DeviceToken'], $Notifications['UserRoleID'], $Message, 0, $Data);
            } elseif ($Notifications['DeviceTypeID'] == 3) { /* android */
                pushNotificationAndroid($Notifications['DeviceToken'], $Notifications['UserRoleID'], $Message, $Data);
            }
        }
    }
}
/*------------------------------*/
/*------------------------------*/
function pushNotificationAndroid($DeviceIDs, $UserRoleID, $Message, $Data = array()) {
    //API URL of FCM
    $URL = 'https://fcm.googleapis.com/fcm/send';
    /*ApiKey available in:  Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/
    if ($UserRoleID == 2) {
        if (ENVIRONMENT == 'production') {
            $ApiKey = 'AIzaSyDZvMfF2HbbG_tuEOQQjzeBDXa7EKPth5M';
        } else {
            $ApiKey = 'AIzaSyDZvMfF2HbbG_tuEOQQjzeBDXa7EKPth5M';
        }
    } else {
        if (ENVIRONMENT == 'production') {
            $ApiKey = 'AIzaSyBe5p4qjA1aID7H0gGADnnhQXspHzIgrLk';
        } else {
            $ApiKey = 'AIzaSyBe5p4qjA1aID7H0gGADnnhQXspHzIgrLk';
        }
    }
    $Fields = array('registration_ids' => array($DeviceIDs), 'data' => array("Message" => $Message, "Data" => $Data));
    //header includes Content type and api key
    $Headers = array('Content-Type:application/json', 'Authorization:key=' . $ApiKey);
    $Ch = curl_init();
    curl_setopt($Ch, CURLOPT_URL, $URL);
    curl_setopt($Ch, CURLOPT_POST, true);
    curl_setopt($Ch, CURLOPT_HTTPHEADER, $Headers);
    curl_setopt($Ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($Ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($Ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($Ch, CURLOPT_POSTFIELDS, json_encode($Fields));
    $Result = curl_exec($Ch);
    $obj = & get_instance();
    /*Save Log*/
    $PushData = array('Body' => json_encode(array_merge($Headers, $Fields), 1), 'DeviceTypeID' => '3', 'Return' => $Result, 'EntryDate' => date("Y-m-d H:i:s"),);
    @$obj->db->insert('log_pushdata', $PushData);
    if ($Result === FALSE) {
        die('FCM Send Error: ' . curl_error($Ch));
    }
    curl_close($Ch);
    return $Result;
}
/*------------------------------*/
/*------------------------------*/
function pushNotificationIphone($DeviceToken = '', $UserRoleID, $Message = '', $Badge = 1, $Data = array()) {
    $Badge = ($Badge == 0 ? 1 : 0);
    $Pass = '123456';
    $Body['aps'] = $Data;
    $Body['aps']['alert'] = $Message;
    $Body['aps']['badge'] = (int)$Badge;
    // if ($sound)//$Body['aps']['sound'] = $sound;
    /* End of Configurable Items */
    $Ctx = @stream_context_create();
    // assume the private key passphase was removed.
    stream_context_set_option($Ctx, 'ssl', 'passphrase', $Pass);

    if (ENVIRONMENT == 'production') {
        $Certificate = 'app2-ck-live.pem';
        @stream_context_set_option($Ctx, 'ssl', 'local_cert', $Certificate);
            $Fp = @stream_socket_client('ssl://gateway.push.apple.com:2195', $Err, $Errstr, 60, STREAM_CLIENT_CONNECT, $Ctx); //For Live
        } else {
            $Certificate = 'app2-ck-dev.pem';
            @stream_context_set_option($Ctx, 'ssl', 'local_cert', $Certificate);
            $Fp = @stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $Err, $Errstr, 60, STREAM_CLIENT_CONNECT, $Ctx); //For Testing
        }

        if (!$Fp) {
            return "Failed to connect $Err $Errstr";
        } else {
            try {
                $obj = & get_instance();
                /*Save Log*/
                $PushData = array('Body' => json_encode($Body, 1), 'DeviceTypeID' => '2', 'Return' => $Certificate, 'EntryDate' => date("Y-m-d H:i:s"),);
                @$obj->db->insert('log_pushdata', $PushData);
                $Payload = @json_encode($Body, JSON_NUMERIC_CHECK);
                $Msg = @chr(0) . @pack("n", 32) . @pack('H*', @str_replace(' ', '', $DeviceToken)) . @pack("n", @strlen($Payload)) . $Payload;
                @fwrite($Fp, $Msg);
                @fclose($Fp);
            }
            catch(Exception $E) {
                return 'Caught exception';
            }
        }
    }
    /*------------------------------*/
    /*------------------------------*/
    function sendSMS($input = array()) {


    //Your authentication key
        $authKey = "318904A8Lu5gDi5e4bba17P1";
    //Multiple mobiles numbers separated by comma
        $mobileNumber = "9993593886";//$input['PhoneNumber'];
    //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "RMPCPL";
    //Your message to send, Add URL encoding here.
        $message = urlencode($input['Text']);
    //Define route
        $route = "1";
    //Prepare you post parameters
        $postData = array();//array('channel' => 2, 'number' => $mobileNumber, 'text' => $message, 'senderid' => $senderId, 'route' => $route, "APIKey" => "tCMw6RIaEUyQUHgAYZEYIQ", "flashsms" => 0);
        
        /*if (ENVIRONMENT == 'production') {
            $url = urlencode("http://smsgateway.ca/SendSMS.aspx?CellNumber=$mobileNumber&AccountKey=zP70k0f8S70AacoogxnU3Q7WVnh460yx&MessageBody=$message");
        } else {
            $url = "http://sms.bulksmsserviceproviders.com/api/send_http.php";
        }*/

        
        //$url = "https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=tCMw6RIaEUyQUHgAYZEYIQ&senderid=ICONIK&channel=2&DCS=0&flashsms=0&number=$mobileNumber&text=$message&route=1";
        
        $url = "http://sms.elpisind.com/api/sendhttp.php?authkey=$authKey&mobiles=$mobileNumber&message=$message&sender=$senderId&route=4&country=91";
        
    // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true, CURLOPT_POST => true, CURLOPT_POSTFIELDS => $postData
    //,CURLOPT_FOLLOWLOCATION => true
        ));
    //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    //get response
        $output = curl_exec($ch);
        //print_r($output);
    //Print error if any
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
    //echo $output;

    }
    /*------------------------------*/
    /*------------------------------*/
    function sendMail($Input = array()) 
    {
        $CI = & get_instance();
        $CI->load->library('email');
        //$config['protocol'] = SMTP_PROTOCOL;
        /*$config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['wordwrap'] = TRUE;*/
        //$config['smtp_crypto'] = "ssl";

        $config = array(
            'protocol' => mail, // 'mail', 'sendmail', or 'smtp'
            /*'smtp_host' => SMTP_HOST, 
            'smtp_port' => 587,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASS,*/
            'smtp_crypto' => 'ssl', //can be 'ssl' or 'tls' for example
            'mailtype' => 'html', //plaintext 'text' mails or 'html'
            'smtp_timeout' => '4', //in seconds
            'charset' => 'utf-8',
            'wordwrap' => TRUE
        );

        
        $CI->email->initialize($config);
        $CI->email->set_newline("\r\n");
        $CI->email->clear();
        
        $CI->email->from($Input['emailFrom'], $Input['emailFromName']);        
        $CI->email->reply_to($Input['emailFrom'], $Input['emailFromName']);

        $CI->email->to($Input['emailTo']);
        $CI->email->bcc("vineet.jain@rmphosphates.com");
        $CI->email->bcc("write2shalabh@gmail.com");

        /*if (defined('TO_BCC') && !empty(TO_BCC)) {
            $CI->email->bcc(TO_BCC);
        }

        if(!empty($Input['emailBcc'])){
            $CI->email->bcc($Input['emailBcc']);
        }*/

        $CI->email->subject($Input['emailSubject']);
        $CI->email->message($Input['emailMessage']);
        if (@$CI->email->send()) {
            return true;
        } else {
        echo $CI->email->print_debugger();
            return false;
        }
    }
    /*------------------------------*/
    /*------------------------------*/
    function emailTemplate($HTML) {
        $CI = & get_instance();
        return $CI->load->view("emailer/layout", array("HTML" => $HTML), TRUE);
    }
    /*------------------------------*/
    /*------------------------------*/
    function checkDirExist($DirName) {
        if (!is_dir($DirName)) mkdir($DirName, 0777, true);
    }
    /*------------------------------*/
    /*------------------------------*/
/*    function validateEmail($Str) {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $Str)) ? FALSE : TRUE;
    }*/
    /*------------------------------*/
    /*------------------------------*/
    function validateDate($Date) {
        if (strtotime($Date)) {
            return true;
        } else {
            return false;
        }
    }

    function get_guid() {
        if (function_exists('com_create_guid')) {
            return strtolower(trim(com_create_guid(), '{}'));
        }
        else {
        mt_srand((double)microtime() * 10000); //optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45); // "-"
        $uuid = substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);
        return strtolower($uuid);
    }
}
/*------------------------------*/
/*------------------------------*/
function dateDiff($FromDateTime, $ToDateTime) {
    $start = date_create($FromDateTime);
    $end = date_create($ToDateTime); // Current time and date
    return $diff = date_diff($start, $end);
    /*echo 'The difference is ';
    echo $diff->y . ' years, ';
    echo $diff->m . ' months, ';
    echo $diff->d . ' days, ';
    echo $diff->h . ' hours, ';
    echo $diff->i . ' minutes, ';
    echo $diff->s . ' seconds';
    // Output: The difference is 28 years, 5 months, 19 days, 20 hours, 34 minutes, 36 seconds
    echo 'The difference in days : ' . $diff->days;*/
    // Output: The difference in days : 10398
    
}


function date_difference($FromDateTime, $ToDateTime) 
{
    /*$start = date_create($FromDateTime);    
    $end = date_create($ToDateTime);    
    $diff = date_diff($start, $end);*/
    
    
    /*$diff = abs(strtotime($FromDateTime) - strtotime($ToDateTime));
    $years = floor($diff / (365*60*60*24));
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));*/


    $now = strtotime($FromDateTime); // or your date as well
    $your_date = strtotime($ToDateTime);
    $datediff = abs($now - $your_date);

    $days = round($datediff / (60 * 60 * 24));

    return $days;    
}


function format_number($qty, $len) 
{ 
    if(isset($qty) && !empty($qty)) 
        return number_format($qty, $len, ".", "");
    else
        return $qty;
}