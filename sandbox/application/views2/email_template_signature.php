<section class="content-header">
<h1>
Set Email Signature
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">


<ul class="nav nav-tabs">
<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template_add_w';" data-toggle="tab" aria-expanded="false">Acknowledgement 1</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template_add_r1';" data-toggle="tab" aria-expanded="false">Acknowledgement 2</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template_add_r';" data-toggle="tab" aria-expanded="false">Stock Movement</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template_signature';" data-toggle="tab" aria-expanded="false">Set Signature</a></li>
</ul>


<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row">
<div class="col-lg-12">
<div class="row" style="background-color: #f5f5f5;padding: 10px; margin-bottom: 2%;">
  
  <div class="form-group">   
    <div class="col-sm-6">
        <label><?php echo MANDATORY;?>Send From Name</label>
        <input type="text" name="signature_name" class="form-control" maxlength="100" value="<?php echo $details['signature_name'];?>">
    </div>

    <div class="col-sm-6">
        <label><?php echo MANDATORY;?>Send Email From</label>
        <input type="email" name="signature_from" class="form-control"  maxlength="100"  value="<?php echo $details['signature_from'];?>">
    </div>

  </div>    


  <div class="form-group">
    
    <div class="col-sm-12">
      
      <label for=""><?php echo MANDATORY;?>Signature</label>
      
      <textarea class="form-control rounded-0 textarea" name="signature_content" id="signature_content" rows="7"><?php echo $details['signature_content'];?></textarea>
      
    </div>

  </div>
  

</div>
</div>
</div>


<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/email_template';" class="btn btn-default btn_process">Cancel</button>
      <input type="hidden" name="hdn_id" value="<?php echo $details['email_signature_id'];?>">
      
    </div>
  </div> 
</div>
</form>


<fieldset style="overflow: auto;">
<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_100;?>">Send From Name</th>
<th style="<?php echo COL_100;?>">Send Email From</th>
<th style="<?php echo COL_300;?>">Signature Content</th>
<th style="<?php echo COL_50;?>">Actions</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">
$(document).ready(function()
{
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/email_template_signature_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/email_template_signature'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>


<style type="text/css">
.wysihtml5-toolbar .btn
{
  min-width: 20px !important;
} 
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
$(function () 
{
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5();
});
</script>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
var table;
 
$(document).ready(function() 
{
    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "asc" ]],       
    "aoColumns": [        
    { "bSortable": true },    
    { "bSortable": true },    
    { "bSortable": true },        
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/email_signature_list",
            "type": "POST",
            "data": function ( data ) {
                //data.template_for = $('#template_for').val(); 
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ], 
    });


    /*$(".fby").change(function()
    {
        table.ajax.reload(null, false);
    });*/
 
});

</script>