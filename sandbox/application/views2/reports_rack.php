<section class="content-header">
<h1>
Reatiler Stock Ageing
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">

<ul class="nav nav-tabs">
<!-- <li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/reports';" data-toggle="tab" aria-expanded="false">Acknowledgement 1</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/reports_ack2';" data-toggle="tab" aria-expanded="false">Acknowledgement 2</a></li> -->

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/reports_wack';" data-toggle="tab" aria-expanded="false">Wholesaler Stock Ageing</a></li>

<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/reports_rack';" data-toggle="tab" aria-expanded="false">Reatiler Stock Ageing</a></li>
</ul>


<?php
$arrs = array("typ"=>"states_aop");
$states = $this->common_model->get_dd_list($arrs);
?>
<div class="row" style="margin-bottom:2%;">
  <div class="form-group"> 

    <div class="col-sm-4">
      <label for="fby_state">State Name</label>
      <select name="fby_state" id="fby_state" class="form-control select2 fby" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'fby_city');">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-4">
      <label for="fby_city">District Name</label>
      <select name="fby_city" id="fby_city" onchange="get_dd_list(this.value, 'retailer_by_district', 'fby_dealer');" class="form-control select2 fby"></select>
    </div>

    <div class="col-sm-4">
      <label for="fby_dealer">Reatiler Name</label>
      <select name="fby_dealer" id="fby_dealer" class="form-control select2 fby"></select>
    </div>
</div>
</div>

<div class="row">
<div class="form-group">
    <div class="col-sm-4">
      <label for="fby_product">Product Name</label>
      <select name="fby_product" id="fby_product" class="form-control select2 fby">
        <option value="">All</option>
        <?php
        foreach($products as $obj)
        {
            echo "<option value='".$obj-> product_id."'>".$obj-> product_code."</option>";
        }
        ?>  
      </select>
    </div>

    <!-- <div class="col-sm-6">
      <label for="fby_keyword">Searh by Keyword</label>
      <input class="form-control fby" name="fby_keyword" id="fby_keyword" type="text">  
    </div> -->    
    
  </div>
</div>

<div class="row btn_row">
    <div class="form-group">
    <div class="col-sm-4">
      <button type="submit" name="btn_fby" id="btn_fby" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;Search</button>&nbsp;
      <button type="button" name="btn_reset" id="btn_reset" class="btn btn-danger"><i class="fa fa-empty"></i>&nbsp;Clear Search</button>
    </div> 

    <?php 
    $report_type = "aging_rack";
    include("reports_export.php"); 
    ?>  
  </div> 
</div>
<br/>
<hr/>



<fieldset style="overflow: auto;">

<div id="message_box"></div>

<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_150;?>">Transaction ID</th>
<th style="<?php echo COL_150;?>">Invoice Date</th>

<th style="<?php echo COL_70;?>">Reatiler ID</th>
<th style="<?php echo COL_150;?>">Name</th>
<th style="<?php echo COL_100;?>">State</th>
<th style="<?php echo COL_100;?>">District</th>
<th style="<?php echo COL_100;?>">Product</th>
<th style="<?php echo COL_70;?>">Qty.</th>
<th style="<?php echo COL_70;?>">Ageing</th>
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;
 
$(document).ready(function() 
{
    $(".select2").select2();    


    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "ordering":false,
    "order": [[ 8, "desc" ]],       
    "aoColumns": [        
    { "bSortable": true },        
    { "bSortable": true },        
    { "bSortable": true },        
    { "bSortable": true },
    { "bSortable": true },        
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/reports_rack_list",
            "type": "POST",
            "data": function ( data ) {    
                //data.saler_type = $('#fby_saler_type').val();
                data.fby_state = $('#fby_state').val();
                data.fby_city = $('#fby_city').val();
                data.fby_dealer = $('#fby_dealer').val();
                data.fby_product = $('#fby_product').val();
                //data.fby_to_date = $('#fby_to_date').val();
                //data.fby_keyword = $('#fby_keyword').val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ],
 
    });


    $("#btn_fby").click(function()
    {
        table.ajax.reload(null, true);
    });


    $("#btn_reset").click(function()
    {
        $('#fby_city, #fby_dealer').find("option:gt(0)").remove();
        
        $(".fby").val("");
        $(".select2").select2().trigger("chosen:updated");

        $("#btn_fby").trigger("click");
    });
 
});
</script>