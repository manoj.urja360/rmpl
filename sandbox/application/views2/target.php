<section class="content-header">
<h1>
Target Settings
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">


<div class="box-body">

<!-- <ul class="nav nav-tabs">
<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/area_of_operation';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/area_of_operation_set';" data-toggle="tab" aria-expanded="false">Set Area Of Operation</a></li>
</ul> -->
<?php
$month = array("01"=>'January', 
"02"=>'February', 
"03"=>'March', 
"04"=>'April', 
"05"=>'May', 
"06"=>'June', 
"07"=>'July', 
"08"=>"August", 
"09"=>"September", 
"10"=>"October", 
"11"=>"November", 
"12"=>"December");
?>
<fieldset>
<div id="message_box"></div>
<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="row"><h4>Set Target for:</h4>
<div class="form-group">

<div class="col-sm-2">
  <label><?php echo MANDATORY;?>Target for Year</label>
  <select class="form-control" name="tyear" id="tyear" onchange="get_old_target();">    
  <?php
  $cy = date("Y");
  for($y=$cy-1; $y<=($cy+1);$y++)
  {
      $s = "";
      if((isset($details->target_year) && $details->target_year == $y) || ($y == $cy)) $s = "selected";
      echo "<option value='$y' $s>$y</option>";
  }?>
  </select>  
</div>


<div class="col-sm-2">
  <label><?php echo MANDATORY;?>Target for Month</label>
  <select class="form-control select2" name="tmonth" id="tmonth" onchange="get_old_target();">    
  <option value=''>Select</option>
  <?php 
  foreach($month as $id => $nm)
  {
      $s = "";
      if(isset($details->target_month) && $details->target_month == $id) $s = "selected";
      echo "<option value='$id' $s>$nm</option>";
  }?>
  </select>
</div>

  <div class="col-sm-3">
      <label for="tstate"><?php echo MANDATORY;?>For State</label>
      <select name="tstate" id="tstate" class="form-control select2" onchange="get_old_target();">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
              $s = "";
              if(isset($details->state_id) && $details->state_id == $obj->state_id)  $s = "selected";
          ?>    
            <option value="<?php echo $obj->state_id;?>" <?php echo $s;?>><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>

  <div class="col-sm-2">
      <label><?php echo MANDATORY;?>Target in %</label>
      <input class="form-control" name="tpercent" id="tpercent" value="<?php if(isset($details->target_percent)) echo $details->target_percent;?>" type="text" maxlength="3" onkeyup="chk_numeric(this); calculate_target_value(this.value);">
  </div>  
    
  <div class="col-sm-3">
      <label><?php echo MANDATORY;?>Target in MT</label>
      <input class="form-control" name="tvalue" id="tvalue" value="<?php if(isset($details->target_value)) echo $details->target_value;?>" type="text" maxlength="10">
      <input type="hidden" name="tvalue_hdn" id="tvalue_hdn" value="0">
  </div>  
</div>
</div>


<div class="row">
<div class="form-group"> 
<div class="col-sm-12 text-center" id="load_old_target">Opening stock will display here...</div>  
</div>
</div>



<br/>
<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary">Save</button>&nbsp;
      <button type="button" name="btn_cancel" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/target';" class="btn btn-default">Cancel</button>
      <input name="hdn_id" value="<?php if(isset($details->target_id)) echo $details->target_id;?>" type="hidden">
    </div>
  </div> 
</div>
</form>


<br/>
<hr/>
<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_150;?>">State Name</th>
<th style="<?php echo COL_100;?>">Target Year</th>
<th style="<?php echo COL_100;?>">Month</th>
<th style="<?php echo COL_100;?>">Target in %</th>              
<th style="<?php echo COL_100;?>">Target in MT</th>              
<th style="<?php echo COL_50;?>">Actions</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>


</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
var table;
 
$(document).ready(function() 
{
    $(".select2").select2();  

    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "ASC" ], [ 1, "DESC" ]],       
    "aoColumns": [        
    { "bSortable": true },
    { "bSortable": true },    
    { "bSortable": true },
    { "bSortable": true },    
    { "bSortable": true },    
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/target_list",
            "type": "POST",
            "data": function ( data ) {
                //data.qs_name = $('#qs_name').val();                
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ],
 
    });
 
    $('#search').click(function()
    { 
        table.ajax.reload(null,false);  //just reload table
    });

    
});



$(document).ready(function()
{
    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/target_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    $("#tvalue").val("");

                    //table.ajax.reload(null,false); 
                    
                    //$("#message_box").fadeOut("1000");
                    window.location.href = base_url+'admin/target'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }


              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });


    <?php
    if(isset($target_id) && !empty($target_id))
    {
    ?>
        setTimeout(function()
        { 
          get_old_target(); 
        }, "500");
    <?php  
    }
    ?>
});


function get_old_target()
{
    var tyear = $("#tyear").val();
    var tmonth = $("#tmonth").val();
    var tstate = $("#tstate").val();

    if(tyear <= 0 || tmonth <= 0 || tstate <= 0) return;    

    $("#load_old_target").html('<img src="'+base_url+'assets/images/loader_add.gif">');

    var formData = {"tyear":tyear, "tmonth":tmonth, "tstate":tstate};

    $.ajax({url : base_url+"admin/get_old_target",
      method: "POST",
      data: formData,
      async: false,
      dataType: 'json',
      success: function(res)
      {   
          //var str = "<tr><td>State</td><td>Period</td><td>Type</td><td>Quantity in MT</td></tr>";
          var total=  0;
          var str = "<tr><td colspan='2'>Opening Stock</td></tr>";
          str = str + "<tr><td>Type</td><td>Quantity in MT</td></tr>";
          if(res.status == 1)
          {
              $.each(res.data, function(i,v)
              {
                  str = str + "<tr><td>"+v.type+"</td><td>"+v.quantity+"</td></tr>";

                  total = parseFloat(total) + parseFloat(v.quantity);
              });

              str = str + "<tr><td><b>Total</b></td><td><b>"+total.toFixed(2)+"</b></td></tr>";

              $("#tvalue_hdn").val(total);
          }
          else
          {
              str = "<tr><td colspan='2'>"+res.message+"</td></tr>";
          } 

          $("#load_old_target").html("<table width='40%' border='1'>"+str+"</table>");         
      }
  });        
    
}


function calculate_target_value(per)
{
    var opn = $("#tvalue_hdn").val();

    if(per != null && per != undefined && per >= 0 && opn >= 0 && opn != null && opn != undefined && !isNaN(per) && !isNaN(opn))
    {
        var t = ((parseFloat(opn) * parseFloat(per)) / 100).toFixed(2);

        $("#tvalue").val(t);
    }
    else
    {
        $("#tvalue").val(0);
    }
}

</script>