<style type="text/css">
.view_gallery_model
{
    position: fixed;
    left: 0%;
    top:4%;
    background-color: #fff;
    border: 5px solid #ddd;
    border-radius: 3px;
    display: none;
    width: 94%;
    height: auto;
    overflow-y: auto; 
    overflow-x: hidden;
    z-index: 999;
    margin: 3% 3%;
}


.modal.and.carousel, .carousel-control 
{
  position: fixed; 
} 


</style>
<section class="content-header">
<h1>
Demos
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">

<div class="box-body">


<ul class="nav nav-tabs">
<li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos';" data-toggle="tab" aria-expanded="false">View All</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add';" data-toggle="tab" aria-expanded="false">Add Demo Stage 1</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add2';" data-toggle="tab" aria-expanded="false">Add Demo Stage 2</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add3';" data-toggle="tab" aria-expanded="false">Add Demo Stage 3</a></li>

<li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/demos_add4';" data-toggle="tab" aria-expanded="false">Add Demo Stage 4</a></li>

</ul>


<?php
$arrs = array("typ"=>"states_aop");
$states = $this->common_model->get_dd_list($arrs);
?>
<div class="row" style="margin-bottom:2%;margin-top:2%;">
  <div class="form-group">    
    <div class="col-sm-4">
      <label for="fby_state">State Name</label>
      <select name="fby_state" id="fby_state" class="form-control select2 fby" onchange="get_dd_list(this.value, 'district_by_state_aop_dd', 'fby_city');">
        <option value="">Select</option>
        <?php
          foreach($states as $obj)
          {
          ?>    
            <option value="<?php echo $obj->state_id;?>"><?php echo $obj->state_name;?></option>
          <?php
          }
        ?>
       </select>
    </div>


    <div class="col-sm-4">
      <label for="fby_city">District Name</label>
      <select name="fby_city" id="fby_city" class="form-control select2 fby" onchange="get_dd_list(this.value, 'district_executive', 'fby_executive');"></select>
    </div> 

    <div class="col-sm-4">
      <label for="fby_executive">Executive Name</label>
      <select name="fby_executive" id="fby_executive" class="form-control select2 fby"></select>
    </div>

     
</div>
</div>

<div class="row">
<div class="form-group">
    <div class="col-sm-2">
      <label for="fby_from_date">From Reg. Date</label>
      <input class="form-control fby" name="fby_from_date" id="fby_from_date" type="text" maxlength="50" value="" readonly>  
    </div>

    <div class="col-sm-2">
      <label for="fby_to_date">To Reg. Date</label>
      <input class="form-control fby" name="fby_to_date" id="fby_to_date" type="text" maxlength="50" value="" readonly>  
    </div>

    

    <div class="col-sm-3">
      <label for="fby_is_approved">Approve/Pending</label>
      <select class="form-control fby" name="fby_is_approved" id="fby_is_approved">  
        <option value="">All</option>
        <option value="0">Pending</option>
        <option value="1">Approved</option>        
      </select>  
    </div>   

    <div class="col-sm-5">
      <label for="fby_keyword">Searh by Keyword</label>
      <input class="form-control fby" name="fby_keyword" id="fby_keyword" type="text">  
    </div> 
    
  </div>
</div>

<div class="row btn_row">
    <div class="form-group">
    <div class="col-sm-4">
      <button type="submit" name="btn_fby" id="btn_fby" class="btn btn-success"><i class="fa fa-search"></i>&nbsp;Search</button>&nbsp;
      <button type="button" name="btn_reset" id="btn_reset" class="btn btn-danger"><i class="fa fa-empty"></i>&nbsp;Clear Search</button>
    </div>

    <div class="col-sm-8">
        
      <div class="btn-group" style="float: right; margin-right: 2%;">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="fa fa-cloud-download"></span>&nbsp;Export</button>
        
        <ul class="dropdown-menu" role="menu">
          <li><a href="#" onclick="export_data('csv');"><i class="fa fa-file-text-o"></i>&nbsp;CSV</a></li>
          

          <li><a href="#" onclick="export_data('xls');"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</a></li>
          

          <li><a href="#" onclick="export_data('pdf');"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a></li>
        </ul>
      </div>      
    </div>  
  </div> 
</div>
<br/>
<hr/>


<fieldset style="overflow: auto;">
<div id="message_box"></div>

<table id="example1" class="table <?php echo TABLE_LISTING_CLASS;?>" width="99%">
<thead>
<tr class="table_head">
<th style="<?php echo COL_70;?>">Photo</th>
<th style="<?php echo COL_70;?>">Reg. No.</th>
<th style="<?php echo COL_200;?>">Full Name</th>
<th style="<?php echo COL_150;?>">Mobile</th>
<th style="<?php echo COL_150;?>">Address</th>
<th style="<?php echo COL_150;?>">Taluka</th>
<th style="<?php echo COL_150;?>">District</th>
<th style="<?php echo COL_150;?>">State</th>
<th style="<?php echo COL_100;?>">Executive Name</th>
<th style="<?php echo COL_70;?>">Actions</th>              
</tr> 
</thead>
<tbody>
</tbody>
</table>
</fieldset> 
</div>
</div>  
</div>
</div>
</section>


<form class="form-horizontal" name="process_form" id="process_form" method="post">
<div class="modal row" id="lightbox">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span></button>
<h4 class="modal-title">Approve Reason</h4>
</div>
<div class="modal-body">
<div class="row">
  <div class="form-group">
    <div class="col-sm-12" style="margin-right:3%; margin-left:3%; width:93%;">
      <label for="approve_reason"><?php echo MANDATORY;?>Approve Reason</label>      
      <textarea name="approve_reason" id="approve_reason" class="form-control" cols="2"></textarea><small>Mamimum 255 characters are allowed.</small>
      <input type="hidden" name="demo_id" id="demo_id" value="">  
    </div>    
  </div>
</div>
</div>
<div class="modal-footer">
<button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>
<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</form>



<link href="<?php echo base_url();?>assets/plugins/serversidedatatable/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/plugins/serversidedatatable/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
function show_popup(demo_id, obj)
{
    $("#demo_id").val(demo_id);
    $("#lightbox").modal({show:true});
    $("#approve_reason").val($(obj).attr("name"));
}



var table;
 
$(document).ready(function() {
 
    $(".select2").select2();


    //datatables
    table = $('#example1').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.        
    "sDom": '<?php echo PAGING_POS;?>',
    "scrollX": <?php echo SCROLL_X;?>,
    "pageLength": <?php echo PAGE_LENGTH;?>,
    "pagingType": "<?php echo PAGING_TYPE;?>",
    "order": [[ 0, "asc" ]],       
    "aoColumns": [        
    { "bSortable": true },        
    { "bSortable": true },
    { "bSortable": true },        
    { "bSortable": true },        
    { "bSortable": true },
    { "bSortable": true },
    { "bSortable": true }, 
    { "bSortable": true },
    { "bSortable": true },        
    { "bSortable": false }
    ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"admin/demos_list",
            "type": "POST",
            "data": function ( data ) 
            {
                data.fby_state = $("#fby_state").val();
                data.fby_city = $("#fby_city").val(); 
                data.fby_executive = $("#fby_executive").val();                
                data.fby_from_date = $("#fby_from_date").val();
                data.fby_to_date = $("#fby_to_date").val();
                data.fby_keyword = $("#fby_keyword").val(); 
                data.fby_is_approved = $("#fby_is_approved").val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            
        },
        ],
 
    });


    $("#btn_fby").click(function()
    {
        table.ajax.reload(null, false);
    });


    $("#btn_reset").click(function()
    {
        $(".fby").val("");
        $(".select2").select2().trigger("chosen:updated");

        $("#btn_fby").trigger("click");
    });
    

    $("#process_form").submit(function()
    {
        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/approve_reason_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
                if(res.status == 1)
                {   
                    alert(res.message);
                    
                    setTimeout(function()
                    {                    
                        window.location.href = base_url+'admin/demos'; 

                    }, 0);
                }
                else
                {
                    alert(res.message);
                }              
                
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
 
});


function export_data(typ)
{
    var file_id = "0";
    var report_type = "demos";

    var fby_state = $("#fby_state").val();
    var fby_city = $("#fby_city").val();
    var fby_product = "0";
    var fby_from_date = $("#fby_from_date").val();
    var fby_to_date = $("#fby_to_date").val();
    var fby_keyword = $("#fby_keyword").val();
    var fby_executive = $("#fby_executive").val();

    if(fby_state == "" || fby_state == null) fby_state = 0;
    if(fby_city == "" || fby_city == null) fby_city = 0;    
    if(fby_from_date == "" || fby_from_date == null) fby_from_date = 0;
    if(fby_to_date == "" || fby_to_date == null) fby_to_date = 0;
    
    if(fby_keyword == "" || fby_keyword == null) fby_keyword = 0;
    if(fby_executive == "" || fby_executive == null) fby_executive = 0;
    
    var url = base_url + "admin/export_data/"+ typ + "/" + report_type + "/" + file_id + "/" + fby_state + "/" + fby_city + "/" + fby_product + "/" + fby_from_date + "/" + fby_to_date + "/" + fby_keyword + "/" + fby_executive;

    window.open(url,"");
}
</script>