<section class="content-header">
<h1>
Edit Company Profile
</h1>
</section>


<!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-default">
<div class="box-body">

<?php
if($this->user_role_id == 1 && $this->user_id == 1)
{
?>
  <ul class="nav nav-tabs">
  <li class="active"><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/profile';" data-toggle="tab" aria-expanded="false">Company Profile</a></li>

  <li class=""><a href="#" onclick="javascript:document.location.href = '<?php echo base_url();?>admin/profile_user';" data-toggle="tab" aria-expanded="false">Your Profile</a></li>
  </ul>
<?php
}
?>


<div id="message_box"></div>

<form class="form-horizontal" name="process_form" id="process_form" method="post">

<div class="row">
  <div class="form-group">        
    <div class="col-sm-2">
      <img src="<?php echo base_url().$details->photo_url;?>?t=<?php echo time();?>" height="50px" style="max-width:200px;">
    </div>

    <div class="col-sm-10">      
      <label for="logo"><?php echo MANDATORY;?>Upload New Logo</label>
      <input class="form-control" name="logo" value="" type="file">
      <input name="photo_url" value="<?php echo $details->photo_url;?>" type="hidden">
    </div>
</div>
</div>

<br/><br/>
<div class="row">
  <div class="form-group">
    <div class="col-sm-12">
      <label for="company_name"><?php echo MANDATORY;?>Company Name</label>
      <input class="form-control" name="company_name" value="<?php echo $details->company_details['company_name'];?>" type="text" maxlength="100">
    </div>
    
  </div>
</div> 



<div class="row"><h4>Company Address</h4>
  <div class="form-group">    
    <div class="col-sm-6">
      <label for="cstates_name"><?php echo MANDATORY;?>Select State Name</label>
      <select name="cstates_name" id="cstates_name" class="form-control select2" onchange="get_dd_list(this.value, 'district_by_state_name', 'ccity_name');">
        <option value="">Select</option>
        <?php
          foreach($states_all as $obj)
          {
              $sel = "";
              if($details->company_details['cstates_name'] == $obj->state_name)
                $sel = "selected";
          ?>    
            <option value="<?php echo $obj->state_name;?>" <?php echo $sel;?>><?php echo $obj->state_name;?></option>
          <?php
            if(isset($sel) && !empty($sel))
            {
          ?>
                <script type="text/javascript">
                  get_dd_list("<?php echo $obj->state_name;?>", 'district_by_state_name', 'ccity_name');                  
                </script>
          <?php    
            }
          }
          ?>              
        <?php  
        ?>
       </select>
    </div>


    <div class="col-sm-6">
      <label for="ccity_name"><?php echo MANDATORY;?>Select City Name</label>
      <select name="ccity_name" id="ccity_name" class="form-control select2">
        <option value="<?php echo $details->company_details['ccity_name'];?>"><?php echo $details->company_details['ccity_name'];?></option>
      </select>  
      <script type="text/javascript">
      $(function()
      { 
          setTimeout(function()
          {
              $("#ccity_name").val("<?php echo $details->company_details['ccity_name'];?>");

              $("#ccity_name").select2().trigger("chosen:updated");
          }, "500");
          
      });
      </script>
    </div>
  </div> 
  
  <div class="form-group">
    <div class="col-sm-6">
      <label for="caddress"><?php echo MANDATORY;?>Address 1</label>
      <input class="form-control" name="caddress" id="caddress" type="text" value="<?php echo $details->company_details['caddress'];?>" maxlength="255">
    </div>

    <div class="col-sm-6">
      <label for="caddress2">Address 2</label>
      <input class="form-control" name="caddress2" id="caddress2" type="text" value="<?php echo $details->company_details['caddress2'];?>" maxlength="255">
    </div>
  </div>  

  <div class="form-group">
    <div class="col-sm-4">
      <label for="pincode">Pincode</label>
      <input class="form-control" name="pincode" id="pincode" type="text" value="<?php echo $details->company_details['pincode'];?>" onkeyup="chk_numeric(this);" maxlength="7">
    </div>
  </div>    
</div>


<div class="row"><h4>Company Contact Info</h4>
  <div class="form-group">
    <div class="col-sm-4">
      <label for="email"><?php echo MANDATORY;?>Email</label>
      <input class="form-control" name="email" value="<?php echo $details->company_details['email'];?>" type="email" maxlength="150">
    </div>

    <div class="col-sm-4">
      <label for="mobile"><?php echo MANDATORY;?>Mobile</label>
      <input class="form-control" name="mobile" value="<?php echo $details->company_details['mobile'];?>" type="text" maxlength="10">
    </div>

    <div class="col-sm-4">
      <label for="landline">Landline Number</label>
      <input class="form-control" name="landline" value="<?php echo $details->company_details['landline'];?>" type="text" maxlength="15">
    </div>
  </div>
</div>







<div class="row"><h4>Social Links</h4>
  <div class="form-group">
    <div class="col-sm-4">
      <label for="twitter">Twitter</label>
      <input class="form-control" name="twitter" value="<?php echo $details->company_details['twitter'];?>" type="url" maxlength="255">
    </div>

    <div class="col-sm-4">
      <label for="facebook">Facebook</label>
      <input class="form-control" name="facebook" value="<?php echo $details->company_details['facebook'];?>" type="url" maxlength="255">
    </div>

    <div class="col-sm-4">
      <label for="linkedin">Linkedin</label>
      <input class="form-control" name="linkedin" value="<?php echo $details->company_details['linkedin'];?>" type="url" maxlength="255">
    </div>    
  </div>


  <div class="form-group">
    <div class="col-sm-4">
      <label for="google">Google</label>
      <input class="form-control" name="google" value="<?php echo $details->company_details['google'];?>" type="url" maxlength="255">
    </div>

    <div class="col-sm-4">
      <label for="instagram">Instagram</label>
      <input class="form-control" name="instagram" value="<?php echo $details->company_details['instagram'];?>" type="url" maxlength="255">
    </div>

    <div class="col-sm-4">
      <label for="youtube">Youtube Channel</label>
      <input class="form-control" name="youtube" value="<?php echo $details->company_details['youtube'];?>" type="url" maxlength="255">
    </div>    
  </div>


  <div class="form-group">
    <div class="col-sm-4">
      <label for="whatsapp">Whatsapp Number</label>
      <input class="form-control" name="whatsapp" value="<?php echo $details->company_details['whatsapp'];?>" type="text" maxlength="10">
    </div>   
  </div>
</div>

<br/>
<div class="row">
    <div class="form-group">
    <div class="col-sm-6">
      <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary btn_process">Save</button>      
    </div>
  </div> 
</div> 


</form>
</div>
</div>  
</div>
</div>
</section>


<script type="text/javascript">

$(document).ready(function()
{
    $("#cstates_name").select2();
    $("#ccity_name").select2()

    $("#process_form").submit(function()
    {
        processing_bar();

        var formData = new FormData($(this)[0]);

        $.ajax({url : base_url+"admin/profile_save",
          method: "POST",
          data: formData,
          async: false,
          dataType: 'json',
          success: function(res)
          {   
              if(res.status == 1)
              {
                  msg = msg_ok + res.message + '</div>';

                  setTimeout(function()
                  {                    
                    window.location.href = base_url+'admin/profile'; 
                    
                  }, time_out);
              }
              else
              {
                  msg = msg_error + res.message + '</div>';

                  hide_msg_box();
              }
              
              show_msg_box(msg);
          },
          cache: false,
          contentType: false,
          processData: false
        });

        return false;
    });
});
</script>