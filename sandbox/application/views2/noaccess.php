<?php
$this->load->view('includes/main_header');
?>

<section class="content-header">
<h1>
Unauthorized Access
</h1>
</section>


<!-- Main content -->
<section class="content">

      <div class="error-page">
        <h2 class="headline text-red">403</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>

          <p>
            You are not allowed to access this module.<br/>
          </p>

          <p>
            Please contact to management.
          </p>

          <p>
            <button class="btn btn-default"><a href='#' onclick="javascript:window.history.go(-1);">Go Back</a></button>
          </p> 
          
        </div>
      </div>
      <!-- /.error-page -->

</section>

<?php
$this->load->view('includes/main_footer'); 

exit;           
?>