<!-- Logo -->
    <a href="<?php echo base_url(); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/images/logo.png?t=<?php echo time();?>" alt="<?php echo PROJECT_NAME;?>"><b><?php echo PROJECT_NAME;?></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?php echo base_url(); ?>assets/images/logo.png?t=<?php echo time();?>" alt="<?php echo PROJECT_NAME;?>"><b><?php echo PROJECT_NAME;?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          
          <!-- Tasks: style can be found in dropdown.less -->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url().$this->user_photo_url;?>?t=<?php echo time();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->user_fullname;?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url().$this->user_photo_url;?>?t=<?php echo time();?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->user_fullname;?> - <small><?php echo $this->user_role_name;?></small>
                  <small>Member since <?php echo $this->user_created_date;?></small>
                </p>
              </li>
              
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                <?php
                if($this->user_role_id == 1 && $this->user_id == 1)
                {
                ?>  
                    <a href="<?php echo base_url(); ?>admin/profile" class="btn btn-default btn-flat">Profile</a>
                <?php
                }
                else
                {
                ?>
                    <a href="<?php echo base_url(); ?>admin/profile_user" class="btn btn-default btn-flat">Profile</a>
                <?php  
                }
                ?>    
                </div>

                <div class="pull-right">
                  <a href="<?php echo base_url(); ?>login/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>