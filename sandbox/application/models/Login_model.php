<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model 
{
    
    public function __construct()
    {
        parent::__construct();
    }

    
    public function check_login($username,$password)
    {
        $sql = "SELECT u.*, r.role_name
        FROM users u
        INNER JOIN users_login ul ON u.user_id = ul.user_id
        LEFT JOIN roles r ON r.role_id = u.role_id
        WHERE u.is_deleted = 0 AND u.admin_access = 'yes' AND u.username = '$username' AND ul.password = '$password' AND ul.password_for = 1
        LIMIT 1";

        $query = $this->db->query($sql);

        return $query->row();
    }


    public function get_aoo($user_id)
    {
        $sql = "SELECT u.state_id, u.district_id, u.state_name, u.district_name
        FROM users_area u        
        WHERE u.user_id = $user_id 
        ORDER BY u.state_name, u.district_name";

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function check_email($username, $email)
    {
        $sql = "SELECT u.admin_access, u.user_id, u.first_name, u.last_name
        FROM users u        
        WHERE u.is_deleted = 0 AND u.email = '$email' AND u.username = '$username'  
        LIMIT 1";

        $query = $this->db->query($sql);

        return $query->row();
    }


}
?>