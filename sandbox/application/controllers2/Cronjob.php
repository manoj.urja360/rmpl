<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob extends CI_Controller 
{
	public function __construct()
    {
        parent::__construct();

        $this->load->model("Cronjob_model");
        $this->load->model("admin_model");
    }    

	public function api_call($cronName, $Typ = "", $InstId = 0)
	{
        $url = base_url().'cronjob/'.$cronName."?typ=".$Typ."&id=".$InstId;                     

        $ch = curl_init($url);        

        $data = ['param1'=>'', 'param2'=>''];        

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);        

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json',
            'App-Key: 12356789',
            'App-Secret: 12356789'
        ));       

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                   

        $result = curl_exec($ch);
           
        echo "<pre>"; print_r($result);        

        curl_close($ch);

		if($Response['ResponseCode'] == 200)
		{
			echo $Response['Message'];
		}

		exit;
	}


	public function trigger_sms()
	{		
		$Response = array();

		$chk = $this->Cronjob_model->trigger_sms();      	
		
		if($chk)
		{
			$Response['status'] 	=	1;

			$Response['message']      	=	"Sent Successfully.";
		}		
		else
		{
			$Response['status'] 	=	0;

			$Response['message']      	=	"Error";
		}

		p($Response);		
	}


	public function trigger_email()
	{		
		$Response = array();

		$chk = $this->Cronjob_model->trigger_email();      	
		
		if($chk)
		{
			$Response['status'] 	=	1;

			$Response['message']      	=	"Sent Successfully.";
		}		
		else
		{
			$Response['status'] 	=	0;

			$Response['message']      	=	"Error";
		}

		p($Response);	
	}
	

	//Alert for acknowledgement 1-------------------------------------------------
	public function alert_for_ack1_due()
	{		
		$Response = array();

		$details = $this->admin_model->last_upload_date('upload_acknowledgement1');
        if(isset($details) && !empty($details))
        {
            $file_id = $details-> file_id;

            $chk = $this->Cronjob_model->alert_for_ack1_due($file_id);

            if($chk)
			{
				$Response['status'] 	=	1;

				$Response['message']      	=	"Sent Successfully.";

				$Response['data'] 	=	$chk;
			}		
			else
			{
				$Response['status'] 	=	0;

				$Response['message']      	=	"Error";

				$Response['data'] 	=	array();
			}
        }		

		p($Response);	
	}


	//Alert for acknowledgement 2-------------------------------------------------
	public function alert_for_ack2_due()
	{		
		$Response = array();

		$details = $this->admin_model->last_upload_date('upload_acknowledgement2');
        if(isset($details) && !empty($details))
        {
            $file_id = $details-> file_id;

            $chk = $this->Cronjob_model->alert_for_ack2_due($file_id);

            if($chk)
			{
				$Response['status'] 	=	1;

				$Response['message']      	=	"Sent Successfully.";

				$Response['data'] 	=	$chk;
			}		
			else
			{
				$Response['status'] 	=	0;

				$Response['message']      	=	"Error";

				$Response['data'] 	=	array();
			}
        }		

		p($Response);	
	}



	//Alert for no stock movement-------------------------------------------------
	public function alert_for_no_stock_movement()
	{		
		$Response = array();       

        $chk = $this->Cronjob_model->alert_for_no_stock_movement();

        if($chk)
		{
			$Response['status'] 	=	1;

			$Response['message']      	=	"Sent Successfully.";

			$Response['data'] 	=	$chk;
		}		
		else
		{
			$Response['status'] 	=	0;

			$Response['message']      	=	"Error";

			$Response['data'] 	=	array();
		}
        		

		p($Response);	
	}



	public function set_dispatch_date_ack1()
	{		
		$Response = array();

		$details = $this->admin_model->last_upload_date('upload_acknowledgement1');
		$details1 = $this->admin_model->last_upload_date_all('upload_acknowledgement1');
        if(isset($details) && !empty($details) && isset($details1) && !empty($details1))
        {            
            $file_id = $details-> file_id;
            $file_ids = $details1;            
           
            $chk = $this->Cronjob_model->set_dispatch_date_ack1($file_id, $file_ids);

            if($chk)
			{
				$Response['status'] 	=	0;

				$Response['message']      	=	"Error";

				$Response['data'] 	=	array();
			}		
			else
			{
				$Response['status'] 	=	1;

				$Response['message']      	=	"Set Successfully.";

				$Response['data'] 	=	$chk;
			}
        }		

		p($Response);	
	}	



	public function set_dispatch_date_ack2()
	{		
		$Response = array();

		$details = $this->admin_model->last_upload_date('upload_acknowledgement2');
		$details1 = $this->admin_model->last_upload_date_all('upload_acknowledgement2');
        if(isset($details) && !empty($details) && isset($details1) && !empty($details1))
        {            
            $file_id = $details-> file_id;
            $file_ids = $details1;            
           	
            $chk = $this->Cronjob_model->set_dispatch_date_ack2($file_id, $file_ids);

            if($chk)
			{
				$Response['status'] 	=	0;

				$Response['message']      	=	"Error";

				$Response['data'] 	=	array();
			}		
			else
			{
				$Response['status'] 	=	1;

				$Response['message']      	=	"Set Successfully.";

				$Response['data'] 	=	$chk;
			}
        }		

		p($Response);	
	}




	public function set_stock_wholesaler()
	{		
		$Response = array();

		$details = $this->admin_model->last_upload_date('upload_dispatch');
		$details1 = $this->admin_model->last_upload_date('upload_wholesaler');
        if(isset($details) && !empty($details) && isset($details1) && !empty($details1))
        {            
            $file_id1 = $details-> file_id;
            $file_id2 = $details1-> file_id;            
           
            $chk = $this->Cronjob_model->set_stock_wholesaler($file_id1, $file_id2);

            if($chk)
			{
				$Response['status'] 	=	0;

				$Response['message']      	=	"Error";

				$Response['data'] 	=	array();
			}		
			else
			{
				$Response['status'] 	=	1;

				$Response['message']      	=	"Set Successfully.";

				$Response['data'] 	=	$chk;
			}
        }		

		p($Response);	
	}


	public function set_stock_retailer()
	{		
		$Response = array();

		$details = $this->admin_model->last_upload_date('upload_acknowledgement2');
		$details1 = $this->admin_model->last_upload_date('upload_retailer');
        if(isset($details) && !empty($details) && isset($details1) && !empty($details1))
        {            
            $file_id1 = $details-> file_id;
            $file_id2 = $details1-> file_id;            
           
            $chk = $this->Cronjob_model->set_stock_retailer($file_id1, $file_id2);

            if($chk)
			{
				$Response['status'] 	=	0;

				$Response['message']      	=	"Error";

				$Response['data'] 	=	array();
			}		
			else
			{
				$Response['status'] 	=	1;

				$Response['message']      	=	"Set Successfully.";

				$Response['data'] 	=	$chk;
			}
        }		

		p($Response);	
	}



	public function set_rating_wholesaler()
	{		
		$Response = array();

		$chk = $this->Cronjob_model->set_rating_wholesaler();

        if($chk)
		{
			$Response['status'] 	=	0;

			$Response['message']      	=	"Error";

			$Response['data'] 	=	array();
		}		
		else
		{
			$Response['status'] 	=	1;

			$Response['message']      	=	"Set Successfully.";

			$Response['data'] 	=	$chk;
		}
        		

		p($Response);	
	}


	public function set_rating_retailer()
	{		
		$Response = array();

		$chk = $this->Cronjob_model->set_rating_retailer();

        if($chk)
		{
			$Response['status'] 	=	0;

			$Response['message']      	=	"Error";

			$Response['data'] 	=	array();
		}		
		else
		{
			$Response['status'] 	=	1;

			$Response['message']      	=	"Set Successfully.";

			$Response['data'] 	=	$chk;
		}
        		

		p($Response);	
	}

	public function set_pos()
	{		
		$Response = array();

		$chk = $this->Cronjob_model->set_pos();

        if($chk)
		{
			$Response['status'] 	=	0;

			$Response['message']      	=	"Error";

			$Response['data'] 	=	array();
		}		
		else
		{
			$Response['status'] 	=	1;

			$Response['message']      	=	"Set Successfully.";

			$Response['data'] 	=	$chk;
		}
        		

		p($Response);	
	}

	

}