<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
	public function __construct()
    {
        parent::__construct();        
        
        $this->load->model("login_model");

    }



	public function index()
	{
		if($this->is_logged_in())
		{
			redirect('/');
		}
		else
		{
			$arr = array("typ" => "users", "id" => 1);
            $data['details'] = $this->common_model->get_detail($arr);
            if(isset($data['details']->company_details) && !empty($data['details']->company_details))
            {
                $data['details']->company_details = json_decode($data['details']->company_details);

                $data['details']->company_details = (array) $data['details']->company_details;
            }

			$this->load->view('login', $data);
		}	
	}


	public function forgot()
	{
		if($this->is_logged_in())
		{
			redirect('/');
		}
		else
		{
			$this->load->view('forgot');
		}	
	}


	public function is_logged_in()
    {
    	$data = $this->session->all_userdata();

    	if(isset($data['user_id']) && !empty($data['user_id']))
    	{
    		redirect('/admin');
    	}
    	else
    	{ 		
    		return false;
    	}
    }


	public function check_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');	

		if($username == "")
		{
			echo json_encode(array("status"=>0,"message"=>"Please enter Username."));
			
			die;	
		}
		elseif($password == "")
		{
			echo json_encode(array("status"=>0,"message"=>"Please enter Password."));
			
			die;	
		}

		$data = $this->login_model->check_login($username,md5($password));

		if(isset($data) && !empty($data))
		{
			if($this->input->post('rememberme') == 1  && !empty($this->input->post('rememberme')))
			{
				$this->input->set_cookie('username',$username,'3600');
				$this->input->set_cookie('password',$password,'3600');
			}


			$this->dbaccess_model->update('users_login',array("last_login_date"=>date(DATETIME_DB)),array("user_id ="=>$data->user_id, "password_for =" => 1));
			
			$session_data = (array)$data;	
			$this->session->set_userdata($session_data);

			echo json_encode(array("status"=>1,"message"=>"Successfully Sign In.")); die;
		}
		else
		{
			echo json_encode(array("status"=>0,"message"=>"Sign In failed."));
		}

		die;
	}


	public function do_login()
	{
		$keyid = $this->input->post('keyid');

		if($keyid == "")
		{
			echo json_encode(array("status"=>0,"message"=>"Please enter key."));			
			die;	
		}

		$this->load->model("webservice_model");
		$data = $this->webservice_model->do_login($keyid);

		if(isset($data) && !empty($data))	
		{			
			if(strtolower($data-> app_access) == "no")
			{
				echo json_encode(array("status"=>0,"message"=>"You do not have permission to access mobile app. Please contact to management."));			
				die;
			}

			$session_key = time().rand(10000, 99999).time().rand(10000, 99999);

			$this->dbaccess_model->update('users_login',array("last_login_date"=>date(DATETIME_DB), "session_key" => $session_key),array("user_id ="=>$data-> user_id, "password_for =" => 2));
						
			$data-> session_key = $session_key;

			$data-> aoo = $this->login_model->get_aoo($data-> user_id);


			echo json_encode(array("status"=>1, "message"=>"Successfully Sign In.", "data" => $data)); die;
		}
		else
		{
			echo json_encode(array("status"=>0, "message"=>"Sign In failed."));
		}

		die;
	}



	public function logout()
	{
		session_destroy();

		$data = $this->session->all_userdata();

		$this->session->unset_userdata($data);

		echo "<script> window.location.href = '".base_url()."'; </script>";

		die;
	}
}
