<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demos extends CI_Controller 
{
	public $user_first_name = "";
    public $user_last_name = "";
    public $user_fullname = "";
    public $user_email = "";
    public $user_mobile = "";
    public $user_role_id = "";
    public $user_role_name = "";
    public $user_id = "";
    public $all_modules = array();
    public $user_state_aop = "0";
    public $user_district_aop = "0";    
    public $user_photo_url = "";
    
	public function __construct()
    {
        parent::__construct();
        
        $this->is_logged_in();
        
        $this->load->model("admin_model");
        $this->load->model("webservice_model");
    }


    public function is_logged_in()
    {
        $data = $this->session->all_userdata();
        
        if(isset($data['user_id']) && !empty($data['user_id']))
        {
            $this->user_first_name = ucfirst($data['first_name']);

            $this->user_last_name = ucfirst($data['last_name']);

            $this->user_fullname = ucwords($data['first_name']." ".$data['last_name']);

            $this->user_email = $data['email'];

            $this->user_mobile = $data['mobile'];

            $this->user_role_id = $data['role_id'];

            $this->user_role_name = $data['role_name'];

            $this->user_id = $data['user_id'];            

            $this->user_photo_url = $data['user_photo_url'];           

            $this->user_created_date = date("M, Y", strtotime($data['created_date']));


            if($this->user_role_id == 2 || $this->user_role_id == 3)
            {
                $this->user_state_aop = $this->common_model->user_state_aop($this->user_id);

                $this->user_district_aop = $this->common_model->user_district_aop($this->user_id);
            }

            return true;
        }
        else
        {
            echo "<script> window.location.href = '".base_url()."login/logout'; </script>"; die;
        }

    }


    public function validate_permission($module_id, $permission_id)
    {
        $chk_pm = $this->common_model->check_permission($this->user_role_id, $module_id, $permission_id);
        
        if(!isset($chk_pm) || empty($chk_pm))
        {            
            $this->load->view('noaccess');            
        }

        return true;
    }

	
    public function demo_stage_1()
    {
        $farmer_id_master = $this->input->post('farmer_id');
        $farmer_name = $this->input->post('farmer_name');
        $farmer_mobile = $this->input->post('farmer_mobile');        
        $farmer_district = $this->input->post('farmer_district');
        $farmer_state = $this->input->post('farmer_state');
        $farmer_taluka = $this->input->post('farmer_taluka');

        $demo_start_date = $this->input->post('demo_start_date');
        $next_stage_date = $this->input->post('next_stage_date');
        $total_acrage = $this->input->post('total_acrage');

        if($farmer_id_master == "" || $farmer_id_master <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Please select any Farmer.")); die;
        }
        elseif($farmer_name == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Name required.")); die;
        }       
        elseif($farmer_mobile == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Mobile Number required."));
            die;    
        }
        elseif(strlen($farmer_mobile) != 10)
        {
            echo json_encode(array("status"=>0,"message"=>"Please enter 10 digit Mobile Number."));
            die;    
        }
        elseif($farmer_district == "" || $farmer_district <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer District required."));
            die;    
        }
        elseif($farmer_state == "" || $farmer_state <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer State required."));
            die;    
        }
        elseif($farmer_taluka == "" || $farmer_taluka <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Farmer Taluka required."));
            die;    
        }
        elseif($total_acrage == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Total Acrage required."));
            die;    
        }
        elseif($demo_start_date == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Demo Start Date required."));
            die;    
        }
        elseif($next_stage_date == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Stage 2 Visit Date required."));
            die;    
        }
                 

        $farmer_email = $this->input->post('farmer_email');


        $farmer_last_soil_test_date = $this->input->post('farmer_last_soil_test_date');        
        if(isset($farmer_last_soil_test_date) && !empty($farmer_last_soil_test_date))
        {
            $farmer_last_soil_test_date = format_date($farmer_last_soil_test_date, DATE_DB);
        }

        $farmer_last_water_test_date = $this->input->post('farmer_last_water_test_date');
        if(isset($farmer_last_water_test_date) && !empty($farmer_last_water_test_date))
        {
            $farmer_last_water_test_date = format_date($farmer_last_water_test_date, DATE_DB);
        }

        

        $data = array("farmer_parent_id" => $farmer_id_master, 
        "farmer_name" => $farmer_name, 
        "farmer_mobile" => $farmer_mobile, 
        "farmer_email" => $farmer_email,         
        "farmer_village" => $this->input->post('farmer_village'), 
        "farmer_taluka" => $farmer_taluka, 
        "farmer_district" => $this->input->post('farmer_district'), 
        "farmer_state" => $this->input->post('farmer_state'),
        "total_acrage" => $this->input->post('total_acrage'),        
        "crop_1" => $this->input->post('crop_1'),
        "crop_2" => $this->input->post('crop_2'),
        "gps_location" => "manual",
        "geo_lat" => 0,
        "geo_long" => 0,
        "soil_test" => $this->input->post('soil_test'),
        "irrigated" => $this->input->post('irrigated'),         
        "demo_crop_id" => $this->input->post('demo_crop_id'), 
        "demo_crop_variety_id" => $this->input->post('demo_crop_variety_id'),
        "demo_crop_reason" => $this->input->post('demo_crop_reason'),
        "acrage_for_demo" => $this->input->post('acrage_for_demo'),        
        "visit_date" => date(DATE_DB, strtotime($this->input->post('visit_date'))),
        "demo_start_date" => date(DATE_DB, strtotime($this->input->post('demo_start_date'))),
        
        "farmer_last_soil_test_date" => $farmer_last_soil_test_date,
        "farmer_last_water_test_date" => $farmer_last_water_test_date,     

        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id,
        "updated_date" => date(DATETIME_DB),                
        "updated_by" => $this->user_id
        );

        $farmerid = $this->dbaccess_model->insert("demos", $data);            
        if($farmerid > 0)
        {
            $farmer_reg_num = $this->common_model->get_farmer_reg_no("demos");
            $farmer_reg_num = $farmer_reg_num + 1;
            $this->dbaccess_model->update("demos", array("farmer_reg_num"=>$farmer_reg_num), array("farmer_id" => $farmerid));



            $sdarr = array("typ"=>'district', "id" => $this->input->post('farmer_district'));
            $sd = $this->common_model->get_detail($sdarr);
            $fstate_name = strtolower(trim($sd-> state_name));
            $fdistrict_name = strtolower(trim($sd-> district_name));
            $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/demo";



            $next_stage_date = $this->input->post('next_stage_date');
            if(isset($next_stage_date) && !empty($next_stage_date))
            {
                $datain = array("farmer_id" => $farmerid,
                    "next_stage_date" => date(DATE_DB, strtotime($next_stage_date)),
                    "for_stage" => "2",
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                );

                $this->dbaccess_model->insert("demos_stage", $datain);
            }          


            for($i=1;$i<=10;$i++)
            {
                if(isset($_POST["crop_last_year_$i"]) && !empty($_POST["crop_last_year_$i"]))
                $crop_last_year[$this->input->post("crop_last_year_$i")] = $this->input->post("crop_last_year_qty_$i"); 


                if(isset($_POST["crop_current_year_$i"]) && !empty($_POST["crop_current_year_$i"]))
                $crop_current_year[$this->input->post("crop_current_year_$i")] =  $this->input->post("crop_current_year_qty_$i"); 


                if(isset($_POST["demos_product_$i"]) && !empty($_POST["demos_product_$i"]))
                $demos_product[$this->input->post("demos_product_$i")] = $this->input->post("demos_product_qty_$i");                 
            }

            

            if(isset($crop_last_year) && !empty($crop_last_year))
            {
                foreach($crop_last_year as $k => $val)
                {
                    $datain = array("farmer_id" => $farmerid,
                    "last_year_crop_id" => $k,
                    "last_year_crop_qty" => $val,                    
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("demos_crop_last_year", $datain);
                }    
            }


            if(isset($crop_current_year) && !empty($crop_current_year))
            {
                foreach($crop_current_year as $k => $val)
                {
                    $datain = array("farmer_id" => $farmerid,
                    "current_year_crop_id" => $k,
                    "current_year_crop_qty" => $val,                    
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("demos_crop_current_year", $datain);
                }    
            }


            if(isset($demos_product) && !empty($demos_product))
            {
                foreach($demos_product as $k => $val)
                {
                    $datain = array("farmer_id" => $farmerid,
                    "product_id" => $k,
                    "product_qty" => $val,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("demos_product", $datain);
                }    
            }            
        }

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;
    }


    public function load_farmer_details()
    {        
        $farmer_id = $this->input->post('farmer_id');

        if($farmer_id == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Please select farmer."));
            die;    
        }

        $arr = array("farmer_id"=>$farmer_id);
        $data = $this->webservice_model->get_farmer_detail($arr);

        if(isset($data['basic']) && !empty($data['basic']))
        {
            echo json_encode(array("status"=>1,"message"=>"", "data"=>$data)); die;            
        }
        else
        {
            echo json_encode(array("status"=>0,"message"=>"No image found."));
            die;
        }        
    }
    


    public function demo_stage_2()
    {        
        $farmerid = $this->input->post('farmer_id');
        $next_stage_date = $this->input->post('next_stage_date');
        $visit_date = $this->input->post('visit_date_stage_2');

        if($farmerid == "" || $farmerid <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Demo ID required.")); 
            die;
        }        
        elseif(!isset($visit_date) || empty($visit_date))
        {
            echo json_encode(array("status"=>0,"message"=>"Visit Date required.")); 
            die;
        }
        elseif(!isset($next_stage_date) || empty($next_stage_date))
        {
            echo json_encode(array("status"=>0,"message"=>"Stage 3 Visit Date required.")); 
            die;
        }


        
        if(!isset($visit_date) || empty($visit_date))
        {
            $visit_date = date(DATE_DB);
        }
        else
        {
            $visit_date = format_date($visit_date, DATE_DB);
        }

        $arr = array("typ"=>"demos", "id"=>$farmerid);
        $data_chk = $this->common_model->is_exist($arr);
        if(!isset($data_chk) || empty($data_chk))
        {
            echo json_encode(array("status"=>0,"message"=>"Demo details not found in the system.")); die;
        } 

        
        if(isset($next_stage_date) && !empty($next_stage_date))
        {
            $datain = array("farmer_id" => $farmerid,
                "next_stage_date" => date(DATE_DB, strtotime($next_stage_date)),
                "for_stage" => 3,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
            );

            $this->dbaccess_model->insert("demos_stage", $datain);
        }       



        $acrage_demo = $this->input->post('acrage_demo');

        for($i=1;$i<=10;$i++)
        {
            if(isset($_POST["company_fertilizer_demo_$i"]) && !empty($_POST["company_fertilizer_demo_$i"]))
            $company_fertilizer_demo[$this->input->post("company_fertilizer_demo_$i")] = $this->input->post("company_fertilizer_demo_qty_$i"); 


            if(isset($_POST["other_company_fertilizer_demo_$i"]) && !empty($_POST["other_company_fertilizer_demo_$i"]))
            $other_company_fertilizer_demo[$this->input->post("other_company_fertilizer_demo_$i")] =  $this->input->post("other_company_fertilizer_demo_qty_$i"); 


            if(isset($_POST["micro_nutrients_demo_$i"]) && !empty($_POST["micro_nutrients_demo_$i"]))
            $micro_nutrients_demo[$this->input->post("micro_nutrients_demo_$i")] = $this->input->post("micro_nutrients_demo_qty_$i");


            if(isset($_POST["weedicides_demo_$i"]) && !empty($_POST["weedicides_demo_$i"]))
            $weedicides_demo[$this->input->post("weedicides_demo_$i")] = $this->input->post("weedicides_demo_qty_$i");
        }
               
        
        if(isset($company_fertilizer_demo) && !empty($company_fertilizer_demo))
        {
            foreach($company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $k,
                "product_qty" => $obj,
                "for_stage" => 2,
                "demo_type" => 1,                
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        }

        if(isset($other_company_fertilizer_demo) && !empty($other_company_fertilizer_demo))
        {
            foreach($other_company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $k,
                "product_qty" => $obj,
                "for_stage" => 2,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        } 


        if(isset($micro_nutrients_demo) && !empty($micro_nutrients_demo))
        {
            foreach($micro_nutrients_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "micro_nutrient_id" => $k,
                "micro_nutrient_qty" => $obj,
                "for_stage" => 2,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_micro_nutrients", $datain);
            }    
        }


        if(isset($weedicides_demo) && !empty($weedicides_demo))
        {
            foreach($weedicides_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "weedicide_id" => $k,
                "weedicide_qty" => $obj,
                "for_stage" => 2,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_weedicides", $datain);
            }    
        }


        $farmer_district = $data_chk->farmer_district;
        $sdarr = array("typ"=>'district', "id" => $farmer_district);
        $sd = $this->common_model->get_detail($sdarr);
        $fstate_name = strtolower(trim($sd-> state_name));
        $fdistrict_name = strtolower(trim($sd-> district_name));
        $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/demo";

        $farmer_reg_num = $data_chk->farmer_reg_num;
                     
        if(isset($_FILES['plot_photos']['name'][0]) && !empty($_FILES['plot_photos']['name'][0]))
        {   
            $files = $_FILES['plot_photos']; 

            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos[]']['name']= $files['name'][$key];
                $_FILES['plot_photos[]']['type']= $files['type'][$key];
                $_FILES['plot_photos[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos[]']['error']= $files['error'][$key];
                $_FILES['plot_photos[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s2_demo_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 2,
                    "demo_type" => 1,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }



        $acrage_comparative = $this->input->post('acrage_comparative');
        for($i=1;$i<=10;$i++)
        {
            if(isset($_POST["company_fertilizer_comparative_$i"]) && !empty($_POST["company_fertilizer_comparative_$i"]))
            $company_fertilizer_demo[$this->input->post("company_fertilizer_comparative_$i")] = $this->input->post("company_fertilizer_comparative_qty_$i"); 


            if(isset($_POST["other_company_fertilizer_comparative_$i"]) && !empty($_POST["other_company_fertilizer_comparative_$i"]))
            $other_company_fertilizer_demo[$this->input->post("other_company_fertilizer_comparative_$i")] =  $this->input->post("other_company_fertilizer_comparative_qty_$i"); 


            if(isset($_POST["micro_nutrients_comparative_$i"]) && !empty($_POST["micro_nutrients_comparative_$i"]))
            $micro_nutrients_demo[$this->input->post("micro_nutrients_comparative_$i")] = $this->input->post("micro_nutrients_comparative_qty_$i");


            if(isset($_POST["weedicides_comparative_$i"]) && !empty($_POST["weedicides_comparative_$i"]))
            $weedicides_demo[$this->input->post("weedicides_comparative_$i")] = $this->input->post("weedicides_comparative_qty_$i");
        }

        
        if(isset($company_fertilizer_demo) && !empty($company_fertilizer_demo))
        {
            foreach($company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $k,
                "product_qty" => $obj,
                "for_stage" => 2,
                "demo_type" => 2,                
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        }

        if(isset($other_company_fertilizer_demo) && !empty($other_company_fertilizer_demo))
        {
            foreach($other_company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $k,
                "product_qty" => $obj,
                "for_stage" => 2,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        } 


        if(isset($micro_nutrients_demo) && !empty($micro_nutrients_demo))
        {
            foreach($micro_nutrients_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "micro_nutrient_id" => $k,
                "micro_nutrient_qty" => $obj,
                "for_stage" => 2,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_micro_nutrients", $datain);
            }    
        }


        if(isset($weedicides_demo) && !empty($weedicides_demo))
        {
            foreach($weedicides_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "weedicide_id" => $k,
                "weedicide_qty" => $obj,
                "for_stage" => 2,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_weedicides", $datain);
            }    
        }


                      
        if(isset($_FILES['plot_photos_comparative']['name'][0]) && !empty($_FILES['plot_photos_comparative']['name'][0]))
        {   
            $files = $_FILES['plot_photos_comparative'];
            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos_comparative[]']['name']= $files['name'][$key];
                $_FILES['plot_photos_comparative[]']['type']= $files['type'][$key];
                $_FILES['plot_photos_comparative[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos_comparative[]']['error']= $files['error'][$key];
                $_FILES['plot_photos_comparative[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s2_comp_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos_comparative[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 2,
                    "demo_type" => 2,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }

        $d = array("current_demo_stage"=>2,
                "acrage_demo"=>$acrage_demo,                
                "acrage_comparative"=>$acrage_comparative,
                "visit_date_stage_2"=>$visit_date
                );
        $this->dbaccess_model->update("demos", $d , array("farmer_id" => $farmerid));

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;

        
    }




    public function demo_stage_3()
    {        
        $farmerid = $this->input->post('farmer_id');
        $next_stage_date = $this->input->post('next_stage_date');
        $visit_date = $this->input->post('visit_date_stage_3');

        if($farmerid == "" || $farmerid <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Demo ID required.")); 
            die;
        }        
        elseif(!isset($visit_date) || empty($visit_date))
        {
            echo json_encode(array("status"=>0,"message"=>"Visit Date required.")); 
            die;
        }
        elseif(!isset($next_stage_date) || empty($next_stage_date))
        {
            echo json_encode(array("status"=>0,"message"=>"Stage 4 Visit Date required.")); 
            die;
        }

        
        if(!isset($visit_date) || empty($visit_date))
        {
            $visit_date = date(DATE_DB);
        }
        else
        {
            $visit_date = format_date($visit_date, DATE_DB);
        }


        $arr = array("typ"=>"demos", "id"=>$farmerid);
        $data_chk = $this->common_model->is_exist($arr);
        if(!isset($data_chk) || empty($data_chk))
        {
            echo json_encode(array("status"=>0,"message"=>"Demo details not found in the system.")); die;
        } 


        if(isset($next_stage_date) && !empty($next_stage_date))
        {
            $datain = array("farmer_id" => $farmerid,
                "next_stage_date" => date(DATE_DB, strtotime($next_stage_date)),
                "for_stage" => 4,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
            );

            $this->dbaccess_model->insert("demos_stage", $datain);
        }       

        for($i=1;$i<=10;$i++)
        {
            if(isset($_POST["company_fertilizer_demo_$i"]) && !empty($_POST["company_fertilizer_demo_$i"]))
            $company_fertilizer_demo[$this->input->post("company_fertilizer_demo_$i")] = $this->input->post("company_fertilizer_demo_qty_$i"); 


            if(isset($_POST["other_company_fertilizer_demo_$i"]) && !empty($_POST["other_company_fertilizer_demo_$i"]))
            $other_company_fertilizer_demo[$this->input->post("other_company_fertilizer_demo_$i")] =  $this->input->post("other_company_fertilizer_demo_qty_$i"); 


            if(isset($_POST["micro_nutrients_demo_$i"]) && !empty($_POST["micro_nutrients_demo_$i"]))
            $micro_nutrients_demo[$this->input->post("micro_nutrients_demo_$i")] = $this->input->post("micro_nutrients_demo_qty_$i");


            if(isset($_POST["weedicides_demo_$i"]) && !empty($_POST["weedicides_demo_$i"]))
            $weedicides_demo[$this->input->post("weedicides_demo_$i")] = $this->input->post("weedicides_demo_qty_$i");
        }
        
               
        
        if(isset($company_fertilizer_demo) && !empty($company_fertilizer_demo))
        {
            foreach($company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $k,
                "product_qty" => $obj,
                "for_stage" => 3,
                "demo_type" => 1,                
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        }

        if(isset($other_company_fertilizer_demo) && !empty($other_company_fertilizer_demo))
        {
            foreach($other_company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $k,
                "product_qty" => $obj,
                "for_stage" => 3,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        } 


        if(isset($micro_nutrients_demo) && !empty($micro_nutrients_demo))
        {
            foreach($micro_nutrients_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "micro_nutrient_id" => $k,
                "micro_nutrient_qty" => $obj,
                "for_stage" => 3,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_micro_nutrients", $datain);
            }    
        }


        if(isset($weedicides_demo) && !empty($weedicides_demo))
        {
            foreach($weedicides_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "weedicide_id" => $k,
                "weedicide_qty" => $obj,
                "for_stage" => 3,
                "demo_type" => 1,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_weedicides", $datain);
            }    
        }


        $farmer_district = $data_chk->farmer_district;
        $sdarr = array("typ"=>'district', "id" => $farmer_district);
        $sd = $this->common_model->get_detail($sdarr);
        $fstate_name = strtolower(trim($sd-> state_name));
        $fdistrict_name = strtolower(trim($sd-> district_name));
        $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/demo";

        $farmer_reg_num = $data_chk->farmer_reg_num;
        
        if(isset($_FILES['plot_photos']['name'][0]) && !empty($_FILES['plot_photos']['name'][0]))
        {   
            $files = $_FILES['plot_photos'];              

            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos[]']['name']= $files['name'][$key];
                $_FILES['plot_photos[]']['type']= $files['type'][$key];
                $_FILES['plot_photos[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos[]']['error']= $files['error'][$key];
                $_FILES['plot_photos[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s3_demo_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 3,
                    "demo_type" => 1,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }


        
        for($i=1;$i<=10;$i++)
        {
            if(isset($_POST["company_fertilizer_comparative_$i"]) && !empty($_POST["company_fertilizer_comparative_$i"]))
            $company_fertilizer_demo[$this->input->post("company_fertilizer_comparative_$i")] = $this->input->post("company_fertilizer_comparative_qty_$i"); 


            if(isset($_POST["other_company_fertilizer_comparative_$i"]) && !empty($_POST["other_company_fertilizer_comparative_$i"]))
            $other_company_fertilizer_demo[$this->input->post("other_company_fertilizer_comparative_$i")] =  $this->input->post("other_company_fertilizer_comparative_qty_$i"); 


            if(isset($_POST["micro_nutrients_comparative_$i"]) && !empty($_POST["micro_nutrients_comparative_$i"]))
            $micro_nutrients_demo[$this->input->post("micro_nutrients_comparative_$i")] = $this->input->post("micro_nutrients_comparative_qty_$i");


            if(isset($_POST["weedicides_comparative_$i"]) && !empty($_POST["weedicides_comparative_$i"]))
            $weedicides_demo[$this->input->post("weedicides_comparative_$i")] = $this->input->post("weedicides_comparative_qty_$i");
        }

        if(isset($company_fertilizer_demo) && !empty($company_fertilizer_demo))
        {
            foreach($company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $k,
                "product_qty" => $obj,
                "for_stage" => 3,
                "demo_type" => 2,                
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        }

        if(isset($other_company_fertilizer_demo) && !empty($other_company_fertilizer_demo))
        {
            foreach($other_company_fertilizer_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "product_id" => $k,
                "product_qty" => $obj,
                "for_stage" => 3,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_product", $datain);
            }    
        } 


        if(isset($micro_nutrients_demo) && !empty($micro_nutrients_demo))
        {
            foreach($micro_nutrients_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "micro_nutrient_id" => $k,
                "micro_nutrient_qty" => $obj,
                "for_stage" => 3,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_micro_nutrients", $datain);
            }    
        }


        if(isset($weedicides_demo) && !empty($weedicides_demo))
        {
            foreach($weedicides_demo as $k => $obj)
            {
                $datain = array("farmer_id" => $farmerid,
                "weedicide_id" => $k,
                "weedicide_qty" => $obj,
                "for_stage" => 3,
                "demo_type" => 2,
                "created_date" => date(DATETIME_DB),                
                "created_by" => $this->user_id
                );
            
                $this->dbaccess_model->insert("demos_weedicides", $datain);
            }    
        }


        
        if(isset($_FILES['plot_photos_comparative']['name'][0]) && !empty($_FILES['plot_photos_comparative']['name'][0]))
        {
            $files = $_FILES['plot_photos_comparative'];              

            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos_comparative[]']['name']= $files['name'][$key];
                $_FILES['plot_photos_comparative[]']['type']= $files['type'][$key];
                $_FILES['plot_photos_comparative[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos_comparative[]']['error']= $files['error'][$key];
                $_FILES['plot_photos_comparative[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s3_comp_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos_comparative[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 3,
                    "demo_type" => 2,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }


        $crop_condition_demo = $this->input->post('crop_condition_demo');
        $vegitative_growth_demo = $this->input->post('vegitative_growth_demo');
        $flowering_growth_demo = $this->input->post('flowering_growth_demo');
        $fruitning_growth_demo = $this->input->post('fruitning_growth_demo');
        $quality_demo = $this->input->post('quality_demo');
        
        $datain = array("farmer_id" => $farmerid,        
        "for_stage" => 3,
        "demo_type" => 1,
        "crop_condition" => $crop_condition_demo,
        "vegitative_growth" => $vegitative_growth_demo,
        "flowering_growth" => $flowering_growth_demo,
        "fruitning_growth" => $fruitning_growth_demo,
        "quality" => $quality_demo,
        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id
        );    
        $this->dbaccess_model->insert("demos_crop_quality", $datain);
        
        
        $crop_condition_demo = $this->input->post('crop_condition_comparative');
        $vegitative_growth_demo = $this->input->post('vegitative_growth_comparative');
        $flowering_growth_demo = $this->input->post('flowering_growth_comparative');
        $fruitning_growth_demo = $this->input->post('fruitning_growth_comparative');
        $quality_demo = $this->input->post('quality_comparative');
        $datain = array("farmer_id" => $farmerid,        
        "for_stage" => 3,
        "demo_type" => 2,
        "crop_condition" => $crop_condition_demo,
        "vegitative_growth" => $vegitative_growth_demo,
        "flowering_growth" => $flowering_growth_demo,
        "fruitning_growth" => $fruitning_growth_demo,
        "quality" => $quality_demo,
        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id
        );    
        $this->dbaccess_model->insert("demos_crop_quality", $datain);


        $d = array("current_demo_stage"=>3, "visit_date_stage_3"=>$visit_date);
        $this->dbaccess_model->update("demos", $d , array("farmer_id" => $farmerid));

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;

        
    }



    public function demo_stage_4()
    {        
        $farmerid = $this->input->post('farmer_id');
        $visit_date = $this->input->post('visit_date');

        if($farmerid == "" || $farmerid <= 0)
        {
            echo json_encode(array("status"=>0,"message"=>"Demo ID required.")); 
            die;
        }
        elseif($visit_date == "")
        {
            echo json_encode(array("status"=>0,"message"=>"Visit Date required.")); 
            die;
        }

        
        $arr = array("typ"=>"demos", "id"=>$farmerid);
        $data_chk = $this->common_model->is_exist($arr);
        if(!isset($data_chk) || empty($data_chk))
        {
            echo json_encode(array("status"=>0,"message"=>"Demo details not found in the system.")); die;
        } 


        
        if(!isset($visit_date) || empty($visit_date))
        {
            $visit_date = date(DATE_DB);
        }
        else
        {
            $visit_date = format_date($visit_date, DATE_DB);
        }


        $yields_demo = $this->input->post('yields_demo');
        $yields_comparative = $this->input->post('yields_comparative');

        
        $quality_demo = $this->input->post('quality_demo');
        $quality_comparative = $this->input->post('quality_comparative');

        $market_price = $this->input->post('market_price');
        $yield_difference = $this->input->post('yield_difference');
        $produce_value = $this->input->post('produce_value');
        $cost_of_production = $this->input->post('cost_of_production');
        $yield_difference_justification = $this->input->post('yield_difference_justification');
        
        $farmers_feedback = $this->input->post('farmers_feedback');
        $executive_remark = $this->input->post('executive_remark');

        $analysis_report_1 = $this->input->post('analysis_report_1');
        $analysis_report_2 = $this->input->post('analysis_report_2');
        $analysis_report_3 = $this->input->post('analysis_report_3');
        $analysis_report_4 = $this->input->post('analysis_report_4');



        $farmer_district = $data_chk->farmer_district;
        $sdarr = array("typ"=>'district', "id" => $farmer_district);
        $sd = $this->common_model->get_detail($sdarr);
        $fstate_name = strtolower(trim($sd-> state_name));
        $fdistrict_name = strtolower(trim($sd-> district_name));
        $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/demo";

        $farmer_reg_num = $data_chk-> farmer_reg_num;

        $guest_name = $this->input->post('guest_name');
        if(isset($_FILES['guest_photo']) && !empty($_FILES['guest_photo']))
        {
            $guest_photo = $_FILES['guest_photo'];            
            if(isset($guest_photo) && !empty($guest_photo))
            {                
                $ext = pathinfo($guest_photo['name'], PATHINFO_EXTENSION);

                $chk = $this->upload_image($path, 'guest_photo', $farmerid.'_'.$this->user_id."_gp_".$guest_photo['name']);
                if(isset($chk) && !empty($chk))
                {
                    
                }
                else
                {
                    $spath = BASE_PATH.'/'.$path."/".$farmerid.'_'.$this->user_id."_gp_".$guest_photo['name'];
                    $this->dbaccess_model->update("demos", array("guest_photo_url"=>$spath), array("farmer_id" => $farmerid));
                }
            }
        }



        $farmer_district = $data_chk->farmer_district;
        $sdarr = array("typ"=>'district', "id" => $farmer_district);
        $sd = $this->common_model->get_detail($sdarr);
        $fstate_name = strtolower(trim($sd-> state_name));
        $fdistrict_name = strtolower(trim($sd-> district_name));
        $path = PATH_ALBUM.$fstate_name."/".$fdistrict_name."/demo";


        if(isset($_FILES['plot_photos']['name'][0]) && !empty($_FILES['plot_photos']['name'][0]))
        {   
            $files = $_FILES['plot_photos'];              

            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos[]']['name']= $files['name'][$key];
                $_FILES['plot_photos[]']['type']= $files['type'][$key];
                $_FILES['plot_photos[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos[]']['error']= $files['error'][$key];
                $_FILES['plot_photos[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s4_demo_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 4,
                    "demo_type" => 1,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }



        if(isset($_FILES['plot_photos_comparative']['name'][0]) && !empty($_FILES['plot_photos_comparative']['name'][0]))
        {
            $files = $_FILES['plot_photos_comparative'];              

            $config = array(
                'upload_path'   => $path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite'     => 1                       
            );
            $this->load->library('upload', $config);

            foreach ($files['name'] as $key => $image) 
            {
                $_FILES['plot_photos_comparative[]']['name']= $files['name'][$key];
                $_FILES['plot_photos_comparative[]']['type']= $files['type'][$key];
                $_FILES['plot_photos_comparative[]']['tmp_name']= $files['tmp_name'][$key];
                $_FILES['plot_photos_comparative[]']['error']= $files['error'][$key];
                $_FILES['plot_photos_comparative[]']['size']= $files['size'][$key];
                
                $config['file_name'] = $farmerid.'_'.$this->user_id."_".$farmer_reg_num."_s4_comp_".$image;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('plot_photos_comparative[]')) 
                {
                    $dataup = $this->upload->data();
                    $ext = $dataup['file_ext'];
                    $unm = $dataup['file_name'];
                    
                    $datain = array("farmer_id" => $farmerid,
                    "plot_photo_name" => $unm,
                    "plot_photo_url" => BASE_PATH.'/'.$path."/".$unm,
                    "farmer_type" => 2,
                    "for_stage" => 4,
                    "demo_type" => 2,
                    "created_date" => date(DATETIME_DB),                
                    "created_by" => $this->user_id
                    );
                
                    $this->dbaccess_model->insert("farmers_plot_photo", $datain);
                } 
                else 
                {
                    //$this->webservice_model->reset_stage($farmerid, 2);
                    echo json_encode(array("status"=>0,"message"=>$this->upload->display_errors()));
                    die;
                }
            }
        }




        $datain = array("farmer_id" => $farmerid,        
        "for_stage" => 4,
        "demo_type" => 1,        
        "quality" => $quality_demo,
        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id
        );    
        $this->dbaccess_model->insert("demos_crop_quality", $datain);
        
        
        $datain = array("farmer_id" => $farmerid,        
        "for_stage" => 4,
        "demo_type" => 2,        
        "quality" => $quality_comparative,
        "created_date" => date(DATETIME_DB),                
        "created_by" => $this->user_id
        );    
        $this->dbaccess_model->insert("demos_crop_quality", $datain);
        

        $d = array("current_demo_stage"=>4,
            "yields_demo"=>$yields_demo,
            "yields_comparative"=>$yields_comparative,

            "market_price"=>$market_price,
            "yield_difference"=>$yield_difference,
            "produce_value"=>$produce_value,
            "cost_of_production"=>$cost_of_production,
            "yield_difference_justification"=>$yield_difference_justification,
            
            "analysis_report_1"=>$analysis_report_1,
            "analysis_report_2"=>$analysis_report_2,
            "analysis_report_3"=>$analysis_report_3,
            "analysis_report_4"=>$analysis_report_4,

            "farmers_feedback"=>$farmers_feedback,
            "executive_remark"=>$executive_remark,

            "guest_name"=>$guest_name,

            "visit_date_stage_4"=>$visit_date
        );

        $this->dbaccess_model->update("demos",$d , array("farmer_id" => $farmerid));

        echo json_encode(array("status"=>1,"message"=>"Saved Successfully.")); 
        
        die;

        
    }



    function upload_image($path, $filename, $newfilename)
    { 
        $config['file_name'] = $newfilename;
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        
        $this->load->library('upload');
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) 
        {
            $error = $this->upload->display_errors();        
        } 
        else 
        {            
            $error = "";
        }

        return $error;
    }




    public function export_data()
    {
        $user_id = $this->user_id;        
        
        $report_type = $this->uri->segment(3);
        $demo_id = $this->uri->segment(4);
        $for_stage = $this->uri->segment(5);
               
        
        if($report_type == "demos_details")
        {            
            if(isset($for_stage) && !empty($for_stage))
                $for_stage = $for_stage;
            else
                $for_stage = "2";

            $this->load->model("Webservice_model");
            $data = $this->Webservice_model->get_demo_farmer_detail(array("farmer_id"=>$demo_id, "farmer_type" => 2));

            $data['for_stage_type'] = $for_stage;
            
            $this->load->view("demos_stage_html", $data);
        }
    
    }

}//End Of Class
?>