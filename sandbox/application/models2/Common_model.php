<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model 
{
    
    public function __construct()
    {
        parent::__construct();
    }


    public function get_counts($ack1, $ack2, $wstk, $rstk, $rpos)
    {
        if(isset($ack1) && !empty($ack1)) $ack1 = "WHERE s.file_id = $ack1";
        if(isset($ack2) && !empty($ack2)) $ack2 = "WHERE s.file_id = $ack2";
        if(isset($wstk) && !empty($wstk)) $wstk = "WHERE s.file_id = $wstk";
        if(isset($rstk) && !empty($rstk)) $rstk = "WHERE s.file_id = $rstk";
        if(isset($rpos) && !empty($rpos)) $rpos = "WHERE s.file_id = $rpos";

        $append = "";
        if($this->user_role_id == 2)
        {
            $append = " AND state_id IN (".$this->user_state_aop.") ";
        }
        elseif($this->user_role_id == 3)
        {
            $append = " AND district_id IN (".$this->user_district_aop.") ";
        }


        $sql = "SELECT * FROM
        (
            SELECT SUM(s.quantity_mt) as quantity, (ss.state_id) as state_id, (ss.district_id) as district_id, ('ack1') as type
            FROM upload_acknowledgement1 s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id
            $ack1 
            GROUP BY ss.district_id


            UNION ALL            
            

            SELECT SUM(s.quantity_mt) as quantity, (w.state_id) as state_id, (w.district_id) as district_id, ('ack2') as type
            FROM upload_acknowledgement2 s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1
            $ack2
            GROUP BY w.district_id


            UNION ALL


            SELECT SUM(s.balance_with_ws) as quantity, (ss.state_id) as state_id, (ss.district_id) as district_id, ('wstk') as type
            FROM upload_wholesaler s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            $wstk
            GROUP BY ss.district_id


            UNION ALL


            SELECT SUM(s.closing_balance) as quantity, (ss.state_id) as state_id, (ss.district_id) as district_id, ('rstk') as type
            FROM upload_retailer s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            $rstk
            GROUP BY ss.district_id


            UNION ALL


            SELECT SUM(s.quantity_mt) as quantity, (ss.state_id) as state_id, (ss.district_id) as district_id, ('rpos') as type
            FROM upload_retailer_pos s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            $rpos
            GROUP BY ss.district_id
            

        ) as temp
        WHERE 1 = 1 $append
        ";


        /*$sql = "SELECT * FROM
        (
            SELECT SUM(s.quantity) as quantity, (ss.state_id) as state_id, (ss.district_id) as district_id, ('ack1') as type
            FROM upload_acknowledgement1 s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            $ack1 
            GROUP BY ss.district_id


            UNION ALL            
            

            SELECT SUM(s.quantity) as quantity, (w.state_id) as state_id, (w.district_id) as district_id, ('ack2') as type
            FROM upload_acknowledgement2 s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 2
            INNER JOIN salers w ON w.saler_sys_id = s.wholesaler_id AND w.saler_type = 1
            $ack2
            GROUP BY w.district_id


            UNION ALL


            SELECT SUM(s.balance_with_ws) as quantity, (ss.state_id) as state_id, (ss.district_id) as district_id, ('wstk') as type
            FROM upload_wholesaler s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id AND ss.saler_type = 1
            $wstk
            GROUP BY ss.district_id


            UNION ALL


            SELECT SUM(s.closing_balance) as quantity, (ss.state_id) as state_id, (ss.district_id) as district_id, ('rstk') as type
            FROM upload_retailer s            
            INNER JOIN salers ss ON ss.saler_sys_id = s.retailer_id AND ss.saler_type = 2
            $rstk
            GROUP BY ss.district_id
            

        ) as temp
        WHERE 1 = 1 $append
        ";*/


        $query = $this->db->query($sql);

        return $query->result();
    }


    public function get_sales($file_id, $year, $month)
    {
        $app = "";
        if(isset($month) && !empty($month)) $app = " AND month(s.sales_date) = '$month' ";

        $sql = "SELECT s.quantity, s.sales_date, s.state_id, p.product_code
            FROM upload_sales s
            INNER JOIN states st ON st.state_id = s.state_id
            INNER JOIN districts dis ON dis.district_id = s.district_id
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id 
            INNER JOIN products p ON p.product_id = s.product_id
            WHERE year(s.sales_date) = '$year' AND company = 'RMPCL' AND s.file_id = '$file_id'
            $app
        ";

        $query = $this->db->query($sql);

        return $query->result();    
    }


    public function get_sales_target($year, $month="")
    {
        $sql = "SELECT s.state_name, s.state_id, t.target_value as total
            FROM target t
            INNER JOIN states s ON s.state_id = t.state_id            
            WHERE t.target_year = '$year' AND t.target_month = '$month'
            ORDER BY s.state_name            
        ";

        $query = $this->db->query($sql);

        return $query->result();    
    }


    public function generate_key($len)
    {
        $key = generate_random_string($len);
        $is_unique = true;

        while($is_unique)
        {
            $sql = "SELECT u.user_id
            FROM users u      
            WHERE u.is_deleted = 0 AND u.keycode = '$key'
            LIMIT 1
            ";

            $query = $this->db->query($sql);

            $row = $query->row(); 

            if(!isset($row) || empty($row))
            {
                $is_unique = false;                
            }   
        }    

        return $key;
    }

    public function get_roles()
    {
        $sql = "SELECT m.*
        FROM roles m        
        WHERE m.is_deleted = 0
        ORDER BY m.role_name
        ";

        $query = $this->db->query($sql);

        return $query->result();    
    }


    public function get_all_modules()
    {
        $sql = "SELECT m.*
        FROM modules m         
        WHERE m.is_deleted = 0 AND m.parent_id = 0        
        ORDER BY m.sort_by
        ";

        $query = $this->db->query($sql);

        return $query->result();    
    }


    public function permitted_modules($user_role_id)
    {
        $sql = "SELECT m.module_id, am.permission_id
        FROM modules m 
        INNER JOIN access_modules am ON am.module_id = m.module_id AND am.role_id = $user_role_id      
        WHERE m.is_deleted = 0        
        ORDER BY m.module_id
        ";

        $query = $this->db->query($sql);

        return $query->result();    
    }

    public function check_permission($user_role_id, $module_id, $permission_id)
    {
        $sql = "SELECT am.permission_id
        FROM access_modules am       
        WHERE am.role_id = $user_role_id AND am.module_id = $module_id AND am.permission_id IN (1, $permission_id)
        LIMIT 1
        ";

        $query = $this->db->query($sql);

        return $query->row();    
    }


    


    public function get_sub_modules($module_id)
    {
        $sql = "SELECT m.*
        FROM modules m        
        WHERE m.is_deleted = 0 AND m.parent_id = $module_id
        ORDER BY m.sort_by_child
        ";

        $query = $this->db->query($sql);

        return $query->result();    
    }

    public function get_modules()
    {
        $sql = "SELECT m.*
        FROM modules m        
        WHERE m.is_deleted = 0 AND m.url != '#'
        ORDER BY m.sort_by
        ";

        $query = $this->db->query($sql);

        return $query->result();    
    }


    public function get_permissions()
    {
        $sql = "SELECT m.*
        FROM permissions m 
        ORDER BY m.permission_id
        ";

        $query = $this->db->query($sql);

        return $query->result();    
    }


    public function get_all_permitted_modules()
    {
        $sql = "SELECT m.*, ap.role_id, ap.permission_id
        FROM modules m
        INNER JOIN access_modules ap ON m.module_id = ap.module_id
        WHERE m.is_deleted = 0
        ORDER BY m.sort_by
        ";

        $query = $this->db->query($sql);

        $menus = $query->result();

        /*$menus = array();
        foreach($modules as $obj)
        {
            $menus[$obj-> parent_id][] = $obj;
        } 
        p($menus);*/
        return $menus;   
    }


    public function get_permitted_modules($arr = array())
    {
        $role_id = $arr['role_id'];

        $sql = "SELECT m.*, ap.role_id, ap.permission_id
        FROM modules m
        LEFT JOIN access_modules ap ON m.module_id = ap.module_id AND ap.role_id = $role_id
        WHERE m.is_deleted = 0
        ORDER BY m.sort_by
        ";

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function clear_permisions($arr = array())
    {
        $role_id = $arr['role_id'];

        $sql = "DELETE FROM access_modules WHERE role_id = $role_id";

        $this->db->query($sql);
    }

    public function find_and_clear_permisions($arr = array())
    {
        $role_id = $arr['role_id'];
        $module_id = $arr['module_id'];

        $sql = "SELECT role_id 
        FROM access_modules 
        WHERE role_id = $role_id AND module_id = $module_id AND permission_id = 1
        LIMIT 1
        ";

        $query = $this->db->query($sql);

        $chk = $query->result();

        if(isset($chk) && !empty($chk))
        {
            $sql = "DELETE FROM access_modules WHERE role_id = $role_id AND module_id = $module_id AND permission_id != 1";

            $this->db->query($sql);
        }
    }

    public function is_already_exist($arr)
    {
        $typ = $arr['typ'];
        $id = $arr['id'];
        $name = $arr['name'];

        if($typ == "units")
        {
            $sql = "SELECT u.unit_id
            FROM units u
            WHERE u.is_deleted = 0 AND u.unit_name = '$name' AND u.unit_id != $id
            LIMIT 1";
        }
        elseif($typ == "soil_type")
        {
            $sql = "SELECT u.soil_type_id
            FROM soil_type u
            WHERE u.is_deleted = 0 AND u.soil_type_name = '$name' AND u.soil_type_id != $id
            LIMIT 1";
        }
        elseif($typ == "roles")
        {
            $sql = "SELECT u.role_id
            FROM roles u
            WHERE u.is_deleted = 0 AND u.role_name = '$name' AND u.role_id != $id
            LIMIT 1";
        }
        elseif($typ == "crops")
        {
            $sql = "SELECT u.crop_id
            FROM crops u
            WHERE u.is_deleted = 0 AND u.crop_name = '$name' AND u.crop_id != $id
            LIMIT 1";
        }
        elseif($typ == "crop_variety_mas")
        {
            $sql = "SELECT u.crop_variety_id
            FROM crop_variety u
            WHERE u.is_deleted = 0 AND u.crop_variety_name = '$name' AND u.crop_id = $id
            LIMIT 1";
        }
        elseif($typ == "crop_variety")
        {
            $parent_id = $arr['parent_id'];

            $sql = "SELECT u.crop_variety_id
            FROM crop_variety u
            WHERE u.is_deleted = 0 AND u.crop_variety_name = '$name' AND u.crop_variety_id != $id AND u.crop_id = $parent_id
            LIMIT 1";
        }
        elseif($typ == "stock_verification_reason")
        {
            $sql = "SELECT u.stock_verification_reason_id
            FROM stock_verification_reason u
            WHERE u.is_deleted = 0 AND u.stock_verification_reason_name = '$name' AND u.stock_verification_reason_id != $id
            LIMIT 1";
        }
        elseif($typ == "product_category")
        {
            $sql = "SELECT u.product_category_id
            FROM product_category u
            WHERE u.is_deleted = 0 AND u.product_category_name = '$name' AND u.product_category_id != $id
            LIMIT 1";
        }
        elseif($typ == "products")
        {
            $sql = "SELECT u.product_id
            FROM products u
            WHERE u.is_deleted = 0 AND u.product_name = '$name' AND u.product_id != $id
            LIMIT 1";
        }
        elseif($typ == "staff")
        {
            $sql = "SELECT u.user_id
            FROM users u
            WHERE u.is_deleted = 0 AND u.username = '$name' AND u.user_id != $id
            LIMIT 1";
        }
        elseif($typ == "wholesalers")
        {
            $sql = "SELECT u.saler_id
            FROM salers u
            WHERE u.is_deleted = 0 AND CONCAT(u.first_name,' ',u.last_name) = '$name' AND u.saler_id != $id AND u.saler_type = 1
            LIMIT 1";
        }
        elseif($typ == "dealers")
        {
            $sql = "SELECT u.saler_id
            FROM salers u
            WHERE u.is_deleted = 0 AND CONCAT(u.first_name,' ',u.last_name) = '$name' AND u.saler_id != $id AND u.saler_type = 2
            LIMIT 1";
        }
        elseif($typ == "wholesalersid")
        {
            $sql = "SELECT u.saler_id
            FROM salers u
            WHERE u.is_deleted = 0 AND saler_sys_id = '$name' AND u.saler_id != $id AND u.saler_type = 1
            LIMIT 1";
        }
        elseif($typ == "dealersid")
        {
            $sql = "SELECT u.saler_id
            FROM salers u
            WHERE u.is_deleted = 0 AND saler_sys_id = '$name' AND u.saler_id != $id AND u.saler_type = 2
            LIMIT 1";
        }
        elseif($typ == "farmers")
        {
            $sql = "SELECT u.farmer_id
            FROM farmers u
            WHERE u.is_deleted = 0 AND u.farmer_mobile = '$name' AND u.farmer_id != $id
            LIMIT 1";
        }
        elseif($typ == "demos")
        {
            $sql = "SELECT u.farmer_id
            FROM demos u
            WHERE u.is_deleted = 0 AND u.farmer_mobile = '$name' AND u.farmer_id != $id
            LIMIT 1";
        }
        elseif($typ == "retailer")
        {
            $sql = "SELECT u.saler_id
            FROM salers u
            WHERE u.is_deleted = 0 AND u.saler_type = 2 AND u.saler_sys_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "target")
        {
            $sql = "SELECT u.target_id
            FROM target u
            WHERE u.target_year = '$name'
            LIMIT 1";
        }
        elseif($typ == "farmers_temp")
        {
            $sql = "SELECT u.farmer_id
            FROM farmers_temp u
            WHERE u.is_deleted = 0 AND u.farmer_mobile = '$name' AND u.farmer_id != $id
            LIMIT 1";
        }

        $query = $this->db->query($sql);

        return $query->row();

    }


    public function get_detail($arr)
    {
        $typ = trim($arr['typ']);
        $id = trim($arr['id']);

        if($typ == "units")
        {
            $sql = "SELECT u.unit_id, u.unit_name
            FROM units u
            WHERE u.is_deleted = 0 AND u.unit_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "soil_type")
        {
            $sql = "SELECT u.soil_type_id, u.soil_type_name
            FROM soil_type u
            WHERE u.is_deleted = 0 AND u.soil_type_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "roles")
        {
            $sql = "SELECT u.role_id, u.role_name
            FROM roles u
            WHERE u.is_deleted = 0 AND u.role_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "crops")
        {
            $sql = "SELECT u.crop_id, u.crop_name
            FROM crops u
            WHERE u.is_deleted = 0 AND u.crop_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "crop_variety")
        {
            $sql = "SELECT u.crop_variety_id, u.crop_variety_name, u.crop_id
            FROM crop_variety u
            WHERE u.is_deleted = 0 AND u.crop_variety_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "stock_verification_reason")
        {
            $sql = "SELECT u.stock_verification_reason_id, u.stock_verification_reason_name
            FROM stock_verification_reason u
            WHERE u.is_deleted = 0 AND u.stock_verification_reason_id = $id
            LIMIT 1";
        }
        elseif($typ == "product_category")
        {
            $sql = "SELECT u.product_category_id, u.product_category_name
            FROM product_category u
            WHERE u.is_deleted = 0 AND u.product_category_id = $id
            LIMIT 1";
        }
        elseif($typ == "products")
        {
            $sql = "SELECT u.*
            FROM products u
            WHERE u.is_deleted = 0 AND u.product_id = $id
            LIMIT 1";
        }
        elseif($typ == "state")
        {
            $sql = "SELECT d.district_name, s.state_name
            FROM districts d
            INNER JOIN states s ON s.state_id = d.state_id
            WHERE d.is_deleted = 0 AND d.district_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "district")
        {
            $sql = "SELECT d.district_name, s.state_name
            FROM districts d
            INNER JOIN states s ON s.state_id = d.state_id
            WHERE d.is_deleted = 0 AND d.district_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "staff")
        {
            $sql = "SELECT u.*
            FROM users u
            WHERE u.is_deleted = 0 AND u.user_id = $id
            LIMIT 1";
        }
        elseif($typ == "wholesalers")
        {
            $sql = "SELECT u.*
            FROM salers u
            WHERE u.is_deleted = 0 AND saler_type = 1 AND u.saler_id = $id
            LIMIT 1";
        }
        elseif($typ == "dealers")
        {
            $sql = "SELECT u.*
            FROM salers u
            WHERE u.is_deleted = 0 AND saler_type = 2 AND u.saler_id = $id
            LIMIT 1";
        }
        elseif($typ == "state_id_by_name")
        {
            $sql = "SELECT u.*
            FROM states u
            WHERE u.state_name = '$id'
            LIMIT 1";
        }
        elseif($typ == "district_id_by_name")
        {
            $sql = "SELECT u.*
            FROM districts u
            WHERE u.district_name = '$id'
            LIMIT 1";
        }
        elseif($typ == "users")
        {
            $sql = "SELECT u.*
            FROM users u
            WHERE u.user_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "templates")
        {
            $sql = "SELECT u.*
            FROM templates u
            WHERE u.is_deleted = 0 AND u.template_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "farmers")
        {
            $sql = "SELECT u.*
            FROM farmers u
            WHERE u.is_deleted = 0 AND u.farmer_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "farmers_temp")
        {
            $sql = "SELECT u.*
            FROM farmers_temp u
            WHERE u.is_deleted = 0 AND u.farmer_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "email_signature")
        {
            $sql = "SELECT u.*
            FROM email_signature u
            WHERE u.is_deleted = 0 AND u.email_signature_id = '$id'
            LIMIT 1";
        }

        $query = $this->db->query($sql);

        return $query->row();
    }
   
 
    
    public function is_exist($arr)
    {
        $typ = $arr['typ'];
        $id = $arr['id'];

        if($typ == "units")
        {
            $sql = "SELECT u.unit_id
            FROM units u
            WHERE u.is_deleted = 0 AND u.unit_id = $id
            LIMIT 1";
        }
        elseif($typ == "soil_type")
        {
            $sql = "SELECT u.soil_type_id
            FROM soil_type u
            WHERE u.is_deleted = 0 AND u.soil_type_id = $id
            LIMIT 1";
        }
        elseif($typ == "crops")
        {
            $sql = "SELECT u.crop_id
            FROM crops u
            WHERE u.is_deleted = 0 AND u.crop_id = $id
            LIMIT 1";
        }
        elseif($typ == "crop_variety")
        {
            $sql = "SELECT u.crop_variety_id
            FROM crop_variety u
            WHERE u.is_deleted = 0 AND u.crop_variety_id = $id
            LIMIT 1";
        }
        elseif($typ == "stock_verification_reason")
        {
            $sql = "SELECT u.stock_verification_reason_id
            FROM stock_verification_reason u
            WHERE u.is_deleted = 0 AND u.stock_verification_reason_id = $id
            LIMIT 1";
        }
        elseif($typ == "area_of_operation")
        {
            $sql = "SELECT u.state_id
            FROM states u
            WHERE u.is_deleted = 0 AND u.state_id = $id
            LIMIT 1";
        }
        elseif($typ == "product_category")
        {
            $sql = "SELECT u.product_category_id
            FROM product_category u
            WHERE u.is_deleted = 0 AND u.product_category_id = $id
            LIMIT 1";
        }
        elseif($typ == "products")
        {
            $sql = "SELECT u.product_id
            FROM products u
            WHERE u.is_deleted = 0 AND u.product_id = $id
            LIMIT 1";
        }
        elseif($typ == "staff")
        {
            $sql = "SELECT u.user_id
            FROM users u
            WHERE u.is_deleted = 0 AND u.user_id = $id
            LIMIT 1";
        }
        elseif($typ == "wholesalers")
        {
            $sql = "SELECT u.saler_id
            FROM salers u
            WHERE u.is_deleted = 0 AND saler_type = 1 AND u.saler_id = $id
            LIMIT 1";
        }
        elseif($typ == "dealers")
        {
            $sql = "SELECT u.saler_id
            FROM salers u
            WHERE u.is_deleted = 0 AND saler_type = 2 AND u.saler_id = $id
            LIMIT 1";
        }        
        elseif($typ == "demos")
        {
            $sql = "SELECT u.farmer_id, u.farmer_district, u.farmer_state, u.farmer_reg_num
            FROM demos u
            WHERE u.is_deleted = 0 AND u.farmer_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "roles")
        {
            $sql = "SELECT u.role_id
            FROM roles u
            WHERE u.is_deleted = 0 AND u.role_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "templates")
        {
            $sql = "SELECT u.template_id
            FROM templates u
            WHERE u.is_deleted = 0 AND u.template_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "farmers")
        {
            $sql = "SELECT u.farmer_id, u.farmer_district, u.farmer_state, u.farmer_reg_num
            FROM farmers u
            WHERE u.is_deleted = 0 AND u.farmer_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "farmers_temp")
        {
            $sql = "SELECT u.farmer_id, u.farmer_district, u.farmer_state
            FROM farmers_temp u
            WHERE u.is_deleted = 0 AND u.farmer_id = '$id'
            LIMIT 1";
        }
        elseif($typ == "email_signature")
        {
            $sql = "SELECT u.*
            FROM email_signature u
            WHERE u.is_deleted = 0 AND u.email_signature_id = '$id'
            LIMIT 1";
        }
        
        $query = $this->db->query($sql);

        return $query->row();
    }    



    public function get_dd_list($arr = array())
    {        
        $typ = $arr['typ'];        

        if($typ == "crops")
        {
            $sql = "SELECT crop_id, crop_name
            FROM crops
            WHERE is_deleted = 0
            ORDER BY crop_name";
        } 
        elseif($typ == "states_all")
        {
            $sql = "SELECT state_id, state_name
            FROM states
            WHERE is_deleted = 0
            ORDER BY state_name";
        }
        elseif($typ == "district_by_state")
        {
            $id = $arr['id'];

            $sql = "SELECT d.district_id, d.state_id, d.district_name, d.is_district_active, s.is_state_active, s.state_name
            FROM districts d
            INNER JOIN states s ON s.state_id = d.state_id AND s.state_id = '$id'
            WHERE d.is_deleted = 0 AND d.state_id = '$id'
            ORDER BY s.state_name, d.district_name";
        }
        elseif($typ == "product_category")
        {
            $sql = "SELECT product_category_id, product_category_name
            FROM product_category
            WHERE is_deleted = 0
            ORDER BY product_category_name";
        }
        elseif($typ == "subunits")
        {
            $sql = "SELECT u.unit_id, u.unit_name, su.subunit_id, su.subunit_name
            FROM units u
            INNER JOIN subunits su ON u.unit_id = su.unit_id AND su.is_deleted = 0
            WHERE u.is_deleted = 0
            ORDER BY u.unit_name, su.subunit_name";
        }
        elseif($typ == "states_aop")
        {
            $sql = "SELECT state_id, state_name
            FROM states
            WHERE is_deleted = 0 AND is_state_active = 1
            ORDER BY state_name";
        }
        elseif($typ == "district_by_state_aop" || $typ == "district_by_state_aop_dd")
        {
            $id = $arr['id'];            

            $sql = "SELECT d.district_id, d.state_id, d.district_name, d.is_district_active, s.is_state_active, s.state_name
            FROM districts d
            INNER JOIN states s ON s.state_id = d.state_id AND s.state_id = '$id' AND s.is_state_active = 1            
            WHERE d.is_deleted = 0 AND d.state_id = '$id' AND d.is_district_active = 1
            ORDER BY s.state_name, d.district_name";
        }
        elseif($typ == "states_aop_user")
        {
            $uid = $arr['uid'];
            $sql = "SELECT s.state_id, s.state_name, ua.user_id
            FROM states s
            LEFT JOIN users_area ua ON ua.state_id = s.state_id AND ua.user_id = $uid
            WHERE s.is_deleted = 0 AND s.is_state_active = 1            
            GROUP BY s.state_id
            ORDER BY s.state_name";
        }
        elseif($typ == "district_by_state_aop_user")
        {
            $id = $arr['id'];
            $uid = $arr['uid'];

            $sql = "SELECT d.district_id, d.state_id, d.district_name, d.is_district_active, s.is_state_active, s.state_name, ua.user_id
            FROM districts d
            INNER JOIN states s ON s.state_id = d.state_id AND s.state_id = '$id' AND s.is_state_active = 1
            LEFT JOIN users_area ua ON ua.state_id = d.state_id AND ua.district_id = d.district_id AND ua.user_id = $uid
            WHERE d.is_deleted = 0 AND d.state_id = '$id' AND d.is_district_active = 1
            ORDER BY s.state_name, d.district_name";
        }
        elseif($typ == "roles")
        {
            $sql = "SELECT role_id, role_name
            FROM roles
            WHERE is_deleted = 0
            ORDER BY role_name";
        }
        elseif($typ == "reporting")
        {
            $sql = "SELECT r.role_name, u.user_id, CONCAT(u.first_name,' ', u.last_name) as fullname
            FROM users u
            INNER JOIN roles r ON r.role_id = u.role_id
            WHERE u.is_deleted = 0
            ORDER BY u.first_name, u.last_name";
        }
        elseif($typ == "state_head")
        {
            $id = $arr['id'];
            $sql = "SELECT u.user_id, CONCAT(u.first_name,' ', u.last_name) as fullname
            FROM users u
            INNER JOIN users_area ua ON ua.user_id = u.user_id AND ua.state_id = $id
            WHERE u.is_deleted = 0 AND ua.state_id = $id AND u.role_id = 2
            GROUP BY u.user_id
            ORDER BY u.first_name, u.last_name";
        }
        elseif($typ == "district_executive")
        {
            $id = $arr['id'];
            $sql = "SELECT u.user_id, CONCAT(u.first_name,' ', u.last_name) as fullname
            FROM users u
            INNER JOIN users_area ua ON ua.user_id = u.user_id AND ua.district_id = $id
            WHERE u.is_deleted = 0 AND ua.district_id = $id AND u.role_id = 3
            GROUP BY u.user_id
            ORDER BY u.first_name, u.last_name";
        }
        elseif($typ == "district_by_state_name")
        {
            $id = $arr['id'];            

            $sql = "SELECT d.district_id, d.state_id, d.district_name, d.is_district_active, s.is_state_active, s.state_name
            FROM districts d
            INNER JOIN states s ON s.state_id = d.state_id AND s.state_name = '$id' AND s.is_state_active = 1            
            WHERE d.is_deleted = 0 AND s.state_name = '$id' AND d.is_district_active = 1
            ORDER BY s.state_name, d.district_name";
        }
        elseif($typ == "state_district_aop")
        {
            $sql = "SELECT d.district_id, d.state_id, d.district_name, s.state_name
            FROM districts d
            INNER JOIN states s ON s.state_id = d.state_id AND s.is_state_active = 1            
            WHERE d.is_deleted = 0 AND d.is_district_active = 1
            ORDER BY s.state_name, d.district_name";
        }
        elseif($typ == "wholesalers")
        {
            $sql = "SELECT u.*
            FROM salers u
            WHERE u.is_deleted = 0 AND u.saler_type = 1
            ORDER BY u.first_name, u.last_name";
        }
        elseif($typ == "retailers")
        {
            $sql = "SELECT u.*
            FROM salers u
            WHERE u.is_deleted = 0 AND u.saler_type = 2
            ORDER BY u.first_name, u.last_name";
        }
        elseif($typ == "products")
        {
            $sql = "SELECT u.*
            FROM products u
            WHERE u.is_deleted = 0 AND u.product_category_id = 1
            ORDER BY u.product_name";
        }
        elseif($typ == "taluka_by_district")
        {
            $id = $arr['id'];

            $sql = "SELECT d.district_id, d.district_name, d.is_district_active, t.taluka_id, t.taluka_name
            FROM taluka t
            INNER JOIN districts d ON d.district_id = t.district_id AND d.district_id = '$id'
            WHERE t.is_deleted = 0 AND t.district_id = '$id'
            ORDER BY t.taluka_name, d.district_name";
        }
        elseif($typ == "email_signature")
        {
            $sql = "SELECT email_signature_id, CONCAT(signature_name, ' - ', signature_from) as signame
            FROM email_signature
            WHERE is_deleted = 0
            ORDER BY signature_name";
        }
        elseif($typ == "retailer_by_district")
        {
            $id = $arr['id'];
            
            $sql = "SELECT saler_sys_id as saler_id, CONCAT(first_name, ' ', last_name) as fullname 
            FROM salers s 
            WHERE is_deleted = 0 AND saler_type = 2 AND district_id IN ($id)
            ORDER BY first_name, last_name";
        }
        elseif($typ == "wholesaler_by_district")
        {
            $id = $arr['id'];
            
            $sql = "SELECT saler_sys_id as saler_id, CONCAT(first_name, ' ', last_name) as fullname 
            FROM salers s 
            WHERE is_deleted = 0 AND saler_type = 1 AND district_id IN ($id)
            ORDER BY first_name, last_name";
        }        
        elseif($typ == "farmers")
        {
            $sql = "SELECT f.farmer_id, f.farmer_reg_num, f.farmer_name
            FROM farmers f
            WHERE f.is_deleted = 0
            ORDER BY f.farmer_name";
        }
        elseif($typ == "demos")
        {
            $sql = "SELECT f.farmer_id, f.farmer_reg_num, f.farmer_name
            FROM demos f
            WHERE f.is_deleted = 0
            ORDER BY f.farmer_name";
        }

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function rating($arr = array())
    {
        $sql = "SELECT m.*
        FROM rating m        
        WHERE m.is_deleted = 0
        ORDER BY m.rating_id
        ";

        $query = $this->db->query($sql);

        return $query->result();
    }
    
    
    public function get_farmer_reg_no($typ="farmers")
    {
        $sql = "SELECT max(farmer_reg_num) as sno
        FROM $typ        
        WHERE is_deleted = 0
        LIMIT 1
        ";

        $query = $this->db->query($sql);

        $row = $query->row();

        if(isset($row) && !empty($row))
        {
            return $row-> sno;
        }
        else
        {
            return 0;
        }

    } 



    public function count_plot_images($arr)
    {
        $farmer_id = $arr['farmer_id'];
        $farmer_type = $arr['farmer_type'];
        $for_stage = $arr['for_stage'];
        $demo_type = $arr['demo_type'];

        if(!isset($farmer_type) || empty($farmer_type)) $farmer_type = 1;
        if(!isset($for_stage) || empty($for_stage)) $for_stage = 2;
        if(!isset($demo_type) || empty($demo_type)) $demo_type = 1;

        $sql = "SELECT count(plot_photo_id) as total
        FROM farmers_plot_photo f        
        WHERE f.farmer_id = $farmer_id AND f.farmer_type = $farmer_type AND f.for_stage = $for_stage AND f.demo_type = $demo_type
        GROUP BY f.farmer_id
        LIMIT 1 
        ";

        $query = $this->db->query($sql);

        $row = $query->row();

        if(isset($row) && !empty($row))
        {
            return $row-> total;
        }
        else
        {
            return 0;
        }

    } 



    public function farmer_plot_photo($arr)
    {        
        $farmer_id = $arr['farmer_id'];        
        if(!isset($arr['farmer_type']) || empty($arr['farmer_type'])) 
            $farmer_type = 1;
        else
            $farmer_type = $arr['farmer_type'];

        if(!isset($arr['for_stage']) || empty($arr['for_stage'])) 
            $for_stage = 2;
        else
            $for_stage = $arr['for_stage'];

        
        $sql = "SELECT f.*
        FROM farmers_plot_photo f        
        WHERE f.farmer_id = $farmer_id AND f.farmer_type = $farmer_type AND f.for_stage = $for_stage
        ORDER BY f.plot_photo_id ASC";        

        $query = $this->db->query($sql);

        return $query->result();
    }
    

    


    public function load_photo_gallery_stock($arr)
    {        
        $id = $arr['id'];
        
        $sql = "SELECT f.*
        FROM stock_verification f        
        WHERE f.id = $id AND f.is_deleted = 0
        LIMIT 1";        

        $query = $this->db->query($sql);

        $result =  $query->result();

        $response = array();
        if(isset($result) && !empty($result))
        {
            foreach($result as $obj)
            {
                if(isset($obj-> stock_photo_url) && !empty($obj-> stock_photo_url))
                    $obj-> stock_photo_url = json_decode($obj-> stock_photo_url);

                $response[] = $obj;
            }
        }

        return $response;
    }


    public function get_sales_yearly($file_id, $year)
    {
        $sql = "SELECT st.state_name, s.sales_date, s.quantity
            FROM upload_sales s
            INNER JOIN states st ON st.state_id = s.state_id
            INNER JOIN districts dis ON dis.district_id = s.district_id
            INNER JOIN salers ss ON ss.saler_sys_id = s.dealer_id
            WHERE year(s.sales_date) = '$year' AND company = 'RMPCL' 
        ";

        $append = $append1 = "";
        if($this->user_role_id == 2)
        {
            $append = " AND st.state_id IN (".$this->user_state_aop.") ";
        }
        elseif($this->user_role_id == 3)
        {
            $append = " AND dis.district_id IN (".$this->user_district_aop.") ";

            $append1 = " AND st.state_id IN (".$this->user_state_aop.") ";
        }


        $month = date("m");
        $sql = "SELECT st.state_name, s.sales_date, s.quantity, 'sales' as type
            FROM upload_sales s
            INNER JOIN states st ON st.state_id = s.state_id
            INNER JOIN districts dis ON dis.district_id = s.district_id                        
            WHERE year(s.sales_date) = '$year' AND company = 'RMPCL' $append
    
            UNION ALL

            SELECT st.state_name, t.target_month as sales_date, t.target_value as quantity, 'target' as type
            FROM target t
            INNER JOIN states st ON st.state_id = t.state_id $append1                       
            WHERE t.target_month = '$month' AND t.target_year = '$year' 
        ";

        $query = $this->db->query($sql);

        return $query->result();    
    }


    public function user_state_aop($user_id)
    {
        $sql = "SELECT state_id
        FROM users_area       
        WHERE user_id = $user_id
        GROUP BY state_id 
        ";

        $query = $this->db->query($sql);

        $data = $query->result(); 

        $str = "0";
        $temp = array();
        if(isset($data) && !empty($data))   
        {
            foreach($data as $obj)
            {
                $temp[] = $obj-> state_id;
            }

            $str = implode(",", $temp);
        }

        return $str;
    }


    public function user_district_aop($user_id)
    {
        $sql = "SELECT district_id
        FROM users_area       
        WHERE user_id = $user_id
        GROUP BY district_id 
        ";

        $query = $this->db->query($sql);

        $data = $query->result(); 

        $str = "0";
        $temp = array();
        if(isset($data) && !empty($data))   
        {
            foreach($data as $obj)
            {
                $temp[] = $obj-> district_id;
            }

            $str = implode(",", $temp);
        }

        return $str;
    }


    public function get_state_head($state_id)
    {
        $sql = "SELECT u.email, u.mobile
        FROM users u
        INNER JOIN users_area ua ON ua.user_id = u.user_id AND ua.state_id = $state_id
        WHERE u.is_deleted = 0 AND ua.state_id = $state_id AND u.role_id = 2
        GROUP BY u.user_id";       

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function get_district_executive($state_id, $district_id)
    {
        $sql = "SELECT u.email, u.mobile
        FROM users u
        INNER JOIN users_area ua ON ua.user_id = u.user_id AND ua.state_id = $state_id AND ua.district_id = $district_id
        WHERE u.is_deleted = 0 AND ua.state_id = $state_id  AND ua.district_id = $district_id AND u.role_id = 3
        GROUP BY u.user_id";       

        $query = $this->db->query($sql);

        return $query->result();
    }


    public function dealersid_for_other_stateid($saler_sys_id, $state_id, $district_id)
    {
        $sql = "SELECT s.saler_id, s.saler_sys_id, ss.state_name, dd.district_name, s.first_name, s.saler_type
            FROM salers s                     
            INNER JOIN states ss ON ss.state_id = s.state_id
            INNER JOIN districts dd ON dd.district_id = s.district_id
            WHERE s.state_id != '$state_id' AND s.saler_sys_id = '$saler_sys_id'
            ORDER BY s.first_name, s.state_name, s.district_name                     
        ";

        $query = $this->db->query($sql);

        return $query->result();    
    }

}
?>